/**
 * @file mainV01.h 
 *
 * REVISION HISTORY
 *==============================================
 * |Ticket   |Date      |Author       |Notes
 * |----:    |:----:    |:----:       |:----
 * |Creation |02/04/22  |Tom O'Connor |Document
 *
 * @par   COPYRIGHT NOTICE: (c) 2022 Inventus Power.  All rights reserved.
 */
/* Inventus Power Proprietary */
#ifndef _MAIN_H_
#define _MAIN_H_


typedef union
{            
    struct   
    {        
        uint16_t bit0          :1;
        uint16_t bit1          :1;
        uint16_t bit2          :1;
        uint16_t bit3          :1;
        uint16_t bit4          :1;
        uint16_t bit5          :1;
        uint16_t bit6          :1;
        uint16_t bit7          :1;
        uint16_t bit8          :1;
        uint16_t bit9          :1;
        uint16_t bit10         :1;
        uint16_t bit11         :1;
        uint16_t bit12         :1;
        uint16_t bit13         :1;
        uint16_t bit14         :1;
        uint16_t bit15         :1;
		               
    }bitWord;                            
    uint16_t word;                
}define_16bits;

typedef union
{            
    struct   
    {        
        uint8_t bit0          :1;
        uint8_t bit1          :1;
        uint8_t bit2          :1;
        uint8_t bit3          :1;
        uint8_t bit4          :1;
        uint8_t bit5          :1;
        uint8_t bit6          :1;
        uint8_t bit7          :1;
 
    }bitByte;                            
    uint8_t byte;                
}define_8bits;

extern uint8_t g_version[4];
extern define_16bits g_flagDsgStatus;
#define g_flagDsgCurr            g_flagDsgStatus.bitWord.bit0
#define g_flagChgCurr            g_flagDsgStatus.bitWord.bit1
#define g_flagButtonLed          g_flagDsgStatus.bitWord.bit2
#define g_flagCurrEnDsgFET       g_flagDsgStatus.bitWord.bit3

extern define_16bits g_testCommandStatus;
/* test command status */
#define g_testPicOrFetOff               g_testCommandStatus.bitWord.bit0
#define g_testPicDsgFetOff              g_testCommandStatus.bitWord.bit1
#define g_testPicChgFetOff              g_testCommandStatus.bitWord.bit2
#define g_testAllGLedOn                 g_testCommandStatus.bitWord.bit3

#define g_testAllRLedOn                 g_testCommandStatus.bitWord.bit4
#define g_testDisableBroadcast          g_testCommandStatus.bitWord.bit5
#define g_testDisableComBq              g_testCommandStatus.bitWord.bit6
#define g_testHeaterOn                  g_testCommandStatus.bitWord.bit7

#define g_testFanOn                     g_testCommandStatus.bitWord.bit8
#define g_testSleep                     g_testCommandStatus.bitWord.bit9  
#define g_testPicByPassFetOff           g_testCommandStatus.bitWord.bit10


extern define_16bits g_PICStatus;
/*#define g_flagBattNeedCharge            g_PICStatus.bitWord.bit0*/
#define g_flagNeedBroadcastBulkCurr     g_PICStatus.bitWord.bit0
#define g_flagBroadcastBulkCurr         g_PICStatus.bitWord.bit1
#define g_flagPreChgVolt                g_PICStatus.bitWord.bit2

extern define_8bits g_flagSystem;
#define g_flagSleep                     g_flagSystem.bitByte.bit0
#define s_flag1ms                       g_flagSystem.bitByte.bit1
#define g_flagCommFail                  g_flagSystem.bitByte.bit2
#define g_flagSaveEn                    g_flagSystem.bitByte.bit3
#define g_flagCal                       g_flagSystem.bitByte.bit4
#define g_flagProgramErr                g_flagSystem.bitByte.bit5

extern define_8bits g_flagSignal;
#define s_flagCHGsignal                 g_flagSignal.bitByte.bit0
#define s_flagDSGsignal                 g_flagSignal.bitByte.bit1
#define s_flagKey                       g_flagSignal.bitByte.bit2
#define g_flagUpKey                     g_flagSignal.bitByte.bit3
#define g_flagKeyLed                    g_flagSignal.bitByte.bit4
#define g_flagLedSOC                    g_flagSignal.bitByte.bit5
#define g_flagLedCellPosition           g_flagSignal.bitByte.bit6
#define g_flagEnterShipLed              g_flagSignal.bitByte.bit7


extern define_16bits g_J1939Status;/*0xF9*/

extern define_16bits g_PerpheralStatus;
#define g_flagCell1BypassEN         g_PerpheralStatus.bitWord.bit0
#define g_flagCell2BypassEN         g_PerpheralStatus.bitWord.bit1
#define g_flagCell1DSGCHGDisEn      g_PerpheralStatus.bitWord.bit2
#define g_flagCell2DSGCHGDisEn      g_PerpheralStatus.bitWord.bit3

extern define_16bits g_DeltaQStatus; /*0xFB*/
#define g_flagCharger1CheckPass             g_DeltaQStatus.bitWord.bit0
#define g_flagBCH1Received1                 g_DeltaQStatus.bitWord.bit1
#define g_flagChargeSetEn1                  g_DeltaQStatus.bitWord.bit2
/*#define g_flagCharger1ZeroCurr              g_DeltaQStatus.bitWord.bit3
#define g_flagIllegalCharger1               g_DeltaQStatus.bitWord.bit4*/
#define g_flagRequestChager1ACL             g_DeltaQStatus.bitWord.bit5
#define g_flagStartCharge1                  g_DeltaQStatus.bitWord.bit6

#define g_flagCharger2CheckPass             g_DeltaQStatus.bitWord.bit8
#define g_flagBCH1Received2                 g_DeltaQStatus.bitWord.bit9
#define g_flagChargeSetEn2                  g_DeltaQStatus.bitWord.bit10
/*#define g_flagCharger2ZeroCurr              g_DeltaQStatus.bitWord.bit11
#define g_flagIllegalCharger2               g_DeltaQStatus.bitWord.bit12*/
#define g_flagRequestChager2ACL             g_DeltaQStatus.bitWord.bit13
#define g_flagStartCharge2                  g_DeltaQStatus.bitWord.bit14


//extern bool g_flagSleep;
extern uint8_t g_resetCause;

#define CLRWDT           		 WDT_Clear()
void TIME5MS_Ovf(void);

#endif
/* Inventus Power Proprietary */
