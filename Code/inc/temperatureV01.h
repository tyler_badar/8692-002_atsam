/**
 * @file temperatureV01.h
 *
 * REVISION HISTORY
 *==============================================
 * |Ticket   |Date      |Author       |Notes
 * |----:    |:----:    |:----:       |:----
 * |Creation |02/04/22  |Tom O'Connor |Document
 * |modified |02/23/22  |broberto     |Document
 *
 * @par   COPYRIGHT NOTICE: (c) 2022 Inventus Power.  All rights reserved.
 */
/* Inventus Power Proprietary */
/*
 * File:   temperatureV01.h
 * Author: markli
 *
 * Created on September 25, 2018, 7:33 PM
 */

#ifndef TEMPERATUREV01_H
#define	TEMPERATUREV01_H

/**
* TODO: Are these resistance values from NTCs?
* 15652U =?
* 15652 = 156,520 K ohms
*/
#define THERMAL_N40C_VALUE                              15652U
#define THERMAL_N30C_VALUE                              15127U
#define THERMAL_N20C_VALUE                              14360U
#define THERMAL_N10C_VALUE                              13325U
#define THERMAL_0C_VALUE                                12033U
#define THERMAL_10C_VALUE                               10548U
#define THERMAL_20C_VALUE                               8975U
#define THERMAL_30C_VALUE                               7430U
#define THERMAL_40C_VALUE                               6013U
#define THERMAL_50C_VALUE                               4786U
#define THERMAL_60C_VALUE                               3782U
#define THERMAL_70C_VALUE                               2976U
#define THERMAL_80C_VALUE                               2339U
#define THERMAL_90C_VALUE                               1843U
#define THERMAL_100C_VALUE                              1458U
#define THERMAL_110C_VALUE                              1179U
#define THERMAL_120C_VALUE                              953U

/**
* Kelvin base temp
*/
#define BASE_TEMP                           2730u

/**
* @brief Kelvin = temperature_Celsius + 273
* I think the macro names should read TEMPERATURE_xK
* for Kelvin not Celsius.
* @brief This block is not used.
*/
#define TEMPERATURE_70C                     ( BASE_TEMP + 700u )
#define TEMPERATURE_65C                     ( BASE_TEMP + 650u )
#define TEMPERATURE_63C                     ( BASE_TEMP + 630u )
#define TEMPERATURE_60C                     ( BASE_TEMP + 600u )
#define TEMPERATURE_58C                     ( BASE_TEMP + 580u )
#define TEMPERATURE_55C                     ( BASE_TEMP + 550u )
#define TEMPERATURE_53C                     ( BASE_TEMP + 530u )
#define TEMPERATURE_30C                     ( BASE_TEMP + 300u )
#define TEMPERATURE_25C                     ( BASE_TEMP + 250u )
#define TEMPERATURE_15C                     ( BASE_TEMP + 150u )
#define TEMPERATURE_5C                      ( BASE_TEMP + 50u )
#define TEMPERATURE_3C                      ( BASE_TEMP + 30u )
#define TEMPERATURE_N5C                     ( BASE_TEMP - 50u )
#define TEMPERATURE_N10C                    ( BASE_TEMP - 100u )

extern uint16_t g_maxTemp;
extern uint16_t g_minTemp;
extern uint16_t g_maxFetTemp;
extern uint16_t g_minFetTemp;
extern uint16_t g_maxPackTemp;
extern uint16_t g_minPackTemp;

extern uint16_t g_ntc3Temp;
extern uint16_t g_ntc4Temp;
extern uint16_t g_ntc5Temp;
extern uint16_t g_ntc6Temp;
extern uint16_t g_ntc8Temp;
extern uint16_t g_ntc10Temp;
extern uint16_t g_ntc11Temp;
extern uint16_t g_ntc12Temp;
extern uint16_t g_ntc14Temp;
extern uint16_t g_ntc15Temp;
extern uint16_t g_ntc16Temp;

/**
* @brief Example showing how to document a function with Doxygen.
*
* Description of what the function does. This part may refer to the parameters
* of the function, like @p param1 or @p param2. A word of code can also be
* inserted like @c this which is equivalent to <tt>this</tt> and can be useful
* to say that the function returns a @c void or an @c int. If you want to have
* more than one word in typewriter font, then just use @<tt@>.
*/
void TemeratrueDetect(void); /**< TODO: Do we need to document function prototypes? */

#endif	/* TEMPERATUREV01_H */
/* Inventus Power Proprietary */
