/**
 * @file  flashV01.h
 *
 * REVISION HISTORY
 *==============================================
 * |Ticket   |Date      |Author       |Notes
 * |----:    |:----:    |:----:       |:----
 * |Creation |02/04/22  |Tom O'Connor |Document
 *
 * @par   COPYRIGHT NOTICE: (c) 2022 Inventus Power.  All rights reserved.
 */
/* Inventus Power Proprietary */
#ifndef _PROGRAM_H_
#define _PROGRAM_H_

#define CAN_BIT_RATE_FLASH_SAVE_ADDRESS   0x7000ul
#define FLASH_SAVE_DATA_CRC8_ADDRESS      0x7002ul

void LoadLssDataFunction(void);
void SaveLssDataFunction(void);

#endif 
/* Inventus Power Proprietary */

