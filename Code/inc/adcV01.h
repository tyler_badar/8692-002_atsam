/**
 * @file  adcV01.h
 *
 * REVISION HISTORY
 *==============================================
 * |Ticket   |Date      |Author       |Notes
 * |----:    |:----:    |:----:       |:----
 * |Creation |02/04/22  |Tom O'Connor |Document
 *
 * @par   COPYRIGHT NOTICE: (c) 2022 Inventus Power.  All rights reserved.
 */
/* Inventus Power Proprietary */
#ifndef _ADC_H_
#define _ADC_H_

#define NTC_SAMPLE_AMOUNT                      11u
#define ADC_SAMPLE_TIMEOUT                     3600u


extern uint16_t g_NTCTemperatureTable[16];
extern uint16_t g_adcReserved1;
extern uint16_t g_adcReserved2;
extern uint16_t g_adcReserved3;
extern uint16_t g_adcReserved4;
extern uint16_t g_adcReserved5;

void SampleFunction(void);
void disableADCSample(void);

extern uint16_t g_adcBattVolt; 
extern uint16_t g_adcPackVolt;
extern uint16_t g_adcPackSP;
extern uint16_t g_adcNTC[NTC_SAMPLE_AMOUNT];
extern uint16_t g_adcPreCHGCurr;

#endif
/* Inventus Power Proprietary */
