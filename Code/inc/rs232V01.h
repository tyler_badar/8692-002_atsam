/**
 * @file  rs232V01.h
 *
 * REVISION HISTORY
 *==============================================
 * |Ticket   |Date      |Author       |Notes
 * |----:    |:----:    |:----:       |:----
 * |Creation |02/04/22  |Tom O'Connor |Document
 *
 * @par   COPYRIGHT NOTICE: (c) 2022 Inventus Power.  All rights reserved.
 */
/* Inventus Power Proprietary */
#ifndef _RS232_H_
#define _RS232_H_
#define TX_DATA_LENGTH      50u

extern uint8_t s_sendData[TX_DATA_LENGTH];

void TxOperation(void);

#endif
/* Inventus Power Proprietary */
