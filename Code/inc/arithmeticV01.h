/**
 * @file  arithmeticV01.h
 *
 * REVISION HISTORY
 *==============================================
 * |Ticket   |Date      |Author       |Notes
 * |----:    |:----:    |:----:       |:----
 * |Creation |02/04/22  |Tom O'Connor |Document
 *
 * @par   COPYRIGHT NOTICE: (c) 2022 Inventus Power.  All rights reserved.
 */
/* Inventus Power Proprietary*/
#ifndef  _ARITHMETIIC_H_
#define  _ARITHMETIIC_H_

uint8_t basic_crc(uint8_t remainder, uint8_t byte);
uint16_t Crc16(uint8_t *ptr, uint16_t len);
void Delay10us(uint16_t cnt_Delay);

#endif

/* Inventus Power Proprietary*/
