/**
 * @file  pwmV01.h
 *
 * REVISION HISTORY
 *==============================================
 * |Ticket   |Date      |Author       |Notes
 * |----:    |:----:    |:----:       |:----
 * |Creation |02/04/22  |Tom O'Connor |Document
 *
 * @par   COPYRIGHT NOTICE: (c) 2022 Inventus Power.  All rights reserved.
 */
/* 
 * File:   pwmV01.h
 * Author: admin
 *
 * Created on October 31, 2018, 1:49 PM
 */

#ifndef PWMV01_H
#define	PWMV01_H

extern uint16_t g_pwmDuty;
extern uint8_t g_flagPWMOn;

void LoadPWMDuty(void);
void PWMControl(void);

#endif	/* PWMV01_H */

