/**
 * @file  common_defineV01.h
 *
 * REVISION HISTORY
 *==============================================
 * |Ticket   |Date      |Author       |Notes
 * |----:    |:----:    |:----:       |:----
 * |Creation |02/04/22  |Tom O'Connor |Document
 *
 * @par   COPYRIGHT NOTICE: (c) 2022 Inventus Power.  All rights reserved.
 */
/* Inventus Power Proprietary */
#ifndef _COMMON_DEFINE_H_
#define _COMMON_DEFINE_H_


#define MAX_PWM_DUTY         2390u     /* max pwm duty = 95% + deadband = 96.6%, 980 * 96.6% * 16 */
#define MIN_PWM_DUTY         8u    
#define PRECHGCURRENT_VALUE  9929u    /*2X0.05X20/3.3X4096*4=9929*/

#define PWM_PERIOD					 1920u      /* 100khz*/  


#define UARTBAUD             38400
/********************************/
#define TIMER5MS_20MS        4u
#define TIMER5MS_25MS        5u
#define TIMER5MS_50MS        10u
#define TIMER5MS_100MS       20u
#define TIMER5MS_200MS       40u
#define TIMER5MS_400MS       80u
#define TIMER5MS_500MS       100u
#define TIMER5MS_800MS       160u
#define TIMER5MS_1S          200u
#define TIMER5MS_1P5S        300u
#define TIMER5MS_2S          400u
#define TIMER5MS_2S5         500u
#define TIMER5MS_3S          600u
#define TIMER5MS_5S          1000u
#define TIMER5MS_6S          1200u
#define TIMER5MS_10S         2000u
#define TIMER5MS_20S         4000u
#define TIMER5MS_20P1S       4020u
#define TIMER5MS_30S         6000u
#define TIMER5MS_35S         7000u
#define TIMER5MS_1H          720000u


#define TIMER20MS_1S         50u
#define TIMER20MS_2S         100u
#define TIMER20MS_2S5        125u
#define TIMER20MS_5S         250u
#define TIMER20MS_20S        1000u 


#define TIMER1S_5S           5u
#define TIMER1S_10S          10u
#define TIMER1S_30S          30u
#define TIMER1S_180S         180u
#define TIMER1S_20M          1200u
#define TIMER1S_30M          1800u
#define TIMER1S_1H           3600u

/********************************/
#define ABS_4V25            4250u
#define ABS_4V2             4200u
#define ABS_4V16            4160u
#define ABS_4V15            4150u
#define ABS_4V1             4100u
#define ABS_4V              4000u
#define ABS_3V              3000u

#define ABS_30V     30000u
#define ABS_25V6    25600u
#define ABS_5V      5000u
#define ABS_2V8     2800u
#define ABS_3V      3000u
#define ABS_1V5     1500u
#define ABS_2V5     2500u
#define ABS_1V2     1200u
#define ABS_1V      1000u
#define ABS_0V5     500u
/********************************/

#define SAMPLE_2P8V_ADC_VALUE     13902u	  /* for MCU ID detect */
#define SAMPLE_1P8V_ADC_VALUE     8937u
#define SAMPLE_0P8V_ADC_VALUE     3972u

#define SAMPLE_2V_ADC_VALUE       9930u		 /* for battery detect */
#define SAMPLE_1V_ADC_VALUE       4965u

#ifndef CANOPEN_PROTOCOL
    #define CANOPEN_PROTOCOL
#endif
#endif
/* Inventus Power Proprietary */

