/**
 * @file  J1939V01.h
 *
 * REVISION HISTORY
 *==============================================
 * |Ticket   |Date      |Author       |Notes
 * |----:    |:----:    |:----:       |:----
 * |Creation |02/04/22  |Tom O'Connor |Document
 *
 * @par   COPYRIGHT NOTICE: (c) 2022 Inventus Power.  All rights reserved.
 */
/* 
 * File:   J1939V01.h
0. * Author: admin
 *
 * Created on May 20, 2019, 4:26 PM
 */

#ifndef J1939V01_H
#define	J1939V01_H

typedef union
{  
    uint8_t pduSIDL; 
    uint8_t pduSIDH; 
    uint8_t pduEIDL;
    uint8_t pduEIDH;
}J1939IDFrame;

typedef struct
{
    uint8_t J1939DataDLC;
    uint8_t J1939Data0;
    uint8_t J1939Data1;
    uint8_t J1939Data2;
    uint8_t J1939Data3;
    uint8_t J1939Data4;
    uint8_t J1939Data5;
    uint8_t J1939Data6;
    uint8_t J1939Data7;
} J1939Data;

typedef union
{
    struct
    {                          
		uint8_t	SourceAddress;
        uint8_t	PDUSpecific;
        uint8_t PDUFormat;
        uint8_t DataPage            : 1;
        uint8_t ExtendedDataPage    : 1;
        uint8_t Priority            : 3;
        uint8_t PDUFormat_Top       : 3;
    }id;
    uint32_t totalID;
}J1939Id;

#define BATT_SOURCE_ADDR_ID         0xF3u

/*******************************************************************************/
/* 0A*20 + 0x7d00 */
#define DELTA_Q_CHG_0A              0x7d00u 
/* 1A*20 + 0x7d00 */
#define DELTA_Q_CHG_1A              20u 
/* 7.5A*20 + 0x7d00 */
#define DELTA_Q_CHG_7A5             150u
/* 20A*20 + 0x7d00 */
#define DELTA_Q_CHG_20A             400u 
/* 33A*20 + 0x7d00 */
#define DELTA_Q_CHG_33A             660u
/* 28V*20 */
#define DELTA_Q_CHG_28V             560u
/*******************************************************************************/

#define DELTA_Q_INITIAL             0u
#define DELTQ_Q_SET_PARAMETER       1u

#define g_flagJ1939Read             g_flagJ1939Status.bitWord.bit0
#define g_flagJ1939Write            g_flagJ1939Status.bitWord.bit1
#define g_flagJ1939ReadErr          g_flagJ1939Status.bitWord.bit2
#define g_flagJ1939WriteErr         g_flagJ1939Status.bitWord.bit3

#define g_flagDisableBAM            g_flagJ1939Status.bitWord.bit4
#define g_flagCompleteIdRequest     g_flagJ1939Status.bitWord.bit5
#define g_flagJ1939ACLEnable        g_flagJ1939Status.bitWord.bit7

#define g_flagJ1939ACLFail          g_flagJ1939Status.bitWord.bit8
#define g_flagJ1939ACLComplete      g_flagJ1939Status.bitWord.bit9   


#define J1939_ARBITRARY_ADDRESS     0x00u
#define J1939_INDUSTRY_GROUP    0u
#define J1939_VEHICLE_INSTANCE  0u
#define J1939_CA_NAME7      (J1939_ARBITRARY_ADDRESS | ((uint8_t)(J1939_INDUSTRY_GROUP << 4u)) | J1939_VEHICLE_INSTANCE) 
#define J1939_VEHICLE_SYSTEM    127u
#define J1939_CA_NAME6      ((uint8_t)(J1939_VEHICLE_SYSTEM << 1u)) 
#define J1939_FUNCTION      4u
#define J1939_CA_NAME5      J1939_FUNCTION 
#define J1939_FUNCTION_INSTANCE     0u
#define J1939_ECU_INSTANCE      0u
#define J1939_CA_NAME4      ((J1939_FUNCTION_INSTANCE << 3u) | J1939_ECU_INSTANCE) 
#define J1939_MANUFACTURER_CODE     1024u
/** #define J1939_IDENTITY_NUMBER 51u **/
#define J1939_CA_NAME3      ((uint8_t)(J1939_MANUFACTURER_CODE >> 3u)) 
#define J1939_CA_NAME2      ((uint8_t)(((uint16_t)(J1939_MANUFACTURER_CODE << 5u) & 0x00E0u) | ((uint8_t)(g_UidForAllocation >> 16u) & 0x1Fu)))
#define J1939_CA_NAME1      ((uint8_t)(g_UidForAllocation >> 8u)) 
#define J1939_CA_NAME0      ((uint8_t)(g_UidForAllocation)) 

#define J1939_GLOBAL_ADDRESS        255
#define J1939_NULL_ADDRESS			254

extern define_16bits g_flagJ1939Status;
extern uint8_t g_j1939Address;

void ControlDeltaQChargerJ1939(void);
void DeltaQJ1939Process(void);
void J1939PGNRequestProcess(uint32_t tmpID);
void J1939Response(void);
void J1939Write(void);
void J1939AddressClaimHandling(void);
void SendJ1939AddressClaim(void);
void BroadcastCellVolt(void);

#endif	/* J1939V01_H */

