/**
 * @file  CANopenV01.h
 *
 * REVISION HISTORY
 *==============================================
 * |Ticket   |Date      |Author       |Notes
 * |----:    |:----:    |:----:       |:----
 * |Creation |02/04/22  |Tom O'Connor |Document
 *
 * @par   COPYRIGHT NOTICE: (c) 2022 Inventus Power.  All rights reserved.
 */
/* 
 * File:   CANopenV01.h
 * Author: admin
 *
 * Created on May 15, 2019, 10:40 AM
 */

#ifndef CANOPENV01_H
#define	CANOPENV01_H

extern uint8_t g_flagCanErr;
extern uint16_t g_canErrTimes;
extern uint16_t g_canOvfTimes;

extern uint8_t g_manufactureName[14u];
extern uint8_t g_picVersion[4u];

#define IP_VENDOR_ID                    0x4dfu
#define IP_PRODUCT_CODE                 8692u
#define IP_PRODUCT_CODE_NUM             002u

#define LSS_OFF_STATE                   0x00u
#define LSS_INIT_STATE                  0x01u
#define LSS_WAIT_STATE                  0x10u
#define LSS_CONFI_STATE                 0x55u
#define LSS_FINAL_STATE                 0xffu

#define LSS_CIA305_ID                   0x7E5u 
#define LSS_CIA305_RESPONSE_ID          0x7E4u 
#define LSS_SWITCH_STATE_CS             0x04u
#define LSS_SWITCH_VENDORID_CS          0x40u
#define LSS_SWITCH_PRODUCTCODE_CS       0x41u
#define LSS_SWITCH_RN_CS                0x42u
#define LSS_SWITCH_SN_CS                0x43u
#define LSS_SWITCH_RESPONSE_CS          0x44u
#define LSS_CONFIGURE_ID_CS             0x11u
#define LSS_CONFIGURE_RATE_CS           0x13u
#define LSS_UPDATE_RATE_CS              0x15u
#define LSS_SAVE_CONFI_CS               0x17u
#define LSS_INQUIRE_VENDORID_CS         0x5Au
#define LSS_INQUIRE_PRODUCTCODE_CS      0x5Bu
#define LSS_INQUIRE_RN_CS               0x5Cu
#define LSS_INQUIRE_SN_CS               0x5Du
#define LSS_INQUIRE_NODEID_CS           0x5Eu

extern uint8_t g_pendNodeID;
extern uint8_t g_fixNodeID;

extern define_8bits g_flagCanOpen;
#define g_flagLssConfiSaveEn            g_flagCanOpen.bitByte.bit0
#define g_flagLssBitRateUpdate          g_flagCanOpen.bitByte.bit1
#define g_flagLssCanSend                g_flagCanOpen.bitByte.bit2


#define PICCAN_COMM_LENGTH       35u
#define BQCAN_COMM_LENGTH        25u

#define TEST_ADDR                            ((uint32_t)(&g_picVersion[0]))

#define CAN_ERR_TIMES_ADDR                   ((uint32_t)(&g_canErrTimes))
#define CAN_OVF_TIMES_ADDR                   ((uint32_t)(&g_canOvfTimes))    

#define MANUFACTURE_NAME_ADDR                ((uint32_t)(&s_manufactureName[0u]))
#define FAULTID_ADDR                         ((uint32_t)(&g_faultID))
#define PIC_CHARGE_MODE_ADDR                 ((uint32_t)(&g_chargeMode))
#define PIC_VERSION_ADDR                     ((uint32_t)(&g_picVersion[0u]))
#define BL_VERSION_ADDR                      ((uint32_t)(&g_bootloaderVerson))
#define READ_UID_ADDR                        ((uint32_t)(&g_Uid))
#define COMM_SUCCEED_RATE_ADDR               ((uint32_t)(&g_successRate))
#define PIC_FET_STATUS_ADDR                  ((uint32_t)(&g_FETstate.byte))

#define PIC_CHG_EN_CTRL_ADDR                 ((uint32_t)(&g_FETEnCH.byte))
#define PIC_DSG_EN_CTRL_ADDR                 ((uint32_t)(&g_FETEnDisCH.byte))
#define PIC_MINTEMP_ADDR                     ((uint32_t)(&g_minTemp))
#define PIC_MAXTEMP_ADDR                     ((uint32_t)(&g_maxTemp))
#define PIC_MINFETTEMP_ADDR                  ((uint32_t)(&g_minFetTemp))
#define PIC_MAXFETTEMP_ADDR                  ((uint32_t)(&g_maxFetTemp))
#define PIC_NTC3TEMP_ADDR                    ((uint32_t)(&g_ntc3Temp))
#define PIC_NTC4TEMP_ADDR                    ((uint32_t)(&g_ntc4Temp))
#define PIC_NTC5TEMP_ADDR                    ((uint32_t)(&g_ntc5Temp))
#define PIC_NTC6TEMP_ADDR                    ((uint32_t)(&g_ntc6Temp))
#define PIC_NTC11TEMP_ADDR                   ((uint32_t)(&g_ntc11Temp))
#define PIC_NTC12TEMP_ADDR                   ((uint32_t)(&g_ntc12Temp))
#define PIC_NTC14TEMP_ADDR                   ((uint32_t)(&g_ntc14Temp))
#define PIC_NTC15TEMP_ADDR                   ((uint32_t)(&g_ntc15Temp))
#define PIC_BATTVOLT_ADDR                    ((uint32_t)(&g_adcBattVolt))
#define PIC_PACKVOLT_ADDR                    ((uint32_t)(&g_adcPackVolt))
#define PIC_PACKSPVOLT_ADDR                  ((uint32_t)(&g_adcPackSP))
#define PIC_TESTSTATUS_ADDR                  ((uint32_t)(&g_testCommandStatus.word))
#define PIC_SYSSIGNAL_ADDR                   ((uint32_t)(&g_chgDsgControl))

#define PIC_CHG_PROTECT_ADDR                 ((uint32_t)(&g_flagPicChgProtect.word))
#define PIC_DSG_PROTECT_ADDR                 ((uint32_t)(&g_flagPicDsgProtect.word))

#define PIC_ADC_VOLT_ADDR                    ((uint16_t)(&g_adcVoltArray[0u])) 
#define PIC_RESETCAUSE_ADDR                  ((uint32_t)(&g_resetCause))

#define PARALLEL_CONTROL    0xe0u
#define NO1_DATA            0x01u
#define NO2_DATA            0x02u
#define NO3_DATA            0x03u
#define NO4_DATA            0x04u
#define WRITE_FET           0x46u

#define BROADCAST_ROW       4u
#define BROADCAST_COLUMN    8u

#define OPERATION_MODE_SHIP              2u
#define OPERATION_MODE_STANDBY           4u
#define OPERATION_MODE_DISCHARGE         5u
#define OPERATION_MODE_CHARGE            6u
#define OPERATION_MODE_FAULT             7u
#define OPERATION_MODE_PERMANENT_FAULT   8u
#define OPERATION_MODE_BALANCING         9u

#define CANOPEN_COMM_LENGTH                     77u
#define FW_VERSION_ADDR                         ((uint32_t)(&g_FWversion[0u]))
#define DEVICE_TYPE_ADDR                        ((uint32_t)(& g_deviceType))
#define ERR_REGISTER_ADDR                       ((uint32_t)(&g_errRegister.byte))
#define MANUF_NAME_ADDR                         ((uint32_t)(&g_manufactureName[0u]))
#define HW_VERSION_ADDR                         ((uint32_t)(&g_HWversion[0u]))
#define COBID_EMCY_ADDR                         ((uint32_t)(&g_cobIDEMCY))
#define INHIBIT_TIME_EMCY_ADDR                  ((uint32_t)(&g_inhibitTimeEMCY))
#define CON_HB_TIME_ADDR                        ((uint32_t)(&g_consumerHeartbeatTime))
#define PRO_HB_TIME_ADDR                        ((uint32_t)(&g_producerHeartbeatTime))
#define IDENTITY_OBJECT_SUB_ADDR                ((uint32_t)(&g_identityObjectSUB))
#define VENDOR_ID_ADDR                          ((uint32_t)(&g_vendorID))
#define PRODUCT_CODE_ADDR                       ((uint32_t)(&g_productCode))
#define REVISION_NUMBER_ADDR                    ((uint32_t)(&g_revisionNumber))
#define SERIAL_NUMBER_ADDR                      ((uint32_t)(&g_serialNumber))
#define SDO_SERVER1_SUB_ADDR                    ((uint32_t)(&g_sdoServer1SUB))
#define COB_ID_RX_ADDR                          ((uint32_t)(&g_cobIDrx))
#define COB_ID_TX_ADDR                          ((uint32_t)(&g_cobIDtx))
#define NOTE_ID_SDO_ADDR                        ((uint32_t)(&g_nodeIDofSDO))
#define CANBATT_STATUS_ADDR                     ((uint32_t)(&g_canBattStatus))
#define CHARGER_STATUS_ADDR                     ((uint32_t)(&g_chargerStatus))
#define TEMPERATURE_ADDR                        ((uint32_t)(&g_temperature))
#define BATTPARMETERS_SUB_ADDR                  ((uint32_t)(&g_battParmetersSUB))
#define BATT_TYPE_ADDR                          ((uint32_t)(&g_batttype))
#define BATT_CAP_ADDR                           ((uint32_t)(&g_batttCap))
#define MAX_CHGCURR_ADDR                        ((uint32_t)(&g_maxCHGcurr))
#define CELL_NUMBER_ADDR                        ((uint32_t)(&g_cellNumber))
#define BATTSN_SUB_ADDR                         ((uint32_t)(&g_battSNSUB))
#define BATTSN_LOW_ADDR                         ((uint32_t)(&g_battSNLow))
#define BATTSN_HIGH_ADDR                        ((uint32_t)(&g_battSNHigh))
#define CUMULATIVE_TOTAL_CHARGE_ADDR            ((uint32_t)(&g_cumulativeTotalCharge))
#define EXPEND_SINCELAST_CHARGE_ADDR            ((uint32_t)(&g_expendSinceLastCharge))
#define RETURND_DURINGLAST_CHARGE_ADDR          ((uint32_t)(&g_returnedDuringLastCharge))
#define BATT_VOLTAGE_ADDR                       ((uint32_t)(&g_battVoltage))
#define CHARGE_CURR_PEQUESTED_ADDR              ((uint32_t)(&g_chargeCurrPequested))
#define CANBATT_SOC_ADDR                        ((uint32_t)(&g_canBattSOC))
#define FERR_ADDR                               ((uint32_t)(&g_freeData))

#define TEST_SUB_ADDR                           ((uint32_t)(&g_testCommandSub))
#define NTC1_ADDR                               ((uint32_t)(&g_NTCTemperatureTable[0]))
#define NTC2_ADDR                               ((uint32_t)(&g_NTCTemperatureTable[1]))
#define NTC3_ADDR                               ((uint32_t)(&g_NTCTemperatureTable[2]))
#define NTC4_ADDR                               ((uint32_t)(&g_NTCTemperatureTable[3]))
#define NTC5_ADDR                               ((uint32_t)(&g_NTCTemperatureTable[4]))
#define NTC6_ADDR                               ((uint32_t)(&g_NTCTemperatureTable[5]))
#define NTC7_ADDR                               ((uint32_t)(&g_NTCTemperatureTable[6]))
#define NTC8_ADDR                               ((uint32_t)(&g_NTCTemperatureTable[7]))
#define NTC9_ADDR                               ((uint32_t)(&g_NTCTemperatureTable[8]))
#define NTC10_ADDR                              ((uint32_t)(&g_NTCTemperatureTable[9]))
#define NTC11_ADDR                              ((uint32_t)(&g_NTCTemperatureTable[10]))
#define NTC12_ADDR                              ((uint32_t)(&g_NTCTemperatureTable[11]))
#define NTC13_ADDR                              ((uint32_t)(&g_NTCTemperatureTable[12]))
#define NTC14_ADDR                              ((uint32_t)(&g_NTCTemperatureTable[13]))
#define NTC15_ADDR                              ((uint32_t)(&g_NTCTemperatureTable[14]))
#define NTC16_ADDR                              ((uint32_t)(&g_NTCTemperatureTable[15]))
#define PACK_VOLTAGE_ADC_VALUE_ADDR             ((uint32_t)(&g_adcPackVolt))
#define BATT_VOLTAGE_ADC_VALUE_ADDR             ((uint32_t)(&g_adcBattVolt))
#define BUCK_CHARGE_ADC_VALUE_ADDR              ((uint32_t)(&g_adcPackSP))
#define RESERVED1_ADC_VALUE_ADDR                ((uint32_t)(&g_adcReserved1))
#define RESERVED2_ADC_VALUE_ADDR                ((uint32_t)(&g_adcReserved2))
#define RESERVED3_ADC_VALUE_ADDR                ((uint32_t)(&g_adcReserved3))
#define RESERVED4_ADC_VALUE_ADDR                ((uint32_t)(&g_adcReserved4))
#define RESERVED5_ADC_VALUE_ADDR                ((uint32_t)(&g_adcReserved5))
#define CHARGE_FAULT_ADDR                       ((uint32_t)(&g_flagPicChgProtect.word))
#define DISCHARGE_FAULT_ADDR                    ((uint32_t)(&g_flagPicDsgProtect.word))
#define CHARGE_MODE_ADDR                        ((uint32_t)(&g_chargeMode))
#define CHARGE_ENABLE_ADDR                      ((uint32_t)(&g_FETEnCH.bitByte))
#define DISCHARGE_ENABLE_ADDR                   ((uint32_t)(&g_FETEnDisCH.bitByte))
#define CHARGER_FAULT_FLAG_ADDR                 ((uint32_t)(&g_chargerFaultFlg.word))
#define PERIPHERAL_STATUS_ADDR                  ((uint32_t)(&g_PerpheralStatus.word))
#define PIC_STATUS1_ADDR                        ((uint32_t)(&g_flagDsgStatus.word))
#define PIC_STATUS2_ADDR                        ((uint32_t)(&g_testCommandStatus.word))
#define CH_DSG_ENABLE_ADDR                      ((uint32_t)(&g_chgDsgControl))
#define CELL_POSITION_ADDR                 		((uint32_t)(&g_cellPosition))

#define TPDO_PARAMETER_SUB_ADDR                 ((uint32_t)(&g_tpdoParameterSub))
#define TPDO_PARAMETER_TYPE_ADDR                ((uint32_t)(&g_tpdoParmeterType))
#define TPDO_PARAMETER_TIME_ADDR                ((uint32_t)(&g_tpdoParameterTime))
#define TPDO_PARAMETER_NODE_ID1_ADDR            ((uint32_t)(&g_tpdoParameterNodeId1))
#define TPDO_PARAMETER_NODE_ID2_ADDR            ((uint32_t)(&g_tpdoParameterNodeId2))
#define TPDO_PARAMETER_NODE_ID3_ADDR            ((uint32_t)(&g_tpdoParameterNodeId3))
#define TPDO_PARAMETER_NODE_ID4_ADDR            ((uint32_t)(&g_tpdoParameterNodeId4))

#define TPDO_MAPPING1_SUB_ADDR                  ((uint32_t)(&g_tpdoMappingParameterSub[0]))
#define TPDO_MAPPING2_SUB_ADDR                  ((uint32_t)(&g_tpdoMappingParameterSub[1]))
#define TPDO_MAPPING3_SUB_ADDR                  ((uint32_t)(&g_tpdoMappingParameterSub[2]))
#define TPDO_MAPPING4_SUB_ADDR                  ((uint32_t)(&g_tpdoMappingParameterSub[4]))

#define TPDO_MAPPING1_ENTR1_ADDR                ((uint32_t)(&g_tpdoMappingParameter[0]))
#define TPDO_MAPPING1_ENTR2_ADDR                ((uint32_t)(&g_tpdoMappingParameter[1]))
#define TPDO_MAPPING1_ENTR3_ADDR                ((uint32_t)(&g_tpdoMappingParameter[2]))
#define TPDO_MAPPING1_ENTR4_ADDR                ((uint32_t)(&g_tpdoMappingParameter[3]))
#define TPDO_MAPPING1_ENTR5_ADDR                ((uint32_t)(&g_tpdoMappingParameter[4]))

#define TPDO_MAPPING2_ENTR1_ADDR                ((uint32_t)(&g_tpdoMappingParameter[5]))
#define TPDO_MAPPING2_ENTR2_ADDR                ((uint32_t)(&g_tpdoMappingParameter[6]))
#define TPDO_MAPPING2_ENTR3_ADDR                ((uint32_t)(&g_tpdoMappingParameter[7]))
#define TPDO_MAPPING2_ENTR4_ADDR                ((uint32_t)(&g_tpdoMappingParameter[8]))
#define TPDO_MAPPING2_ENTR5_ADDR                ((uint32_t)(&g_tpdoMappingParameter[9]))

#define TPDO_MAPPING3_ENTR1_ADDR                ((uint32_t)(&g_tpdoMappingParameter[10]))
#define TPDO_MAPPING3_ENTR2_ADDR                ((uint32_t)(&g_tpdoMappingParameter[11]))
#define TPDO_MAPPING3_ENTR3_ADDR                ((uint32_t)(&g_tpdoMappingParameter[12]))
#define TPDO_MAPPING3_ENTR4_ADDR                ((uint32_t)(&g_tpdoMappingParameter[13]))
#define TPDO_MAPPING3_ENTR5_ADDR                ((uint32_t)(&g_tpdoMappingParameter[14]))

#define TPDO_MAPPING4_ENTR1_ADDR                ((uint32_t)(&g_tpdoMappingParameter[15]))
#define TPDO_MAPPING4_ENTR2_ADDR                ((uint32_t)(&g_tpdoMappingParameter[16]))
#define TPDO_MAPPING4_ENTR3_ADDR                ((uint32_t)(&g_tpdoMappingParameter[17]))
#define TPDO_MAPPING4_ENTR4_ADDR                ((uint32_t)(&g_tpdoMappingParameter[18]))
#define TPDO_MAPPING4_ENTR5_ADDR                ((uint32_t)(&g_tpdoMappingParameter[19]))

#define CAN_BATT_SOH_ADDR                       ((uint32_t)(&g_battSOH))
#define CAN_BATT_OPERATION_MODE_ADDR            ((uint32_t)(&g_operationMode))
#define CAN_BATT_CHARGE_FAULT_ADDR              ((uint32_t)(&g_flagPicChgProtect.word))
#define CAN_BATT_DISCHARGE_FAULT_ADDR           ((uint32_t)(&g_flagPicDsgProtect.word))
#define CAN_BATT_CURRENT_ADDR                   ((uint32_t)(&g_battCurr))
#define CAN_REGEN_CURRENT_ADDR                  ((uint32_t)(&g_regenCurrLimit))
#define CAN_CHARGE_CURRENT_ADDR                 ((uint32_t)(&g_chgCurrLimit))
#define CAN_DISCHARGE_CURRENT_ADDR              ((uint32_t)(&g_dsgCurrLimit))
#define CAN_MIN_CELL_TEMP_ADDR                  ((uint32_t)(&g_minTemp))
#define CAN_MAX_CELL_TEMP_ADDR                  ((uint32_t)(&g_maxTemp))
#define CAN_MIN_CELL_VOLT_ADDR                  ((uint32_t)(&g_minCellVolt))
#define CAN_MAX_CELL_VOLT_ADDR                  ((uint32_t)(&g_maxCellVolt))
#define CAN_SYSTEM_SOC_ADDR                     ((uint32_t)(&g_sysSOC))
#define CAN_CELL_BALANCING_STATUS_ADDR          ((uint32_t)(&g_balanceState))
#define CAN_PACK_VOLTAGE_ADDR                   ((uint32_t)(&g_battVolt))
#define CAN_REMAIN_RUN_TIME_ADDR                ((uint32_t)(&g_bqRunTimeToEmpty))
#define CAN_HEATER_STATUS_ADDR                  ((uint32_t)(&g_heaterState))
#define CAN_HEATER_CONTROL_ADDR                 ((uint32_t)(&g_heaterControl))




#define LSS_OFF_STATE                   0x00u
#define LSS_INIT_STATE                  0x01u
#define LSS_WAIT_STATE                  0x10u
#define LSS_CONFI_STATE                 0x55u
#define LSS_FINAL_STATE                 0xffu

#define LSS_CIA305_ID                   0x7E5u 
#define LSS_CIA305_RESPONSE_ID          0x7E4u 
#define LSS_SWITCH_STATE_CS             0x04u
#define LSS_SWITCH_VENDORID_CS          0x40u
#define LSS_SWITCH_PRODUCTCODE_CS       0x41u
#define LSS_SWITCH_RN_CS                0x42u
#define LSS_SWITCH_SN_CS                0x43u
#define LSS_SWITCH_RESPONSE_CS          0x44u
#define LSS_CONFIGURE_ID_CS             0x11u
#define LSS_CONFIGURE_RATE_CS           0x13u
#define LSS_UPDATE_RATE_CS              0x15u
#define LSS_SAVE_CONFI_CS               0x17u
#define LSS_INQUIRE_VENDORID_CS         0x5Au
#define LSS_INQUIRE_PRODUCTCODE_CS      0x5Bu
#define LSS_INQUIRE_RN_CS               0x5Cu
#define LSS_INQUIRE_SN_CS               0x5Du
#define LSS_INQUIRE_NODEID_CS           0x5Eu

#define LSS_ID_REMOTE_SLAVE_ID_CS            0x46u
#define LSS_ID_REMOTE_SLAVE_PRODUCTCODE_CS   0x47u
#define LSS_ID_REMOTE_SLAVE_RN_L_CS          0x48u
#define LSS_ID_REMOTE_SLAVE_RN_H_CS          0x49u
#define LSS_ID_REMOTE_SLAVE_SN_L_CS          0x4Au
#define LSS_ID_REMOTE_SLAVE_SN_H_CS          0x4Bu
#define LSS_ID_SLAVE_CS                      0x4Fu
#define LSS_ID_NON_CFG_REMOTE_SLAVE_CS       0x4Cu
#define LSS_ID_NON_CFG_SLAVE_CS              0x50u
#define LSS_FAST_SCAN_CS                     0x51u


#define NODE_STATE_RESETMCU             0u
#define NODE_STATE_RESETNODE            1u
#define NODE_STATE_RESETCOMM            2u
#define NODE_STATE_PREOPERATION         3u
#define NODE_STATE_OPERATION            4u
#define NODE_STATE_STOPPED              5u

#define SDO_DATA_LENGTH                 8u
#define NMT_DATA_LENGTH                 2u
#define HEARTBEAT_DATA_LENGTH           8u
#define TPDO1_DATA_LENGTH               8u
#define NMT_CS_OPERATION                0x01u
#define NMT_CS_STOPPED                  0x02u
#define NMT_CS_PREOPERATION             0x80u
#define NMT_CS_RESETNODE                0x81u
#define NMT_CS_RESETCOMM                0x82u
#define HEARTBEAT_STATE_BOOT_UP         0x00u
#define HEARTBEAT_STATE_STOPPED         0x04u
#define HEARTBEAT_STATE_OPERATION       0x05u
#define HEARTBEAT_STATE_PREOPERATION    0x7fu

#define OBJECT_SUCCEED                       0u
#define OBJECT_NOT_EXIST                     1u
#define OBJECT_LENGTH_NOT_MATCH              2u
#define OBJECT_NOT_SUPPORT_WRITE             3u
#define SUB_INDEX_NOT_EXIST                  4u


#define g_flagGenericError          g_errRegister.bitByte.bit0
#define g_flagCurrentError          g_errRegister.bitByte.bit1
#define g_flagVoltageError          g_errRegister.bitByte.bit2
#define g_flagTempError             g_errRegister.bitByte.bit3
#define g_flagCommError             g_errRegister.bitByte.bit4
#define g_flagDeviceProfileError    g_errRegister.bitByte.bit5


extern uint8_t g_picVersion[4u];
extern uint8_t g_pendNodeID;
extern uint8_t g_fixNodeID;
extern uint8_t g_pendBitRate;
extern uint8_t g_fixBitRate;
extern uint16_t g_lssUpdateRateTime;
extern uint8_t g_nodeState;
extern uint32_t g_productCode;
extern uint32_t g_lsspPoductCode;

uint8_t readCommandProcess(uint8_t s_CMD);
//void CANopenProcessSDO(uint8_t dataLength);
void AutoBroadcast (void);
void CANopenProcessSDO(void);
void CANopenProcessNMT(void);
void heartBeatProcess(void);
void CANopenProcessLSS(void);
void ProcessCanOpenReceiveData(void);

#endif	/* CANOPENV01_H */

