/**
 * @file  ledV01.h
 *
 * REVISION HISTORY
 *==============================================
 * |Ticket   |Date      |Author       |Notes
 * |----:    |:----:    |:----:       |:----
 * |Creation |02/04/22  |Tom O'Connor |Document
 *
 * @par   COPYRIGHT NOTICE: (c) 2022 Inventus Power.  All rights reserved.
 */
/* Inventus Power Proprietary */
#ifndef _LED_H_
#define _LED_H_

/*Led faultID*/
#define LED_FAULTID                    ((uint32_t)(&g_faultID))
#define LED_FAULTID_NOT                 0
#define LED_FAULTID_F0                  1
#define LED_FAULTID_F1                  2
#define LED_FAULTID_F2                  3
#define LED_FAULTID_F3                  4
#define LED_FAULTID_F4                  5
#define LED_FAULTID_F5                  6
#define LED_FAULTID_F6                  7
#define LED_FAULTID_F7                  8
#define LED_FAULTID_F8                  9
#define LED_FAULTID_F9                  10
#define LED_FAULTID_F10                  11
#define LED_FAULTID_F11                  12
#define LED_FAULTID_F12                  13
#define LED_FAULTID_F13                  14
#define LED_FAULTID_F14                  15
#define LED_FAULTID_F15                  16

/*define displayFaultID0-15*/
#define DISPLAY_FAULTID_F0     {LED5_Clear();LED4_Clear();LED3_Clear();LED2_Clear();}
#define DISPLAY_FAULTID_F1     {LED5_Set();  LED4_Clear();LED3_Clear();LED2_Clear();}
#define DISPLAY_FAULTID_F2     {LED5_Clear();LED4_Set();  LED3_Clear();LED2_Clear();}
#define DISPLAY_FAULTID_F3     {LED5_Set();  LED4_Set();  LED3_Clear();LED2_Clear();}
#define DISPLAY_FAULTID_F4     {LED5_Clear();LED4_Clear();LED3_Set();  LED2_Clear();}
#define DISPLAY_FAULTID_F5     {LED5_Set();  LED4_Clear();LED3_Set();  LED2_Clear();}
#define DISPLAY_FAULTID_F6     {LED5_Clear();LED4_Set();  LED3_Set();  LED2_Clear();}
#define DISPLAY_FAULTID_F7     {LED5_Set();  LED4_Set();  LED3_Set();  LED2_Clear();}
#define DISPLAY_FAULTID_F8     {LED5_Clear();LED4_Clear();LED3_Clear();LED2_Set();  }
#define DISPLAY_FAULTID_F9     {LED5_Set();  LED4_Clear();LED3_Clear();LED2_Set();  }
#define DISPLAY_FAULTID_F10    {LED5_Clear();LED4_Set();  LED3_Clear();LED2_Set();  }
#define DISPLAY_FAULTID_F11    {LED5_Set();  LED4_Set();  LED3_Clear();LED2_Set();  }
#define DISPLAY_FAULTID_F12    {LED5_Clear();LED4_Clear();LED3_Set();  LED2_Set();  }
#define DISPLAY_FAULTID_F13    {LED5_Set();  LED4_Clear();LED3_Set();  LED2_Set();  }
#define DISPLAY_FAULTID_F14    {LED5_Clear();LED4_Set();  LED3_Set();  LED2_Set();  }
#define DISPLAY_FAULTID_F15    {LED5_Set();  LED4_Set();  LED3_Set();  LED2_Set();  }

extern uint8_t g_faultID;

void LedControl(void);
void initLed(void);
void allLed_displayOff(void);

#endif
/* Inventus Power Proprietary */
