/**
 * @file  detectV01.h
 *
 * REVISION HISTORY
 *==============================================
 * |Ticket   |Date      |Author       |Notes
 * |----:    |:----:    |:----:       |:----
 * |Creation |02/04/22  |Tom O'Connor |Document
 *
 * @par   COPYRIGHT NOTICE: (c) 2022 Inventus Power.  All rights reserved.
 */
/* Inventus Power Proprietary */
/* 
 * File:   detectV01.h
 * Author: admin
 *
 * Created on September 12, 2018, 3:26 PM
 */

#ifndef DETECTV01_H
#define	DETECTV01_H




/* pack volt - batt volt */
#define ADC_26V4    5837u
#define ADC_3V      663u
#define ADC_1V      221u

/* 0mA/10 */
#define BROADCAST_CHG_CURR_0A       0u
/* 7500mA/10 */
#define BROADCAST_CHG_CURR_7A5      750u
/* 20000mA/10 */
#define BROADCAST_CHG_CURR_20A      2000u
/* 0A/10 */
#define BROADCAST_DSG_CURR_0A       0u
/* 200mA/10 */
#define BROADCAST_DSG_CURR_200MA    20u
/* 5000mA/10 */
#define BROADCAST_DSG_CURR_5A       500u
/* 15000mA/10 */
#define BROADCAST_DSG_CURR_15A      1500u
/* 50000mA/10 */
#define BROADCAST_REGEN_CURR_50A    5000u
/* 60000mA/10 */
#define BROADCAST_DSG_CURR_60A      6000u

#define CURR_LIMIT_SOC_10            10u
#define CURR_LIMIT_SOC_15            15u
#define CURR_LIMIT_SOC_25            25u
#define CURR_LIMIT_SOC_30            30u

#define CURR_LIMIT_VOLT_3V30        3300u
#define CURR_LIMIT_VOLT_3V50        3500u
#define CURR_LIMIT_VOLT_3V45        3450u
#define CURR_LIMIT_VOLT_3V65        3650u

#define CURRENT_RATE      5

#define DSG_CURR_5A               ((-5000) / CURRENT_RATE)
#define DSG_CURR_3A               ((-3000) / CURRENT_RATE)
#define DSG_CURR_2A               ((-2000) / CURRENT_RATE)
#define DSG_CURR_100MA            ((-100) / CURRENT_RATE)
#define DSG_CURR_150MA            ((-150) / CURRENT_RATE)
#define DSG_CURR_500MA            ((-500) / CURRENT_RATE)
        
#define CHG_CURR_1A               (1000 / CURRENT_RATE)
#define CHG_CURR_500MA            (500 / CURRENT_RATE)
#define CHG_CURR_100MA            (100 / CURRENT_RATE)
#define CHG_CURR_150MA            (150 / CURRENT_RATE)

#define NO_CHG_DSG       0u
#define CHARGE_EN        1u
#define DISCHARGE_EN     2u
#define SIGNAL_ERR       3u

#define NONE_MODE                   0x00u                   
#define SLAVE_MODE            		0x01u
#define MASTER_MODE          		0x02u

#define CELL_ERR_POSITION           0x00u 
#define CELL_1S_POSITION            0x01u                   
#define CELL_2S_POSITION            0x02u


extern uint8_t g_chgDsgControl;
extern uint8_t g_commMode;
extern uint8_t g_cellPosition;
extern uint8_t g_checkPositionResult;
extern uint16_t g_shipLedTimer;
extern uint16_t s_AddrAllocateTime;

void DetectFunction(void);
void RemovePackDetectFunction (void);
void TestCmdOvfDetectFunction (void);
void EIC_Interrupt(uint8_t s_Channel);

#endif	/* DETECTV01_H */
/* Inventus Power Proprietary */
