/**
 * @file  i2cMasterV02.h
 *
 * REVISION HISTORY
 *==============================================
 * |Ticket   |Date      |Author       |Notes
 * |----:    |:----:    |:----:       |:----
 * |Creation |02/04/22  |Tom O'Connor |Document
 *
 * @par   COPYRIGHT NOTICE: (c) 2022 Inventus Power.  All rights reserved.
 */
/* Inventus Power Proprietary */
#ifndef _I2CMASTER_H_
#define _I2CMASTER_H_

#include "mainV01.h"

#define SPEED              1u

/*communicate with battery*/
#define CMD_ACCESS          0x00u
#define CMD_TEMP            0x08u
#define CMD_VOLT            0x09u
#define CMD_PACK_VOLT       0x90u
#define CMD_CURR            0x0Au
#define CMD_SOC             0x0Du
#define CMD_CHG_CURR        0x14u
#define CMD_CHG_VOLT        0x15u
#define CMD_BATT_STATUS     0x16u
#define CMD_MAX_TEMP        0xddu
#define CMD_MIN_TEMP        0xdcu
#define CMD_MAXFET_TEMP     0xDBu
#define CMD_MINFET_TEMP     0xDAu
#define CMD_MAXPACK_TEMP    0xE3u
#define CMD_MINPACK_TEMP    0xE2u
#define CMD_CONTROL_FET     0xdfu
#define CMD_MAX_CELLVOLT    0xe8u
#define CMD_MIN_CELLVOLT    0xe9u
#define CMD_CHG_PROTECT1    0x70u
#define CMD_CHG_PROTECT2    0x71u
#define CMD_DSG_PROTECT1    0x72u
#define CMD_DSG_PROTECT2    0x73u
#define CMD_SOH             0x4fu
#define CMD_OPERATION_MODE  0xdeu
#define CMD_BQ_WARNING      0xf9u
#define CMD_SIGNAL          0x04u
#define CMD_BQ_TIME_TO_EMPTY    0x12u
#define CMD_BQ_SN               0x1cu

#define    BATT_ADDR        0x0Bu
#define    SBS_COMM_LENGTH     21u
#define    SBS_DATA_LENGTH     12U

#define ACCESS_ADDR                     ((uint32_t)(&g_access))
#define TEMP_ADDR                       ((uint32_t)(&g_battTemp))
#define VOLT_ADDR                       ((uint32_t)(&g_battVolt))
#define PACK_VOLT_ADDR                  ((uint32_t)(&g_packVolt))
#define CURR_ADDR                       ((uint32_t)(&g_battCurr))
#define SOC_ADDR                        ((uint32_t)(&g_battSOC))
#define BATT_STATUS_ADDR                ((uint32_t)(&g_battStatus.word))
#define MAX_TEMP_ADDR                   ((uint32_t)(&g_maxTemp))
#define MIN_TEMP_ADDR                   ((uint32_t)(&g_minTemp))
#define CONTROL_FET_ADDR                ((uint32_t)(&g_controlBqFet.wordValue)) 
#define MIN_CELLVOLT_ADDR               ((uint32_t)(&g_minCellVolt))
#define MAX_CELLVOLT_ADDR               ((uint32_t)(&g_maxCellVolt))
#define MAXFET_TEMP_ADDR                ((uint32_t)(&g_maxFetTemp))
#define MINFET_TEMP_ADDR                ((uint32_t)(&g_minFetTemp))
#define MAXPACK_TEMP_ADDR               ((uint32_t)(&g_maxPackTemp))
#define MINPACK_TEMP_ADDR               ((uint32_t)(&g_minPackTemp))

#define CHG_PROTECT_ADDR1               ((uint32_t)(&g_8050ChgProtect1.word))
#define CHG_PROTECT_ADDR2               ((uint32_t)(&g_8050ChgProtect2.word))
#define DSG_PROTECT_ADDR1               ((uint32_t)(&g_8050DsgProtect1.word))
#define DSG_PROTECT_ADDR2               ((uint32_t)(&g_8050DsgProtect2.word))
#define SOH_ADDR                        ((uint32_t)(&g_battSOH))
#define BQ_WARNING_ADDR                 ((uint32_t)(&g_bqWarningAlarm))
#define BATT_SIGNAL_ADDR                ((uint32_t)(&g_battSignal.word))

#define TOTAL_AH_CHARGE_H_ADDR          ((uint32_t)(&g_CumulativeTotalAhChargeH))
#define TOTAL_AH_CHARGE_L_ADDR          ((uint32_t)(&g_CumulativeTotalAhChargeL))
#define AH_EXP_LAST_CHARGE_ADDR         ((uint32_t)(&g_AhExpendedSinceLastCharge))
#define AH_RET_LAST_CHARGE_ADDR         ((uint32_t)(&g_AhReturnedDuringLastCharge))
#define BQ_TIME_TO_EMPTY_ADDR           ((uint32_t)(&g_bqRunTimeToEmpty))
#define BQ_SN_ADDR                      ((uint32_t)(&g_battSN))


#define OPERATION_MODE_ADDR             ((uint32_t)(&g_operationMode))

typedef union
{
    struct
    {
        uint8_t DSGbyte;  /* LSB */
        uint8_t CHGbyte;  /* MSB */  
    } bytes;
    uint16_t wordValue;
} define_fet;

#define FET_ON          0x56u
#define FET_OFF         0xABu


#define g_flag8050DSGOn             (g_battStatus.bitWord.bit1)
#define g_flag8050CHGOn             (g_battStatus.bitWord.bit2)
#define g_flag8050FC                (g_battStatus.bitWord.bit5)
#define g_flag8050Shipmode          (g_battStatus.bitWord.bit13)

/*******************************************************************************/
/*0x70 Cmd*/
#define g_8050turnOffChgMossfet     (g_8050ChgProtect1.bitWord.bit15)
#define g_8050CHT                   (g_8050ChgProtect1.bitWord.bit14)
#define g_8050OCC                   (g_8050ChgProtect1.bitWord.bit13)
#define g_8050OCV                   (g_8050ChgProtect1.bitWord.bit12)
#define g_8050CLT                   (g_8050ChgProtect1.bitWord.bit11)
#define g_8050ChgFuseHigh           (g_8050ChgProtect1.bitWord.bit9)  
#define g_8050ChgSUV                (g_8050ChgProtect1.bitWord.bit5) 

#define MASK_8050CHGPRO1			0xefff
/*******************************************************************************/
/*0x71 Cmd*/
#define g_8050ChgOCDAFE             (g_8050ChgProtect2.bitWord.bit12)
#define g_8050ChgSCDAFE             (g_8050ChgProtect2.bitWord.bit11)
#define g_8050Bq76940ChgErr         (g_8050ChgProtect2.bitWord.bit9)  
#define g_8050Bq76940ChgComFail     (g_8050ChgProtect2.bitWord.bit8)  
#define g_8050CELLCHT			    (g_8050ChgProtect2.bitWord.bit7)  
#define g_8050CELLCLT			    (g_8050ChgProtect2.bitWord.bit6)  
#define g_8050CHT_MOS				(g_8050ChgProtect2.bitWord.bit5) 
#define g_8050CMOSLT				(g_8050ChgProtect2.bitWord.bit4) 
#define g_8050ChgOCC2               (g_8050ChgProtect2.bitWord.bit3)
#define g_8050Bq76940ChgHWSC        (g_8050ChgProtect2.bitWord.bit2)
#define g_8050ChgOCCAFE		        (g_8050ChgProtect2.bitWord.bit1)

#define MASK_8050CHGPRO2			0xffff

/*******************************************************************************/
/*0x72 Cmd*/
#define g_8050Shutdown              (g_8050DsgProtect1.bitWord.bit14)
#define g_8050DHT                   (g_8050DsgProtect1.bitWord.bit13)
#define g_8050DLT                   (g_8050DsgProtect1.bitWord.bit12)
#define g_8050OCD                   (g_8050DsgProtect1.bitWord.bit11)
#define g_8050CUV                   (g_8050DsgProtect1.bitWord.bit10)
#define g_8050DsgFuseHigh           (g_8050DsgProtect1.bitWord.bit8)
#define g_8050CmdShutdown           (g_8050DsgProtect1.bitWord.bit4)
#define g_8050DsgSUV                (g_8050DsgProtect1.bitWord.bit3)

#define MASK_8050DSGPRO1			0x3fef

/*******************************************************************************/
/*0x73 Cmd*/
#define g_8050DsgOCCAFE             (g_8050DsgProtect2.bitWord.bit12)
#define g_8050DsgSCDAFE             (g_8050DsgProtect2.bitWord.bit11)
#define g_8050Bq76940DsgErr         (g_8050DsgProtect2.bitWord.bit9)
#define g_8050Bq76940DsgComFail     (g_8050DsgProtect2.bitWord.bit8)
#define g_8050CELLDHT               (g_8050DsgProtect2.bitWord.bit7) 
#define g_8050CELLDLT               (g_8050DsgProtect2.bitWord.bit6) 
#define g_8050DHT_MOS               (g_8050DsgProtect2.bitWord.bit5) 
#define g_8050MOSDLT				(g_8050DsgProtect2.bitWord.bit4) 
#define g_8050OCD2                  (g_8050DsgProtect2.bitWord.bit3)
#define g_8050DHWS        			(g_8050DsgProtect2.bitWord.bit2) 
#define g_8050OCD3                  (g_8050DsgProtect2.bitWord.bit1)

#define MASK_8050DSGPRO2			0xffff
 


extern define_16bits g_battSignal;
#define g_flagStartUpSignal               (g_battSignal.bitWord.bit0)
#define g_flagSleepSignal                 (g_battSignal.bitWord.bit1)
#define g_flagShipSignal                  (g_battSignal.bitWord.bit2)
#define g_flagBQSleepWake                 (g_battSignal.bitWord.bit3)
#define g_flagLedSignal                   (g_battSignal.bitWord.bit10)
#define g_flagInterlock                   (g_battSignal.bitWord.bit12)
#define g_flagIgnition                    (g_battSignal.bitWord.bit14)


extern define_fet g_controlBqFet;
extern uint16_t g_battTemp;
extern uint16_t g_battVolt;
extern uint16_t g_packVolt;
extern int16_t g_battCurr;
extern uint16_t g_battSOC;
extern uint16_t g_sysSOC;
extern uint16_t g_battSOH;
extern uint16_t g_operationMode;
extern uint16_t g_bqWarningAlarm;
extern define_16bits g_battStatus;
extern define_16bits g_8050ChgProtect1;
extern define_16bits g_8050ChgProtect2;
extern define_16bits g_8050DsgProtect1;
extern define_16bits g_8050DsgProtect2;
extern uint16_t g_battChgCurr;
extern uint16_t g_battChgVolt;
extern uint16_t g_minCellVolt;
extern uint16_t g_maxCellVolt;

extern uint16_t g_totalCommTime;
extern uint16_t g_successCommTime;
extern uint8_t g_successRate;
//extern uint8_t g_flagCommFail;

extern uint16_t g_CumulativeTotalAhChargeH;
extern uint16_t g_CumulativeTotalAhChargeL;
extern uint16_t g_AhExpendedSinceLastCharge;
extern uint16_t g_AhReturnedDuringLastCharge;
extern uint16_t g_bqRunTimeToEmpty;
extern uint16_t g_battSN;


void Communication(void);
bool readBattData(uint8_t smbCmd,uint32_t cmdLength,uint8_t *P);
bool writeBattData(uint8_t smbCmd,uint32_t cmdLength,uint8_t *P);

#endif
/* Inventus Power Proprietary */
