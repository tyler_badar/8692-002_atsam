/**
 * @file  canV01.h
 *
 * REVISION HISTORY
 *==============================================
 * |Ticket   |Date      |Author       |Notes
 * |----:    |:----:    |:----:       |:----
 * |Creation |02/04/22  |Tom O'Connor |Document
 *
 * @par   COPYRIGHT NOTICE: (c) 2022 Inventus Power.  All rights reserved.
 */
/* 
 * File:   canV01.h
 * Author: markli
 *
 * Created on 2018?1?18?, ??1:49
 */

#ifndef CAN_H
#define	CAN_H

#include "i2cMasterV02.h"
#include "chgdsgV01.h"

#define  CAN_FREQUENCY    48000u /*48MHz*/

extern uint8_t g_parallelQuantity;
extern uint8_t g_canAddr;
extern uint8_t s_startAddrAllocateDebounce;
extern uint8_t s_requestAllocateTimes;
extern bool g_flagParallel;

extern uint8_t g_note_ID;
extern uint8_t g_BitRate;
extern uint32_t s_uidArray[MAX_PARALLEL_COUNT];
extern uint8_t g_flagTest;
extern bool g_flagLoadCanSettingSucceed;
extern uint16_t s_can0HoldTime;
extern uint16_t g_batcanHoldTime;

#define BATT_DEFAULT_ID          0x31u
#define BATT_DEFAULT_SN          0x01u

#define BATT_DEFAULT_ADDR        0x01u
#define UID_ADDR                 0x7ffcu
#define BLF_VERSION_ADDR         0x3ffcu
#define MCU_SN_ADDR              0x0080a00u

#define BATT_RESPONSE_ID         0x13ful
#define BATT_ID                  0x17ful
#define PIC_RESPONSE_ID          0x12ful
#define PIC_ID                   0x16ful
#define ADDRALLOCATION_ID        0x18FFFFF3
#define ENTERBOOTLOADER_ID       0x18FFBD00

#define COBID_RSDO            0x631 
#define BOOTLOADER_ID         0x631u 
#define COBID_TSDO            0x5B1

#define READOTHERBATT_ID         0x18FFDE91
#define READ_RESPONSE_ID         0x18FFDF91


#define J1939FILTER_ID           0xFFFFFF00
#define J1939PGNREQUEST_ID       0x18EAF300
#define J1939_RESPONSE_ID        0x18FF00F3 
#define J1939_RESPONSEERR_ID     0x18E800F3 
#define J1939WRITE_ID            0x18FFBD00

#define BATT_DEFAULT_ADDR        0x01u

#define LED_CTR_CMD              0x21u
#define CHGFET_CTR_CMD           0x47u
#define DSGFET_CTR_CMD           0x48u
#define ORFET_CTR_CMD            0x49u
#define BROADCAST_CTR_CMD        0x50u
#define HEATER_CTR_CMD           0x51u
#define FAN_CTR_CMD              0x52u
#define SMBUS_CTR_CMD            0x53u
#define SLEEP_CTR_CMD            0x54u



#define SDO_DOWNLOAD_FUNCCODE           0x600u
#define SDO_UPLOAD_FUNCCODE             0x580u
#define NMT_COB_ID                      0x000u
#define HEARTBEAT_FUNCCODE              0x700u
#define TPDO1_FUNCCODE                  0x180u

#define OBJECT_SUCCEED                       0u
#define OBJECT_NOT_EXIST                     1u
#define OBJECT_LENGTH_NOT_MATCH              2u
#define OBJECT_NOT_SUPPORT_WRITE             3u
#define SUB_INDEX_NOT_EXIST                  4u

extern define_8bits g_flagCan;
#define s_flagResetCan0                         g_flagCan.bitByte.bit0
#define s_flagUpdate                            g_flagCan.bitByte.bit1
#define s_flagCanSend                           g_flagCan.bitByte.bit2

extern uint32_t g_Uid;
extern uint32_t g_bootloaderVerson;

extern bool g_flagAddrAllocateFail;
extern bool g_flagCompleteAddrAllocate;

extern uint8_t g_requestAllocateTimes;
extern bool g_flagRequestAddrAllocate;

extern uint8_t Can0_tx_message[8];
extern uint32_t Can0_tx_messageID;
extern uint8_t Can0_tx_messageLength;

extern uint8_t Can0_rx_message[8];
extern uint32_t Can0_rx_messageID;
extern uint8_t Can0_rx_messageLength;

extern uint8_t Can1_rx_message[8];
extern uint32_t Can1_rx_messageID;    
extern uint8_t Can1_rx_messageLength;

void CanTransmit (void);
void Can_ConfigSet(void);
void ProcessCanReceiveData(void);
void resetUidArray(void);
void can0HoldFunc(void);

#endif	/* CAN_H */

