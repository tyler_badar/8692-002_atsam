/**
 * @file  ModBusV01.h
 *
 * REVISION HISTORY
 *==============================================
 * |Ticket   |Date      |Author       |Notes
 * |----:    |:----:    |:----:       |:----
 * |Creation |02/04/22  |Tom O'Connor |Document
 *
 * @par   COPYRIGHT NOTICE: (c) 2022 Inventus Power.  All rights reserved.
 */
/******************************************************************/
#ifndef _MODBUSV01_H_
#define _MODBUSV01_H_

/* receive buff length */
#define RX_BUFF_LENGTH       40u
#define TX_BUFF_LENGTH       40u

/* receive status */
#define RECEIVE_STATUS_RESET       0u
#define RECEIVE_STATUS_START       1u
#define RECEIVE_STATUS_STOP        2u

/* modbus start and stop signal */
#define MOD_BUS_STAR			    0x3Au
#define MOD_BUS_STOP_CR             0x0Du
#define MOD_BUS_STOP_LF             0x0Au

/* pass and fail define */
#ifndef PASS
#define PASS      0u
#endif

#ifndef FAIL
#define FAIL      0x55u
#endif

#define RELAY_ON    0xff00u
#define RELAY_OFF   0x0000u

/* error code */
#define INVALID_REGISTER           0x3032

/* G_CALL addr */
#define GENERAL_ADDR               0x00u

#define BIT_REGISTER_DATA_TABLE_LENGTH         64u

/* modbus function flag */
#define g_flagWriteEn                   g_modeBusFunction.bitWord.bit0
#define g_flagReceiveGeneralAddr        g_modeBusFunction.bitWord.bit1
#define g_flagUsart1                    g_modeBusFunction.bitWord.bit2

#define g_flagErrorCode                 g_modeBusFunction.bitWord.bit15
/******************************************************************/
void ResponseMODbusCmd(void);
void ModBus_ConfigSet(void);
void USART1_ISR_RX_Handler(void);
/******************************************************************/
extern uint8_t g_modbusRXbuff[];
extern uint16_t g_timerTx;
/******************************************************************/
#endif
/******************************************************************/
