/**
 * @file chgdsgV01.h 
 *
 * REVISION HISTORY
 *==============================================
 * |Ticket   |Date      |Author       |Notes
 * |----:    |:----:    |:----:       |:----
 * |Creation |02/04/22  |Tom O'Connor |Document
 *
 * @par   COPYRIGHT NOTICE: (c) 2022 Inventus Power.  All rights reserved.
 */
/* Inventus Power Proprietary */
/* 
 * File:   dischargeV01.h
 * Author: admin
 *
 * Created on September 12, 2018, 4:28 PM
 */

#ifndef CHGDSGV01_H
#define	CHGDSGV01_H

#define MAX_PARALLEL_COUNT      10u

#define STANDBY_MODE            0u
#define PRE_DISCHARGE_MODE      1u
#define DISCHARGE_MODE          2u
#define PRE_DISCHARGE2_MODE     3u
#define PRE_CHARGE_MODE         4u
#define FAST_CHARGE_MODE        5u
#define TERMINATION_MODE        6u
#define FAULT_MODE              7u
/*******************************************************************************/        
#define g_flagCHT                       g_flagPicChgProtect.bitWord.bit0
#define g_flagCLT                       g_flagPicChgProtect.bitWord.bit1
#define g_flagOCP                       g_flagPicChgProtect.bitWord.bit2
#define g_flagOVP                       g_flagPicChgProtect.bitWord.bit3

#define g_flagChgShortCircuit           g_flagPicChgProtect.bitWord.bit4
#define g_flagChgOtherProtect           g_flagPicChgProtect.bitWord.bit5
#define g_flagCHT_MOS                   g_flagPicChgProtect.bitWord.bit6
#define g_flagChgSUVP                   g_flagPicChgProtect.bitWord.bit7

#define g_flagChgAFEComFail             g_flagPicChgProtect.bitWord.bit8
#define g_flagPreChargeOvf              g_flagPicChgProtect.bitWord.bit11

#define g_flagChgParallelErr            g_flagPicChgProtect.bitWord.bit12
#define g_flagChgSignalErr              g_flagPicChgProtect.bitWord.bit13
#define g_flagChgBulkFail               g_flagPicChgProtect.bitWord.bit14
#define g_flagChgComFail                g_flagPicChgProtect.bitWord.bit15
/*******************************************************************************/
#define g_flagDHT                       g_flagPicDsgProtect.bitWord.bit0
#define g_flagDLT                       g_flagPicDsgProtect.bitWord.bit1
#define g_flagOCD                       g_flagPicDsgProtect.bitWord.bit2
#define g_flagCUV                       g_flagPicDsgProtect.bitWord.bit3

#define g_flagDsgShortCircuit           g_flagPicDsgProtect.bitWord.bit4
#define g_flagDsgOtherProtect           g_flagPicDsgProtect.bitWord.bit5
#define g_flagDHT_MOS                   g_flagPicDsgProtect.bitWord.bit6
#define g_flagDsgSUVP                   g_flagPicDsgProtect.bitWord.bit7

#define g_flagDsgAFEComFail             g_flagPicDsgProtect.bitWord.bit8

#define g_flagDsgParallelErr            g_flagPicDsgProtect.bitWord.bit12           
#define g_flagDsgSignalErr              g_flagPicDsgProtect.bitWord.bit13
#define g_flagDsgBulkFail               g_flagPicDsgProtect.bitWord.bit14
#define g_flagDsgComFail                g_flagPicDsgProtect.bitWord.bit15
/*******************************************************************************/
#define g_flagORFETEnCH            	g_FETEnCH.bitByte.bit0
#define g_flagDSGFETEnCH           	g_FETEnCH.bitByte.bit1
#define g_flagCHGFETEnCH           	g_FETEnCH.bitByte.bit2
#define g_flagPreChgFetEn           g_FETEnCH.bitByte.bit3      
#define g_flagByPassFETEn           g_FETEnCH.bitByte.bit4 
#define g_flagForcePreChgEn         g_FETEnCH.bitByte.bit5

/*******************************************************************************/
#define g_flagORFETEnDisCH          g_FETEnDisCH.bitByte.bit0
#define g_flagDSGFETEnDisCH         g_FETEnDisCH.bitByte.bit1
#define g_flagCHGFETEnDisCH         g_FETEnDisCH.bitByte.bit2
/*******************************************************************************/
/*define_8bits g_FETstate*/
#define g_flagORFET             	g_FETstate.bitByte.bit0
#define g_flagDSGFET             	g_FETstate.bitByte.bit1
#define g_flagCHGFET            	g_FETstate.bitByte.bit2
#define g_flagPreChgFET             g_FETstate.bitByte.bit3 

#define g_flagByPassFET             g_FETstate.bitByte.bit4 
#define g_flagPreDSGFET             g_FETstate.bitByte.bit5 
#define g_flagDSGPROTECT            g_FETstate.bitByte.bit6
#define g_flagCHGPROTECT            g_FETstate.bitByte.bit7

extern 	define_8bits g_FETEnState;
#define s_flagChgFetEn             	g_FETEnState.bitByte.bit0
#define s_flagDsgFetEn             	g_FETEnState.bitByte.bit1
#define s_flagOrFetEn            	g_FETEnState.bitByte.bit2
#define s_flagBypassFetEn           g_FETEnState.bitByte.bit3 


/*******************************************************************************/

/*define_8bits g_bqFetState/g_IDbqFetState*/
#define s_flagbqCHGFet              g_bqFetState.bitByte.bit6
#define s_flagbqDSGFet              g_bqFetState.bitByte.bit7 

/*define_8bits g_systemError*/
#define g_flagPositionError         g_systemError.bitByte.bit0



extern define_8bits g_IDFETEnCH[MAX_PARALLEL_COUNT];
extern define_8bits g_IDFETEnDisCH[MAX_PARALLEL_COUNT];
extern define_8bits g_IDFETstate[MAX_PARALLEL_COUNT];
extern uint16_t g_IDVolt[MAX_PARALLEL_COUNT];
extern int16_t g_IDCurr[MAX_PARALLEL_COUNT];
extern uint8_t g_IDChargeMode[MAX_PARALLEL_COUNT];
extern uint8_t g_IDSOC[MAX_PARALLEL_COUNT];
extern uint16_t g_minVolt;
extern uint16_t g_maxVolt;
extern uint16_t g_chargeVolt;

extern define_8bits g_FETstate;
extern define_8bits g_FETEnCH;
extern define_8bits g_FETEnDisCH;
extern uint8_t g_chargeMode;
extern define_16bits g_flagPicChgProtect;
extern define_16bits g_flagPicDsgProtect;
extern define_16bits g_chargerFaultFlg;
extern bool g_flagdisPreDsgFet;

extern uint8_t g_IDParallelUpdateArray[MAX_PARALLEL_COUNT];
extern uint8_t g_picParallelUpdateVaule;
extern uint16_t g_IDchgCurrLimit[MAX_PARALLEL_COUNT];
extern uint16_t g_IDdsgCurrLimit[MAX_PARALLEL_COUNT];
extern uint16_t g_IDregenCurrLimit[MAX_PARALLEL_COUNT];
extern uint16_t g_chgCurrLimit;
extern uint16_t g_dsgCurrLimit;
extern uint16_t g_regenCurrLimit;
extern uint16_t g_broadcastChgCurrLimit;
extern uint16_t g_broadcastDsgCurrLimit;
extern uint16_t g_broadcastRegenCurrLimit;
extern uint8_t g_IDbqFetState[MAX_PARALLEL_COUNT];
extern uint8_t g_IDCellS[MAX_PARALLEL_COUNT];
extern define_8bits g_bqFetState;
extern uint8_t g_IDPackPosition[MAX_PARALLEL_COUNT];
extern define_8bits g_systemError;
extern uint8_t g_IDSystemError[MAX_PARALLEL_COUNT];
extern uint16_t g_chargeCurr;
extern uint16_t g_maxDsgS1Volt;
extern uint16_t g_maxDsgS2Volt;
extern uint8_t g_IDCHGProtect[MAX_PARALLEL_COUNT] ;
extern uint8_t g_balanceState;

void MasterControlFunction(void);
void FetControlFunction(void);
void Protection (void);
void ChargeModeFunction(void);
void EIC_Interrupt(uint8_t s_Channel);


#endif	/* DISCHARGEV01_H */

/* Inventus Power Proprietary */
