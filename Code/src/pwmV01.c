/**
 * @file  pwmV01.c
 *
 * Main ATSAM program
 *
 * REVISION HISTORY
 *==============================================
 * |Ticket   |Date      |Author       |Notes
 * |----:    |:----:    |:----:       |:----
 * |Creation |02/04/22  |Tom O'Connor |Document
 *
 * @par   COPYRIGHT NOTICE: (c) 2022 Inventus Power.  All rights reserved.
 */
/* Inventus Power Proprietary */
#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes
#include "common_defineV01.h"
#include "adcV01.h"
#include "pwmV01.h"

static void IncreasePWM(uint16_t diff);
static void DecreasePWM(uint16_t diff);

uint16_t g_pwmDuty;
uint8_t g_flagPWMOn;

/**
****************************************************************************
* @brief LoadPWMDuty
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void LoadPWMDuty(void)
{ 
    if (g_pwmDuty >= MAX_PWM_DUTY)
    {
        g_pwmDuty = MAX_PWM_DUTY;     
    }
    if (g_pwmDuty < MIN_PWM_DUTY)
    {
        g_pwmDuty = MIN_PWM_DUTY;
    }
    TCC1_REGS->TCC_CC[0] = g_pwmDuty;
}

/**
****************************************************************************
* @brief PWMControl
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void PWMControl(void)
{ 
    if (g_flagPWMOn == 1u)
    { 
        if (g_adcPreCHGCurr >= PRECHGCURRENT_VALUE)
        {
            g_pwmDuty++;
        }
        else
        {
            g_pwmDuty--;
        }       
    }
    else
    {
        g_pwmDuty = MAX_PWM_DUTY;
    }
    LoadPWMDuty();
}

/* Inventus Power Proprietary */
