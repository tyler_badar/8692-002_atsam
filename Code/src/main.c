/**  
 * @file  main.c 
 * @brief   This file contains the "main" function for a project.
 *  
 * Description:
 *   This file contains the "main" function for a project.  The
 *   "main" function calls the "SYS_Initialize" function to initialize the state
 *   machines of all modules in the system
 *
 * REVISION HISTORY 
 *==============================================
 * |Ticket   |Date      |Author       |Notes
 * |----:    |:----:    |:----:       |:----
 * |Creation |02/04/22  |Tom O'Connor |Document
 * |Modified |02/14/22  |Tyler Badar  |Added function descriptions
 * 
 * @par   COPYRIGHT NOTICE: (c) 2022 Inventus Power.  All rights reserved. 
 */ 

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************
#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes
#include "common_defineV01.h"
#include "ModBusV01.h"
#include "mainV01.h"
#include "ledV01.h" 
#include "rs232V01.h"
#include "i2cMasterV02.h"
#include "canV01.h"
#include "adcV01.h"
#include "temperatureV01.h"
#include "chgdsgV01.h"
#include "detectV01.h"
#include "pwmV01.h"
#include "CANopenV01.h"
#include "flashV01.h"
#include "plib_port.h"


/**
  * Flag to indicate 5ms interrupt triggered
  */
volatile bool s_flag5ms; 


static uint8_t s_timer20ms;           /** Global 20ms timer */
static uint8_t s_timer1s;             /** Global 1s timer */
define_16bits g_flagDsgStatus;        /** Global flag DSG status*/
define_16bits g_testCommandStatus;    /** Global test Command Status*/
define_16bits g_PerpheralStatus;      /** Global Perheral Status*/
define_16bits g_PICStatus;            /** Global PIC Status */
define_16bits g_DeltaQStatus;         /** Global Delta Q status*/
define_16bits g_J1939Status;          /** Global J1939 Status */
define_16bits g_DeltaQFail;           /** Global Delta Q fail */
define_8bits g_flagSystem;            /** Global System flag*/
define_8bits g_flagSignal;            /** Global Signal flag */


uint8_t g_resetCause = 0u;            /** Global 20ms timer */

void Init_Ram(void);
void sleepFunction(void);

/**
****************************************************************************
* @brief TIME5MS_Ovf
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void TIME5MS_Ovf(void) 
{
    s_flag5ms = true;
}

/**
****************************************************************************
* @brief read_flag5ms
*
* Wrapper function to encapsulate global variables for doxygen issue
*
* @param NONE
* @return int
*****************************************************************************
*/
int read_flag5ms(){
   return s_read_flag5ms;
}

/**
****************************************************************************
* @brief read_timer20ms
*
* Wrapper function to encapsulate global variables for doxygen issue
*
* @param NONE
* @return int
*****************************************************************************
*/
int read_timer20ms(){
   return s_read_timer20ms;
}

/**
****************************************************************************
* @brief read_timer1s
*
* Wrapper function to encapsulate global variables for doxygen issue
*
* @param NONE
* @return int
*****************************************************************************
*/
int read_timer1s(){
   return s_read_timer1s;
}


/**
****************************************************************************
* @brief Main ATSAM Loop
* 
* Description:
*
*   This method is the main program loop for the ATSAM.
*
*   High level:
*       - initializes communications, WDT, system timer, PWM, and RAM
*       - handles communications, propagating faults, controlling LEDs & FETs
*
* @param NONE
* @return int
*****************************************************************************
*/
int main ( void )
{
    int i;
    
    int flag5ms   = read_flag5ms();
    int timer20ms = read_timer20ms(); 
    int timer1s   = read_timer1s();

    SYS_Initialize ( NULL );
    WDT_Enable();
    SYSTICK_TimerStart();
    Can_ConfigSet();
    TCC1_PWMStart();
    ModBus_ConfigSet();
    Init_Ram();
    LoadLssDataFunction();
    
    while ( true )
    {
        
        SYS_Tasks ( );
        ProcessCanReceiveData();
        ResponseMODbusCmd();

        if (flag5ms)
        {  
            flag5ms = false;
            timer20ms++;
            timer1s++;

            heartBeatProcess();
            can0HoldFunc();
            
            SampleFunction();
            MasterControlFunction();
            FetControlFunction();
            ChargeModeFunction();
            DetectFunction();
            Protection();
            LedControl();
            CanTransmit();
            PWMControl();
        }
        if (timer20ms >= TIMER5MS_20MS)
        {
            timer20ms = 0u;
            
            CLRWDT;
            Communication();
            TemeratrueDetect();
            
        }
        if (timer1s >= TIMER5MS_1S)
        {
            timer1s = 0u;
            TxOperation();
            sleepFunction();
        }
    }
    
    return ( EXIT_FAILURE );
}

/**
****************************************************************************
* @brief Init_Ram
*
* Initializes RAM:
*   - reads Uid and bootloaderVersion from flash
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void Init_Ram(void)
{    
    NVMCTRL_RegionUnlock(UID_ADDR);
    NVMCTRL_Read(&g_Uid,4,UID_ADDR);
    NVMCTRL_RegionLock(UID_ADDR);
    
    NVMCTRL_RegionUnlock(BLF_VERSION_ADDR);
    NVMCTRL_Read(&g_bootloaderVerson,4,BLF_VERSION_ADDR);
    NVMCTRL_RegionLock(BLF_VERSION_ADDR);
    
    g_resetCause = RSTC_ResetCauseGet();
}

/**
****************************************************************************
* @brief sleepFunction
*
* Description:
*   - puts pack in sleep mode 
*   - sleep mode loop resets PWM, LEDs, ADCs, WDT, and communications
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void sleepFunction(void)
#if 1
{
    static uint8_t s_enableSleepTime = 0u;
    
    if ((g_testSleep == 1u) && (g_flagUpKey == 1u) && 
            (g_chgDsgControl == NO_CHG_DSG) && (g_flagdisPreDsgFet == 0u))
    {
        g_flagSleep = 1u;
    }
    if ((g_flagUpKey == 1u) && (g_chgDsgControl == NO_CHG_DSG) && 
            (g_testCommandStatus.word == 0u) && (g_flagdisPreDsgFet == 0u))
    {
        if (++s_enableSleepTime >= TIMER1S_10S)
        {
            g_flagSleep = 1u;
            s_enableSleepTime = 0u;
        }
    }
    else
    {
        s_enableSleepTime = 0u;
    }
    
    while (g_flagSleep == 1u)
    {
        ISO_VCC_CTR_Clear();
        TCC1_PWMStop();
        allLed_displayOff();
        disableADCSample();
        WDT_Disable();
        PM_StandbyModeEnter();
        WDT_Enable();
        TCC1_PWMStart();
        CAN0_Initialize();
        CAN1_Initialize();
        Can_ConfigSet();
        ModBus_ConfigSet();
    }
}
#else
//{
//    static uint8_t s_enableSleepTime = 0u;
//    if ((g_testSleep == 1u) && (g_flagUpKey == 1u) && 
//            (g_chgDsgControl == NO_CHG_DSG) && (g_flagdisPreDsgFet == 0u))
//    {
//        g_flagSleep = 1u;
//    }
//    if ((g_flagUpKey == 1u) && (g_chgDsgControl == NO_CHG_DSG) && 
//            (g_testCommandStatus.word == 0u) && (g_flagdisPreDsgFet == 0u))
//    {
//        if (++s_enableSleepTime >= TIMER1S_10S)
//        {
//            g_flagSleep = 1u;
//            s_enableSleepTime = 0u;
//        }
//    }
//    else
//    {
//        s_enableSleepTime = 0u;
//    }
//    
//    while (g_flagSleep == 1u)
//    {      
//        CAN0_Disable();
//        SERCOM1_USART_Disable();
//        gpio_set_pin_function(GPIO(GPIO_PORTB,22),GPIO_PIN_FUNCTION_OFF);
//        gpio_set_pin_function(GPIO(GPIO_PORTB,23),GPIO_PIN_FUNCTION_OFF);
//        gpio_set_pin_function(GPIO(GPIO_PORTA,16),GPIO_PIN_FUNCTION_OFF);
//        gpio_set_pin_function(GPIO(GPIO_PORTA,17),GPIO_PIN_FUNCTION_OFF);
//        
//        ISO_VCC_CTR_Clear();
//        allLed_displayOff();
//        disableADCSample();
//        PM_StandbyModeEnter();
//        CLRWDT;
//        
//        if (g_flagSleep == 0u)
//        {
//            gpio_set_pin_function(GPIO(GPIO_PORTB,22),PINMUX_PB22G_CAN0_TX);
//            gpio_set_pin_function(GPIO(GPIO_PORTB,23),PINMUX_PB23G_CAN0_RX);
//            gpio_set_pin_function(GPIO(GPIO_PORTA,16),PINMUX_PA16C_SERCOM1_PAD0);
//            gpio_set_pin_function(GPIO(GPIO_PORTA,17),PINMUX_PA17C_SERCOM1_PAD1);
//            SERCOM1_USART_Initialize();
//            CAN0_Initialize();
//            Can_ConfigSet();
//            ModBus_ConfigSet();
//        }
//    }
//}
//
#endif
/*
 End of File
*/

