/**
 * @file  CANopenV01_handler.c
 *
 * REVISION HISTORY
 *==============================================
 * |Ticket   |Date      |Author       |Notes
 * |----:    |:----:    |:----:       |:----
 * |Creation |02/15/22  |broberto     |Moved handlers from CANopenV01.c
 *
 * @par   COPYRIGHT NOTICE: (c) 2022 Inventus Power.  All rights reserved.
 */
/* Inventus Power Proprietary */

#if !defined (_CANOPENV01_H_)
#define _CANOPENV01_H_

uint8_t handler_g_flagCanErr (int action, uint8_t value );
uint8_t handler_g_canMasterCmd (int action, uint8_t value );
uint8_t handler_g_canMasterFuncC (int action, uint8_t value );
uint8_t handler_g_nodeState (int action, uint8_t value );
uint32_t handler_g_cobIDEMCY (int action, uint32_t value );
uint16_t handler_g_inhibitTimeEMCY (int action, uint16_t value );
uint32_t handler_g_consumerHeartbeatTime (int action, uint32_t value );
uint16_t handler_g_producerHeartbeatTime (int action, uint16_t value );
uint8_t handler_g_identityObjectSUB (int action, uint8_t value );
uint32_t handler_g_vendorID (int action, uint32_t value );
uint32_t handler_g_productCode (int action, uint32_t value );
uint32_t handler_g_revisionNumber (int action, uint32_t value );
uint32_t handler_g_serialNumber (int action, uint32_t value );
uint8_t handler_g_sdoServer1SUB (int action, uint8_t value );
uint32_t handler_g_cobIDrx (int action, uint32_t value );
uint32_t handler_g_cobIDtx (int action, uint32_t value );
uint32_t handler_g_cobIDtx (int action, uint32_t value );
uint8_t handler_g_nodeIDofSDO (int action, uint8_t value );
uint16_t handler_g_canErrTimes (int action, uint16_t value );
uint16_t handler_g_canOvfTimes (int action, uint16_t value );
uint8_t handler_g_pendNodeID (int action, uint8_t value );
uint8_t handler_g_fixNodeID (int action, uint8_t value );
uint8_t handler_g_pendBitRate (int action, uint8_t value );
uint8_t handler_g_fixBitRate (int action, uint8_t value );
uint8_t handler_g_lssState (int action, uint8_t value );
uint8_t handler_g_lssSwitchIndx (int action, uint8_t value );
uint16_t handler_g_lssUpdateRateTime (int action, uint16_t value );
uint32_t handler_g_lssVendorID (int action, uint32_t value );
uint32_t handler_g_lsspPoductCode (int action,  uint32_t value );
uint32_t handler_g_lssRevisionNumber (int action, uint32_t value );
uint32_t handler_g_lssSerialNumber (int action, uint32_t value);
uint8_t handler_g_canBattStatus (int action , uint8_t value );
uint8_t handler_g_chargerStatus (int action , uint8_t value ) ;
uint16_t handler_g_temperature (int action , uint16_t value ) ;
uint8_t handler_g_battParmetersSUB (int action , uint8_t value );
uint8_t handler_g_batttype (int action , uint8_t value );
uint16_t handler_g_batttCap (int action , uint16_t value );
uint16_t handler_g_maxCHGcurr (int action , uint16_t value );
uint16_t handler_g_cellNumber (int action , uint16_t value );
uint8_t handler_g_battSNSUB (int action , uint8_t value );
uint32_t handler_g_battSNLow (int action , uint32_t value );
uint32_t handler_g_battSNHigh (int action , uint32_t value );
uint32_t handler_g_cumulativeTotalCharge (int action , uint32_t value );
uint32_t handler_g_expendSinceLastCharge (int action , uint32_t value );
uint32_t handler_g_returnedDuringLastCharge (int action , uint32_t value );
uint32_t handler_g_battVoltage (int action , uint32_t value );
uint32_t handler_g_chargeCurrPequested (int action , uint32_t value );
uint32_t handler_g_canBattSOC (int action , uint32_t value );
uint32_t handler_g_freeData (int action , uint32_t value );
uint8_t handler_g_testCommandSub  (int action, uint8_t value);
uint8_t handler_g_canAbortCode (int action, uint8_t value);
uint8_t handler_g_tpdoParameterSub (int action, uint8_t value);
uint8_t handler_g_tpdoParmeterType (int action, uint8_t value);
uint16_t handler_g_tpdoParameterTime (int action, uint16_t value);
uint32_t handler_g_tpdoParameterNodeId1 (int action, uint32_t value);
uint32_t handler_g_tpdoParameterNodeId2 (int action, uint32_t value);
uint32_t handler_g_tpdoParameterNodeId3 (int action, uint32_t value);
uint32_t handler_g_tpdoParameterNodeId4 (int action, uint32_t value);
uint16_t handler_g_heaterState(int action, uint16_t value);
uint16_t handler_g_heaterControl(int action, uint16_t value);
uint8_t handler_g_flagLssIdentifyMatch(int action, uint8_t value);
uint8_t handler_g_consumerHeartbeatTimeSUB(int action, uint8_t value);
uint8_t handler_g_lssIdentifyIndx(int action, uint8_t value);
uint8_t handler_g_flagLssIdentifyMatch(int action, uint8_t value);

#endif