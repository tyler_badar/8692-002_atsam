/**
 * @file canV01.c
 *  
 * Description:
 *   This file contains the CAN libraries.
 *
 *
 * REVISION HISTORY
 *==============================================
 * |Ticket   |Date      |Author       |Notes
 * |----:    |:----:    |:----:       |:----
 * |Creation |02/04/22  |Tom O'Connor |Document
 * |Modified |02/16/22  |Tyler Badar  |Added function descriptions
 *
 * @par   COPYRIGHT NOTICE: (c) 2022 Inventus Power.  All rights reserved.
 */

/* Inventus Power Proprietary */

#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes
#include "common_defineV01.h"
#include "mainV01.h"
#include "i2cMasterV02.h"
#include "J1939V01.h"
#include "detectV01.h"
#include "chgdsgV01.h"
#include "canV01.h"
#include "CANopenV01.h"
#include "arithmeticV01.h"

uint8_t Can0MessageRAM[CAN0_MESSAGE_RAM_CONFIG_SIZE] __attribute__((aligned (32)));
uint8_t Can1MessageRAM[CAN1_MESSAGE_RAM_CONFIG_SIZE] __attribute__((aligned (32)));

uint8_t Can0_tx_message[8];
uint32_t Can0_tx_messageID = 0;
uint8_t Can0_tx_messageLength = 8;

uint8_t Can0_rx_message[8];
uint32_t Can0_rx_messageID = 0;
uint8_t Can0_rx_messageLength = 8;

uint8_t Can1_rx_message[8];
uint32_t Can1_rx_messageID = 0;
uint8_t Can1_rx_messageLength = 8;

typedef union
{
    uint8_t uid[4];  
    uint32_t uidL;
} define_long;
define_long s_tempUid;
uint32_t g_Uid;
uint32_t g_bootloaderVerson;

define_8bits g_flagCan;
uint16_t s_can0HoldTime;
uint16_t g_batcanHoldTime;

uint8_t g_canAddr = BATT_DEFAULT_ADDR;
uint8_t g_note_ID = 0xffu;
uint8_t g_BitRate;
uint32_t s_uidArray[MAX_PARALLEL_COUNT] = {0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u};

uint8_t g_requestAllocateTimes = 0u;
uint16_t s_AddrAllocateTime = 0u;
uint8_t g_parallelQuantity = 1u;
static uint16_t s_testTime = 0;

bool g_flagRequestAddrAllocate = 1u;
bool s_flagSendOneUid = 0u;
bool g_flagParallel = 0u;
bool s_flagResetUidArray = 0u;
bool g_flagAddrAllocateFail = 0u;
bool g_flagCompleteAddrAllocate = 0u;
bool g_flagLoadCanSettingSucceed = 0u;


uint16_t s_canFree = 0x00u;
CAN_MSG_RX_FRAME_ATTRIBUTE MsgFrame_Attr;
uint16_t *TimeStamp;
uint8_t g_flagTest=255;


void ResponseBqCmd();
void ResponsePicCmd();
void RequestIdAllocation(void);
void SendOutUid(void);
void ResponseAddrAllocation(void);
void StartAddrAllocation(void);
void RequestOtherBatteryInfo(void);
void ReceiveMasterCanCMD(void);
void ReveiveOtherBattInfo(void);
void intoBootloader(void);

/**
****************************************************************************
* @brief Can_ConfigSet
*
* configure CAN message arrays
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void Can_ConfigSet(void)
{
    
    ISO_VCC_CTR_OutputEnable();
    ISO_VCC_CTR_Set();
    CAN0_MessageRAMConfigSet(Can0MessageRAM);
    CAN1_MessageRAMConfigSet(Can1MessageRAM);
}

/**
****************************************************************************
* @brief CanTransmit
*
* configure node IDs, autobroadcast battery data, request data from 
* parallel packs, and read BQ8050 chg/dsg flags
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void CanTransmit (void)
{   
    ISO_VCC_CTR_OutputEnable();
    ISO_VCC_CTR_Set();
    RequestIdAllocation();
    SendOutUid();
    StartAddrAllocation();
    
    
    
    AutoBroadcast();
    
    
    
    RequestOtherBatteryInfo();
    
    
    
    if (g_testCommandStatus.word != 0u)
    {
        if (++s_testTime >= TIMER5MS_5S)
        {
            g_testCommandStatus.word = 0u;
        }
    }
    else
    {
        s_testTime = 0u;
    }

    if (g_flag8050CHGOn == 0u)
    {
        s_flagbqCHGFet = 0u;
    }
    else
    {
        s_flagbqCHGFet = 1u;
    }
    if (g_flag8050DSGOn == 0u)
    {
        s_flagbqDSGFet = 0u;
    }
    else
    {
        s_flagbqDSGFet = 1u;
    }
}

/**
****************************************************************************
* @brief GetDelayTime
*
* put together delay array and perform crc 
*
* @param NONE
* @return uint8_t
*****************************************************************************
*/
uint8_t GetDelayTime(void)
{
    uint8_t i;
    uint8_t timerBuffer[8];
    uint16_t delayTime;
    
    timerBuffer[0] = (uint8_t)SYSTICK_TimerCounterGet();
    timerBuffer[1] = (uint8_t)(SYSTICK_TimerCounterGet() >> 8u);
    timerBuffer[2] = (uint8_t)(SYSTICK_TimerCounterGet() >> 16u);
    timerBuffer[3] = (uint8_t)(SYSTICK_TimerCounterGet() >> 24u);
    timerBuffer[4] = s_tempUid.uid[0];
    timerBuffer[5] = s_tempUid.uid[1];
    timerBuffer[6] = s_tempUid.uid[2];
    timerBuffer[7] = s_tempUid.uid[3];
    
    delayTime = 0u;
    for (i = 0u; i < 8u; i++)
    {
        delayTime = basic_crc(delayTime, timerBuffer[i]);
    }
    return (delayTime);
}


/**
****************************************************************************
* @brief GetUidCrc8
*
* crc uid array part of delay array and return
*
* @param NONE
* @return uint8_t
*****************************************************************************
*/
uint8_t GetUidCrc8(void)
{
    uint8_t tempValue;
    
    tempValue = 0u;
    tempValue = basic_crc(tempValue, s_tempUid.uid[0]);
    tempValue = basic_crc(tempValue, s_tempUid.uid[1]);
    tempValue = basic_crc(tempValue, s_tempUid.uid[2]);
    tempValue = basic_crc(tempValue, s_tempUid.uid[3]);
    
    return (tempValue);
}

/**
****************************************************************************
* @brief GetSysTemClkCrc8
*
* crc timer part of delay array and return
*
* @param NONE
* @return uint8_t
*****************************************************************************
*/
uint8_t GetSysTemClkCrc8(void)
{
    uint8_t tempValue;
    
    tempValue = 0u;
    tempValue = basic_crc(tempValue, (uint8_t)SYSTICK_TimerCounterGet());
    tempValue = basic_crc(tempValue, (uint8_t)(SYSTICK_TimerCounterGet() >> 8u));
    tempValue = basic_crc(tempValue, (uint8_t)(SYSTICK_TimerCounterGet() >> 16u));
    tempValue = basic_crc(tempValue, (uint8_t)(SYSTICK_TimerCounterGet() >> 24u));
    
    return (tempValue);
}

/**
****************************************************************************
* @brief RequestIdAllocation
*
* put together ID allocation CAN message using crc versions of uid and 
* timer arrays and send
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void RequestIdAllocation(void)
{    
    static uint8_t delayTime1 = 0u;
    static uint8_t flagWaitDelay1 = 0u;
    static uint8_t s_waitTime1 = 0u;
    
    if (g_requestAllocateTimes >= 6u)
    {
        g_requestAllocateTimes = 6u;
        g_flagRequestAddrAllocate = 0u;
        g_flagCompleteIdRequest = 1u;
    }
    else
    {
        if ((flagWaitDelay1 == 0u) && (s_flagSendOneUid == 0u))
        {
            flagWaitDelay1 = 1u;
            s_waitTime1 = 0u;
            s_tempUid.uidL = g_Uid;
            delayTime1 = GetDelayTime();
        }
    }
    if ((g_flagRequestAddrAllocate == 1u) && (++s_waitTime1 >= delayTime1) && (flagWaitDelay1 == 1u))
    {
        s_waitTime1 = 0u;
        flagWaitDelay1 = 0u;
        
        Can0_tx_message[0] = GetUidCrc8();
        Can0_tx_message[1] = GetSysTemClkCrc8();
        Can0_tx_message[2] = 0x55u;
        Can0_tx_message[3] = 0xeeu;
        Can0_tx_message[4] = 0x00;
        Can0_tx_message[5] = 0x00;
        Can0_tx_message[6] = 0x00;
        Can0_tx_message[7] = 0x00;
        if (CAN0_MessageTransmit(ADDRALLOCATION_ID,0x4u,Can0_tx_message,CAN_MODE_NORMAL, CAN_MSG_ATTR_TX_FIFO_DATA_FRAME) == true)
        {
            g_requestAllocateTimes++;
            s_flagSendOneUid = 1u;
        }
    }   
}

/**
****************************************************************************
* @brief StartAddrAllocation
*
* set node ID for this pack taking other packs into account
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void StartAddrAllocation(void)
{
    uint8_t i;
    uint8_t j;
    uint32_t tmp32;
    uint32_t avgValue;
    uint8_t avgPosition;
    uint32_t finalBuffer[MAX_PARALLEL_COUNT];
    
    if ((++s_AddrAllocateTime >= TIMER5MS_2S) && (g_flagCompleteAddrAllocate == 0u))
    {
        s_AddrAllocateTime = 0u;
        s_uidArray[0] = g_Uid;
        for (i = 1u; i < MAX_PARALLEL_COUNT; i++)
        {
            g_IDFETEnCH[i].byte = 0u;
            g_IDFETEnDisCH[i].byte = 0u;
            g_IDFETstate[i].byte = 0u;
            g_IDVolt[i] = 0u;
            g_IDChargeMode[i] = 0u;
            g_IDCurr[i] = 0u;  
            g_IDCellS[i] = 0u;
            g_IDbqFetState[i] = 0u;
        } 
        
        avgValue = 0u;
        for (i = 0u; i < g_parallelQuantity; i++)
        {
            avgValue += s_uidArray[i];
        }
        avgValue = avgValue / g_parallelQuantity;
        for (i = 0u; i < g_parallelQuantity; i++)
        {
            for (j = 0u; (j + 1) < g_parallelQuantity; j++)
            {
                if (s_uidArray[j] >= s_uidArray[j + 1u])
                {
                    tmp32 = s_uidArray[j];
                    s_uidArray[j] = s_uidArray[j + 1u];
                    s_uidArray[j + 1u] = tmp32;
                }
            }
        }
        for (i = 0u; i < g_parallelQuantity; i++)  
        {
            if (s_uidArray[i] >= avgValue)
            {
                break;
            }
        }
        avgPosition = i;
        if (avgPosition == 0u)   
        {
            for (i = 0; i < g_parallelQuantity; i++)
            {
                finalBuffer[i] = s_uidArray[i];   
            }
        }
        else
        {
            j = 0u;
            for (i = avgPosition; i < g_parallelQuantity; i++)  
            {
                finalBuffer[i - avgPosition] = s_uidArray[i];
                j++;
            }
            for (i = 0u; (i + j) < g_parallelQuantity; i++)
            {
                finalBuffer[j + i] = s_uidArray[avgPosition - 1u - i];
            }
        }
        
        for (i = 0u; i < g_parallelQuantity; i++)
        {
            if (g_Uid == s_uidArray[i])
            {
                g_canAddr = i + 1u;
            }
		    if (g_flagLoadCanSettingSucceed == 0u)
	        {
	            g_fixNodeID = 0x30u + g_canAddr;
	        }
        }
        g_flagCompleteAddrAllocate = 1u;
        
        
    }
}

/**
****************************************************************************
* @brief SendOutUid
*
* send uid
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void SendOutUid(void)
{    
    static uint8_t delayTime2 = 0u;
    static uint8_t flagWaitDelay2 = 0u;
    static uint8_t s_waitTime2 = 0u;
    
    if ((s_flagSendOneUid == 1u) && (flagWaitDelay2 == 0u))
    {
        flagWaitDelay2 = 1u;
        s_waitTime2 = 0u;
        s_tempUid.uidL = g_Uid;
        delayTime2 = GetDelayTime();
    }
    
    if ((s_flagSendOneUid == 1u) && (++s_waitTime2 >= delayTime2) && (flagWaitDelay2 == 1))
    {
        s_waitTime2 = 0u;
        flagWaitDelay2 = 0u;
        
        Can0_tx_message[0] = GetUidCrc8();
        Can0_tx_message[1] = GetSysTemClkCrc8();
        Can0_tx_message[2] = 0x55u;
        Can0_tx_message[3] = 0xffu;
        Can0_tx_message[4] = s_tempUid.uid[0];
        Can0_tx_message[5] = s_tempUid.uid[1];
        Can0_tx_message[6] = s_tempUid.uid[2];
        Can0_tx_message[7] = s_tempUid.uid[3];
        if (CAN0_MessageTransmit(ADDRALLOCATION_ID,0x8u,Can0_tx_message,CAN_MODE_NORMAL, CAN_MSG_ATTR_TX_FIFO_DATA_FRAME) == true)
        {
            s_flagSendOneUid = 0u;
        }
    }   
}


/**
****************************************************************************
* @brief ProcessCanReceiveData
*
* receive CAN0 messages from FIFO0&1 and respond based on message ID; pass 
* to canOpen libraries if message received in CAN1
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void ProcessCanReceiveData(void)
{
    uint32_t Can0_rx_messageFilterID;

    if (CAN0_MessageReceive(&Can0_rx_messageID, &Can0_rx_messageLength,Can0_rx_message,TimeStamp,CAN_MSG_ATTR_RX_FIFO0,&MsgFrame_Attr) == true)
    {
    	g_batcanHoldTime = 0;
        switch (Can0_rx_messageID)
        {
            case BATT_ID:
            {
                ResponseBqCmd();
                break;
            }
            case PIC_ID:
            {
                ResponsePicCmd();
                break;
            }
            case BOOTLOADER_ID:
            {
                intoBootloader();
                
                break;
            }
        }
        
    }
    
    if (CAN0_MessageReceive(&Can0_rx_messageID, &Can0_rx_messageLength,Can0_rx_message,TimeStamp,CAN_MSG_ATTR_RX_FIFO1,&MsgFrame_Attr) == true)
    {
	    g_batcanHoldTime = 0;
        switch (Can0_rx_messageID)
        {
            case ADDRALLOCATION_ID:
            {
                ResponseAddrAllocation();
                break;
            }
            case READOTHERBATT_ID:
            {
                ReceiveMasterCanCMD();
                break;
            }
            case READ_RESPONSE_ID:
            {
                ReveiveOtherBattInfo();
                break;
            }
            case ENTERBOOTLOADER_ID:
            {
                intoBootloader();
                break;
            }
        }
        Can0_rx_messageFilterID = Can0_rx_messageID & J1939FILTER_ID;
        if (Can0_rx_messageFilterID == J1939PGNREQUEST_ID)
        {
            J1939PGNRequestProcess(Can0_rx_messageID);
        }
    }
    
    if (CAN1_MessageReceive(&Can0_rx_messageID, &Can0_rx_messageLength,Can0_rx_message,TimeStamp,CAN_MSG_ATTR_RX_FIFO0,&MsgFrame_Attr) == true)
    {
        ProcessCanOpenReceiveData();
    }
    
    if (CAN1_MessageReceive(&Can0_rx_messageID, &Can0_rx_messageLength,Can0_rx_message,TimeStamp,CAN_MSG_ATTR_RX_FIFO1,&MsgFrame_Attr) == true)
    {

    }
    
}

/**
****************************************************************************
* @brief ResponseBqCmd
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void ResponseBqCmd(void)
{
    uint8_t bq_funcCode;
    uint8_t bq_CMD;
    uint8_t bq_CmdLength;
    bq_funcCode = Can0_rx_message[0];
    bq_CMD = Can0_rx_message[1]; 
    bq_CmdLength = Can0_rx_message[3]; 
    if ((bq_funcCode == g_canAddr) && (Can0_rx_message[2] == 0x52) && (Can0_rx_message[4] == 0x52) &&
            (Can0_rx_message[5] == 0x65) && (Can0_rx_message[6] == 0x61) && (Can0_rx_message[7] == 0x64))
    {
        Can0_tx_message[0] = bq_funcCode;
        Can0_tx_message[1] = bq_CMD;
        Can0_tx_message[2] = 0x00;
        Can0_tx_message[3] = bq_CmdLength;
        Can0_tx_message[4] = 0x00;
        Can0_tx_message[5] = 0x00;
        Can0_tx_message[6] = 0x00;
        Can0_tx_message[7] = 0x00;
        if (readBattData(bq_CMD,(uint32_t)bq_CmdLength,&Can0_tx_message[4]) == false)
        {  
            Can0_tx_message[2] = 0x01;
            Can0_tx_message[3] = 0x04;
            Can0_tx_message[4] = 0x52;
            Can0_tx_message[5] = 0x65;
            Can0_tx_message[6] = 0x61;
            Can0_tx_message[7] = 0x64;
        }
        CAN0_MessageTransmit(BATT_RESPONSE_ID,0x8u,Can0_tx_message,CAN_MODE_NORMAL, CAN_MSG_ATTR_TX_FIFO_DATA_FRAME);
    }
    else if ((bq_funcCode == (0x80 | g_canAddr)) && (Can0_rx_message[2] == 0x57))
    {
        Can0_tx_message[0] = bq_funcCode;
        Can0_tx_message[1] = bq_CMD;
        Can0_tx_message[2] = 0x00;
        Can0_tx_message[3] = bq_CmdLength;
        Can0_tx_message[4] = 0x00;
        Can0_tx_message[5] = 0x00;
        Can0_tx_message[6] = 0x00;
        Can0_tx_message[7] = 0x00;
        if (writeBattData(bq_CMD,(uint32_t)bq_CmdLength,&Can0_rx_message[4]) == false)
        {
            Can0_tx_message[2] = 0x01u;
            Can0_tx_message[3] = 0x04;
            Can0_tx_message[4] = 0x52;
            Can0_tx_message[5] = 0x65;
            Can0_tx_message[6] = 0x61;
            Can0_tx_message[7] = 0x64;
        }
        CAN0_MessageTransmit(BATT_RESPONSE_ID,0x8u,Can0_tx_message,CAN_MODE_NORMAL, CAN_MSG_ATTR_TX_FIFO_DATA_FRAME);
    }
}


/**
****************************************************************************
* @brief ResponsePicCmd
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void ResponsePicCmd(void)
{
    bool s_flagPass = 0u;
    
    if ((Can0_rx_message[0] == (0x80 | g_canAddr)) && (Can0_rx_message[7] == 0x00u) && (Can0_rx_message[2] == 0x57u) && 
            (Can0_rx_message[3] == 0x72u) && (Can0_rx_message[4] == 0x69u) && (Can0_rx_message[5] == 0x74u))
    {
        s_testTime = 0u;
        if (Can0_rx_message[1] == LED_CTR_CMD)
        {
            s_flagPass = 1u;
            if (Can0_rx_message[6] == 0x01u)
            {
                g_testAllGLedOn = 1u;
                g_testAllRLedOn = 0u;
            }
            else if (Can0_rx_message[6] == 0x02u)
            {
                g_testAllRLedOn = 1u;
                g_testAllGLedOn = 0u;
            }
            else
            {
                g_testAllRLedOn = 0u;
                g_testAllGLedOn = 0u;
            }    
        } 
        if (Can0_rx_message[1] == CHGFET_CTR_CMD)
        {
            s_flagPass = 1u;
            if (Can0_rx_message[6] == 0x01u)
            {
                g_testPicChgFetOff = 1u;
            }
            else
            {
                g_testPicChgFetOff = 0u;
            }    
        } 
        if (Can0_rx_message[1] == DSGFET_CTR_CMD)
        {
            s_flagPass = 1u;
            if (Can0_rx_message[6] == 0x01u)
            {
                g_testPicDsgFetOff = 1u;
            }
            else
            {
                g_testPicDsgFetOff = 0u;
            }    
        } 
        if (Can0_rx_message[1] == ORFET_CTR_CMD)
        {
            s_flagPass = 1u;
            if (Can0_rx_message[6] == 0x01u)
            {
                g_testPicOrFetOff = 1u;
            }
            else
            {
                g_testPicOrFetOff = 0u;
            }    
        } 
        if (Can0_rx_message[1] == BROADCAST_CTR_CMD)
        {
            s_flagPass = 1u;
            if (Can0_rx_message[6] == 0x01u)
            {
                g_testDisableBroadcast = 1u;
            }
            else
            {
                g_testDisableBroadcast = 0u;
            }    
        } 
        if (Can0_rx_message[1] == SMBUS_CTR_CMD)
        {
            s_flagPass = 1u;
            if (Can0_rx_message[6] == 0x01u)
            {
                g_testDisableComBq = 1u;
            }
            else
            {
                g_testDisableComBq = 0u;
            }    
        } 
        if (Can0_rx_message[1] == SLEEP_CTR_CMD)
        {
            s_flagPass = 1u;
            if (Can0_rx_message[6] == 0x01u)
            {
                g_testSleep = 1u;
            }
            else
            {
                g_testSleep = 0u;
            }    
        } 
    } 
    Can0_tx_message[0] = Can0_rx_message[0];
    Can0_tx_message[1] = Can0_rx_message[1];
    Can0_tx_message[2] = Can0_rx_message[2];
    Can0_tx_message[3] = Can0_rx_message[3];
        
    Can0_tx_message[4] = Can0_rx_message[4];
    Can0_tx_message[5] = Can0_rx_message[5];
    Can0_tx_message[6] = Can0_rx_message[6];
    Can0_tx_message[7] = Can0_rx_message[7];
    if (s_flagPass == 1u)
    {
        CAN0_MessageTransmit(PIC_RESPONSE_ID,0x8u,Can0_tx_message,CAN_MODE_NORMAL, CAN_MSG_ATTR_TX_FIFO_DATA_FRAME);
    }
}

/**
****************************************************************************
* @brief resetUidArray
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void resetUidArray(void)
{
    uint8_t i;
    g_parallelQuantity = 1u;
    for (i = 0u; i < MAX_PARALLEL_COUNT; i++)
    {
        s_uidArray[i] = 0u;
    } 
    s_uidArray[0] = g_Uid;
    g_flagAddrAllocateFail = 0u;
    g_flagParallel = 0u;
    g_flagCompleteIdRequest = 0u;
}

/**
****************************************************************************
* @brief ResponseAddrAllocation
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void ResponseAddrAllocation(void)
{
    static uint8_t s_AddrAllocateFailTimes = 0;
    uint32_t tmpUid;
    uint8_t i;
    if ((Can0_rx_message[2] == 0x55u))
    {
        
        if (Can0_rx_message[3] == 0xeeu)
        {   
            s_AddrAllocateTime = 0u;
            s_flagSendOneUid = 1u;
            if (g_flagCompleteAddrAllocate == 1u)
            {
                g_flagCompleteAddrAllocate = 0u;
                s_AddrAllocateFailTimes = 0u;
                
            }
        }
        
        if ((Can0_rx_message[3] == 0xffu) && (g_flagCompleteAddrAllocate == 0u))
        {
            s_AddrAllocateTime = 0u;
            tmpUid = ((((uint32_t)Can0_rx_message[7] << 24u) + ((uint32_t)Can0_rx_message[6] << 16u)) +
                    ((uint32_t)Can0_rx_message[5] << 8u)) + (uint32_t)Can0_rx_message[4];
            
            for (i = 0u; i < MAX_PARALLEL_COUNT; i++)
            {
                if (tmpUid == s_uidArray[i])
                {
                    break;
                }
                if (s_uidArray[i] == 0u)
                {
                    s_uidArray[i] = tmpUid;
                    g_parallelQuantity = i + 1u;
                    g_flagParallel = 1u;
                    break;
                }
            }
            if ((i >= MAX_PARALLEL_COUNT) || (i == 0u))
            {
                g_flagAddrAllocateFail = 1u; 
            }
        }  
    }
}


#define READ_SLAVE_DATA_LENGTH 4u
/**
****************************************************************************
* @brief RequestOtherBatteryInfo
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void RequestOtherBatteryInfo(void)
{
    static uint8_t s_requestPeriod = 0u;
    static bool s_flagReadBatt = 0u;
    static uint8_t s_canAddr = 0x01;
    static uint8_t s_commandPointer = 0u;
    
    if (++s_requestPeriod >= TIMER5MS_500MS)
    {
        s_requestPeriod = 0u;
        s_flagReadBatt = 1u;
    }
    














    if ((g_commMode == MASTER_MODE) && (g_flagParallel == 1u) && (s_flagReadBatt == 1u))
    {
        if (s_commandPointer == 0u)   
        {
            Can0_tx_message[0] = (0x80 | s_canAddr);
            Can0_tx_message[1] = 0x46u;
            Can0_tx_message[2] = 0x57u;
            Can0_tx_message[3] = 0x72u;
            Can0_tx_message[4] = 0x69u;
            
            Can0_tx_message[5] = g_IDSystemError[s_canAddr - 1u];
            Can0_tx_message[6] = g_IDFETEnCH[s_canAddr - 1u].byte;
            Can0_tx_message[7] = g_IDFETEnDisCH[s_canAddr - 1u].byte;
        }
        if (s_commandPointer == 1u)  
        {
            Can0_tx_message[0] = s_canAddr;
            Can0_tx_message[1] = 0xe0u;
            Can0_tx_message[2] = 0x52u;
            Can0_tx_message[3] = 0x65u;
            Can0_tx_message[4] = 0x61u;
            Can0_tx_message[5] = 0x64u;
            Can0_tx_message[6] = 0x00u;
            Can0_tx_message[7] = 0x00u;
        }
        if (s_commandPointer == 2u)  
        {
            Can0_tx_message[0] = s_canAddr;
            Can0_tx_message[1] = 0x01u;
            Can0_tx_message[2] = 0x52u;
            Can0_tx_message[3] = 0x65u;
            Can0_tx_message[4] = 0x61u;
            Can0_tx_message[5] = 0x64u;
            Can0_tx_message[6] = 0x00u;
            Can0_tx_message[7] = 0x00u;
        }
        if (s_commandPointer == 3u)  
        {
            Can0_tx_message[0] = s_canAddr;
            Can0_tx_message[1] = 0x02u;
            Can0_tx_message[2] = 0x52u;
            Can0_tx_message[3] = 0x65u;
            Can0_tx_message[4] = 0x61u;
            Can0_tx_message[5] = 0x64u;
            Can0_tx_message[6] = 0x00u;
            Can0_tx_message[7] = 0x00u;
        }
        if (CAN0_MessageTransmit(READOTHERBATT_ID,0x8u,Can0_tx_message,CAN_MODE_NORMAL, CAN_MSG_ATTR_TX_FIFO_DATA_FRAME) == true)
        {
            if (++s_commandPointer >= READ_SLAVE_DATA_LENGTH)
            {
                s_commandPointer = 0u;
                s_canAddr++;
            }
            if (s_canAddr >= (g_parallelQuantity + 1u))
            {
                s_flagReadBatt = 0u;
                s_canAddr = 2u;
            }
        }
    }
} 

/**
****************************************************************************
* @brief ReceiveMasterCanCMD
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void ReceiveMasterCanCMD(void)
{
    uint8_t batt_funcCode;
    uint8_t batt_CMD;
    batt_funcCode = Can0_rx_message[0];
    batt_CMD = Can0_rx_message[1];  
    
    
    if ((batt_funcCode ==  (0x80 | g_canAddr)) && (batt_CMD == 0x46u) && (Can0_rx_message[2] == 0x57u) && 
            (Can0_rx_message[3] == 0x72u) && (Can0_rx_message[4] == 0x69u) )
    {
		g_systemError.byte = Can0_rx_message[5];
        g_FETEnCH.byte = Can0_rx_message[6];
        g_FETEnDisCH.byte = Can0_rx_message[7];
        
        Can0_tx_message[0] = g_canAddr;
        Can0_tx_message[1] = 0xe0;
        
        Can0_tx_message[2] = (uint8_t)g_battVolt;
        Can0_tx_message[3] = (uint8_t)(g_battVolt >> 8u);       
        Can0_tx_message[4] = (uint8_t)g_battCurr;
        Can0_tx_message[5] = (uint8_t)(g_battCurr >> 8u);
        Can0_tx_message[6] = (g_chargeMode & 0x0f) + ((g_cellPosition << 4u) & 0x30) + (g_bqFetState.byte & 0xc0);
        Can0_tx_message[7] = g_FETstate.byte;

        CAN0_MessageTransmit(READ_RESPONSE_ID,0x8u,Can0_tx_message,CAN_MODE_NORMAL, CAN_MSG_ATTR_TX_FIFO_DATA_FRAME);
    } 
#if 0    
    
    if ((batt_funcCode == g_canAddr) && (batt_CMD == 0xe0u) && (Can0_rx_message[2] == 0x52u) && 
            (Can0_rx_message[3] == 0x65u) && (Can0_rx_message[4] == 0x61u) && (Can0_rx_message[5] == 0x64u))
    {      
        Can0_tx_message[0] = batt_funcCode;
        Can0_tx_message[1] = batt_CMD;
        
        Can0_tx_message[2] = (uint8_t)g_battVolt;
        Can0_tx_message[3] = (uint8_t)(g_battVolt >> 8u);       
        Can0_tx_message[4] = (uint8_t)g_battCurr;
        Can0_tx_message[5] = (uint8_t)(g_battCurr >> 8u);
        Can0_tx_message[6] = g_chargeMode;
        Can0_tx_message[7] = g_FETstate.byte;
        CAN0_MessageTransmit(READ_RESPONSE_ID,0x8u,Can0_tx_message,CAN_MODE_NORMAL, CAN_MSG_ATTR_TX_FIFO_DATA_FRAME);
    }
#endif	
    
    if ((batt_funcCode == g_canAddr) && (batt_CMD == 0x01u) && (Can0_rx_message[2] == 0x52u) && 
            (Can0_rx_message[3] == 0x65u) && (Can0_rx_message[4] == 0x61u) && (Can0_rx_message[5] == 0x64u))
    {      
        Can0_tx_message[0] = batt_funcCode;
        Can0_tx_message[1] = batt_CMD;
             
        Can0_tx_message[2] = (uint8_t)g_checkPositionResult;
        Can0_tx_message[3] = g_battSOC;
        Can0_tx_message[4] = (uint8_t)g_dsgCurrLimit;
        Can0_tx_message[5] = (uint8_t)(g_dsgCurrLimit >> 8u);
        Can0_tx_message[6] = (uint8_t)g_chgCurrLimit;
        Can0_tx_message[7] = (uint8_t)(g_chgCurrLimit >> 8u);

        CAN0_MessageTransmit(READ_RESPONSE_ID,0x8u,Can0_tx_message,CAN_MODE_NORMAL, CAN_MSG_ATTR_TX_FIFO_DATA_FRAME);
    }
    
    if ((batt_funcCode == g_canAddr) && (batt_CMD == 0x02u) && (Can0_rx_message[2] == 0x52u) && 
            (Can0_rx_message[3] == 0x65u) && (Can0_rx_message[4] == 0x61u) && (Can0_rx_message[5] == 0x64u))
    {      
        Can0_tx_message[0] = batt_funcCode;
        Can0_tx_message[1] = batt_CMD;
        
        Can0_tx_message[2] = (uint8_t)g_regenCurrLimit;
        Can0_tx_message[3] = (uint8_t)(g_regenCurrLimit >> 8u);       
        Can0_tx_message[4] = 0u;
        Can0_tx_message[5] = 0u;
        Can0_tx_message[6] = 0u;
        Can0_tx_message[7] = 0u;
        CAN0_MessageTransmit(READ_RESPONSE_ID,0x8u,Can0_tx_message,CAN_MODE_NORMAL, CAN_MSG_ATTR_TX_FIFO_DATA_FRAME);
    }
    if (batt_funcCode == g_canAddr)
    {
        g_picParallelUpdateVaule = 0u;
        g_IDParallelUpdateArray[0u] = 0u;
    }
}

/**
****************************************************************************
* @brief ReveiveOtherBattInfo
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void ReveiveOtherBattInfo(void)
{
    uint8_t batt_funcCode;
    uint8_t batt_CMD;
	uint8_t IDSOC_Ini = 0u;
    uint16_t readProductCode;
    uint8_t readSubProductCode;
    static uint8_t s_debounceProductCodeError = 0u;
	
    batt_funcCode = Can0_rx_message[0];
    batt_CMD = Can0_rx_message[1];  
    
    g_IDParallelUpdateArray[batt_funcCode - 1u] = 0u;  
    
    if ((g_commMode == MASTER_MODE) && (batt_CMD == 0xe0u) && (batt_funcCode < (MAX_PARALLEL_COUNT + 1u)) && (batt_funcCode >= 2u))
    {
        g_IDVolt[batt_funcCode - 1u] = (uint16_t)Can0_rx_message[3] * 256 + Can0_rx_message[2]; 
        g_IDCurr[batt_funcCode - 1u] = (int16_t)Can0_rx_message[5] * 256u + Can0_rx_message[4];
        g_IDChargeMode[batt_funcCode - 1u] = Can0_rx_message[6]& 0x0f;
		g_IDCellS[batt_funcCode - 1u] = (Can0_rx_message[6] >> 4u) & 0x03;
        g_IDbqFetState[batt_funcCode - 1u] = Can0_rx_message[6] & 0xc0;
        g_IDFETstate[batt_funcCode - 1u].byte = Can0_rx_message[7];
    } 
    
    if ((g_commMode == MASTER_MODE) && (batt_CMD == 0x01u) && (batt_funcCode < (MAX_PARALLEL_COUNT + 1u)) && (batt_funcCode >= 2u))
    {
        g_IDPackPosition[batt_funcCode - 1u] = Can0_rx_message[2]; 
        g_IDSOC[batt_funcCode - 1u] = Can0_rx_message[3];
        g_IDdsgCurrLimit[batt_funcCode - 1u] = (uint16_t)Can0_rx_message[5] * 256 + Can0_rx_message[4]; 
		g_IDchgCurrLimit[batt_funcCode - 1u] = (int16_t)Can0_rx_message[7] * 256u + Can0_rx_message[6];
    } 
    
    if ((g_commMode == MASTER_MODE) && (batt_CMD == 0x02u) && (batt_funcCode < (MAX_PARALLEL_COUNT + 1u)) && (batt_funcCode >= 2u))
    {
        g_IDregenCurrLimit[batt_funcCode - 1u] = (uint16_t)Can0_rx_message[3] * 256 + Can0_rx_message[2];
    }
    
    
    
}


void can0HoldFunc(void)
{
    static uint16_t s_can0UpdateTime;
    uint16_t const s_canOpenCmdT[]={1000u,800u,500u,250u,125u,125u,50u,20u,10u};
    uint16_t s_bitRateTmp;
    uint16_t s_bitRateTmp2;
    uint8_t ntseg2;
    uint8_t ntseg1;

    s_can0HoldTime++;
    if (s_can0HoldTime >= TIMER5MS_5S)
    {
        s_can0HoldTime = 0u;
        s_flagResetCan0 = 1u; 
    }
    
    if (g_flagLssBitRateUpdate == 1u)
    {
        if (++s_can0UpdateTime >= g_lssUpdateRateTime)
        {
            s_flagResetCan0 = 1u;
            g_flagLssBitRateUpdate = 0u;
            s_can0UpdateTime = 0u;
        }
    }
    else
    {
        s_can0UpdateTime = 0u;
    }
    
    if (s_flagResetCan0 == 1u)
    {
        s_flagResetCan0 = 0u;
        s_bitRateTmp = CAN_FREQUENCY / s_canOpenCmdT[g_BitRate];
        s_bitRateTmp = s_bitRateTmp - 3u;
        s_bitRateTmp2 = ((s_bitRateTmp * 75u) + 50u) / 100u;
        if (s_bitRateTmp2 >= 0xffu)
        {
            ntseg1 = 0xffu;
        }
        else
        {
            ntseg1 = (uint8_t)s_bitRateTmp2;
        }
        ntseg2 = (uint8_t)(s_bitRateTmp - s_bitRateTmp2);
        CAN1_Disable();
        CAN1_Initialize_Bit_Rate(ntseg2, ntseg1);
        CAN1_MessageRAMConfigSet(Can1MessageRAM);
    }

	if(++g_batcanHoldTime > TIMER5MS_1S)
	{
		g_batcanHoldTime = 0;
		Can0_tx_message[0] = 0x05u;
		if (CAN0_MessageTransmit((HEARTBEAT_FUNCCODE + g_canAddr),0x1u,Can0_tx_message,CAN_MODE_NORMAL, CAN_MSG_ATTR_TX_FIFO_DATA_FRAME) == false)
		{
			CAN0_Disable();
			CAN0_Initialize();
			CAN0_MessageRAMConfigSet(Can0MessageRAM);
		}
	}
}


#define BL_REQUEST (0x5048434DU)
#define BL_REQUEST_LEN (16)
#define APP_SRAM_START 0x20000000U
static uint32_t *sram = (uint32_t *) APP_SRAM_START;

void intoBootloader(void)
{  
    if ((Can0_rx_message[0] == 0x2bu) && (Can0_rx_message[1] == 0x01u) && (Can0_rx_message[2] == 0xc1u) && (Can0_rx_message[3] == 0x00u) &&
            (Can0_rx_message[4] == 0x00u) && (Can0_rx_message[5] == 0x0fu) && (Can0_rx_message[6] == 0x00u) && (Can0_rx_message[7] == 0x00u))
    {
        
        sram[0] = BL_REQUEST;
        sram[1] = BL_REQUEST;
        sram[2] = BL_REQUEST;
        sram[3] = BL_REQUEST;
        NVIC_SystemReset();
    } 
}
/* Inventus Power Proprietary */
