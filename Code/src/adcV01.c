/**
 * @file  adcV01.c
 * @brief This file contains the ADC libraries.
 *  
 * Description:
 *   This file contains the functionality for the ADC modules.
 *
 * REVISION HISTORY
 *==============================================
 * |Ticket   |Date      |Author       |Notes
 * |----:    |:----:    |:----:       |:----
 * |Creation |02/04/22  |Tom O'Connor |Document
 * |Modified |02/14/22  |Tyler Badar  |Added function descriptions
 *
 * @par   COPYRIGHT NOTICE: (c) 2022 Inventus Power.  All rights reserved.
 */

/* Inventus Power Proprietary*/
#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes
#include "adcV01.h"
#include "mainV01.h"
#include "common_defineV01.h"
#include "arithmeticV01.h"


void NTCSampleFunction(void);

uint16_t g_adcNTC[NTC_SAMPLE_AMOUNT] = {0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u};
uint16_t g_adcPackSP = 0;
uint16_t g_adcBattVolt = 0;
uint16_t g_adcPackVolt = 0;
uint16_t g_adcPreCHGCurr = 0;
static uint8_t s_sampleChannel = NTC_SAMPLE_AMOUNT;

uint16_t g_NTCTemperatureTable[16] = {2980, 2980, 2980, 2980, 2980, 2980, 2980, 2980,
                                      2980, 2980, 2980, 2980, 2980, 2980, 2980, 2980};

uint16_t g_adcReserved1 = 0;
uint16_t g_adcReserved2 = 0;
uint16_t g_adcReserved3 = 0;
uint16_t g_adcReserved4 = 0;
uint16_t g_adcReserved5 = 0;



/**
****************************************************************************
* @brief ReadAdc
* 
* reads selected ADC channel 4 times and averages those values. lastValue 
* param included in average if != 0.

* @param [in] lastValue 
* @param [in] AdcChannel 
* @return  AdcValue
* @todo remove #if 0 define
*****************************************************************************
*/
uint16_t ReadAdc(uint16_t lastValue, ADC_POSINPUT AdcChannel) 
{
#if 0
    uint8_t i;
    uint8_t j;
    uint16_t tmpAdcValue;
    uint32_t s_adcTimeOut;
    tmpAdcValue = 0;
    j = 0;
    
    
    ADC0_ChannelSelect(AdcChannel, ADC_NEGINPUT_GND);   
    Delay10us(10);
    for (i = 0; i < 2; i++)
    {
        ADC0_ConversionStart();
        s_adcTimeOut = 0u;
        while(!ADC0_ConversionStatusGet())
        {
            if (++s_adcTimeOut >= ADC_SAMPLE_TIMEOUT)
            {
                break;
            }
        } 
        tmpAdcValue = ADC0_ConversionResultGet();   
    }
    
    tmpAdcValue = 0;
    for (i = 0; i < 4; i++)
    {
        ADC0_ConversionStart();
        s_adcTimeOut = 0u;
        while(!ADC0_ConversionStatusGet())
        {
            if (++s_adcTimeOut >= ADC_SAMPLE_TIMEOUT)
            {
                break;
            }
        } 
        tmpAdcValue += ADC0_ConversionResultGet();   
    }
    
    if (lastValue != 0)  
    {
        tmpAdcValue = ((lastValue * 3) + tmpAdcValue) / 4;
    }
#else
	uint8_t i;
	uint8_t j;
	uint16_t tmpAdcValue;

	tmpAdcValue = 0;
	j = 0;

	
	ADC0_ChannelSelect(AdcChannel, ADC_NEGINPUT_GND);	
	Delay10us(10);
	for (i = 0; i < 4; i++)
	{
		ADC0_ConversionStart();
		while(!ADC0_ConversionStatusGet()){}	 
		tmpAdcValue += ADC0_ConversionResultGet();	 
	}

	if (lastValue != 0)  
	{
		tmpAdcValue = ((lastValue * 3) + tmpAdcValue) / 4;
	}

#endif
    return (tmpAdcValue); 
}


/**
****************************************************************************
* @brief SampleFunction
*
* samples NTC in AIN0 and ADCs in AIN1-3 & AIN11 and updates running average
* values.
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void SampleFunction(void) 
{
    ADC0_Enable(); 
    NTCSampleFunction();
    g_adcPackSP = ReadAdc(g_adcPackSP, ADC_POSINPUT_AIN1);
    g_adcBattVolt = ReadAdc(g_adcBattVolt, ADC_POSINPUT_AIN2);
    g_adcPackVolt = ReadAdc(g_adcPackVolt, ADC_POSINPUT_AIN3);
    g_adcPreCHGCurr = ReadAdc(0, ADC_POSINPUT_AIN11);
    
}

/**
****************************************************************************
* @brief disableADCSample
*
* disables ADC0 through CTRL Register, set NTC sample channel => 11,
* enable input on NTCs
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void disableADCSample(void) 
{
    ADC0_Disable();
    s_sampleChannel = NTC_SAMPLE_AMOUNT;
    CELL_NTC1_InputEnable();
    CELL_NTC2_InputEnable();
    CELL_NTC3_InputEnable();
    CELL_NTC4_InputEnable();
    CELL_NTC5_InputEnable();
    CELL_NTC6_InputEnable();
    BAT_NTC1_InputEnable();
    BAT_NTC2_InputEnable();
    MOS_NTC1_InputEnable();
    MOS_NTC2_InputEnable();
    BUCK_MOS_NTC_InputEnable();
}

/**
****************************************************************************
* @brief NTCSampleFunction
*
* read ADC in AIN0 with last value grabbed from adcNTC array indexed by 
* sampleChannel value
* 
* @param NONE
* @return NONE
*****************************************************************************
*/
void NTCSampleFunction(void) 
{
    switch (s_sampleChannel)
    {
        case 0:   
        {
            
            g_adcNTC[s_sampleChannel] = ReadAdc(g_adcNTC[s_sampleChannel], ADC_POSINPUT_AIN0);
            s_sampleChannel = 1u;
            CELL_NTC1_InputEnable();
            CELL_NTC2_OutputEnable();
            CELL_NTC3_InputEnable();
            CELL_NTC4_InputEnable();
            CELL_NTC5_InputEnable();
            CELL_NTC6_InputEnable();
            BAT_NTC1_InputEnable();
            BAT_NTC2_InputEnable();
            MOS_NTC1_InputEnable();
            MOS_NTC2_InputEnable();
            BUCK_MOS_NTC_InputEnable();
            break;
        }
    
        case 1: 
        {
            
            g_adcNTC[s_sampleChannel] = ReadAdc(g_adcNTC[s_sampleChannel], ADC_POSINPUT_AIN0);
            s_sampleChannel = 2u;
            CELL_NTC1_InputEnable();
            CELL_NTC2_InputEnable();
            CELL_NTC3_OutputEnable();
            CELL_NTC4_InputEnable();
            CELL_NTC5_InputEnable();
            CELL_NTC6_InputEnable();
            BAT_NTC1_InputEnable();
            BAT_NTC2_InputEnable();
            MOS_NTC1_InputEnable();
            MOS_NTC2_InputEnable();
            BUCK_MOS_NTC_InputEnable();    
            break;
        }
        
        case 2:
        {
            
            g_adcNTC[s_sampleChannel] = ReadAdc(g_adcNTC[s_sampleChannel], ADC_POSINPUT_AIN0);
            s_sampleChannel = 3u;
            CELL_NTC1_InputEnable();
            CELL_NTC2_InputEnable();
            CELL_NTC3_InputEnable();
            CELL_NTC4_OutputEnable();
            CELL_NTC5_InputEnable();
            CELL_NTC6_InputEnable();
            BAT_NTC1_InputEnable();
            BAT_NTC2_InputEnable();
            MOS_NTC1_InputEnable();
            MOS_NTC2_InputEnable();
            BUCK_MOS_NTC_InputEnable();
            break;
        }
        
        case 3:
        {
            
            g_adcNTC[s_sampleChannel] = ReadAdc(g_adcNTC[s_sampleChannel], ADC_POSINPUT_AIN0);
            s_sampleChannel = 4u;
            CELL_NTC1_InputEnable();
            CELL_NTC2_InputEnable();
            CELL_NTC3_InputEnable();
            CELL_NTC4_InputEnable();
            CELL_NTC5_OutputEnable();
            CELL_NTC6_InputEnable();
            BAT_NTC1_InputEnable();
            BAT_NTC2_InputEnable();
            MOS_NTC1_InputEnable();
            MOS_NTC2_InputEnable();
            BUCK_MOS_NTC_InputEnable();
            break;
        }
        case 4:
        {
            
            g_adcNTC[s_sampleChannel] = ReadAdc(g_adcNTC[s_sampleChannel], ADC_POSINPUT_AIN0);
            s_sampleChannel = 5u;
            CELL_NTC1_InputEnable();
            CELL_NTC2_InputEnable();
            CELL_NTC3_InputEnable();
            CELL_NTC4_InputEnable();
            CELL_NTC5_InputEnable();
            CELL_NTC6_OutputEnable();
            BAT_NTC1_InputEnable();
            BAT_NTC2_InputEnable();
            MOS_NTC1_InputEnable();
            MOS_NTC2_InputEnable();
            BUCK_MOS_NTC_InputEnable();
            break;
        }
        case 5:
        {
            
            g_adcNTC[s_sampleChannel] = ReadAdc(g_adcNTC[s_sampleChannel], ADC_POSINPUT_AIN0);
            s_sampleChannel = 6u;
            CELL_NTC1_InputEnable();
            CELL_NTC2_InputEnable();
            CELL_NTC3_InputEnable();
            CELL_NTC4_InputEnable();
            CELL_NTC5_InputEnable();
            CELL_NTC6_InputEnable();
            BAT_NTC1_OutputEnable();
            BAT_NTC2_InputEnable();
            MOS_NTC1_InputEnable();
            MOS_NTC2_InputEnable();
            BUCK_MOS_NTC_InputEnable();
            break;
        }
        case 6:
        {
            
            g_adcNTC[s_sampleChannel] = ReadAdc(g_adcNTC[s_sampleChannel], ADC_POSINPUT_AIN0);
            s_sampleChannel = 7u;
            CELL_NTC1_InputEnable();
            CELL_NTC2_InputEnable();
            CELL_NTC3_InputEnable();
            CELL_NTC4_InputEnable();
            CELL_NTC5_InputEnable();
            CELL_NTC6_InputEnable();
            BAT_NTC1_InputEnable();
            BAT_NTC2_OutputEnable();
            MOS_NTC1_InputEnable();
            MOS_NTC2_InputEnable();
            BUCK_MOS_NTC_InputEnable();
            break;
        }
        case 7:
        {
            
            g_adcNTC[s_sampleChannel] = ReadAdc(g_adcNTC[s_sampleChannel], ADC_POSINPUT_AIN0);
            s_sampleChannel = 8u;
            CELL_NTC1_InputEnable();
            CELL_NTC2_InputEnable();
            CELL_NTC3_InputEnable();
            CELL_NTC4_InputEnable();
            CELL_NTC5_InputEnable();
            CELL_NTC6_InputEnable();
            BAT_NTC1_InputEnable();
            BAT_NTC2_InputEnable();
            MOS_NTC1_OutputEnable();
            MOS_NTC2_InputEnable();
            BUCK_MOS_NTC_InputEnable();
            break;
        }
        case 8:
        {
            g_adcNTC[s_sampleChannel] = ReadAdc(g_adcNTC[s_sampleChannel], ADC_POSINPUT_AIN0);
            s_sampleChannel = 9u;
            CELL_NTC1_InputEnable();
            CELL_NTC2_InputEnable();
            CELL_NTC3_InputEnable();
            CELL_NTC4_InputEnable();
            CELL_NTC5_InputEnable();
            CELL_NTC6_InputEnable();
            BAT_NTC1_InputEnable();
            BAT_NTC2_InputEnable();
            MOS_NTC1_InputEnable();
            MOS_NTC2_OutputEnable();
            BUCK_MOS_NTC_InputEnable();  
            break;
        }
        case 9:
        {
            g_adcNTC[s_sampleChannel] = ReadAdc(g_adcNTC[s_sampleChannel], ADC_POSINPUT_AIN0);
            s_sampleChannel = 10u;
            CELL_NTC1_InputEnable();
            CELL_NTC2_InputEnable();
            CELL_NTC3_InputEnable();
            CELL_NTC4_InputEnable();
            CELL_NTC5_InputEnable();
            CELL_NTC6_InputEnable();
            BAT_NTC1_InputEnable();
            BAT_NTC2_InputEnable();
            MOS_NTC1_InputEnable();
            MOS_NTC2_InputEnable();
            BUCK_MOS_NTC_OutputEnable(); 
            break;
        }
        case 10:
        {
            g_adcNTC[s_sampleChannel] = ReadAdc(g_adcNTC[s_sampleChannel], ADC_POSINPUT_AIN0);
            s_sampleChannel = 0u;
            CELL_NTC1_OutputEnable();
            CELL_NTC2_InputEnable();
            CELL_NTC3_InputEnable();
            CELL_NTC4_InputEnable();
            CELL_NTC5_InputEnable();
            CELL_NTC6_InputEnable();
            BAT_NTC1_InputEnable();
            BAT_NTC2_InputEnable();
            MOS_NTC1_InputEnable();
            MOS_NTC2_InputEnable();
            BUCK_MOS_NTC_InputEnable(); 
            break;
        }
        default:
        {           
            s_sampleChannel = 0u;
            CELL_NTC1_OutputEnable();
            CELL_NTC2_InputEnable();
            CELL_NTC3_InputEnable();
            CELL_NTC4_InputEnable();
            CELL_NTC5_InputEnable();
            CELL_NTC6_InputEnable();
            BAT_NTC1_InputEnable();
            BAT_NTC2_InputEnable();
            MOS_NTC1_InputEnable();
            MOS_NTC2_InputEnable();
            BUCK_MOS_NTC_InputEnable();  
            break;
        }   
    }  
}

/* Inventus Power Proprietary */
