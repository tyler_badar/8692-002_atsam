/**
 * @file  arithmeticV01.c
 *  
 * Description:
 *   This file contains delay and crc functions.
 *
 *
 * REVISION HISTORY
 *==============================================
 * |Ticket   |Date      |Author       |Notes
 * |----:    |:----:    |:----:       |:----
 * |Creation |02/04/22  |Tom O'Connor |Document
 * |Modified |02/16/22  |Tyler Badar  |Added function descriptions
 *
 * @par   COPYRIGHT NOTICE: (c) 2022 Inventus Power.  All rights reserved.
 */

/* Inventus Power Proprietary */

/*****************************************************************************/
#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes
#include "arithmeticV01.h"

/**
****************************************************************************
* @brief Delay10us
*
* delay with 10us multiplier based on cnt_Delay parameter
* 
* @param [in] cnt_Delay
*
* @return NONE
*****************************************************************************
*/
void Delay10us(uint16_t cnt_Delay)
{
    uint16_t i;
    uint16_t j;
    for (i = 0u; i < cnt_Delay; i++)
    {
        for (j = 0u; j < 25u; j++)
        {
            ;
        }  
    }
}

/**
****************************************************************************
* @brief basic_crc
*
* perform crc on passed in value
*
* @param [in] remainder 
* @param [in] byte
*
* @return uint8_t
*****************************************************************************
*/
uint8_t basic_crc(uint8_t remainder, uint8_t byte)
{
    uint8_t i = 0u;
    remainder ^= byte;
    for (i = 8u; i > 0u; --i)
    {
        if ((remainder & 0x80u) != 0u)
        {
            remainder = (uint8_t)(remainder << 1u) ^ 0x07u;
        }
        else
        {
            remainder = (uint8_t)(remainder << 1u);
        }
    }

    return remainder;

}


#define POLY 0x8408U

/**
****************************************************************************
* @brief Crc16
*
* perform crc on 16 bit value read from passed in pointer
*
* @param  [in] *ptr
* @param  [in] len
*
* @return CrC
*****************************************************************************
*/
uint16_t Crc16(uint8_t *ptr, uint16_t len)
{
    uint8_t i;
    uint16_t tmpData;
    uint16_t tmp;

    tmp = 0xffffU;
    if ( len == 0U ) 
    {
        
        tmp = ~tmp;
    }
    else
    {
        do
        {
            tmpData = (uint16_t)0xff & *ptr++;
            for ( i = 0U; i < 8U; i++ )
            {
                if ( ((tmp & 0x0001U) ^ (tmpData & 0x0001U)) != 0u )
                {
                    tmp = (tmp >> 1U) ^ POLY;
                }
                else
                {
                    tmp >>= 1U;
                }
                tmpData >>= 1U;
            }
        }
        while ( (--len) != 0u );
    
        tmp = ~tmp;
        tmpData = tmp;
        tmp = (tmp << 8U) | ((tmpData >> 8U) & 0xFFU);
    }

    return (tmp);
}

/* Inventus Power Proprietary */


