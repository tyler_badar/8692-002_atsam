/**
 * @file  chgdsgV01.c
 *
 * Main ATSAM program
 *
 * REVISION HISTORY
 *==============================================
 * |Ticket   |Date      |Author       |Notes
 * |----:    |:----:    |:----:       |:----
 * |Creation |02/04/22  |Tom O'Connor |Document
 * |Modified |02/22/22  |Tyler Badar  |Added function descriptions
 *
 * @par   COPYRIGHT NOTICE: (c) 2022 Inventus Power.  All rights reserved.
 */
/* Inventus Power Proprietary */
#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes
#include "mainV01.h"
#include "common_defineV01.h"
#include "i2cMasterV02.h"
#include "detectV01.h"
#include "canV01.h"
#include "pwmV01.h"
#include "J1939V01.h"

#include "chgdsgV01.h"

uint16_t g_maxDsgS1Volt = 0u;
uint16_t g_maxDsgS2Volt = 0u;
uint8_t g_chargeMode = STANDBY_MODE;

define_8bits g_IDFETEnCH[MAX_PARALLEL_COUNT] = {0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u}; 
define_8bits g_IDFETEnDisCH[MAX_PARALLEL_COUNT] = {0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u};
define_8bits g_IDFETstate[MAX_PARALLEL_COUNT] = {0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u};
uint16_t g_IDVolt[MAX_PARALLEL_COUNT] = {0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u};
uint8_t g_IDChargeMode[MAX_PARALLEL_COUNT] = {0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u};
int16_t g_IDCurr[MAX_PARALLEL_COUNT] = {0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u};
uint8_t g_IDSOC[MAX_PARALLEL_COUNT] = {0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u};

uint8_t g_IDPackPosition[MAX_PARALLEL_COUNT] = {0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u};
uint8_t g_IDSystemError[MAX_PARALLEL_COUNT] = {0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u};

uint16_t g_minVolt = 0u;
uint16_t g_maxVolt = 0u;
uint16_t g_chargeVolt = 0u;

define_8bits g_FETstate;
define_8bits g_FETEnCH;
define_8bits g_bqFetState;
define_8bits g_FETEnDisCH;
define_8bits g_FETEnState;
define_8bits g_systemError;
define_16bits g_flagPicChgProtect;
define_16bits g_flagPicDsgProtect;
define_16bits g_chargerFaultFlg;
static uint8_t g_IDchargePause = 0u;
uint8_t g_IDCHGProtect[MAX_PARALLEL_COUNT] = {0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u};
uint8_t g_chargeLeastEN = 0u;
uint8_t g_preChargeNUM = 0u;
uint8_t g_fastChargeNUM = 0u;

bool g_flag8050ChgProtect = 0u;
bool g_flag8050DsgProtect = 0u;
bool g_flagdisPreDsgFet = 0u;

uint8_t g_IDParallelUpdateArray[MAX_PARALLEL_COUNT] = {0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u};
uint8_t g_picParallelUpdateVaule = 0u;
uint16_t g_IDchgCurrLimit[MAX_PARALLEL_COUNT] = {0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u};
uint16_t g_IDdsgCurrLimit[MAX_PARALLEL_COUNT] = {0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u};
uint16_t g_IDregenCurrLimit[MAX_PARALLEL_COUNT] = {0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u};
uint8_t g_IDCellS[MAX_PARALLEL_COUNT] = {0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u};  
uint8_t g_IDbqFetState[MAX_PARALLEL_COUNT] = {0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u};


uint16_t g_chgCurrLimit = 0u;
uint16_t g_dsgCurrLimit = 0u;
uint16_t g_regenCurrLimit = 0u;
uint16_t g_broadcastChgCurrLimit = 0u;
uint16_t g_broadcastDsgCurrLimit = 0u;
uint16_t g_broadcastRegenCurrLimit = 0u;
uint16_t g_S1minVolt = 0u;
uint16_t g_S1maxVolt = 0u;
uint16_t g_S2minVolt = 0u;
uint16_t g_S2maxVolt = 0u;
uint16_t g_chargeCurr = 0u;
uint8_t g_balanceState = 0u;



static void ChargeControlFunction(void);
static void DischargeControlFunction(void);
static void CalculateMinMaxVolt(void);
static void ChargeProtection(void);
static void MasterChargeProtect(void);

/**
****************************************************************************
* @brief MasterControlFunction
*
* if master pack:
*   - set virtual batt data 
*   - check pack position array 
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void MasterControlFunction(void)
{
    uint8_t i;
    static uint8_t s_flagPositionError = 0u;
    
	if (g_commMode == MASTER_MODE)
	{
		g_IDFETstate[0u].byte = g_FETstate.byte;
        g_IDSOC[0u] = g_battSOC;
        g_IDVolt[0u] = g_battVolt;
        g_IDChargeMode[0u] = g_chargeMode;
        g_IDCellS[0] = g_cellPosition;
        g_IDbqFetState[0] = g_bqFetState.byte;
        g_IDPackPosition[0] = g_checkPositionResult;
        CalculateMinMaxVolt();    
		ChargeControlFunction();
        MasterChargeProtect();
		DischargeControlFunction();	
        g_systemError.byte = g_IDSystemError[0u];
        g_picParallelUpdateVaule = 0u;
		g_IDParallelUpdateArray[0u] = 0u;
        
        for (i = 0u; i < MAX_PARALLEL_COUNT; i++)
        {
            if (g_IDPackPosition[i] == 0xAAu)
            {
                break;
            }
        }
        
        if (i >= MAX_PARALLEL_COUNT)
        {
            s_flagPositionError = 0u;
        }
        else
        {
            s_flagPositionError = 1u;
        }
        
        for (i = 0u; i < MAX_PARALLEL_COUNT; i++)
        {
            if (s_flagPositionError == 1u)
            {
                g_IDSystemError[i] |= 0x01u;
                g_IDFETEnCH[i].byte = 0u;
                g_IDFETEnDisCH[i].byte = 0u;
            }
            else
            {
                g_IDSystemError[i] &= 0xfeu;
            }
        }
		g_FETEnCH.byte = g_IDFETEnCH[0u].byte;
		g_FETEnDisCH.byte = g_IDFETEnDisCH[0u].byte;
	}
}

/**
****************************************************************************
* @brief CalculateMinMaxVolt
*
* calculate min/max voltage from parallel pack voltage array
*
* @param NONE
* @return NONE
*****************************************************************************
*/
static void CalculateMinMaxVolt(void)
{
	uint8_t i;	
	g_S1minVolt = 5000u;
	g_S1maxVolt = 0u;
    g_S2minVolt = 5000u;
	g_S2maxVolt = 0u;
	for(i = 0u; i < MAX_PARALLEL_COUNT; i++)
	{
        if (g_IDCellS[i] == CELL_1S_POSITION)
        {
            if ((g_S1minVolt >= g_IDVolt[i]) && (g_IDVolt[i] != 0u))
            {
                g_S1minVolt = g_IDVolt[i];
            }
            if (g_S1minVolt < g_IDVolt[i])
            {
                g_S1minVolt = g_IDVolt[i];
            }
        }
        if (g_IDCellS[i] == CELL_2S_POSITION)
        {
            if ((g_S2minVolt >= g_IDVolt[i]) && (g_IDVolt[i] != 0u))
            {
                g_S2minVolt = g_IDVolt[i];
            }
            if (g_S2minVolt < g_IDVolt[i])
            {
                g_S2minVolt = g_IDVolt[i];
            }
        }
	}
}

/**
****************************************************************************
* @brief MasterChargeProtect
*
* handle chg protect flags for parallel packs
*
* @param NONE
* @return NONE
*****************************************************************************
*/
static void MasterChargeProtect(void)
{
	static uint16_t g_IDCHGprotectTimer[MAX_PARALLEL_COUNT] = {0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u};
	static uint16_t g_IDCHGrecoverTimer[MAX_PARALLEL_COUNT] = {0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u};
	static uint16_t g_IDCHGPauseTimer = 0u;
    
    uint8_t i;
    
	for(i = 0u; i < MAX_PARALLEL_COUNT; i++)
	{
		
        if ((g_IDFETstate[i].byte & 0x80u) == 0x80u)
		{
			if (++g_IDCHGprotectTimer[i] >= TIMER5MS_1S)
			{
				g_IDCHGProtect[i] = 1u;
			}
		}
		else
		{
			g_IDCHGprotectTimer[i] = 0u;
		}
        
		if ((g_IDCHGProtect[i] == 1u) && ((g_IDFETstate[i].byte & 0x80u) == 0u))
		{
			g_IDCHGrecoverTimer[i]++;
            
			if (g_IDCHGrecoverTimer[i] >= TIMER5MS_1S)
			{
				g_IDchargePause = 1u; 
				g_IDCHGProtect[i] = 0u;
			}
		}
		else
		{
			g_IDCHGrecoverTimer[i] = 0u;
		}
	}
    
    
    for(i = 0u; i < MAX_PARALLEL_COUNT; i++)
    {
        if ((g_IDchargePause == 0u) || ((g_IDFETstate[i].byte & 0x0fu) != 0u))
        {
            break;
        }                     
    }
    if (i >= MAX_PARALLEL_COUNT)
    {
        if (++g_IDCHGPauseTimer >= TIMER5MS_5S)
		{
			g_IDchargePause = 0u;
			g_chargeMode = STANDBY_MODE;
		}
    }
    else
    {
        g_IDCHGPauseTimer = 0u;
    }
}

/**
****************************************************************************
* @brief ChargeControlFunction
*
* - handle charge/pre-charge flags for parallel packs
* - calculate charge voltage and current
*
* @param NONE
* @return NONE
*****************************************************************************
*/
static void ChargeControlFunction(void)
{  
    static uint16_t g_Cell1DisFETdebounce[MAX_PARALLEL_COUNT] = {0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u};
    static uint16_t g_Cell2DisFETdebounce[MAX_PARALLEL_COUNT] = {0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u};
	static uint8_t g_IDChargeEN[MAX_PARALLEL_COUNT] = {0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u};
	static uint16_t s_minCell1SCHGgVolt = 0u;
    static uint16_t s_minCell2SCHGgVolt = 0u;
    static uint8_t s_cell1SbyPassEn = 0u;
    static uint8_t s_cell2SbyPassEn = 0u;
    static uint8_t s_cell1DSGCHGDisEn = 0u;
    static uint8_t s_cell2DSGCHGDisEn = 0u;
    
	uint8_t i;
	uint16_t s_tempCell1SVolt = 0u;
    uint16_t s_tempCell2SVolt = 0u;

    static uint16_t s_debounceCell1DSGCHGDisEn = 0u;
    static uint16_t s_debounceCell2DSGCHGDisEn = 0u;
    static uint8_t s_flagDisableCell1BQFET = 0u;
    static uint8_t s_flagDisableCell2BQFET = 0u;
    
    static uint8_t cellPosition = 0u;
    static uint8_t s_flagOnePosition = 0u;
	static uint8_t g_chargerNUM = 1u;

	uint16_t g_tempchargeCurr;
    uint16_t g_tempchargeVolt;	
	uint16_t g_tempVolt = 0u;
	uint8_t flagPrecharge1S = 0;
	uint8_t flagPrecharge2S = 0;
    
    for(i = 0u; i < MAX_PARALLEL_COUNT; i++)
    {
        if (((g_IDChargeMode[i] >= PRE_CHARGE_MODE) && (g_IDChargeMode[i] <= TERMINATION_MODE))
            && (g_IDCHGProtect[i] == 0u))
        {
            g_IDChargeEN[i] = 1u;	
        }
        else
        {
            g_IDChargeEN[i] = 0u;
        }
    }
    
    if ((g_IDchargePause == 1u) || (g_chargeMode == PRE_DISCHARGE2_MODE))
	{
		for(i = 0u; i < MAX_PARALLEL_COUNT; i++)
        {
            g_IDChargeEN[i] = 0u;
        }
	}
    
     for(i = 0u; i < MAX_PARALLEL_COUNT; i++)
     {
         if ((g_IDChargeEN[i] == 1u) && (g_IDChargeMode[i] != TERMINATION_MODE) && (g_IDCellS[i] == CELL_1S_POSITION))
         {
             break;  
         }
     }
    if (i >= MAX_PARALLEL_COUNT)
    {
        s_cell1SbyPassEn = 1u;
    }
    else
    {
        s_cell1SbyPassEn = 0u;
    }
    
    for(i = 0u; i < MAX_PARALLEL_COUNT; i++)
     {
         if ((g_IDChargeEN[i] == 1u) && (g_IDChargeMode[i] != TERMINATION_MODE) && (g_IDCellS[i] == CELL_2S_POSITION))
         {
             break;
         }
     }
    if (i >= MAX_PARALLEL_COUNT)
    {
        s_cell2SbyPassEn = 1u;
    }
    else
    {
        s_cell2SbyPassEn = 0u;
    }
     
    if ((s_cell1SbyPassEn == 1u) && (s_cell2SbyPassEn == 1u))
    {
        s_cell1SbyPassEn = 0u;
        s_cell2SbyPassEn = 0u;
    }

    for(i = 0u; i < MAX_PARALLEL_COUNT; i++)
	{		
        if ((g_IDChargeMode[i] == PRE_CHARGE_MODE) && (g_IDChargeEN[i] == 1u))
        {
            
            if(g_IDCellS[i]==CELL_1S_POSITION)
            {
				flagPrecharge1S = 1;
			}
			else if(g_IDCellS[i]==CELL_2S_POSITION)
            {
				flagPrecharge2S = 1;
			}
        }
    }
    
    
    
    
    
    
    
    
    {
        
        
        for(i = 0u; i < MAX_PARALLEL_COUNT; i++)
        {
            
            if (g_IDCellS[i] == CELL_1S_POSITION)
            {
            	if(flagPrecharge1S == 1)
            	{
					g_IDFETEnCH[i].byte = 0x08u; 
				}
				else
				{
	            	g_IDFETEnCH[i].byte &= 0xf7u; 
	                if ((g_IDChargeEN[i] == 1u) && (s_cell1SbyPassEn == 0u))
	                {
	                    g_IDFETEnCH[i].byte |= 0x05u;	
	                }
	                else
	                {
	                    g_IDFETEnCH[i].byte = 0x00u;
	                }
                }
            }
            else if (g_IDCellS[i] == CELL_2S_POSITION)
            {
            	if(flagPrecharge2S == 1)
            	{
					g_IDFETEnCH[i].byte = 0x08u; 
				}
				else
				{            
	            	g_IDFETEnCH[i].byte &= 0xf7u; 
	                if ((g_IDChargeEN[i] == 1u) && (s_cell2SbyPassEn == 0u))
	                {
	                    g_IDFETEnCH[i].byte |= 0x05u;	
	                }
	                else
	                {
	                    g_IDFETEnCH[i].byte = 0x00u;
	                }
				}
            }
            else
            {
                g_IDFETEnCH[i].byte = 0x00u;
            }
        }
        s_minCell1SCHGgVolt = ABS_30V;
        s_minCell2SCHGgVolt = ABS_30V;
        s_tempCell1SVolt = ABS_30V;
        s_tempCell2SVolt = ABS_30V;
        for(i = 0u; i < MAX_PARALLEL_COUNT; i++)
        {
            if ((g_IDChargeEN[i] == 1u) && (s_tempCell1SVolt >= g_IDVolt[i]) && (g_IDCellS[i] == CELL_1S_POSITION))
            {
                s_tempCell1SVolt = g_IDVolt[i];
            }
            if ((g_IDChargeEN[i] == 1u) && (s_tempCell2SVolt >= g_IDVolt[i]) && (g_IDCellS[i] == CELL_2S_POSITION))
            {
                s_tempCell2SVolt = g_IDVolt[i];
            }
            if (((g_IDFETstate[i].byte & 0x02u) == 0x02u) && (s_minCell1SCHGgVolt >= g_IDVolt[i]) && (g_IDCellS[i] == CELL_1S_POSITION))
            {
                s_minCell1SCHGgVolt = g_IDVolt[i];
            }
            if (((g_IDFETstate[i].byte & 0x02u) == 0x02u) && (s_minCell2SCHGgVolt >= g_IDVolt[i]) && (g_IDCellS[i] == CELL_2S_POSITION))
            {
                s_minCell2SCHGgVolt = g_IDVolt[i];
            }
        }
        if (s_minCell1SCHGgVolt == ABS_30V)
        {
            s_minCell1SCHGgVolt = s_tempCell1SVolt;
        }	
        if (s_minCell2SCHGgVolt == ABS_30V)
        {
            s_minCell2SCHGgVolt = s_tempCell2SVolt;
        }
        for(i = 0u; i < MAX_PARALLEL_COUNT; i++)
        {
            
            if (((g_IDFETstate[i].byte & 0x02u) == 0x02u) && (g_IDChargeEN[i] == 1u))
            {
                g_IDFETEnCH[i].byte |= 0x02u;
            }
            
            if (((g_IDFETstate[i].byte & 0x02u) != 0x02u) && (g_IDChargeEN[i] == 1u) && (g_IDCellS[i] == CELL_1S_POSITION)
                    && (g_IDVolt[i] <= s_tempCell1SVolt) &&  (g_IDChargeMode[i] >= FAST_CHARGE_MODE) && (flagPrecharge1S == 0u))
            {
                if (++g_Cell1DisFETdebounce[i] >= TIMER5MS_30S)
                {
                    g_IDFETEnCH[i].byte |= 0x02u;
                }
            }
            else
            {
                g_Cell1DisFETdebounce[i] = 0u;
            }
            if (((g_IDFETstate[i].byte & 0x02u) != 0x02u) && (g_IDChargeEN[i] == 1u) && (g_IDCellS[i] == CELL_2S_POSITION)
                    && (g_IDVolt[i] <= s_tempCell2SVolt) &&  (g_IDChargeMode[i] >= FAST_CHARGE_MODE) && (flagPrecharge2S == 0u))
            {
                if (++g_Cell2DisFETdebounce[i] >= TIMER5MS_30S)
                {
                    g_IDFETEnCH[i].byte |= 0x02u;
                }
            }
            else
            {
                g_Cell2DisFETdebounce[i] = 0u;
            }
        }
		
    }
    
    
    for(i = 0u; i < MAX_PARALLEL_COUNT; i++)
    {
        if ((g_IDbqFetState[i] != 0u) && (g_IDCellS[i] == CELL_1S_POSITION))
        {
            break;
        }
    }
    if (i >= MAX_PARALLEL_COUNT)
    {
        if (++s_debounceCell1DSGCHGDisEn >= TIMER5MS_2S)
        {
            s_cell1DSGCHGDisEn = 1u;
            g_flagCell1DSGCHGDisEn = 1u;
        }
    }
    else
    {
        s_debounceCell1DSGCHGDisEn = 0u;
        s_cell1DSGCHGDisEn = 0u;
        g_flagCell1DSGCHGDisEn = 0u;
    }
    
    for(i = 0u; i < MAX_PARALLEL_COUNT; i++)
    {
        if ((g_IDbqFetState[i] != 0u) && (g_IDCellS[i] == CELL_2S_POSITION))
        {
            break;
        }
    }
    if (i >= MAX_PARALLEL_COUNT)
    {
        if (++s_debounceCell2DSGCHGDisEn >= TIMER5MS_2S)
        {
            s_cell2DSGCHGDisEn = 1u;
            g_flagCell2DSGCHGDisEn = 1u;
        }
    }
    else
    {
        s_debounceCell2DSGCHGDisEn = 0u;
        s_cell2DSGCHGDisEn = 0u;
        g_flagCell2DSGCHGDisEn = 0u;
    }
    
    for(i = 0u; i < MAX_PARALLEL_COUNT; i++)
    {
        if ((s_cell1SbyPassEn == 1u) && (g_IDCellS[i] == CELL_1S_POSITION))
        {
            if (s_cell1DSGCHGDisEn == 1u) 
            {
                g_IDFETEnCH[i].byte = 0x10u;
            }
            else
            {
                g_IDFETEnCH[i].byte = 0x00u;
            }
        }
        else if ((s_cell2SbyPassEn == 1u) && (g_IDCellS[i] == CELL_2S_POSITION))
        {
            if (s_cell2DSGCHGDisEn == 1u) 
            {
                g_IDFETEnCH[i].byte = 0x10u;
            }
            else
            {
                g_IDFETEnCH[i].byte = 0x00u;
            }
        }
        else
        {
            g_IDFETEnCH[i].byte &= 0xefu;
        }
    }
    
    if ((s_cell1SbyPassEn == 1u) && (g_flagCell1DSGCHGDisEn == 1u))
    {
        g_flagCell1BypassEN = 1u;
    }
    else
    {
        g_flagCell1BypassEN = 0u;
    }
    if ((s_cell2SbyPassEn == 1u) && (g_flagCell2DSGCHGDisEn == 1u))
    {
        g_flagCell2BypassEN = 1u;
    }
    else
    {
        g_flagCell2BypassEN = 0u;
    }
    
    cellPosition = g_IDCellS[0];
    for(i = 1u; i < MAX_PARALLEL_COUNT; i++)
    {
        if (((g_IDCellS[i] == CELL_1S_POSITION) || (g_IDCellS[i] == CELL_2S_POSITION)) && (g_IDCellS[i] != cellPosition))
        {
            break;
        }
    }
    if (i >= MAX_PARALLEL_COUNT)
    {
        s_flagOnePosition = 1u;
    }
    else
    {
        s_flagOnePosition = 0u;
    }
    for(i = 0u; i < MAX_PARALLEL_COUNT; i++)
    {
        g_IDFETEnCH[i].byte &= 0xdfu;
        if ((g_flagCell1BypassEN == 1u) && (g_flagCell2BypassEN == 0u) && (s_flagOnePosition == 0u))
        {      
            if ((g_IDChargeEN[i] == 1u) && (g_IDCellS[i] == CELL_2S_POSITION))
            {
                g_IDFETEnCH[i].byte = 0x28u; 
            }
        }
        if ((g_flagCell1BypassEN == 0u) && (g_flagCell2BypassEN == 1u) && (s_flagOnePosition == 0u))
        {      
            if ((g_IDChargeEN[i] == 1u) && (g_IDCellS[i] == CELL_1S_POSITION))
            {
                g_IDFETEnCH[i].byte = 0x28u;
            }
        }
    }
    
    for(i = 0u; i < MAX_PARALLEL_COUNT; i++)
    {
        if (((g_IDFETstate[i].byte & 0x10u) == 0x10u) && (g_IDCellS[i] == CELL_1S_POSITION))
        {
            break;
        }
    }
    if (i >= MAX_PARALLEL_COUNT)
    {
        s_flagDisableCell1BQFET = 0u;
    }
    else
    {
        s_flagDisableCell1BQFET = 1u;
    }
    for(i = 0u; i < MAX_PARALLEL_COUNT; i++)
    {
        if (((g_IDFETstate[i].byte & 0x10u) == 0x10u) && (g_IDCellS[i] == CELL_2S_POSITION))
        {
            break;
        }
    }
    if (i >= MAX_PARALLEL_COUNT)
    {
        s_flagDisableCell2BQFET = 0u;
    }
    else
    {
        s_flagDisableCell2BQFET = 1u;
    }
    for(i = 0u; i < MAX_PARALLEL_COUNT; i++)
    {
        if ((s_flagDisableCell1BQFET == 1u) && (g_IDCellS[i] == CELL_1S_POSITION))
        {
            g_IDFETEnCH[i].byte &= 0xf8u;
        }
        if ((s_flagDisableCell2BQFET == 1u) && (g_IDCellS[i] == CELL_2S_POSITION))
        {
            g_IDFETEnCH[i].byte &= 0xf8u;
        }
    }

	
    
    
	
	g_preChargeNUM = 0u;
	g_fastChargeNUM = 0u;
	g_chargeLeastEN = 0u;
    
	for(i = 0u; i < MAX_PARALLEL_COUNT; i++)
	{	
    
        if (g_IDChargeEN[i] == 1u)
		{
			g_chargeLeastEN = 1u;
		}
		if ((g_IDFETstate[i].byte & 0x02u) == 0x02u)
		{
			if (g_IDChargeMode[i] == PRE_CHARGE_MODE)
			{
				g_preChargeNUM++;
			}
			else
			{
				g_fastChargeNUM++;
			}
		}
	}
	
    if ((g_chargeLeastEN == 1u) && (g_preChargeNUM == 0u) && (g_fastChargeNUM == 0u))
    {
        g_preChargeNUM = 1u;
    }

	
	if (((uint16_t)g_preChargeNUM + (uint16_t)g_fastChargeNUM) == 0u)
	{
		
          
        g_tempchargeCurr = 0u;
        g_tempchargeVolt = 0u;
        
    }
	else
	{
        
        if ( g_preChargeNUM != 0u)
		{
			g_tempchargeCurr = DELTA_Q_CHG_7A5 * g_preChargeNUM;
		}
		else
		{
			g_tempchargeCurr = DELTA_Q_CHG_20A * g_fastChargeNUM;
		}
		g_tempchargeVolt = DELTA_Q_CHG_28V;
	}
	if ((g_IDchargePause == 1u) || (g_chargeMode == PRE_DISCHARGE2_MODE) || (g_chargeMode == TERMINATION_MODE))
	{
		g_tempchargeCurr = 0u;
        g_tempchargeVolt = 0u;
	}
    g_tempchargeCurr = DELTA_Q_CHG_0A + (g_tempchargeCurr / g_chargerNUM);
	if (g_tempchargeCurr >= (DELTA_Q_CHG_33A + DELTA_Q_CHG_0A))
	{
		g_chargeCurr = (DELTA_Q_CHG_33A + DELTA_Q_CHG_0A);
	}
	else
	{
		g_chargeCurr = g_tempchargeCurr;
	}
    g_chargeVolt = g_tempchargeVolt;
}

/**
****************************************************************************
* @brief DischargeControlFunction
*
* - handle dsg protect/en flags for parallel packs
* 
* @param NONE
* @return NONE
*****************************************************************************
*/
void DischargeControlFunction(void)
{
	uint8_t i;	
    uint16_t g_maxDsgVolt;
    static uint8_t g_IDdischargeEN[MAX_PARALLEL_COUNT] = {0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u};
    static uint8_t g_IDDSGProtect[MAX_PARALLEL_COUNT] = {0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u};
    static uint16_t g_IDDSGprotectTimer[MAX_PARALLEL_COUNT] = {0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u};
    static uint16_t g_IDDSGrecoverTimer[MAX_PARALLEL_COUNT] = {0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u};
    static uint16_t g_forceTurnOnOrDebounce[MAX_PARALLEL_COUNT] = {0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u};
    
	
    for(i = 0u; i < MAX_PARALLEL_COUNT; i++)
	{
        if ((g_IDFETstate[i].byte & 0x40u) == 0x40u)
		{
			if (++g_IDDSGprotectTimer[i] >= TIMER5MS_1S)
			{
				g_IDDSGProtect[i] = 1u;
			}
		}
		else
		{
			g_IDDSGprotectTimer[i] = 0u;
		}

		if ((g_IDDSGProtect[i] == 1u) && ((g_IDFETstate[i].byte & 0x40u) == 0u))
		{
			if (++g_IDDSGrecoverTimer[i] >= TIMER5MS_1S)
			{
				g_IDDSGProtect[i] = 0u;
			}
		}
		else
		{
			g_IDDSGrecoverTimer[i] = 0u;
		}
	}
    
	
    for(i = 0u; i < MAX_PARALLEL_COUNT; i++)
    {
        if ((g_IDChargeMode[i] == DISCHARGE_MODE) && (g_IDDSGProtect[i] == 0u))
        {
            g_IDdischargeEN[i] = 1u;	
        }
        else
        {
            g_IDdischargeEN[i] = 0u;	
        }
    }
    
	
	for(i = 0u; i < MAX_PARALLEL_COUNT; i++)
	{
		if (g_IDdischargeEN[i] == 1u)
		{
			g_IDFETEnDisCH[i].byte |= 0x06u;	
		}
		else
		{
			g_IDFETEnDisCH[i].byte = 0x00u;
		}
	}

	g_maxDsgS1Volt = 0u;
    g_maxDsgS2Volt = 0u;
	for(i = 0u; i < MAX_PARALLEL_COUNT; i++)
	{
        if (g_IDCellS[i] == CELL_1S_POSITION)
        {
            if ((g_IDdischargeEN[i] == 1u) && (g_maxDsgS1Volt < g_IDVolt[i]))
            {
                g_maxDsgS1Volt = g_IDVolt[i];	
            }
        }
        if (g_IDCellS[i] == CELL_2S_POSITION)
        {
            if ((g_IDdischargeEN[i] == 1u) && (g_maxDsgS2Volt < g_IDVolt[i]))
            {
                g_maxDsgS2Volt = g_IDVolt[i];	
            }
        }
	}
	for(i = 0u; i < MAX_PARALLEL_COUNT; i++)
	{
		if (((g_IDFETstate[i].byte & 0x01u) == 0x01u) && (g_IDdischargeEN[i] == 1u))
		{
			g_IDFETEnDisCH[i].byte |= 0x01u;	
        }
        g_maxDsgVolt = 0u;
        if (g_IDCellS[i] == CELL_1S_POSITION)
        {
            g_maxDsgVolt = g_maxDsgS1Volt;	
        }
        if (g_IDCellS[i] == CELL_2S_POSITION)
        {
            g_maxDsgVolt = g_maxDsgS2Volt;	
        }
        if (((g_IDFETstate[i].byte & 0x01u) != 0x01u)  && (g_IDdischargeEN[i] == 1u) && ((g_IDVolt[i] + ABS_0V5) >= g_maxDsgVolt) && (g_maxDsgVolt != 0u))
        {
            if (++g_forceTurnOnOrDebounce[i] >= TIMER5MS_3S)   
            {
                g_IDFETEnDisCH[i].byte |= 0x01u;
            }
        }
        else
        {
            g_forceTurnOnOrDebounce[i] = 0u;
        }
	}
}


/**
****************************************************************************
* @brief Protection
*
* propagate protection flags from BQ8050 into fault flags 
* 
* @param NONE
* @return NONE
*****************************************************************************
*/
void Protection (void)

{  
    static uint16_t s_communicationFailDebounce = 0u;
    static uint16_t s_communicationSucceedDebounce = 0u;
    static uint16_t s_bq8050ChgFetOffTimer = 0u;
    static uint16_t s_bq8050ChgFetOnTimer = 0u;
    static uint16_t s_bq8050DsgFetOffTimer = 0u;
    static uint16_t s_bq8050DsgFetOnTimer = 0u;
     
    
    if (g_flagCommFail == 1u)
    {
        s_communicationSucceedDebounce = 0u;
        if (++s_communicationFailDebounce >= TIMER5MS_6S)
        {
            g_flagChgComFail = 1u;
            g_flagDsgComFail = 1u;
        }
    }
    else
    {
        s_communicationFailDebounce = 0u;
        if (++s_communicationSucceedDebounce >= TIMER5MS_10S)  
        {
            g_flagChgComFail = 0u;
            g_flagDsgComFail = 0u;
        }
    }
    
    
    if ((((g_8050ChgProtect2.word & MASK_8050CHGPRO2) != 0u) || ((g_8050ChgProtect1.word & MASK_8050CHGPRO1) != 0u)) && (g_flagChgComFail == 0u))
    {
        if (++s_bq8050ChgFetOffTimer >= TIMER5MS_1S)
        {
            g_flag8050ChgProtect = 1u;
        }
    }
    else
    {
        s_bq8050ChgFetOffTimer = 0u;
    }
    if (((g_8050ChgProtect2.word & MASK_8050CHGPRO2) == 0u) && ((g_8050ChgProtect1.word & MASK_8050CHGPRO1) == 0u) && (g_flagChgComFail == 0u))
    {
        if (++s_bq8050ChgFetOnTimer >= TIMER5MS_1S)
        {
            g_flag8050ChgProtect = 0u;
        }
    }
    else
    {
        s_bq8050ChgFetOnTimer = 0u;
    }
    
    if (g_flag8050ChgProtect == 1u)
    {  
        if ((g_8050ChgOCCAFE == 1u) || (g_8050ChgOCDAFE == 1u) || (g_8050ChgSCDAFE == 1u) || (g_8050Bq76940ChgHWSC))
        {
            g_flagChgShortCircuit = 1u;
        }
        else if ((g_8050OCC == 1u) || (g_8050ChgOCC2 == 1u))
        {
            g_flagOCP = 1u;
        }
        else if (g_8050OCV == 1u)
        {
            g_flagOVP = 1u;
        }
        else if ((g_8050CHT == 1u) || (g_8050CELLCHT == 1))       
        {
            g_flagCHT = 1u;
        }
        else if ((g_8050CLT == 1u) || (g_8050CELLCLT == 1))         
        {
            g_flagCLT = 1u;
        }
		else if (g_8050ChgSUV == 1u)    
        {
            g_flagChgSUVP = 1u;
        }
		else if (g_8050CHT_MOS == 1u)   
        {
            g_flagCHT_MOS = 1u;
        }
		else if (g_8050Bq76940ChgComFail == 1u)  
        {
            g_flagChgAFEComFail = 1u;
        }
        else
        {
            g_flagChgOtherProtect = 1u;    
        }
    }
    else
    {
        g_flagPicChgProtect.word &= 0xff00u;
    }
    
    if (g_flagPicChgProtect.word == 0u)
    {
        g_flagCHGPROTECT = 0u;
    }
    else
    {
        g_flagCHGPROTECT = 1u;
    }
        
    
    if ((((g_8050DsgProtect2.word & MASK_8050DSGPRO2) != 0u) || ((g_8050DsgProtect1.word & MASK_8050DSGPRO1) != 0u)) && (g_flagDsgComFail == 0u))
    {
        if (++s_bq8050DsgFetOffTimer >= TIMER5MS_1S)
        {
            g_flag8050DsgProtect = 1u;
        }
    }
    else
    {
        s_bq8050DsgFetOffTimer = 0u;
    }
    if ((((g_8050DsgProtect2.word & MASK_8050DSGPRO2) == 0u) && ((g_8050DsgProtect1.word & MASK_8050DSGPRO1) == 0u)) && (g_flagDsgComFail == 0u))
    {
        if (++s_bq8050DsgFetOnTimer >= TIMER5MS_1S)
        {
            g_flag8050DsgProtect = 0u;
        }
    }
    else
    {
        s_bq8050DsgFetOnTimer = 0u;
    }
    
    if (g_flag8050DsgProtect == 1u)
    {     
        if ((g_8050DsgOCCAFE == 1u) || (g_8050DsgSCDAFE == 1u) || (g_8050DHWS == 1))
        {
            g_flagDsgShortCircuit = 1u;
        }
        else if ((g_8050OCD == 1u) || (g_8050OCD2 == 1u)) 
        {
            g_flagOCD = 1u;
        }
        else if (g_8050CUV == 1u)
        {
            g_flagCUV = 1u;
        }
        else if ((g_8050DHT == 1u) || (g_8050CELLDHT == 1))  
        {
            g_flagDHT = 1u;
        }
        else if ((g_8050DLT == 1u) || (g_8050CELLDLT == 1))  
        {
            g_flagDLT = 1u;
        }
		else if (g_8050DsgSUV == 1u)    
        {
            g_flagDsgSUVP = 1u;
        }
		else if (g_8050DHT_MOS == 1u)   
        {
            g_flagDHT_MOS = 1u;
        }
		else if (g_8050Bq76940DsgComFail == 1u)  
        {
            g_flagDsgAFEComFail = 1u;
        }
        else
        {
            g_flagDsgOtherProtect = 1u;    
        }
    }
    
    if ((g_chgDsgControl == NO_CHG_DSG) && (g_flag8050DsgProtect == 0u))
    {
        g_flagDSGPROTECT = 0u;
        g_flagPicDsgProtect.word &= 0xff00u;
    }
    
    if (g_flagPicDsgProtect.word != 0u)
    {
        g_flagDSGPROTECT = 1u;
    }  

}



/**
****************************************************************************
* @brief ChargeModeFunction
*
* - determine chargeMode from chgDsgControl flag
* - handle transitions between prechg, fast chg, and termination modes
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void ChargeModeFunction(void)
{
    static uint16_t s_fastChargeModeDebounce = 0u;
    static uint16_t s_terminationToFascChgTimer = 0u; 
    static uint16_t s_terminationTimer = 0u;
    static uint32_t s_debouncePreChargeOvf = 0u;

	if (g_chgDsgControl == CHARGE_EN)   
	{
		if (g_chargeMode < PRE_CHARGE_MODE)
        { 
        	g_chargeMode = PRE_CHARGE_MODE;
        }      
	}
	else if (g_chgDsgControl == DISCHARGE_EN)  
	{
		if (g_chargeMode != DISCHARGE_MODE)
		{
			g_chargeMode = DISCHARGE_MODE;
		}
	}
	else
	{
		g_chargeMode = STANDBY_MODE;
		
        g_flagPreChargeOvf = 0u;
	}
	
    if (g_flagCompleteAddrAllocate == 0u)
    {
        g_chargeMode = STANDBY_MODE;
    }
    
	
    if ((g_chargeMode == PRE_CHARGE_MODE) && (g_minCellVolt >= ABS_2V5))
    {
        if (++s_fastChargeModeDebounce >= TIMER5MS_2S)
        {
            g_chargeMode = FAST_CHARGE_MODE;
        }
    }
    else
    {
        s_fastChargeModeDebounce = 0u;
    }

    
    if (g_chargeMode == PRE_CHARGE_MODE)
    {
        if (g_flagChgCurr == 1u)
        {
            if (++s_debouncePreChargeOvf >= TIMER5MS_1H)
            {
                g_flagPreChargeOvf = 1u;
            }
        }
    }
    else
    {
        s_debouncePreChargeOvf = 0u;
    }
	
    
    if ((g_chargeMode == FAST_CHARGE_MODE) && (g_flag8050FC != 0u))
    {
        if (++s_terminationTimer >= TIMER5MS_5S)
        {
            g_chargeMode = TERMINATION_MODE;
        }
    }
    else
    {
        s_terminationTimer = 0u;
    } 
    
    if ((g_chargeMode == TERMINATION_MODE) && (g_flag8050FC == 0u))
    {
        if (++s_terminationToFascChgTimer >= TIMER5MS_5S)
        {
            g_chargeMode = FAST_CHARGE_MODE;
        }
    }
    else
    {
        s_terminationToFascChgTimer = 0u;
    } 
}

/**
****************************************************************************
* @brief FetControlFunction
*
* - read flags and propagate to FET control
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void FetControlFunction(void)
{
    static uint16_t s_disPreDsgFetTime = 0u;
    BULK_CAP_CHG_OutputEnable();
    ORD_DSG_OutputEnable();  
    BUCK_EN_OutputEnable();
    BYPASS_CTR_OutputEnable();
    
    if (g_flagDsgCurr == 0u)
    {
        if (++s_disPreDsgFetTime >= TIMER5MS_30S)
        {
            g_flagdisPreDsgFet = 0u;
        }
    }
    else
    {
        g_flagdisPreDsgFet = 1u;
        s_disPreDsgFetTime = 0u;
    }
    if ((g_8050DsgProtect1.word == 0u) && (g_8050DsgProtect2.word == 0u) 
            && (g_flagdisPreDsgFet == 0u) && (g_chgDsgControl != CHARGE_EN) && ( g_systemError.byte == 0u))
    {
        BULK_CAP_CHG_Set();
        g_flagPreDSGFET = 1u;
    }
    else
    {
        BULK_CAP_CHG_Clear();
        g_flagPreDSGFET = 0u;
    }

    if (((g_chargeMode == PRE_CHARGE_MODE) || (g_flagForcePreChgEn == 1u)) && (g_flagPicChgProtect.word == 0u) && (g_flagPreChgFetEn == 1u) && (g_testPicOrFetOff == 0u))
    {
        BUCK_EN_Set();
        g_flagPreChgFET = 1u;
        g_flagPWMOn = 1u; 
    }
    else
    {
        BUCK_EN_Clear();
        g_flagPreChgFET = 0u;
        g_flagPWMOn = 0u; 
    }
	
    s_flagChgFetEn = 0u;
	if ((g_chargeMode >= FAST_CHARGE_MODE) && (g_chargeMode <= TERMINATION_MODE))  
	{
		if ((g_flagPicChgProtect.word == 0u) && (g_flagCHGFETEnCH == 1u))
		{
            s_flagChgFetEn = 1u;
		}
		else
		{	
            s_flagChgFetEn = 0u;
		}
	}
	else if(g_chargeMode == DISCHARGE_MODE)
	{
		if ((g_flagDSGPROTECT == 0u) && (g_flagCHGFETEnDisCH == 1u))
		{
            s_flagChgFetEn = 1u;
		}
		else
		{            
            s_flagChgFetEn = 0u;
		}
	}
    if ((s_flagChgFetEn == 1u) && (g_testPicChgFetOff == 0u))
    {
        g_flagCHGFET = 1u;
        g_controlBqFet.bytes.CHGbyte = FET_ON;
    }
    else
    {
        g_flagCHGFET = 0u;
        g_controlBqFet.bytes.CHGbyte = FET_OFF;
    }
         
	
    s_flagOrFetEn = 0u;
	if ((g_chargeMode >= FAST_CHARGE_MODE) && (g_chargeMode <= TERMINATION_MODE))  
	{
		if ((g_flagPicChgProtect.word == 0u) && (g_flagORFETEnCH == 1u))
		{			
            s_flagOrFetEn = 1u;
		}
		else
		{
            s_flagOrFetEn = 0u;
		}
	}
	else if(g_chargeMode == DISCHARGE_MODE)
	{
		if ((g_flagDSGPROTECT == 0u) && ((g_flagORFETEnDisCH == 1u) || (g_flagDsgCurr == 1u)))
		{
            s_flagOrFetEn = 1u;
		}
        else
        {
            s_flagOrFetEn = 0u;
        }
	}
    
   
    
   if ((s_flagOrFetEn == 1u) && (g_testPicOrFetOff == 0u))
    {
        g_flagORFET = 1u;
        ORD_DSG_Set();
    }
    else
    {
        g_flagORFET = 0u;
        ORD_DSG_Clear();
    }

	
    s_flagDsgFetEn = 0u;
	if ((g_chargeMode >= PRE_CHARGE_MODE) && (g_chargeMode <= TERMINATION_MODE))  
	{
		if ((g_flagPicChgProtect.word == 0u) && ((g_flagDSGFETEnCH == 1u) || ((g_flagCurrEnDsgFET == 1u) && (g_flagForcePreChgEn == 0u))))
		{
            s_flagDsgFetEn = 1u;
		}
		else
		{
            s_flagDsgFetEn = 0u;
		}
	}
	else if(g_chargeMode == DISCHARGE_MODE)
	{
		if ((g_flagDSGPROTECT == 0u) && (g_flagDSGFETEnDisCH == 1u))
		{
            s_flagDsgFetEn = 1u;
		}
		else
		{
            s_flagDsgFetEn = 0u;
		}
	}
    if ((s_flagDsgFetEn == 1u) && (g_testPicDsgFetOff == 0u))
    {
        g_flagDSGFET = 1u;
        g_controlBqFet.bytes.DSGbyte = FET_ON;
    }
    else
    {
        g_flagDSGFET = 0u;
        g_controlBqFet.bytes.DSGbyte = FET_OFF;	    
    }
    
    s_flagBypassFetEn = 0u;
    if (g_chargeMode >= PRE_CHARGE_MODE)
	{
		if (g_flagByPassFETEn == 1u)
		{
            s_flagBypassFetEn = 1u;
		}
		else
		{
            s_flagBypassFetEn = 0u;
		}
	}
    
    if ((s_flagBypassFetEn == 1u) && (g_testPicByPassFetOff == 0u))
    {
        g_flagByPassFET = 1u;
        BYPASS_CTR_Set();
    }
    else
    {
        g_flagByPassFET = 0u;
        BYPASS_CTR_Clear();  
    }
}
/* Inventus Power Proprietary */
