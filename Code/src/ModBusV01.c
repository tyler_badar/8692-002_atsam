/**
 * @file  ModBusV01.c
 *
 * REVISION HISTORY
 *==============================================
 * |Ticket   |Date      |Author       |Notes
 * |----:    |:----:    |:----:       |:----
 * |Creation |02/04/22  |Tom O'Connor |Document
 *
 * @par   COPYRIGHT NOTICE: (c) 2022 Inventus Power.  All rights reserved.
 */
/******************************************************************/
#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes
#include "common_defineV01.h"
#include "mainV01.h"
#include "rs232V01.h"
#include "i2cMasterV02.h"
#include "canV01.h"
#include "adcV01.h"
#include "temperatureV01.h"
#include "detectV01.h"
#include "CANopenV01.h"
#include "ModBusV01.h"

static uint8_t ModBusSlaveAddrDetect(void);
static uint8_t ModBusFunctionCodeDetect(void);
static uint8_t ModbusReadBits(void);
static uint8_t ModBusWriteBit(void);
static uint8_t ModBusWriteBits(void);
static uint8_t ModBusReadBlock(void);
static uint8_t ModBusWriteWord(void);
static void ModBusErrProcessing(void);
static uint8_t CalculateLRC(uint8_t length, uint8_t *LrcArrary);
static uint8_t ReadBitsQuantityDetect(void);
static uint8_t ReadBitsRegisterDetect(void);
static uint8_t ReadBitsProcessing(void);
static uint8_t WriteBitDataDetect(void);
static uint8_t WriteBitRegisterDetect(void);
static uint8_t WriteBitProcessing(void);
static uint8_t WriteBitsQuantityDetect(void);
static uint8_t WriteBitsRegisterDetect(void);
static uint8_t WriteBitsProcessing(void);
static uint8_t ReadBlockQuantityDetect(void);
static uint8_t ReadBlockRegisterDetect(void);
static uint8_t ReadBlockProcessing(void);
static uint8_t WriteWordDataDetect(void);
static uint8_t WriteWordRegisterDetect(void);
static uint8_t WriteWordProcessing(void);

uint8_t g_RXbuff[RX_BUFF_LENGTH];

uint8_t g_modbusRXbuff[RX_BUFF_LENGTH];
uint8_t g_modbusTXbuff[TX_BUFF_LENGTH];
uint8_t g_modbusRXhexBuff[RX_BUFF_LENGTH];
uint8_t g_modbusTXhexBuff[TX_BUFF_LENGTH];

uint8_t g_TxHexLength;
uint8_t g_TxDataLength;
uint16_t g_timerTx = 0u;
uint8_t g_RxBuffP = 0;
static bool g_flagTxEn;
static bool g_flagTxBusy;
uint8_t g_RxLength;
uint8_t ModBus_CMD;

static uint8_t const s_asciiTable[] = {0x30u, 0x31u, 0x32u, 0x33u,0x34u, 0x35u, 0x36u, 0x37u,0x38u, 0x39u, 0x41u, 0x42u, 0x43u, 0x44u, 0x45u, 0x46u};

static uint16_t s_timerReceive = 0u;

uint8_t g_reveiveHexBuffLength;

/**
****************************************************************************
* @brief USART1_ISR_RX_Handler
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void USART1_ISR_RX_Handler(void)
{
    uint8_t s_RxTmp;
    uint8_t i;
    s_RxTmp = SERCOM1_REGS->USART_INT.SERCOM_DATA;
    if (s_RxTmp == MOD_BUS_STAR)
    {
        g_RxBuffP = 0u;
    }
    if (g_RxBuffP < RX_BUFF_LENGTH)
    {
        g_RXbuff[g_RxBuffP] = s_RxTmp;
    }
    if ((g_RXbuff[g_RxBuffP - 1] == MOD_BUS_STOP_CR) && (s_RxTmp == MOD_BUS_STOP_LF))
    {
        g_flagTxEn =1u;
        g_RxLength = g_RxBuffP + 1u;
        for (i = 0u; i < g_RxLength; i++)
        {
            g_modbusRXbuff[i] = g_RXbuff[i];
        }
    }  
    g_RxBuffP++;
}


/**
****************************************************************************
* @brief AsciiToHex
*
*  @param [in] *ascii 
*  @param [in]  length
*  @param [out] *buff
*
* @return flag  [PASS | FAIL]
*
*****************************************************************************
*/
uint8_t AsciiToHex(uint8_t *ascii, uint8_t length, uint8_t *buff)
{
    uint8_t flag;
    uint8_t calculateLRC;
    uint8_t *p;
    uint8_t i;
    uint8_t j;
    uint8_t tmpArray[RX_BUFF_LENGTH * 2u];
    flag = PASS;
    p = ascii;
            
    if (length < 13u)  
    {
        flag = FAIL;
    }
    else
    {
        if (((length - 3u) % 2u) != 0u)
        {
            flag = FAIL;
        }
        else
        {
            g_reveiveHexBuffLength = (length - 3u) / 2u;
        }
    }
    
    if (flag == PASS)
    {
        for (i = 1u; i < (length - 2u); i++)
        {
            p++;
            for (j = 0u; j < 16u; j++)
            {
                if (*p == s_asciiTable[j])
                {
                    tmpArray[i - 1u] = j; 
                    break;
                }
            }
            if (j >= 16u)
            {
                flag = FAIL;
                break;
            }
        }
    }
    
    if (flag == PASS)
    {
        p = buff;
        for (i = 0u; i < ((length - 3u) / 2u); i++)
        {
            *p = (uint8_t)(tmpArray[i * 2u] << 4u) + tmpArray[(i * 2u) + 1u];
            p++;
        }
    }
    
    if (flag == PASS)
    {
        p = buff; 
        calculateLRC = 0u;
        
        for (i = 0u; i < ((length - 5u) / 2u); i++)
        { 
            calculateLRC += *p;
            p++;
        }
        calculateLRC = (uint8_t)(~calculateLRC) + 1u;
        if (calculateLRC != *p)
        {
            flag = FAIL;
        }
    }
        
    return flag;
}

/**
****************************************************************************
* @brief HexToAscii
*
*  @param [in] *hex 
*  @param [in]  length
*  @param [out] *buff
*
* @return flag  [PASS | FAIL]
*
*****************************************************************************
*/
uint8_t HexToAscii(uint8_t *hex, uint8_t length, uint8_t *buff)
{
    uint8_t tmpAsciiBuffLength;
    uint8_t *p;
    uint8_t tmpArray[TX_BUFF_LENGTH];
    uint8_t i;
    uint8_t j;
    uint8_t flag;
   
    tmpAsciiBuffLength = 0u;
    flag = PASS;
    p = hex;
    
    for (i = 0u; i < length; i++)
    {
        tmpArray[2u * i] = (uint8_t)(*p >> 4u) & 0x0fu;
        tmpArray[(2u * i) + 1u] = (uint8_t)(*p & 0x0fu);
        p++;
    }
    
    for (i = 0u; i < (length * 2u); i++)
    {
        for (j = 0u; j < 16u; j++)
        {
            
            if (tmpArray[i] == j)
            {
                tmpArray[i] = s_asciiTable[j];
                break;
            }                
        }
        if (j >= 16u)
        {
            flag = FAIL;
        }
    }
    if (flag == PASS)
    {
        *buff = MOD_BUS_STAR;
        for (i = 0u; i < (length * 2u); i++)
        {
            buff++;
            *buff = tmpArray[i];
        }
        buff++;
        *buff = MOD_BUS_STOP_CR;
        buff++;
        *buff = MOD_BUS_STOP_LF;
        tmpAsciiBuffLength = 2u * length + 3u;
    }
    return tmpAsciiBuffLength;
}



/**
****************************************************************************
* @brief CalculateLRC
*
* @param [in] length
* @param [out] *LrcArrary

* @return tmpLRC or 0
*****************************************************************************
*/
static uint8_t CalculateLRC(uint8_t length, uint8_t *LrcArrary)
{
    uint8_t tmpLRC;
    uint8_t i;
    tmpLRC = 0u;
    for (i = 0u; i < length; i++)
    {
        tmpLRC += *LrcArrary;
        LrcArrary++;
    }
    tmpLRC = (uint8_t)(~tmpLRC) + 1u;
    return tmpLRC;
}


/**
****************************************************************************
* @brief ModBus_ConfigSet
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void ModBus_ConfigSet(void)
{
    RE_RS485_OutputEnable();
    DE_RS485_OutputEnable();
    RE_RS485_Clear();
    DE_RS485_Clear();
    
    SERCOM1_REGS->USART_INT.SERCOM_INTENSET = SERCOM_USART_INT_INTENSET_ERROR_Msk;

    
    SERCOM1_REGS->USART_INT.SERCOM_INTENSET = SERCOM_USART_INT_INTENSET_RXC_Msk;
}


/**
****************************************************************************
* @brief MODbusWrite
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void MODbusWrite(void)
{
    if ((g_modbusRXhexBuff[2u] != 0x00) || ((g_modbusRXhexBuff[3u] != 0x00)))
    {
        g_modbusTXhexBuff[1] = g_modbusRXhexBuff[1u] + 0x80;
        g_modbusTXhexBuff[2] = 2u;
    }
    else if (g_modbusRXhexBuff[4u] != 0x04)
    {
        g_modbusTXhexBuff[1] = g_modbusRXhexBuff[1u] + 0x80;
        g_modbusTXhexBuff[2] = 6u;
    }
    else
    {
        if (writeBattData(ModBus_CMD,2u,&g_modbusRXhexBuff[5]) == true)
        {
            g_TxHexLength = 7u;
            g_modbusTXhexBuff[1] = g_modbusRXhexBuff[1u];
            g_modbusTXhexBuff[2] = g_modbusRXhexBuff[2u];
            g_modbusTXhexBuff[3] = g_modbusRXhexBuff[3u];
            g_modbusTXhexBuff[4] = g_modbusRXhexBuff[4u];
            g_modbusTXhexBuff[5] = g_modbusRXhexBuff[5u];
            g_modbusTXhexBuff[6] = g_modbusRXhexBuff[6u];
        }
        else
        {
            g_modbusTXhexBuff[1u] = g_modbusRXhexBuff[1u] + 0x80;
            g_modbusTXhexBuff[2u] = 4u;
        }
    }
}

/**
****************************************************************************
* @brief MODbusRead
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void MODbusRead(void)
{
    uint8_t tmp_Length;
    if (g_modbusRXhexBuff[2u] != 0x00)
    {
        g_modbusTXhexBuff[1] = g_modbusRXhexBuff[1u] + 0x80;
        g_modbusTXhexBuff[2] = 2u;
    }
    else if (g_modbusRXhexBuff[4u] != 0x00)
    {
        g_modbusTXhexBuff[1] = g_modbusRXhexBuff[1u] + 0x80;
        g_modbusTXhexBuff[2] = 3u;
    }
    else
    {
        tmp_Length = readCommandProcess(ModBus_CMD);
        if (tmp_Length == 255u)
        {
            g_modbusTXhexBuff[1u] = g_modbusRXhexBuff[1u] + 0x80;
            g_modbusTXhexBuff[2u] = 4u;
        }
        else if(tmp_Length >= 5u)
        {
            g_modbusTXhexBuff[1u] = g_modbusRXhexBuff[1u] + 0x80;
            g_modbusTXhexBuff[2u] = 2u;
        }
        else if(tmp_Length != g_modbusRXhexBuff[5u])
        {
            g_modbusTXhexBuff[1u] = g_modbusRXhexBuff[1u] + 0x80;
            g_modbusTXhexBuff[2u] = 3u;          
        }
        else
        {
            g_TxHexLength = 3u + tmp_Length;
            g_modbusTXhexBuff[1] = g_modbusRXhexBuff[1u];
            g_modbusTXhexBuff[2] = tmp_Length * 2u;
            g_modbusTXhexBuff[3] = Can0_tx_message[4u];
            g_modbusTXhexBuff[4] = Can0_tx_message[5u];
            g_modbusTXhexBuff[5] = Can0_tx_message[6u];
            g_modbusTXhexBuff[6] = Can0_tx_message[7u];
        }
    }
}

/**
****************************************************************************
* @brief ResponseMODbusCmd
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void ResponseMODbusCmd(void)
{
    uint8_t i;
    uint8_t tmp_result;
     
    
    
    
    RE_RS485_OutputEnable();
    DE_RS485_OutputEnable();
    
    if (g_flagTxBusy == 1u)
    {
        RE_RS485_Set();
        DE_RS485_Set();   
        if (SERCOM1_USART_WriteIsBusy() == false)
        {
            g_flagTxBusy = 0u;
            SYSTICK_DelayMs(5u);
        }
    }
    else
    {
        RE_RS485_Clear();
        DE_RS485_Clear(); 
    }
    
    if (g_flagTxEn == 1u)
    {
        g_flagTxEn = 0u;
        tmp_result = AsciiToHex(&g_modbusRXbuff[0u], g_RxLength, &g_modbusRXhexBuff[0u]);
        if ((tmp_result == PASS) && (g_modbusRXhexBuff[0u] == (0xFA + g_canAddr)))
        {
            RE_RS485_Set();
            DE_RS485_Set();
            g_flagTxBusy = 1u;
            g_modbusTXhexBuff[0u] = (0xFA + g_canAddr);
            ModBus_CMD = g_modbusRXhexBuff[3u];
            g_TxHexLength = 3u;
            if (g_modbusRXhexBuff[1] == 0x44)
            {
                MODbusRead();
            }
            else if (g_modbusRXhexBuff[1] == 0x45)
            {
                MODbusWrite();
            }
            else
            {
                g_modbusTXhexBuff[1] = g_modbusRXhexBuff[1] + 0x80;
                g_modbusTXhexBuff[2] = 1u;
            }
            g_modbusTXhexBuff[g_TxHexLength] = CalculateLRC(g_TxHexLength,&g_modbusTXhexBuff[0u]);
            HexToAscii(&g_modbusTXhexBuff[0u],(g_TxHexLength + 1u),&g_modbusTXbuff[0u]);
            g_TxDataLength = g_TxHexLength * 2u + 5u;
            SERCOM1_USART_Write(&g_modbusTXbuff[0u],g_TxDataLength);
        }
    } 
}
/******************************END*********************************/
