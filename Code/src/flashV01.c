/**
 * @file  flashV01.c
 *
 * REVISION HISTORY
 *==============================================
 * |Ticket   |Date      |Author       |Notes
 * |----:    |:----:    |:----:       |:----
 * |Creation |02/04/22  |Tom O'Connor |Document
 * |Modified |02/28/22  |Tyler Badar  |Added function descriptions
 *
 * @par   COPYRIGHT NOTICE: (c) 2022 Inventus Power.  All rights reserved.
 */
/* Inventus Power Proprietary */
#include <xc.h>
#include "flashV01.h"
#include "definitions.h" 
#include "arithmeticV01.h"
#include "canV01.h"
#include "CANopenV01.h"

#define APP_DATA_REGION_START_ADDRESS       (0x4000UL)

static void UnlockRegion(void);
static void LockRegion(void);
static uint8_t ReadFlashByte(uint32_t addr);
static uint8_t WriteSelfFlash(const uint32_t addr, uint32_t *ptr);
static uint8_t EraseSelfFlash256Byte(uint32_t addr);

/**
****************************************************************************
* @brief UnlockRegion
*
* - unlock app data memory region and wait until busy flag is cleared
*
* @param NONE
* @return NONE
*****************************************************************************
*/
static void UnlockRegion(void)
{
    NVMCTRL_RegionUnlock(APP_DATA_REGION_START_ADDRESS);
    while(NVMCTRL_IsBusy() == true)
    {
        
    };
}

/**
****************************************************************************
* @brief LockRegion
*
* - lock app data memory region and wait until busy flag is cleared
*
* @param NONE
* @return NONE
*****************************************************************************
*/
static void LockRegion(void) 
{
    NVMCTRL_RegionLock(APP_DATA_REGION_START_ADDRESS);
    while (NVMCTRL_IsBusy() == true) 
    {
        
    };
}

/**
****************************************************************************
* @brief ReadFlashByte
*
* - read a byte from flash at addr specified by input parameter
*
* @param [in] addr
* @return byteValue
*****************************************************************************
*/
static uint8_t ReadFlashByte(uint32_t addr)
{
    return (uint8_t)(*((uint32_t *)addr));
}

/**
****************************************************************************
* @brief WriteSelfFlash 
*
* - write input data parameter (*ptr) to flash at addr
* - wait for busy flag to clear with 30000us timeout
* 
* @param [in] addr
* @param [in] *ptr
* @return errCode
*****************************************************************************
*/
static uint8_t WriteSelfFlash(const uint32_t addr,uint32_t *ptr)
{
    uint8_t i;
    uint8_t tryCnt;
    uint8_t errCode;
    uint16_t overTime;
    uint32_t *readAdd;
     
    overTime = 0U;
    readAdd = (uint32_t *)addr;
    errCode = 0xFFU;
    
    for (tryCnt = 0U; tryCnt < 3U; tryCnt++)
    {
        errCode = 0x00U;
        
        
        NVMCTRL_PageWrite(ptr, addr);
        while (NVMCTRL_IsBusy() == true)
        {
            Delay10us(10U);
            overTime++;
            if (overTime >= 300U)
            {
                errCode = 0x01U;
                break;
            }
        }

        
        for (i = 0U; i < 16U; i++)
        {
            if (readAdd[i] != ptr[i])
            {
                errCode = 0x02U;  
                break;
            }
        }

        if (0U == errCode)
        {
            break;
        }
    }
    
    return errCode;
}

/**
****************************************************************************
* @brief EraseSelfFlash256Byte 
*
* - erase a row of flash at addr
* - wait for busy flag to clear with 30000us timeout
*
* @param [in] addr
* @return errCode
*****************************************************************************
*/
static uint8_t EraseSelfFlash256Byte(uint32_t addr)
{
    uint8_t errCode;
    uint16_t overTime;
    
    errCode = 0U;
    overTime = 0U;
    
    
    NVMCTRL_RowErase(addr);

    
    while (NVMCTRL_IsBusy() == true)
    {
        Delay10us(10U);
        overTime++;
        if (overTime >= 300U)
        {
            errCode = 0x01U;
            break;
        }
    }
    return errCode;
}

/**
****************************************************************************
* @brief SaveLssDataFunction
*
* - save LSS config data if flagged 
* 
* @param NONE
* @return NONE
*****************************************************************************
*/
void SaveLssDataFunction(void)
{
    uint8_t saveDataBuffer[64] = {0x00};
    uint8_t calCrc8;
       
    if (g_flagLssConfiSaveEn == 1u)
    {
        UnlockRegion();
        
        calCrc8 = 0u;
        saveDataBuffer[0] = g_fixNodeID;
        calCrc8 = basic_crc(calCrc8, saveDataBuffer[0]);
        saveDataBuffer[1] = g_fixBitRate;
        calCrc8 = basic_crc(calCrc8, saveDataBuffer[1]);
        saveDataBuffer[2] = calCrc8;
        
        if (EraseSelfFlash256Byte(CAN_BIT_RATE_FLASH_SAVE_ADDRESS) == 0u)
        {
            if (WriteSelfFlash(CAN_BIT_RATE_FLASH_SAVE_ADDRESS, (uint32_t *)(&saveDataBuffer[0])) == 0u)
            {
                g_flagLssConfiSaveEn = 0u;
            }
        }
        LockRegion();
    }
}

/**
****************************************************************************
* @brief LoadLssDataFunction
*
* - load LSS config data and check crc
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void LoadLssDataFunction(void)
{
    uint8_t readDataBuffer[4];
    uint8_t calCrc8;
    
    g_fixBitRate = 0u;
    g_fixNodeID = 0xffu;
    g_flagLoadCanSettingSucceed = 1u;
    
    UnlockRegion();
    
    NVMCTRL_Read((uint32_t *)(&readDataBuffer[0]), 4u,CAN_BIT_RATE_FLASH_SAVE_ADDRESS);
    calCrc8 = 0u;
    calCrc8 = basic_crc(calCrc8, readDataBuffer[0]);
    calCrc8 = basic_crc(calCrc8, readDataBuffer[1]);
    
    if (calCrc8 == readDataBuffer[2])
    {
        g_fixNodeID = readDataBuffer[0u];
        g_fixBitRate = readDataBuffer[1u];
    }
	else
	{
		g_flagLoadCanSettingSucceed = 0u;
	}
    g_BitRate = g_fixBitRate;
    g_pendBitRate = g_fixBitRate;
    LockRegion();
}


/* Inventus Power Proprietary */

