/**
 * @file  J1939V01.c
 *
 * REVISION HISTORY
 *==============================================
 * |Ticket   |Date      |Author       |Notes
 * |----:    |:----:    |:----:       |:----
 * |Creation |02/04/22  |Tom O'Connor |Document
 * |Modified |03/07/22  |Tyler Badar  |Added function descriptions
 *
 * @par   COPYRIGHT NOTICE: (c) 2022 Inventus Power.  All rights reserved.
 */
/* Inventus Power Proprietary */

#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes
#include "common_defineV01.h"
#include "mainV01.h"
#include "canV01.h"
#include "i2cMasterV02.h"
#include "J1939V01.h"
#include "CANopenV01.h"

static void ResetCurrErrParameter(void);
static void ResetVoltErrParameter(void);
static void InitialDeltaQCurr(void);
static void InitialDeltaQVolt(void);
static void SetDeltaQCurr(uint16_t currVaule);
static void SetDeltaQVolt(uint16_t VoltVaule);

J1939Data g_delatQJ1939Data;

static uint16_t g_deltaQBCH1Timer = 0u;
uint16_t g_deltaQBCH1Curr = 0u;


static uint8_t s_currentDebounce = 0u;
static uint8_t s_voltageDebounce = 0u;
static uint8_t s_flagCurr = 0u;
static uint8_t s_flagVolt = 0u;
static uint16_t s_setCurrOvf = 0u;
static uint16_t s_setVoltOvf = 0u;
static uint8_t s_chargerCtrlStep = DELTA_Q_INITIAL;



static uint8_t g_readPgn = 0u;
static uint8_t g_readPgnLSB = 0u;


static uint8_t g_errPgnMSB = 0u;
static uint8_t g_errPgn = 0u;
static uint8_t g_errPgnLSB = 0u;
static uint8_t g_errSourceAddr = 0u;
define_16bits g_flagJ1939Status;

static uint8_t s_j1939ReadType;


#define J1939_READ_NONE       0u
#define J1939_READ_BYTE       1u
#define J1939_READ_WORD       2u
#define J1939_READ_DWORD      3u

#define J1939_WRITE_NONE       0u
#define J1939_WRITE_BYTE       1u
#define J1939_WRITE_WORD       2u
#define J1939_WRITE_DWORD      3u


static uint8_t test2[5u]={4, 1, 2, 3, 4};
uint8_t g_j1939Address = BATT_SOURCE_ADDR_ID;
static uint8_t s_claimAddr = BATT_SOURCE_ADDR_ID;


#define CELL_VOLT_LENGTH    8u

/**
****************************************************************************
* @brief BroadcastCellVolt
*
* @param NONE
* @return NONE
* @todo Empty function
*****************************************************************************
*/
void BroadcastCellVolt(void)
{
    
}


/**
****************************************************************************
* @brief SendJ1939AddressClaim
*
* @param NONE
* @return NONE
* @todo Empty function
*****************************************************************************
*/
void SendJ1939AddressClaim(void)
{
}

/**
****************************************************************************
* @brief J1939AddressClaimHandling
*
* @param NONE
* @return NONE
* @todo Empty function
*****************************************************************************
*/
void J1939AddressClaimHandling(void)
{
    
}


/**
****************************************************************************
* @brief J1939Write
*
* @param NONE
* @return NONE
* @todo Empty function
*****************************************************************************
*/
void J1939Write(void)
{
    
}


/**
****************************************************************************
* @brief J1939Response
*
* @param NONE
* @return NONE
* @todo Empty function
*****************************************************************************
*/
void J1939Response(void)
{
    
}

/**
****************************************************************************
* @brief J1939PGNRequestProcess
*
* - transmit J1939 PGN Request
*
* @param [in] tmpID
* @return NONE
*****************************************************************************
*/
void J1939PGNRequestProcess(uint32_t tmpID)
{
    uint16_t J1939PGN_H;
    uint8_t can_CMD;
    uint8_t can_CmdLength;
    uint32_t s_j1939_tmpID;
    can_CMD = Can0_rx_message[0];
    J1939PGN_H = (uint16_t)Can0_rx_message[2] * 256u + Can0_rx_message[1]; 
    if (J1939PGN_H == 0x00ffu)
    {
        s_j1939_tmpID = J1939_RESPONSE_ID | ((uint32_t)can_CMD * 256u);
        Can0_tx_message[0] = 0x4b;
        Can0_tx_message[1] = g_canAddr;
        Can0_tx_message[2] = 0xc1;
        Can0_tx_message[3] = can_CMD;
        Can0_tx_message[4] = 0x00;
        Can0_tx_message[5] = 0x00;
        Can0_tx_message[6] = 0x00;
        Can0_tx_message[7] = 0x00;
        can_CmdLength = readCommandProcess(can_CMD);
        if (can_CmdLength >= 250u)
        {  
            s_j1939_tmpID = J1939_RESPONSEERR_ID | ((tmpID * 256u) & 0x0000ff00);
            can_CmdLength = 4u;
        }
        CAN0_MessageTransmit(s_j1939_tmpID,(0x4u + can_CmdLength),Can0_tx_message,CAN_MODE_NORMAL, CAN_MSG_ATTR_TX_FIFO_DATA_FRAME);
    }
}


/**
****************************************************************************
* @brief DeltaQJ1939Process
*
* @param NONE
* @return NONE
* @todo Empty function
*****************************************************************************
*/
void DeltaQJ1939Process(void)
{    
  
}

/**
****************************************************************************
* @brief ControlDeltaQChargerJ1939
*
* @param NONE
* @return NONE
* @todo Empty function
*****************************************************************************
*/
void ControlDeltaQChargerJ1939(void)
{
    
}


/**
****************************************************************************
* @brief SetDeltaQCurr
*
* @param [in] currValue
* @return NONE
* @todo Empty function
*****************************************************************************
*/
static void SetDeltaQCurr(uint16_t currVaule)
{
    
}

/**
****************************************************************************
* @brief SetDeltaQVolt
*
* @param [in] VoltVaule
* @return NONE
* @todo Empty function
*****************************************************************************
*/
static void SetDeltaQVolt(uint16_t VoltVaule)
{
    
}
/* Inventus Power Proprietary */
