/**
 * @file  CANopenV01.c 
 * @brief   This file contains the CANOpen protocol libraries.
 *  
 * Description:
 *   This file contains the CANOpen protocol libraries.
 * 
 *
 *
 * REVISION HISTORY
 *==============================================
 * |Ticket   |Date      |Author       |Notes
 * |----:    |:----:    |:----:       |:----
 * |Creation |02/04/22  |Tom O'Connor |Document
 * |modified |02/15/22  |broberto     |Removed handlers
 * |Modified |02/15/22  |Tyler Badar  |Added function descriptions
 *
 * @par   COPYRIGHT NOTICE: (c) 2022 Inventus Power.  All rights reserved.
 */
/* Inventus Power Proprietary */

#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes
#include "common_defineV01.h"
#include "mainV01.h"
#include "canV01.h"
#include "i2cMasterV02.h"
#include "adcV01.h"
#include "detectV01.h"
#include "chgdsgV01.h"
#include "temperatureV01.h"
#include "ledV01.h"
#include "rs232V01.h"
#include "CANopenV01.h"
#include "flashV01.h"
#include "J1939V01.h"

#include "CANopenV01_handler.h"

/**
 * @todo Need to FIX
 */
static union
{
  uint32_t longTmp;
  uint16_t intTmp[2u];
  uint8_t charTmp[4u];
}s_crcTable;

uint8_t g_flagCanErr = 0u;

/**
 * @todo Fix
 */
define_8bits volatile g_packsCommStatus;
uint8_t g_canMasterCmd = 0;
uint8_t g_canMasterFuncC = 0u;

uint8_t g_nodeState = NODE_STATE_RESETNODE;

/**
 * @todo Need to FIX
 */
uint8_t g_picVersion[4u] = {3u, (uint8_t)'A', (uint8_t)'0', (uint8_t)'7'};

/**
 * @todo Need to FIX
 */
uint8_t s_manufactureName[2u] = {(uint8_t)'I', (uint8_t)'P'};

/**
 * @todo Need to FIX
 */
uint8_t g_FWversion[8u] = {'0','1','0','0','2','0','0','7'};

uint32_t g_deviceType = 0x00000000u;
define_8bits g_errRegister;

/**
 * @todo Need to FIX
 */
uint8_t g_manufactureName[14u] = {'I','n','v','e','n','t','u','s',' ','P','o','w','e','r'};

/**
 * @todo Need to FIX
 */
uint8_t g_HWversion[4u] = {'0','1','0','1'};

uint32_t g_cobIDEMCY = 0x80 + BATT_DEFAULT_ID;

uint16_t g_inhibitTimeEMCY = 0u;
uint32_t g_consumerHeartbeatTime = 0u;
uint16_t g_producerHeartbeatTime = 200u;//1s

uint8_t g_identityObjectSUB = 4u;

uint32_t g_vendorID = IP_VENDOR_ID;
uint32_t g_productCode = IP_PRODUCT_CODE;
uint32_t g_revisionNumber = IP_PRODUCT_CODE_NUM;
uint32_t g_serialNumber = 0u;

uint8_t g_sdoServer1SUB = 3u;
uint32_t g_cobIDrx = 0x600 + BATT_DEFAULT_ID;
uint32_t g_cobIDtx = 0x580 + BATT_DEFAULT_ID;
uint8_t g_nodeIDofSDO = BATT_DEFAULT_ID;
uint16_t g_canErrTimes = 0u;
uint16_t g_canOvfTimes = 0u;

uint8_t g_pendNodeID;
uint8_t g_fixNodeID;

uint8_t g_pendBitRate;
uint8_t g_fixBitRate=0;

uint8_t g_lssState = LSS_WAIT_STATE;
uint8_t g_lssSwitchIndx;
uint16_t g_lssUpdateRateTime;
static uint32_t g_lssVendorID=0;
uint32_t g_lsspPoductCode=0;
uint32_t g_lssRevisionNumber=0;
uint32_t g_lssSerialNumber=0;

define_8bits g_flagCanOpen;

uint8_t g_canBattStatus = 0u;

uint8_t g_chargerStatus = 0u;

uint16_t g_temperature = 0u;

uint8_t g_battParmetersSUB = 4u;
uint8_t g_batttype = 0xa0;
uint16_t g_batttCap = 19u;
uint16_t g_maxCHGcurr = 22u;
uint16_t g_cellNumber = 48u;
uint8_t g_battSNSUB = 2u;
uint32_t g_battSNLow = 0x30303030u;
uint32_t g_battSNHigh = 0x30303030u;
uint32_t g_cumulativeTotalCharge = 00u;
uint32_t g_expendSinceLastCharge = 00u;
uint32_t g_returnedDuringLastCharge = 00u;
uint32_t g_battVoltage = 00u;
uint32_t g_chargeCurrPequested = 320u;
uint32_t g_canBattSOC = 0u;

uint32_t g_freeData = 41u;
uint8_t g_testCommandSub = 41u;

uint8_t g_canAbortCode = 0u;

/**
 * @todo Need to FIX
 */
uint8_t const s_abortCodeTab[6u][4u]=
{
    {0x00,0x00,0x00,0x00},
    {0x00,0x00,0x02,0x06},
    {0x10,0x00,0x07,0x06},
    {0x02,0x00,0x01,0x06},
    {0x11,0x00,0x09,0x06},
    {0x00,0x00,0x00,0x00}
};

/**
 * @todo Need to FIX
 */
uint8_t const s_returnReadCSTab[5u] = {0x00,0x4f,0x4b,0x00,0x43};
/**
 * @todo Need to FIX
 */
uint8_t const s_endByteLengthTab[8u] = {0x00,0x0f,0x0d,0x0b,0x09,0x07,0x05,0x03};

/**
 * @todo Need to FIX
 */
static uint8_t s_broadcastData[BROADCAST_ROW][BROADCAST_COLUMN] = {
                                    /* pack info */
                                    {0x01u, NO1_DATA, 0u, 0u, 0u, 0u, 0u, 0u},
                                    {0x01u, NO2_DATA, 0u, 0u, 0u, 0u, 0u, 0u},
                                    {0x01u, NO3_DATA, 0u, 0u, 0u, 0u, 0u, 0u},
                                    {0x01u, NO4_DATA, 0u, 0u, 0u, 0u, 0u, 0u}}; 

uint8_t g_tpdoParameterSub = 0x03u;
uint8_t g_tpdoParmeterType = 0xfeu;
uint16_t g_tpdoParameterTime = 4000u;
uint32_t g_tpdoParameterNodeId1 = 0u;
uint32_t g_tpdoParameterNodeId2 = 0u;
uint32_t g_tpdoParameterNodeId3 = 0u;
uint32_t g_tpdoParameterNodeId4 = 0u;

uint16_t g_heaterState = 0u;
uint16_t g_heaterControl = 0u;

uint8_t handler_g_flagLssIdentifyMatch=0;

/**
 * @TODO Fix Variable
 */
uint8_t const g_tpdoMappingParameterSub[4] = {0x5u, 0x4u, 0x4u,0x5};
/**
 * @TODO Fix Variable
 */
uint32_t const g_tpdoMappingParameter[20] = 
{
    0x60810008u, 0x48000008u, 0x48010010u, 0x48020010u, 0x48030010u,
    0x48040010u, 0x48050010u, 0x48060010u, 0x48070010u, 0x00000000u,
    0x60100010u, 0x48080010u, 0x48090010u, 0x480F0010u, 0x00000000u,
    0x480E0010u, 0x480A0010u, 0x480B0010u, 0x480C0008u, 0x480D0008u
};

uint8_t g_consumerHeartbeatTimeSUB = 0u;

uint8_t g_lssIdentifyIndx=0;
uint8_t g_flagLssIdentifyMatch=0;

static void ConvertBattSNFunction(uint16_t battSN);
static void UpdateCiaBitRegister(void);
static void EmergencyObjectFunction(void); 
static void FastScanFunction(uint32_t IDNumber, uint8_t checkBit, uint8_t LssSub, uint8_t LssNext);


/**
****************************************************************************
* @brief CanOpenAppResetProcess
* 
* resets canOpen
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void canOpenAppResetProcess(void)
{
    handler_g_cobIDEMCY (SET, 0x80 + g_note_ID);
    handler_g_inhibitTimeEMCY (SET, 0u);
    handler_g_consumerHeartbeatTime (SET, 0u);
    handler_g_producerHeartbeatTime (SET, 200u);//1000u;
    handler_g_identityObjectSUB (SET, 4u);
    handler_g_vendorID (SET, IP_VENDOR_ID);
    handler_g_productCode (SET, IP_PRODUCT_CODE);
    handler_g_revisionNumber (SET, IP_PRODUCT_CODE_NUM);
    handler_g_serialNumber (SET, handler_g_Uid(GET, 0));
    handler_g_sdoServer1SUB (SET, 3u);
    handler_g_cobIDrx (SET, 0x600 + handler_g_fixNodeID(GET, 0));
    handler_g_cobIDtx (SET, 0x580 + handler_g_fixNodeID(GET,0));
    handler_g_nodeIDofSDO (SET, handler_g_fixNodeID(GET,0));
    handler_g_pendNodeID (SET, handler_g_fixNodeID(GET,0));
}
/**
****************************************************************************
* @brief CanOpenCommResetProcess
*
* reset canOpen values
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void canOpenCommResetProcess(void)
{
    handler_g_canBattStatus (SET, 1u);
    handler_g_chargerStatus (SET, 0u);
    handler_g_temperature (SET, 0u);
    handler_g_battParmetersSUB (SET, 4u);
    handler_g_batttype (SET, 0xa0);
    handler_g_batttCap (SET, 19u);
    handler_g_maxCHGcurr (SET, 22u);
    handler_g_cellNumber (SET, 48u);
    handler_g_battSNSUB (SET, 2u);
    handler_g_battSNLow (SET, 0x30303030u);
    handler_g_battSNHigh (SET, 0x30303030u);
    handler_g_cumulativeTotalCharge (SET, 00u);
    handler_g_expendSinceLastCharge (SET, 00u);
    handler_g_returnedDuringLastCharge (SET, 00u);
    handler_g_battVoltage (SET, 00u);
    handler_g_chargeCurrPequested (SET, 320u);
    handler_g_canBattSOC (SET, 0u);
    handler_g_freeData (SET, 0u);
    
    handler_g_errRegister.byte (SET, 0x00u);
    handler_g_note_ID (SET, g_pendNodeID);
    
    handler_g_cobIDrx (SET, 0x600 + g_note_ID);
    handler_g_cobIDtx (SET, 0x580 + g_note_ID);
    handler_g_nodeIDofSDO (SET, g_note_ID);
    handler_g_cobIDEMCY (SET, 0x80 + g_note_ID);
    handler_g_tpdoParameterNodeId1 (SET, 0x40000000u + 0x180u + g_note_ID);
    handler_g_tpdoParameterNodeId2 (SET, 0x40000000u + 0x280u + g_note_ID);
    handler_g_tpdoParameterNodeId3 (SET, 0x40000000u + 0x380u + g_note_ID);
    handler_g_tpdoParameterNodeId4 (SET, 0x40000000u + 0x480u + g_note_ID);
    handler_g_consumerHeartbeatTimeSUB (SET, 1u);
    handler_g_consumerHeartbeatTime (SET, ((uint32_t)g_note_ID << 16) + 2000u);

}
/**
****************************************************************************
* @brief CanOpenDataProcess
*
* set canOpen battery values
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void canOpenDataProcess(void)
{
    uint32_t tmp_g_battVoltage=0;
    uint8_t  tmp_g_battSOC=0;
    uint32_t tmp_g_AhExpendedSinceLastCharge=0;
    uint32_t tmp_g_AhReturnedDuringLastCharge=0;
    uint32_t tmp_g_CumulativeTotalAhChargeH=0; 
    uint32_t tmp_g_CumulativeTotalAhChargeL=0;

    handler_g_canBattStatus (SET, 1u);
    handler_g_chargerStatus (SET, 0u);
    handler_g_temperature (SET, (((int16_t)tmp_g_battTemp) - (int16_t)2730) * (int16_t)8u / (int16_t)10u);
    handler_g_battSNSUB (SET, 2u);
    handler_g_battSNLow (SET, 0x30303030u);
    handler_g_battSNHigh (SET, 0x30303030u);
    handler_g_cumulativeTotalCharge (SET, (uint32_t)tmp_g_CumulativeTotalAhChargeH * 65536u + (uint32_t)tmp_g_CumulativeTotalAhChargeL);
    handler_g_expendSinceLastCharge (SET, tmp_g_AhExpendedSinceLastCharge);
    handler_g_returnedDuringLastCharge (SET, tmp_g_AhReturnedDuringLastCharge);
    handler_g_battVoltage (SET, (uint32_t)tmp_g_battVoltage);
    handler_g_canBattSOC (SET, (uint8_t)tmp_g_battSOC);


    ConvertBattSNFunction(handler_g_battSN(GET,0));
    UpdateCiaBitRegister();	
}
/**
****************************************************************************
* @brief heartBeatProcess
*
* broadcast nodeState on pre-determined heartbeat timer variable
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void heartBeatProcess(void)
{
    static uint16_t s_heartBeatTimer = 0u;
    static uint16_t s_broadcastTimer = 0u;
    
    if (g_flagCompleteAddrAllocate == 0u)
    {
        g_nodeState = NODE_STATE_RESETMCU;
    }
    if ((g_flagCompleteAddrAllocate == 1u) &&(g_nodeState == NODE_STATE_RESETMCU))
    {
        g_nodeState = NODE_STATE_RESETNODE;
        s_heartBeatTimer = 0u;
    }
    canOpenDataProcess();
	EmergencyObjectFunction();
	
    if ((++s_heartBeatTimer >= g_producerHeartbeatTime) && (g_nodeState != NODE_STATE_RESETMCU) && (g_producerHeartbeatTime != 0u))
    {
        s_heartBeatTimer = 0u;
        if (g_nodeState == NODE_STATE_RESETNODE)
        {
            Can0_tx_message[0u] = HEARTBEAT_STATE_BOOT_UP;
            canOpenAppResetProcess();
            canOpenCommResetProcess();
            if ((g_note_ID == 0xffu) || (g_pendNodeID == 0xffu))
            {
                g_nodeState = NODE_STATE_RESETNODE;
            }
            else
            {
                g_nodeState = NODE_STATE_PREOPERATION;
            }
        }
        else if (g_nodeState == NODE_STATE_RESETCOMM)
        {
            Can0_tx_message[0u] = HEARTBEAT_STATE_BOOT_UP;
            canOpenCommResetProcess();
            if ((g_note_ID == 0xffu) || (g_pendNodeID == 0xffu))
            {
                g_nodeState = NODE_STATE_RESETCOMM;
            }
            else
            {
                g_nodeState = NODE_STATE_PREOPERATION;
            }
        }
        else if (g_nodeState == NODE_STATE_PREOPERATION)
        {
        	if ((g_note_ID != 0xffu) && (g_pendNodeID != 0xffu))
            {
                g_nodeState = NODE_STATE_OPERATION;
            }
		
            Can0_tx_message[0u] = HEARTBEAT_STATE_PREOPERATION;
        }
        else if (g_nodeState == NODE_STATE_OPERATION)
        {
            Can0_tx_message[0u] = HEARTBEAT_STATE_OPERATION;
        }
        else if (g_nodeState == NODE_STATE_STOPPED)
        {
            Can0_tx_message[0u] = HEARTBEAT_STATE_STOPPED;
        }
        CAN1_MessageTransmit((HEARTBEAT_FUNCCODE + g_note_ID),0x1u,Can0_tx_message,CAN_MODE_NORMAL, CAN_MSG_ATTR_TX_FIFO_DATA_FRAME);
    }
    /*if (g_nodeState == NODE_STATE_OPERATION)
    {
        if (++s_broadcastTimer >= TIMER5MS_2S5)
        {
            s_broadcastTimer = 0u;
            Can0_tx_message[0u] = g_note_ID;
            Can0_tx_message[1u] = g_chargerStatus;
            Can0_tx_message[2u] = (uint8_t)(g_temperature);
            Can0_tx_message[3u] = (uint8_t)(g_temperature >> 8u);
            Can0_tx_message[4u] = 0u;
            Can0_tx_message[5u] = 0u;
            Can0_tx_message[6u] = 0u;
            Can0_tx_message[7u] = 0u;
            CAN1_MessageTransmit((TPDO1_FUNCCODE + g_note_ID),0x8u,Can0_tx_message,CAN_MODE_NORMAL, CAN_MSG_ATTR_TX_FIFO_DATA_FRAME);
        }
    }*/
}

/**
****************************************************************************
* @brief ProcessCanOpenReceiveData
*
* determine how to process canOpen message
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void ProcessCanOpenReceiveData(void)
{
    if ((Can0_rx_messageID == SDO_DOWNLOAD_FUNCCODE + g_note_ID) && (Can0_rx_messageLength == 8u))
    {
        CANopenProcessSDO();
    }
    if ((Can0_rx_messageID == NMT_COB_ID) && (Can0_rx_messageLength == 2u))
    {
        CANopenProcessNMT();
    }
    if ((Can0_rx_messageID == LSS_CIA305_ID) && (Can0_rx_messageLength == 8u))
    {
        CANopenProcessLSS();
    }
}

/**
****************************************************************************
* @brief  CANopenProcessNMT
*
* set nodeState based on received CStype
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void CANopenProcessNMT(void)
{
    uint8_t s_CStype;
    uint8_t s_nodeID;
    s_CStype = Can0_rx_message[0];
    s_nodeID = Can0_rx_message[1];
    if ((s_nodeID == g_note_ID) || (s_nodeID == 0x00u))
    {
        if (s_CStype == NMT_CS_RESETNODE)
        {
            g_nodeState = NODE_STATE_RESETNODE;
        }
        if (s_CStype == NMT_CS_RESETCOMM)
        {
            g_nodeState = NODE_STATE_RESETCOMM;
        }
        if (s_CStype == NMT_CS_PREOPERATION)
        {
            g_nodeState = NODE_STATE_PREOPERATION;
        }
        if (s_CStype == NMT_CS_OPERATION)
        {
            g_nodeState = NODE_STATE_OPERATION;
        }
        if (s_CStype == NMT_CS_STOPPED)
        {
            g_nodeState = NODE_STATE_STOPPED;
        }
    }
}

/**
****************************************************************************
* @brief  CANopenProcessLSS
*
* set LSS data based on CStype
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void CANopenProcessLSS(void)
{
    uint8_t s_CStype;
    uint8_t s_int8Tmp;
    uint8_t s_int8Tmp2;
    uint16_t s_int16Tmp;
    uint32_t s_int32Tmp;
    
    g_flagLssCanSend = 0u;
    Can0_tx_message[0u] = 0u;
    Can0_tx_message[1u] = 0u;
    Can0_tx_message[2u] = 0u;
    Can0_tx_message[3u] = 0u;
    Can0_tx_message[4u] = 0u;
    Can0_tx_message[5u] = 0u;
    Can0_tx_message[6u] = 0u;
    Can0_tx_message[7u] = 0u;
    s_CStype = Can0_rx_message[0];
    s_crcTable.charTmp[0u] = Can0_rx_message[1u];
    s_crcTable.charTmp[1u] = Can0_rx_message[2u];
    s_crcTable.charTmp[2u] = Can0_rx_message[3u];
    s_crcTable.charTmp[3u] = Can0_rx_message[4u];
    s_int8Tmp = Can0_rx_message[1u]; 
    s_int8Tmp2 = Can0_rx_message[2u]; 
    switch (s_CStype)
    {
        case LSS_SWITCH_STATE_CS:   // 04 00 00 00 00 00 00 00
        {
            if (s_int8Tmp == 0x00u)
            {
                handler_g_lssState (SET, LSS_WAIT_STATE);
            }
            if ((s_int8Tmp == 0x01u) && (handler_g_lssState(GET,0) == LSS_WAIT_STATE))
            {
                handler_g_lssState (SET, LSS_CONFI_STATE);
            }
			
            break;
        }
        case LSS_SWITCH_VENDORID_CS: // 40 df 04 00 00 00 00 00
        {
            if (handler_g_lssState(GET,0) == LSS_WAIT_STATE)
            {
                handler_g_lssVendorID (SET, s_crcTable.longTmp);
                handler_g_lssSwitchIndx (SET, 1u);
            }
            break;
        }
        case LSS_SWITCH_PRODUCTCODE_CS: // 41 f4 21 00 00 00 00 00
        {
            if (handler_g_lssSwitchIndx(GET,0) == 1u)
            {
                handler_g_lsspPoductCode (SET, s_crcTable.longTmp);
                handler_g_lssSwitchIndx  (SET, 2u);
            }
            break;
        }
        case LSS_SWITCH_RN_CS: // 42 02 00 00 00 00 00 00
        {
            if (handler_g_lssSwitchIndx(GET,0) == 2u)
            {
                handler_g_lssRevisionNumber (SET, s_crcTable.longTmp);
                handler_g_lssSwitchIndx (SET, 3u);
            }
            break;
        }
        case LSS_SWITCH_SN_CS: // 43 ff ff ff ff  00 00 00
        {
            if (handler_g_lssSwitchIndx(GET,0) == 3u)
            {
                handler_g_lssSerialNumber (SET, s_crcTable.longTmp);
                handler_g_lssSwitchIndx (SET, 4u);
				
                if ((handler_g_lssVendorID(GET,0) == handler_g_vendorID(GET,0)) && (handler_g_lsspPoductCode(GET,0) == handler_g_productCode(GET,0)) 
                        && (handler_g_lssRevisionNumber(GET,0) == handler_g_revisionNumber(GET,0)) && (handler_g_lssSerialNumber(GET,0) == handler_g_serialNumber(GET,0)))
                {
                    handler_g_lssState (SET, LSS_CONFI_STATE);
                    handler_g_flagLssCanSend (SET, 1u);
                    Can0_tx_message[0u] = LSS_SWITCH_RESPONSE_CS;
                }
            }
            break;
        }
        case LSS_CONFIGURE_ID_CS: // 11 FF 00 00 00 00 00 00
        {
            if (handler_g_lssState(GET,0) == LSS_CONFI_STATE)
            {
                if (((s_int8Tmp >= 1u) && (s_int8Tmp < 0x80u)) || (s_int8Tmp == 0xffu))
                {
                    handler_g_pendNodeID (SET, s_int8Tmp);
                }
                else
                {
                    Can0_tx_message[1u] = 0x01;
                }
                Can0_tx_message[0u] = LSS_CONFIGURE_ID_CS;
                handler_g_flagLssCanSend (SET, 1u);
            }
            break;
        }
        case LSS_CONFIGURE_RATE_CS:
        {
            if (handler_g_lssState(GET,0) == LSS_CONFI_STATE)
            {
                if ((s_int8Tmp == 0u) && (s_int8Tmp2 < 4u))
                {
                    handler_g_pendBitRate (SET, s_int8Tmp2);
                }
                else
                {
                    Can0_tx_message[1u] = 0x01;
                }
                Can0_tx_message[0u] = LSS_CONFIGURE_RATE_CS;
                handler_g_flagLssCanSend (SET, 1u);
            }
            break;
        }
        case LSS_UPDATE_RATE_CS:
        {
            if (handler_g_lssState(GET,0) == LSS_CONFI_STATE)
            {
                handler_g_lssUpdateRateTime (SET, s_crcTable.intTmp[0]);
                handler_g_BitRate (SET, handler_g_pendBitRate);
                handler_g_flagLssBitRateUpdate (SET, 1u);
            }
            break;
        }
        case LSS_SAVE_CONFI_CS: // 17 00 00 00 00 00 00 00
        {
            if (handler_g_lssState(GET,0) == LSS_CONFI_STATE)
            {
                handler_g_flagLssConfiSaveEn (SET, 1u);
                handler_g_fixNodeID (SET, g_pendNodeID);
                handler_g_fixBitRate (SET, g_pendBitRate);
                SaveLssDataFunction();
                if (handler_g_flagLssConfiSaveEn(GET,0) == 1u)
                {
                    Can0_tx_message[1u] = 0x02;
                }
                Can0_tx_message[0u] = LSS_SAVE_CONFI_CS;
                handler_g_flagLssCanSend (SET, 1u);
            }
            break;
        }
        case LSS_INQUIRE_VENDORID_CS:
        {
			if (handler_g_lssState(GET,0) == LSS_CONFI_STATE)
            {
	            Can0_tx_message[0u] = LSS_INQUIRE_VENDORID_CS;
	            s_crcTable.longTmp = handler_g_vendorID(GET,0);
	            Can0_tx_message[1u] = s_crcTable.charTmp[0u];
	            Can0_tx_message[2u] = s_crcTable.charTmp[1u];
	            Can0_tx_message[3u] = s_crcTable.charTmp[2u];
	            Can0_tx_message[4u] = s_crcTable.charTmp[3u];
	            handler_g_flagLssCanSend (SET, 1u);
			}
            break;
        }
        case LSS_INQUIRE_PRODUCTCODE_CS:
        {
			if (handler_g_lssState(GET,0) == LSS_CONFI_STATE)
            {
	            Can0_tx_message[0u] = LSS_INQUIRE_PRODUCTCODE_CS;
	            s_crcTable.longTmp = handler_g_productCode(GET,0);
	            Can0_tx_message[1u] = s_crcTable.charTmp[0u];
	            Can0_tx_message[2u] = s_crcTable.charTmp[1u];
	            Can0_tx_message[3u] = s_crcTable.charTmp[2u];
	            Can0_tx_message[4u] = s_crcTable.charTmp[3u];
	            handler_g_flagLssCanSend (SET, 1u);
			}
            break;
        }
        case LSS_INQUIRE_RN_CS:
        {
			if (handler_g_lssState(GET,0) == LSS_CONFI_STATE)
            {
	            Can0_tx_message[0u] = LSS_INQUIRE_RN_CS;
	            s_crcTable.longTmp = handler_g_revisionNumber(GET,0);
	            Can0_tx_message[1u] = s_crcTable.charTmp[0u];
	            Can0_tx_message[2u] = s_crcTable.charTmp[1u];
	            Can0_tx_message[3u] = s_crcTable.charTmp[2u];
	            Can0_tx_message[4u] = s_crcTable.charTmp[3u];
	            handler_g_flagLssCanSend (SET, 1u);
			}
            break;
        }
        case LSS_INQUIRE_SN_CS:
        {
			if (handler_g_lssState(GET,0) == LSS_CONFI_STATE)
            {
	            Can0_tx_message[0u] = LSS_INQUIRE_SN_CS;
	            s_crcTable.longTmp = handler_g_serialNumber(GET,0);
	            Can0_tx_message[1u] = s_crcTable.charTmp[0u];
	            Can0_tx_message[2u] = s_crcTable.charTmp[1u];
	            Can0_tx_message[3u] = s_crcTable.charTmp[2u];
	            Can0_tx_message[4u] = s_crcTable.charTmp[3u];
	            handler_g_flagLssCanSend (SET, 1u);
			}
            break;
        }
        case LSS_INQUIRE_NODEID_CS:
        {
			if (handler_g_lssState(GET,0) == LSS_CONFI_STATE)
            {
	            Can0_tx_message[0u] = LSS_INQUIRE_NODEID_CS;
	            Can0_tx_message[1u] = handler_g_pendNodeID(GET,0);
	            handler_g_flagLssCanSend (SET, 1u);
			}
            break;
        }
        case LSS_ID_REMOTE_SLAVE_ID_CS:    /*0x46*/
        {
            handler_g_lssIdentifyIndx (SET, 1u);
            handler_g_flagLssIdentifyMatch (SET, 1u);
            if (s_crcTable.longTmp != handler_g_vendorID(GET,0))
            {
                handler_g_flagLssIdentifyMatch (SET, 0u);
            }
            break;
        }
        case LSS_ID_REMOTE_SLAVE_PRODUCTCODE_CS:    /*0x47*/
        {
            if ((handler_g_lssIdentifyIndx(GET,0) == 1u) && (handler_g_flagLssIdentifyMatch(GET,0) == 1u))
            {
                handler_g_lssIdentifyIndx (SET, 2u);
                if (s_crcTable.longTmp != handler_g_productCode(GET,0))
                {
                    handler_g_flagLssIdentifyMatch (SET, 0u);
                }
            }
            break;
        }
        case LSS_ID_REMOTE_SLAVE_RN_L_CS:    /*0x48*/
        {
            if ((handler_g_lssIdentifyIndx(GET,0) == 2u) && (handler_g_flagLssIdentifyMatch(GET,0) == 1u))
            {
                handler_g_lssIdentifyIndx (SET, 3u);
                if (handler_g_revisionNumber(GET,0) < s_crcTable.longTmp)
                {
                    handler_g_flagLssIdentifyMatch (SET, 0u);
                }
            }
            break;
        }
        case LSS_ID_REMOTE_SLAVE_RN_H_CS:    /*0x49*/
        {
            if ((handler_g_lssIdentifyIndx(GET,0) == 3u) && (handler_g_flagLssIdentifyMatch(GET,0) == 1u))
            {
                handler_g_lssIdentifyIndx (SET, 4u);
                if (s_crcTable.longTmp < handler_g_revisionNumber(GET,0))
                {
                    handler_g_flagLssIdentifyMatch (SET, 0u);
                }
            }
            break;
        }
        case LSS_ID_REMOTE_SLAVE_SN_L_CS:    /*0x4A*/
        {
            if ((handler_g_lssIdentifyIndx(GET,0) == 4u) && (handler_g_flagLssIdentifyMatch(GET,0) == 1u))
            {
                handler_g_lssIdentifyIndx (SET, 5u);
                if (handler_g_serialNumber(GET,0) < s_crcTable.longTmp)
                {
                    handler_g_flagLssIdentifyMatch (SET, 0u);
                }
            }
            break;
        }
        case LSS_ID_REMOTE_SLAVE_SN_H_CS:    /*0x4B*/
        {
            if ((handler_g_lssIdentifyIndx(GET,0) == 5u) && (handler_g_flagLssIdentifyMatch(GET,0) == 1u))
            {
                handler_g_lssIdentifyIndx (SET, 6u);
                if (s_crcTable.longTmp < handler_g_serialNumber(GET,0))
                {
                    handler_g_flagLssIdentifyMatch (SET, 0u);
                }
                
                if (handler_g_flagLssIdentifyMatch(GET,0) == 1u)
                {
                    Can0_tx_message[0u] = LSS_ID_SLAVE_CS;
                    handler_g_flagLssCanSend (SET, 1u);
                }
            }
            break;
        }
        case LSS_ID_NON_CFG_REMOTE_SLAVE_CS:    /*0x4C*/
        {
            if (handler_g_pendNodeID(GET,0) == 0xffu)
            {
                Can0_tx_message[0u] = LSS_ID_NON_CFG_SLAVE_CS;
                handler_g_flagLssCanSend (SET, 1u);
            }
            break;
        }
        case LSS_FAST_SCAN_CS: // 51 00 00 00 00 80 00 00
        {
			
            if ((handler_g_pendNodeID(GET,0) == 0xffu) && (handler_g_lssState(GET,0) == LSS_WAIT_STATE))
            {
                FastScanFunction(s_crcTable.longTmp, Can0_rx_message[5u], Can0_rx_message[6u], Can0_rx_message[7u]);
            }
            break;
        }		
    }
    if (handler_g_flagLssCanSend(GET,0) == 1u)
    {
        CAN1_MessageTransmit(LSS_CIA305_RESPONSE_ID,0x8u,Can0_tx_message,CAN_MODE_NORMAL, CAN_MSG_ATTR_TX_FIFO_DATA_FRAME);    
    }
}

/**
****************************************************************************
* @brief  readCommandProcess
*
* Analysis command, and send data to CAN buffer
*
* @param [in] s_CMD 
* @return uint8_t
*****************************************************************************
*/
uint8_t readCommandProcess(uint8_t s_CMD)
{
    uint8_t i;
    uint8_t bq_CmdLength;
    uint8_t pic_CmdLength;
    uint8_t pic_CMDP;
    uint8_t* pic_CanDataP;
    uint8_t return_Value;
    bool s_flagBqCAN;
    bool s_flagPicCAN;
    uint8_t const s_BqCanCmdT[]= {0x00,0x08,0x09,0x0a,0x0b,
                                  0x0d,0x0f,0x16,0x17,0x18,
                                  0x1b,0x1c,0x23,0x3c,0x3d,
                                  0x3e,0x3f,0x4f,0x70,0x71,
                                  0x72,0x73,0xe8,0xe9,0x00};
    uint32_t const s_PicCanCmdT[]= 
    {
        (0x20 + (0x02u * 256u)), MANUFACTURE_NAME_ADDR,
        (0x86 + (0x01u * 256u)), FAULTID_ADDR,
        (0x90 + (0x02u * 256u)), PIC_CHG_PROTECT_ADDR,//
        (0x91 + (0x02u * 256u)), PIC_DSG_PROTECT_ADDR,//
        (0xd2 + (0x01u * 256u)), PIC_SYSSIGNAL_ADDR,
        
        (0xd3 + (0x01u * 256u)), PIC_FET_STATUS_ADDR,
        (0xd4 + (0x04u * 256u)), READ_UID_ADDR,
        (0xd5 + (0x01u * 256u)), COMM_SUCCEED_RATE_ADDR,
        (0xd6 + (0x04u * 256u)), PIC_VERSION_ADDR,
        (0xd7 + (0x04u * 256u)), BL_VERSION_ADDR,
        
        (0xd8 + (0x02u * 256u)), PIC_MINTEMP_ADDR,
        (0xd9 + (0x02u * 256u)), PIC_MAXTEMP_ADDR,
        (0xda + (0x02u * 256u)), PIC_MINFETTEMP_ADDR,
        (0xdb + (0x02u * 256u)), PIC_MAXFETTEMP_ADDR,
        (0xdc + (0x01u * 256u)), PIC_TESTSTATUS_ADDR,
        
        (0xf0 + (0x01u * 256u)), PIC_CHARGE_MODE_ADDR,
        (0xf1 + (0x01u * 256u)), PIC_CHG_EN_CTRL_ADDR,
        (0xf2 + (0x01u * 256u)), PIC_DSG_EN_CTRL_ADDR,
        (0xf3 + (0x02u * 256u)), PIC_PACKVOLT_ADDR,
        (0xf4 + (0x02u * 256u)), PIC_BATTVOLT_ADDR,
        
        (0xf5 + (0x02u * 256u)), PIC_PACKSPVOLT_ADDR,
        (0xf6 + (0x0u * 256u)), PIC_VERSION_ADDR,
        (0xf7 + (0x02u * 256u)), CAN_ERR_TIMES_ADDR,
        (0xf8 + (0x02u * 256u)), CAN_OVF_TIMES_ADDR,
        (0xff + (0x01u * 256u)), PIC_RESETCAUSE_ADDR,
        
        (0xc1 + (0x02u * 256u)), PIC_NTC3TEMP_ADDR,  /*7*/
        (0xc2 + (0x02u * 256u)), PIC_NTC4TEMP_ADDR,
        (0xc3 + (0x02u * 256u)), PIC_NTC5TEMP_ADDR,
        (0xc4 + (0x02u * 256u)), PIC_NTC6TEMP_ADDR,
        (0xc5 + (0x02u * 256u)), PIC_NTC11TEMP_ADDR,
        
        (0xc6 + (0x02u * 256u)), PIC_NTC12TEMP_ADDR,  /*8*/
        (0xc7 + (0x02u * 256u)), PIC_NTC14TEMP_ADDR,
        (0xc8 + (0x02u * 256u)), PIC_NTC15TEMP_ADDR,
        (0xf9 + (0x04u * 256u)), TEST_ADDR,
        0u,0u
    };
    Can0_tx_message[4] = 0x00;
    Can0_tx_message[5] = 0x00;
    Can0_tx_message[6] = 0x00;
    Can0_tx_message[7] = 0x00;
    s_flagBqCAN = 0u;
    s_flagPicCAN = 0u;
    for (i = 0; i < BQCAN_COMM_LENGTH; i++)
    {
        if (s_CMD == s_BqCanCmdT[i])
        {
            s_flagBqCAN = 1u;
            break;
        }
    }
    for (i = 0; i < PICCAN_COMM_LENGTH; i++)
    {
        if (s_CMD == (uint8_t)s_PicCanCmdT[i * 2u])
        {
            pic_CMDP = i;
            s_flagPicCAN = 1u;
            break;
        }
    }
    if (s_flagBqCAN == 1u)
    {
        if (s_CMD == 0x23)
        {
            bq_CmdLength = 4u;
            return_Value = 4u;
            Can0_tx_message[0] = 0x43;
        }
        else
        {
            bq_CmdLength = 2u;
            return_Value = 2u;
            Can0_tx_message[0] = 0x4b;
        }
        if (readBattData(s_CMD,(uint32_t)bq_CmdLength,&Can0_tx_message[4]) == false)
        {  
            return_Value = 255u;
        }
    }
    else if (s_flagPicCAN == 1u)
    {
        pic_CmdLength = s_PicCanCmdT[pic_CMDP * 2] / 256u;
        pic_CanDataP = (uint8_t*)s_PicCanCmdT[pic_CMDP * 2 + 1u];
        for (i = 0; i < 4; i++)
        {
            if (i < pic_CmdLength)
            {
                Can0_tx_message[i + 4u] = *pic_CanDataP;
                pic_CanDataP++;
            }
        }
        return_Value = pic_CmdLength;
        if (pic_CmdLength == 1u)
        {
            Can0_tx_message[0] = 0x4f;
        }
        else if (pic_CmdLength == 2u)
        {
            Can0_tx_message[0] = 0x4b;
        }
        else
        {
            Can0_tx_message[0] = 0x43;
        }
    }
    else
    {
        return_Value = 254u;
        Can0_tx_message[0] = 0x80;
        Can0_tx_message[4] = 0x11;
        Can0_tx_message[5] = 0x00;
        Can0_tx_message[6] = 0x09;
        Can0_tx_message[7] = 0x06;   
    }
    return return_Value;
}

/**
****************************************************************************
* @brief CANopenProcessSDO
*
* put together SDO
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void CANopenProcessSDO(void)
{
    uint8_t i;
    uint8_t s_CStype;
    uint32_t s_index;
    uint8_t s_canDataLength;
    uint8_t* s_canOpenDataP;
    uint8_t s_canOpenCMDP;
    
    static uint8_t s_blockDataBuff[20u];
    static uint8_t s_blockDataP;
    static uint8_t s_flagReadBlock;
    static uint8_t s_flagBlockOdd;
    static uint8_t s_canBlockLength;
    static uint32_t s_canBlockIndex;
 
    uint32_t const s_canOpenCmdT[]=
    {
        (0x04ul + (0x100000ul * 256u)), DEVICE_TYPE_ADDR,
        (0x01ul + (0x100100ul * 256u)), ERR_REGISTER_ADDR,
        (0x0Eul + (0x100800ul * 256u)), MANUF_NAME_ADDR,
        (0x04ul + (0x100900ul * 256u)), HW_VERSION_ADDR,
        (0x08ul + (0x100A00ul * 256u)), FW_VERSION_ADDR,
        
        (0x04ul + (0x101400ul * 256u)), COBID_EMCY_ADDR,
        (0x82ul + (0x101500ul * 256u)), INHIBIT_TIME_EMCY_ADDR,
        (0x84ul + (0x101600ul * 256u)), CON_HB_TIME_ADDR,
        (0x82ul + (0x101700ul * 256u)), PRO_HB_TIME_ADDR,
        (0x01ul + (0x101800ul * 256u)), IDENTITY_OBJECT_SUB_ADDR,
        
        (0x04ul + (0x101801ul * 256u)), VENDOR_ID_ADDR,
        (0x04ul + (0x101802ul * 256u)), PRODUCT_CODE_ADDR,
        (0x04ul + (0x101803ul * 256u)), REVISION_NUMBER_ADDR,
        (0x04ul + (0x101804ul * 256u)), SERIAL_NUMBER_ADDR,
        (0x01ul + (0x120000ul * 256u)), SDO_SERVER1_SUB_ADDR,
        
        (0x04ul + (0x120001ul * 256u)), COB_ID_RX_ADDR,
        (0x04ul + (0x120002ul * 256u)), COB_ID_TX_ADDR,
        (0x01ul + (0x120003ul * 256u)), NOTE_ID_SDO_ADDR,
        (0x01ul + (0x600000ul * 256u)), CANBATT_STATUS_ADDR,
        (0x01ul + (0x600100ul * 256u)), CHARGER_STATUS_ADDR,
        
        (0x02ul + (0x601000ul * 256u)), TEMPERATURE_ADDR,
        (0x01ul + (0x602000ul * 256u)), BATTPARMETERS_SUB_ADDR,
        (0x01ul + (0x602001ul * 256u)), BATT_TYPE_ADDR,
        (0x02ul + (0x602002ul * 256u)), BATT_CAP_ADDR,
        (0x02ul + (0x602003ul * 256u)), MAX_CHGCURR_ADDR,
        
        (0x02ul + (0x602004ul * 256u)), CELL_NUMBER_ADDR,
        (0x01ul + (0x603000ul * 256u)), BATTSN_SUB_ADDR,
        (0x04ul + (0x603001ul * 256u)), BATTSN_LOW_ADDR,
        (0x04ul + (0x603002ul * 256u)), BATTSN_HIGH_ADDR,
        (0x04ul + (0x605000ul * 256u)), CUMULATIVE_TOTAL_CHARGE_ADDR,
        
        (0x02ul + (0x605100ul * 256u)), EXPEND_SINCELAST_CHARGE_ADDR,
        (0x02ul + (0x605200ul * 256u)), RETURND_DURINGLAST_CHARGE_ADDR,
        (0x04ul + (0x606000ul * 256u)), BATT_VOLTAGE_ADDR,
        (0x02ul + (0x607000ul * 256u)), CHARGE_CURR_PEQUESTED_ADDR,
        (0x01ul + (0x608100ul * 256u)), CANBATT_SOC_ADDR,
        
        (0x01ul + (0xD00000ul * 256u)), TEST_SUB_ADDR,
        (0x02ul + (0xD00001ul * 256u)), NTC1_ADDR,
        (0x02ul + (0xD00002ul * 256u)), NTC2_ADDR,
        (0x02ul + (0xD00003ul * 256u)), NTC3_ADDR,
        (0x02ul + (0xD00004ul * 256u)), NTC4_ADDR,
        
        (0x02ul + (0xD00005ul * 256u)), NTC5_ADDR,
        (0x02ul + (0xD00006ul * 256u)), NTC6_ADDR,
        (0x02ul + (0xD00007ul * 256u)), NTC7_ADDR,
        (0x02ul + (0xD00008ul * 256u)), NTC8_ADDR,
        (0x02ul + (0xD00009ul * 256u)), NTC9_ADDR,
        
        (0x02ul + (0xD0000Aul * 256u)), NTC10_ADDR,
        (0x02ul + (0xD0000Bul * 256u)), NTC11_ADDR,
        (0x02ul + (0xD0000Cul * 256u)), NTC12_ADDR,
        (0x02ul + (0xD0000Dul * 256u)), NTC13_ADDR,
        (0x02ul + (0xD0000Eul * 256u)), NTC14_ADDR,
        
        (0x02ul + (0xD0000Ful * 256u)), NTC15_ADDR,
        (0x02ul + (0xD00010ul * 256u)), NTC16_ADDR,
        (0x02ul + (0xD00011ul * 256u)), PACK_VOLTAGE_ADC_VALUE_ADDR,
        (0x02ul + (0xD00012ul * 256u)), BATT_VOLTAGE_ADC_VALUE_ADDR,
        (0x02ul + (0xD00013ul * 256u)), BUCK_CHARGE_ADC_VALUE_ADDR,
        
        (0x02ul + (0xD00014ul * 256u)), RESERVED1_ADC_VALUE_ADDR,
        (0x02ul + (0xD00015ul * 256u)), RESERVED2_ADC_VALUE_ADDR,
        (0x02ul + (0xD00016ul * 256u)), RESERVED3_ADC_VALUE_ADDR,
        (0x02ul + (0xD00017ul * 256u)), RESERVED4_ADC_VALUE_ADDR,
        (0x02ul + (0xD00018ul * 256u)), RESERVED5_ADC_VALUE_ADDR,
        
        (0x04ul + (0xD00020ul * 256u)), PIC_VERSION_ADDR,
        (0x04ul + (0xD00021ul * 256u)), BL_VERSION_ADDR,
        (0x04ul + (0xD00022ul * 256u)), READ_UID_ADDR,
        (0x02ul + (0xD00030ul * 256u)), CHARGE_FAULT_ADDR,
        (0x02ul + (0xD00031ul * 256u)), DISCHARGE_FAULT_ADDR,
        
        (0x02ul + (0xD00032ul * 256u)), CHARGE_MODE_ADDR,
        (0x02ul + (0xD00033ul * 256u)), CHARGE_ENABLE_ADDR,
        (0x02ul + (0xD00034ul * 256u)), DISCHARGE_ENABLE_ADDR,
        (0x02ul + (0xD00035ul * 256u)), CELL_POSITION_ADDR,          // CHARGER_FAULT_FLAG_ADDR,
        (0x02ul + (0xD00036ul * 256u)), PERIPHERAL_STATUS_ADDR,
        
        (0x02ul + (0xD00037ul * 256u)), CAN_ERR_TIMES_ADDR,
        (0x02ul + (0xD00038ul * 256u)), CAN_OVF_TIMES_ADDR,
        (0x02ul + (0xD00039ul * 256u)), PIC_STATUS1_ADDR,
        (0x02ul + (0xD0003Aul * 256u)), PIC_STATUS2_ADDR,
        (0x02ul + (0xD0003Bul * 256u)), CH_DSG_ENABLE_ADDR,
        
        (0x02ul + (0xD0003Cul * 256u)), PIC_FET_STATUS_ADDR,
        (0x02ul + (0xD0003Dul * 256u)), COMM_SUCCEED_RATE_ADDR,
        (0x01ul + (0x180000ul * 256u)), TPDO_PARAMETER_SUB_ADDR,
        (0x04ul + (0x180001ul * 256u)), TPDO_PARAMETER_NODE_ID1_ADDR,
        (0x01ul + (0x180002ul * 256u)), TPDO_PARAMETER_TYPE_ADDR,
        
        (0x02ul + (0x180003ul * 256u)), TPDO_PARAMETER_TIME_ADDR,
        (0x01ul + (0x180100ul * 256u)), TPDO_PARAMETER_SUB_ADDR,
        (0x04ul + (0x180101ul * 256u)), TPDO_PARAMETER_NODE_ID2_ADDR,
        (0x01ul + (0x180102ul * 256u)), TPDO_PARAMETER_TYPE_ADDR,
        (0x02ul + (0x180103ul * 256u)), TPDO_PARAMETER_TIME_ADDR,
        
        (0x01ul + (0x180200ul * 256u)), TPDO_PARAMETER_SUB_ADDR,
        (0x04ul + (0x180201ul * 256u)), TPDO_PARAMETER_NODE_ID3_ADDR,
        (0x01ul + (0x180202ul * 256u)), TPDO_PARAMETER_TYPE_ADDR,
        (0x02ul + (0x180203ul * 256u)), TPDO_PARAMETER_TIME_ADDR,
        (0x01ul + (0x180300ul * 256u)), TPDO_PARAMETER_SUB_ADDR,
        
        (0x04ul + (0x180301ul * 256u)), TPDO_PARAMETER_NODE_ID4_ADDR,
        (0x01ul + (0x180302ul * 256u)), TPDO_PARAMETER_TYPE_ADDR,
        (0x02ul + (0x180303ul * 256u)), TPDO_PARAMETER_TIME_ADDR,
        (0x01ul + (0x480000ul * 256u)), CAN_BATT_SOH_ADDR,
        (0x02ul + (0x480100ul * 256u)), CAN_BATT_OPERATION_MODE_ADDR,
        
        (0x02ul + (0x480200ul * 256u)), CAN_BATT_CHARGE_FAULT_ADDR,
        (0x02ul + (0x480300ul * 256u)), CAN_BATT_DISCHARGE_FAULT_ADDR,
        (0x02ul + (0x480400ul * 256u)), CAN_BATT_CURRENT_ADDR,
        (0x02ul + (0x480500ul * 256u)), CAN_REGEN_CURRENT_ADDR,
        (0x02ul + (0x480600ul * 256u)), CAN_CHARGE_CURRENT_ADDR,

		(0x02ul + (0x480700ul * 256u)), CAN_DISCHARGE_CURRENT_ADDR,
        (0x02ul + (0x480800ul * 256u)), CAN_MIN_CELL_TEMP_ADDR,
        (0x02ul + (0x480900ul * 256u)), CAN_MAX_CELL_TEMP_ADDR,
        (0x02ul + (0x480A00ul * 256u)), CAN_MIN_CELL_VOLT_ADDR,
        (0x02ul + (0x480B00ul * 256u)), CAN_MAX_CELL_VOLT_ADDR,

		(0x01ul + (0x480C00ul * 256u)), CAN_SYSTEM_SOC_ADDR,
        (0x01ul + (0x480D00ul * 256u)), CAN_CELL_BALANCING_STATUS_ADDR,
        (0x02ul + (0x480E00ul * 256u)), CAN_PACK_VOLTAGE_ADDR,
        (0x02ul + (0x480F00ul * 256u)), CAN_REMAIN_RUN_TIME_ADDR,
        (0x02ul + (0x481000ul * 256u)), CAN_HEATER_STATUS_ADDR,

		(0x82ul + (0x481100ul * 256u)), CAN_HEATER_CONTROL_ADDR,
        (0x01ul + (0x1A0000ul * 256u)), TPDO_MAPPING1_SUB_ADDR,
        (0x04ul + (0x1A0001ul * 256u)), TPDO_MAPPING1_ENTR1_ADDR,
        (0x04ul + (0x1A0002ul * 256u)), TPDO_MAPPING1_ENTR2_ADDR,
        (0x04ul + (0x1A0003ul * 256u)), TPDO_MAPPING1_ENTR3_ADDR,

		(0x04ul + (0x1A0004ul * 256u)), TPDO_MAPPING1_ENTR4_ADDR,
        (0x04ul + (0x1A0005ul * 256u)), TPDO_MAPPING1_ENTR5_ADDR,
        (0x01ul + (0x1A0100ul * 256u)), TPDO_MAPPING2_SUB_ADDR,
        (0x04ul + (0x1A0101ul * 256u)), TPDO_MAPPING2_ENTR1_ADDR,
        (0x04ul + (0x1A0102ul * 256u)), TPDO_MAPPING2_ENTR2_ADDR,

		(0x04ul + (0x1A0103ul * 256u)), TPDO_MAPPING2_ENTR3_ADDR,
        (0x04ul + (0x1A0104ul * 256u)), TPDO_MAPPING2_ENTR4_ADDR,
        (0x01ul + (0x1A0200ul * 256u)), TPDO_MAPPING3_SUB_ADDR,
        (0x04ul + (0x1A0201ul * 256u)), TPDO_MAPPING3_ENTR1_ADDR,
        (0x04ul + (0x1A0202ul * 256u)), TPDO_MAPPING3_ENTR2_ADDR,

		(0x04ul + (0x1A0203ul * 256u)), TPDO_MAPPING3_ENTR3_ADDR,
        (0x04ul + (0x1A0204ul * 256u)), TPDO_MAPPING3_ENTR4_ADDR,
        (0x01ul + (0x1A0300ul * 256u)), TPDO_MAPPING4_SUB_ADDR,
        (0x04ul + (0x1A0301ul * 256u)), TPDO_MAPPING4_ENTR1_ADDR,
        (0x04ul + (0x1A0302ul * 256u)), TPDO_MAPPING4_ENTR2_ADDR,

		(0x04ul + (0x1A0303ul * 256u)), TPDO_MAPPING4_ENTR3_ADDR,
        (0x04ul + (0x1A0304ul * 256u)), TPDO_MAPPING4_ENTR4_ADDR,
        (0x04ul + (0x1A0305ul * 256u)), TPDO_MAPPING4_ENTR5_ADDR,        

        (0x01ul + (0x000000ul * 256u)), FERR_ADDR
    };
    
    handler_g_canAbortCode (SET, OBJECT_SUCCEED);
    Can0_tx_message[1u] = Can0_rx_message[1u];
    Can0_tx_message[2u] = Can0_rx_message[2u];
    Can0_tx_message[3u] = Can0_rx_message[3u];
    Can0_tx_message[4] = 0u;
    Can0_tx_message[5] = 0u;
    Can0_tx_message[6] = 0u;
    Can0_tx_message[7] = 0u;  
        
    s_CStype = Can0_rx_message[0u];
    s_index = (((uint32_t)Can0_rx_message[2u] * 256ul + (uint32_t)Can0_rx_message[1]) * 256ul + (uint32_t)Can0_rx_message[3]) * 256ul;//((*256)*256)*256

	//for (i = 0; i < CANOPEN_COMM_LENGTH; i++)
	for (i = 0; i < (sizeof(s_canOpenCmdT) >> 3); i++)
    {
        if ((s_index & 0xffff0000) == (s_canOpenCmdT[i * 2u] & 0xffff0000))
        {
            s_canOpenCMDP = i;
            break;
        }
    }
    
    //if (i >= CANOPEN_COMM_LENGTH)
    if (i >= (sizeof(s_canOpenCmdT) >> 3))
    {
        handler_g_canAbortCode (SET, OBJECT_NOT_EXIST);
    }
    //for (i = 0; i < CANOPEN_COMM_LENGTH; i++)
    for (i = 0; i < (sizeof(s_canOpenCmdT) >> 3); i++)
    {
        if (s_index == (s_canOpenCmdT[i * 2u] & 0xffffff00))
        {
            s_canOpenCMDP = i;
            break;
        }
    }
    
    //if ((i >= CANOPEN_COMM_LENGTH) && (handler_g_canAbortCode(GET,0) == OBJECT_SUCCEED))
    if ((i >= (sizeof(s_canOpenCmdT) >> 3)) && (handler_g_canAbortCode(GET,0) == OBJECT_SUCCEED))
    {
        handler_g_canAbortCode (SET, SUB_INDEX_NOT_EXIST);
    }
    handler_g_flagTest (SET, s_canOpenCMDP);
    s_canDataLength = (uint8_t)s_canOpenCmdT[s_canOpenCMDP * 2u];
    s_canOpenDataP = (uint8_t*)s_canOpenCmdT[s_canOpenCMDP * 2u + 1u];
    switch (s_CStype)
    {
        case 0x21u:
        {
            s_flagReadBlock = 0u;
            handler_g_canAbortCode (SET, OBJECT_NOT_SUPPORT_WRITE);
            break;
        }
        case 0x2Fu:
        {
            s_flagReadBlock = 0u;
            handler_g_canAbortCode (SET, OBJECT_NOT_SUPPORT_WRITE);
            break;
        }
        case 0x23u:
        {
            s_flagReadBlock = 0u;
            if (s_canDataLength < 0x80u)
            {
                handler_g_canAbortCode (SET, OBJECT_NOT_SUPPORT_WRITE);
            }
            else if (s_canDataLength != 0x84u)
            {
                handler_g_canAbortCode (SET, OBJECT_LENGTH_NOT_MATCH);
            }
            else
            {
                Can0_tx_message[0u] = 0x60u;
                *s_canOpenDataP = Can0_rx_message[4u];
                *(s_canOpenDataP + 1u) = Can0_rx_message[5u];
                *(s_canOpenDataP + 2u) = Can0_rx_message[6u];        
                *(s_canOpenDataP + 3u) = Can0_rx_message[7u];        
            }
            break;
        }
        case 0x2Bu:
        {
            s_flagReadBlock = 0u;
            if (s_canDataLength < 0x80u)
            {
                handler_g_canAbortCode (SET, OBJECT_NOT_SUPPORT_WRITE);
            }
            else if (s_canDataLength != 0x82u)
            {
                handler_g_canAbortCode (SET, OBJECT_LENGTH_NOT_MATCH);
            }
            else
            {
                Can0_tx_message[0u] = 0x60;
                *s_canOpenDataP = Can0_rx_message[4u];
                *(s_canOpenDataP + 1u) = Can0_rx_message[5u];  
            }
            break;
        }
        case 0x40u:
        {
            s_canDataLength &= 0x7fu;
            if (s_canDataLength >= 5u)
            {
                Can0_tx_message[0u] = 0x41;
                Can0_tx_message[4u] = s_canDataLength; 
                s_canBlockLength = s_canDataLength;
                s_flagReadBlock = 1u;
                s_flagBlockOdd = 0u;
                s_canBlockIndex = s_index;
                s_blockDataP = 0u;
                for (i = 0; i < 20u; i++)
                {
                    s_blockDataBuff[i] = 0u;
                }
                for (i = 0; i < s_canDataLength; i++)
                {
                    s_blockDataBuff[i] = *s_canOpenDataP;
                    s_canOpenDataP++;
                }
            }
            else
            {
                Can0_tx_message[0u] = s_returnReadCSTab[s_canDataLength];
                for (i = 0; i < s_canDataLength; i++)
                {
                    Can0_tx_message[4u + i] = *s_canOpenDataP;
                    s_canOpenDataP++;
                }
            }
            break;
        }
        case 0x60u:
        case 0x70u:
        {
            if ((s_flagReadBlock == 0u) || (s_canBlockIndex != s_index))
            {
                handler_g_canAbortCode (SET, OBJECT_NOT_EXIST);
            }
            if (s_CStype == 0x60u)
            {
                if (s_flagBlockOdd == 1u)
                {
                    handler_g_canAbortCode (SET, OBJECT_NOT_EXIST);
                }
                s_flagBlockOdd = 1u;
            }
            else
            {
                if (s_flagBlockOdd == 0u)
                {
                    handler_g_canAbortCode (SET, OBJECT_NOT_EXIST);
                }
                s_flagBlockOdd = 0u;
            }
            
            for (i = 0; i < 7u; i++)
            {
                Can0_tx_message[1u + i] = s_blockDataBuff[s_blockDataP + i];
            }
            s_blockDataP += 7u;
            
            if (s_canBlockLength >= 8u)
            {
                Can0_tx_message[0u] = (s_CStype & 0x10u);
                s_canBlockLength -= 7u;
            }
            else
            {
                Can0_tx_message[0u] = s_endByteLengthTab[s_canBlockLength] + (s_CStype & 0x10u);
                s_flagReadBlock = 0u;
            }
            break;
        }
        default:
        { 
            handler_g_canAbortCode (SET, OBJECT_NOT_EXIST);
            break;
        }   
    }
    if (handler_g_canAbortCode(GET,0) != OBJECT_SUCCEED)
    {
        s_flagReadBlock = 0u;
        Can0_tx_message[0] = 0x80;
        Can0_tx_message[4] = s_abortCodeTab[handler_g_canAbortCode(GET,0)][0u];
        Can0_tx_message[5] = s_abortCodeTab[handler_g_canAbortCode(GET,0)][1u];
        Can0_tx_message[6] = s_abortCodeTab[handler_g_canAbortCode(GET,0)][2u];
        Can0_tx_message[7] = s_abortCodeTab[handler_g_canAbortCode(GET,0)][3u];   
    }
    if ((handler_g_nodeState(GET,0) == NODE_STATE_OPERATION) || (handler_g_nodeState(GET,0) == NODE_STATE_PREOPERATION))
    {
        CAN1_MessageTransmit((SDO_UPLOAD_FUNCCODE + handler_g_note_ID(GET,0)),0x8u,Can0_tx_message,CAN_MODE_NORMAL, CAN_MSG_ATTR_TX_FIFO_DATA_FRAME);
    }
}
/**
* @brief AutoBroadcast 
*
* every 400ms, average parallel SOC values, load broadcast data, 
* calculate broadcast ID, and broadcast
*
* @param NONE
* @return NONE
*/
void AutoBroadcast (void)
{
    static uint8_t s_parallelTimer = 0u;
    static uint8_t s_dataNum = NO1_DATA;
    
    uint32_t canID;
    uint32_t sumSOC = 0u;
    uint8_t i;
       
	static uint16_t s_debounceBQSleepWakeEn = 0u;
	static uint16_t s_debounceBQSleepWakeDisEN = 0u;
	static uint8_t s_flagBQSleepWake = 0u;
	/* gor Genie Request*/
	if (g_flagBQSleepWake == 1u)
	{
		s_debounceBQSleepWakeDisEN = 0u;
		if (++s_debounceBQSleepWakeEn >= TIMER5MS_1S)
		{
		   s_flagBQSleepWake = 1u;
		}
	}
	else
	{
		s_debounceBQSleepWakeEn = 0u;
		if (++s_debounceBQSleepWakeDisEN >= TIMER5MS_1S)
		{
			s_flagBQSleepWake = 0u;
		}
	}

    
    //if ((++s_parallelTimer >= TIMER5MS_400MS) && (g_testDisableBroadcast == 0u) )
    if (/*(TXB0CONbits.TXREQ == 0b0) &&*/ (++s_parallelTimer >= TIMER5MS_400MS) && (g_testDisableBroadcast == 0u) 
        && ((g_nodeState == NODE_STATE_OPERATION) || (s_flagBQSleepWake == 1u)) /*&& (g_flagJ1939ACLFail == 0u)*/ && (g_flagDisableBAM == 0u) && (g_flagCompleteIdRequest == 1u))
    {
        s_parallelTimer = 0u;//TIMER5MS_400MS;  

        /*update sysSOC*/
        for (i = 0u; i < g_parallelQuantity; i++)
        {
            sumSOC += g_IDSOC[i];
        }
        g_sysSOC = (uint8_t)(sumSOC/g_parallelQuantity);

        /*update Operation Mode*/
        if((g_flagPicChgProtect.word == 0u) && (g_flagPicDsgProtect.word == 0u))
        {
            if (g_chargeMode == STANDBY_MODE)
            {
                g_operationMode = OPERATION_MODE_STANDBY;
            }
            else if ((g_chargeMode >= PRE_DISCHARGE_MODE) && (g_chargeMode <= PRE_DISCHARGE2_MODE))
            {
                g_operationMode = OPERATION_MODE_DISCHARGE;
            }
            else if ((g_chargeMode >= PRE_CHARGE_MODE) && (g_chargeMode <= TERMINATION_MODE))
            {
                g_operationMode = OPERATION_MODE_CHARGE;
            }
            else
            {
                g_operationMode = OPERATION_MODE_FAULT;
            }
        }
        else
        {
            g_operationMode = OPERATION_MODE_FAULT;
        }

        /***Load Broadcast Data***/
        s_broadcastData[0u][0u] = (uint8_t)handler_g_battSOC(GET,0);
        s_broadcastData[0u][1u] = (uint8_t)handler_g_battSOH(GET,0);
        s_broadcastData[0u][2u] = (uint8_t)handler_g_operationMode(GET,0);
        s_broadcastData[0u][3u] = (uint8_t)(handler_g_operationMode(GET,0) >> 8u);
        s_broadcastData[0u][4u] = (uint8_t)g_flagPicChgProtect.word(GET,0);//ChargeFault
        s_broadcastData[0u][5u] = (uint8_t)(g_flagPicChgProtect.word >> 8u);
        s_broadcastData[0u][6u] = (uint8_t)g_flagPicDsgProtect.word;//DischargeFault
        s_broadcastData[0u][7u] = (uint8_t)(g_flagPicDsgProtect.word >> 8u);

        s_broadcastData[1u][0u] = (uint8_t)handler_g_battCurr(GET,0);//Current
        s_broadcastData[1u][1u] = (uint8_t)(handler_g_battCurr(GET,0) >> 8u);
        s_broadcastData[1u][2u] = (uint8_t)handler_g_broadcastRegenCurrLimit(GET,0); // Regen Current Limit
        s_broadcastData[1u][3u] = (uint8_t)(handler_g_broadcastRegenCurrLimit(GET,0) >> 8u);
        s_broadcastData[1u][4u] = (uint8_t)handler_g_broadcastChgCurrLimit(GET,0);    // ChargeCurrent Limit
        s_broadcastData[1u][5u] = (uint8_t)(handler_g_broadcastChgCurrLimit(GET,0) >> 8u);
        s_broadcastData[1u][6u] = (uint8_t)handler_g_broadcastDsgCurrLimit(GET,0);//DischargeCurrentLimit
        s_broadcastData[1u][7u] = (uint8_t)(handler_g_broadcastDsgCurrLimit(GET,0) >> 8u);

        s_broadcastData[2u][0u] = (uint8_t)handler_g_battTemp(GET,0);//Temperature
        s_broadcastData[2u][1u] = (uint8_t)(handler_g_battTemp(GET,0) >> 8u);
        s_broadcastData[2u][2u] = (uint8_t)handler_g_minTemp(GET,0);
        s_broadcastData[2u][3u] = (uint8_t)(handler_g_minTemp(GET,0) >> 8u);
        s_broadcastData[2u][4u] = (uint8_t)handler_g_maxTemp(GET,0);
        s_broadcastData[2u][5u] = (uint8_t)(handler_g_maxTemp(GET,0) >> 8u);
        s_broadcastData[2u][6u] = (uint8_t)handler_g_bqRunTimeToEmpty(GET,0);
        s_broadcastData[2u][7u] = (uint8_t)(handler_g_bqRunTimeToEmpty(GET,0) >> 8u);


        s_broadcastData[3u][0u] = (uint8_t) handler_g_packVolt(GET,0);//PackVolt
        s_broadcastData[3u][1u] = (uint8_t)(handler_g_packVolt(GET,0) >> 8u);
        s_broadcastData[3u][2u] = (uint8_t) handler_g_minCellVolt(GET,0);//MinCellVolt
        s_broadcastData[3u][3u] = (uint8_t)(handler_g_minCellVolt(GET,0) >> 8u);
        s_broadcastData[3u][4u] = (uint8_t) handler_g_maxCellVolt(GET,0);//MaxCellVolt
        s_broadcastData[3u][5u] = (uint8_t)(handler_g_maxCellVolt(GET,0) >> 8u);
        s_broadcastData[3u][6u] = (uint8_t) handler_g_sysSOC(GET,0);
        s_broadcastData[3u][7u] = (uint8_t) handler_g_balanceState(GET,0);//Cell Balancing Status


    #ifdef CANOPEN_PROTOCOL
        canID = ((handler_s_dataNum(GET,0) - 1u) * 0x100u) + 0x180u + handler_g_note_ID(GET,0); 
    #else
        canID = 0x18FFBC00ul + BATT_SOURCE_ADDR_ID;
    #endif
        Can0_tx_message[0] = s_broadcastData[(3u + s_dataNum) - 4u][0u];
        Can0_tx_message[1] = s_broadcastData[(3u + s_dataNum) - 4u][1u];       
        Can0_tx_message[2] = s_broadcastData[(3u + s_dataNum) - 4u][2u];        
        Can0_tx_message[3] = s_broadcastData[(3u + s_dataNum) - 4u][3u];        
        Can0_tx_message[4] = s_broadcastData[(3u + s_dataNum) - 4u][4u];        
        Can0_tx_message[5] = s_broadcastData[(3u + s_dataNum) - 4u][5u];        
        Can0_tx_message[6] = s_broadcastData[(3u + s_dataNum) - 4u][6u];        
        Can0_tx_message[7] = s_broadcastData[(3u + s_dataNum) - 4u][7u];

        if (CAN1_MessageTransmit(canID,0x8u,Can0_tx_message,CAN_MODE_NORMAL, CAN_MSG_ATTR_TX_FIFO_DATA_FRAME) == true)
        {
    //            if ((g_commMode == MASTER_MODE) &&(((uint16_t)Can0_tx_message[4] + (uint16_t)Can0_tx_message[5] * 256u) == BROADCAST_DSG_CURR_200MA))
    //            {
    //                g_flagBroadcastBulkCurr = 1u;
    //                g_flagNeedBroadcastBulkCurr = 0u;
    //            }
            if (handler_s_dataNum(GET,0) == NO1_DATA)
            {
                handler_s_dataNum(GET,0) = NO2_DATA;
            }
            else if (handler_s_dataNum(GET,0) == NO2_DATA)
            {
                handler_s_dataNum(GET,0) = NO3_DATA;
            }
            else if (handler_s_dataNum(GET,0) == NO3_DATA)
            {
                handler_s_dataNum(GET,0) = NO4_DATA;
            }
            else
            {
                handler_s_dataNum(GET,0) = NO1_DATA;
                s_parallelTimer = 0u;
            }
        }
    } 
}



/**
****************************************************************************
* @brief ConvertBattSNFunction
*
* convert battSN to 5 element 8-bit array
*
* @param [in] battSN
* @return NONE
*
*****************************************************************************
*/
static void ConvertBattSNFunction(uint16_t battSN)
{
    uint8_t i;
    uint8_t bitBuffer[5];
    uint16_t temp;
    uint16_t factor;
    
    temp = battSN;
    factor = 10000u;
    for (i = 0u; i < 5u; i++)
    {
        if (temp >= factor)
        {
            bitBuffer[i] = (uint8_t)(temp / factor);
        }
        else
        {
            bitBuffer[i] = 0u;
        }
        temp -= ((uint16_t)bitBuffer[i] * factor);
        factor = factor / 10u;
    }
    for (i = 0u; i < 5u; i++)
    {
        bitBuffer[i] += 0x30u;
    }
    
    handler_g_battSNLow ( SET, ((uint32_t)bitBuffer[3] << 24u) + ((uint32_t)bitBuffer[2] << 16u) + ((uint32_t)bitBuffer[1] << 8u) + (uint32_t)bitBuffer[0]);
    handler_g_battSNHigh ( SET,  0x30303000u + (uint32_t)bitBuffer[4]);
}

/**
****************************************************************************
* @brief UpdateCiaBitRegister
*
* set cia batt and charger status
*
* @param NONE
* @return NONE
*
*****************************************************************************
*/
static void UpdateCiaBitRegister(void)
{   
    /*battery status*/
    if ((handler_g_chgDsgControl (GET, 0)  == CHARGE_EN) && (g_flagPicChgProtect.word == 0u))
    {
        handler_g_canBattStatus ( SET,  0x01u);
    }
    else
    {
        handler_g_canBattStatus (SET, 0x00u);
    }
    
    /*charger status*/
    if ((handler_g_chgDsgControl(GET,0) == CHARGE_EN) && ((handler_g_flagChgCurr(GET,0) == 1u) || (handler_g_flagBCH1Received1(GET,0) == 1u) || (handler_g_flagBCH1Received2(GET,0) == 1u)))
    {
        handler_g_chargerStatus(SET, 0x01u);
    }
    else
    {
        handler_g_chargerStatus (SET,  0x00u);
    }   
}

/**
****************************************************************************
* @brief FastScanFunction
*
* check LSS value
*
* @param [in] IDNumber, 
* @param [in] checkBit, 
* @param [in] LssSub, 
* @param [in] LssNext
* 
* @return NONE
*
*****************************************************************************
*/
static void FastScanFunction(uint32_t IDNumber, uint8_t checkBit, uint8_t LssSub, uint8_t LssNext)
{
    static uint8_t LSSPos = 0u;
    uint32_t checkValue;
    uint32_t maskValue;
    uint8_t i;
    
    /*load check value base on LSS Sub*/
    if (LssSub == 0u)
    {
        checkValue = handler_g_vendorID(GET, 0);
    }
    else if (LssSub == 1u)
    {
        checkValue = handler_g_productCode(GET,0);
    }
    else if (LssSub == 2u)
    {
        checkValue = handler_g_revisionNumber(GET,0);
    }
    else if (LssSub == 3u)
    {
        checkValue = handler_g_serialNumber(GET,0);
    }
    else
    {
        checkValue = 0u;
    }
    /**************************************************************************/
    if (checkBit == 0x80u)
    {
        LSSPos = 0u;
        Can0_tx_message[0u] = LSS_ID_SLAVE_CS;
        handler_g_flagLssCanSend(SET, 1u);
    }
    else if (LSSPos == LssSub)
    {
        maskValue = 0xFFFFFFFFu << checkBit;
        if ((checkValue & maskValue) == (IDNumber & maskValue))
        {
            Can0_tx_message[0u] = LSS_ID_SLAVE_CS;
            handler_g_flagLssCanSend (SET, 1u);
            
            LSSPos = LssNext;
            if ((checkBit == 0u) && (LssSub == 3u))
            {
                handler_g_lssState (SET, LSS_CONFI_STATE);
            }
        }
    }
}

/**
****************************************************************************
* @brief EmergencyObjectFunction
*
* check fault flags, set error code, and send fault through CAN
*
* @param NONE
* @return NONE
*
*****************************************************************************
*/
static void EmergencyObjectFunction(void) 
{
    static uint8_t flagSendEMCY = 0u;
    static uint16_t ErrorCode = 0u;
    static uint16_t LastErrorCode = 0u;
    static uint8_t LastErrorRegister = 0u;
    uint16_t SendId;
    static define_16bits s_checkFlag;
    
    if (((g_flagPicChgProtect.word != 0u) && (g_chgDsgControl == CHARGE_EN)) || ((g_flagPicDsgProtect.word != 0u) && (g_chgDsgControl == DISCHARGE_EN)))
    {
        /*******************************************************************************************************/
        /*error register*/
        /*******************************************************************************************************/
        g_flagGenericError = 1u;
        
        if ((((g_flagOCP == 1u) || (g_flagChgShortCircuit == 1u)) && (g_chgDsgControl == CHARGE_EN)) || 
            (((g_flagOCD == 1u) || (g_flagDsgShortCircuit == 1u)) && (g_chgDsgControl == DISCHARGE_EN)))
        {
            g_flagCurrentError = 1u;
        }
        if ((((g_flagOVP == 1u) || (g_flagChgSUVP == 1u)) && (g_chgDsgControl == CHARGE_EN)) || 
            (((g_flagCUV == 1u) || (g_flagDsgSUVP == 1u)) && (g_chgDsgControl == DISCHARGE_EN)))
        {
            g_flagVoltageError = 1u;
        }
        if ((((g_flagCHT == 1u) || (g_flagCLT == 1u) || (g_flagCHT_MOS == 1u)) && (g_chgDsgControl == CHARGE_EN)) || 
            (((g_flagDHT == 1u) || (g_flagDLT == 1u) || (g_flagDHT_MOS == 1u)) && (g_chgDsgControl == DISCHARGE_EN)))
        {
            g_flagTempError = 1u;
        }
        if (((g_flagChgAFEComFail == 1u) && (g_chgDsgControl == CHARGE_EN)) || 
            ((g_flagDsgAFEComFail == 1u) && (g_chgDsgControl == DISCHARGE_EN)))
        {
            g_flagCommError = 1u;
        }
        if ((((g_flagChgOtherProtect == 1u) || ((g_flagPicChgProtect.word & 0xfe00u) != 0u)) && (g_chgDsgControl == CHARGE_EN)) || 
            (((g_flagDsgOtherProtect == 1u) || ((g_flagPicDsgProtect.word & 0xfe00u) != 0u)) && (g_chgDsgControl == DISCHARGE_EN)))
        {
            g_flagDeviceProfileError = 1u;
        }
        
        /********************************************************************************************************/
        /*EMCY*/
        /********************************************************************************************************/
        if ((g_flagCLT == 1u) && (g_chgDsgControl == CHARGE_EN))
        {
            if ((LastErrorCode != 0x4200u) && (s_checkFlag.bitWord.bit0 == 0u))
            {
                s_checkFlag.bitWord.bit0 = 1u;
                ErrorCode = 0x4200u;
            }
        }
        else
        {
            if (s_checkFlag.bitWord.bit0 == 1u)
            {
                s_checkFlag.bitWord.bit0 = 0u;
                ErrorCode = 0x0000u;
            }
        }
        /************************************************************************/
        if (((g_flagCHT == 1u) || (g_flagCHT_MOS == 1u)) && (g_chgDsgControl == CHARGE_EN))
        {
            if ((LastErrorCode != 0x4201u) && (s_checkFlag.bitWord.bit1 == 0u))
            {
                s_checkFlag.bitWord.bit1 = 1u;
                ErrorCode = 0x4201u;
            }
        }
        else
        {
            if (s_checkFlag.bitWord.bit1 == 1u)
            {
                s_checkFlag.bitWord.bit1 = 0u;
                ErrorCode = 0x0000u;
            }
        }
        /************************************************************************/
        if ((g_flagDLT == 1u) && (g_chgDsgControl == DISCHARGE_EN))
        {
            if ((LastErrorCode != 0x4202u) && (s_checkFlag.bitWord.bit2 == 0u))
            {
                s_checkFlag.bitWord.bit2 = 1u;
                ErrorCode = 0x4202u;
            }
        }
        else
        {
            if (s_checkFlag.bitWord.bit2 == 1u)
            {
                s_checkFlag.bitWord.bit2 = 0u;
                ErrorCode = 0x0000u;
            }
        }
        /************************************************************************/
        if (((g_flagDHT == 1u) || (g_flagDHT_MOS == 1u)) && (g_chgDsgControl == DISCHARGE_EN))
        {
            if ((LastErrorCode != 0x4203u) && (s_checkFlag.bitWord.bit3 == 0u))
            {
                s_checkFlag.bitWord.bit3 = 1u;
                ErrorCode = 0x4203u;
            }
        }
        else
        {
            if (s_checkFlag.bitWord.bit3 == 1u)
            {
                s_checkFlag.bitWord.bit3 = 0u;
                ErrorCode = 0x0000u;
            }
        }
        /************************************************************************/
        if (((g_flagOCP == 1u) && (g_chgDsgControl == CHARGE_EN)) || 
            ((g_flagOCD == 1u) && (g_chgDsgControl == DISCHARGE_EN)))
        {
            if ((LastErrorCode != 0x2000u) && (s_checkFlag.bitWord.bit4 == 0u))
            {
                s_checkFlag.bitWord.bit4 = 1u;
                ErrorCode = 0x2000u;
            }
        }
        else
        {
            if (s_checkFlag.bitWord.bit4 == 1u)
            {
                s_checkFlag.bitWord.bit4 = 0u;
                ErrorCode = 0x0000u;
            }
        }
        /*************************************************************************/
        if (((g_flagChgShortCircuit == 1u) && (g_chgDsgControl == CHARGE_EN)) || 
            ((g_flagDsgShortCircuit == 1u) && (g_chgDsgControl == DISCHARGE_EN)))
        {
            if ((LastErrorCode != 0x2001u) && (s_checkFlag.bitWord.bit5 == 0u))
            {
                s_checkFlag.bitWord.bit5 = 1u;
                ErrorCode = 0x2001u;
            }
        }
        else
        {
            if (s_checkFlag.bitWord.bit5 == 1u)
            {
                s_checkFlag.bitWord.bit5 = 0u;
                ErrorCode = 0x0000u;
            }
        }
        /*************************************************************************/
        if ((g_flagOVP == 1u) && (g_chgDsgControl == CHARGE_EN)) 
        {
            if ((LastErrorCode != 0x3000u) && (s_checkFlag.bitWord.bit6 == 0u))
            {
                s_checkFlag.bitWord.bit6 = 1u;
                ErrorCode = 0x3000u;
            }
        }
        else
        {
            if (s_checkFlag.bitWord.bit6 == 1u)
            {
                s_checkFlag.bitWord.bit6 = 0u;
                ErrorCode = 0x0000u;
            }
        }
        /*************************************************************************/
        if ((g_flagCUV == 1u) && (g_chgDsgControl == DISCHARGE_EN))
        {
            if ((LastErrorCode != 0x3001u) && (s_checkFlag.bitWord.bit7 == 0u))
            {
                s_checkFlag.bitWord.bit7 = 1u;
                ErrorCode = 0x3001u;
            }
        }
        else
        {
            if (s_checkFlag.bitWord.bit7 == 1u)
            {
                s_checkFlag.bitWord.bit7 = 0u;
                ErrorCode = 0x0000u;
            }
        }
        /*************************************************************************/
        if (((g_flagChgSUVP == 1u) && (g_chgDsgControl == CHARGE_EN)) || 
            ((g_flagDsgSUVP == 1u) && (g_chgDsgControl == DISCHARGE_EN)))
        {
            if ((LastErrorCode != 0x3002u) && (s_checkFlag.bitWord.bit8 == 0u))
            {
                s_checkFlag.bitWord.bit8 = 1u;
                ErrorCode = 0x3002u;
            }
        }
        else
        {
            if (s_checkFlag.bitWord.bit8 == 1u)
            {
                s_checkFlag.bitWord.bit8 = 0u;
                ErrorCode = 0x0000u;
            }
        }
        /*************************************************************************/
        if (((g_flagChgBulkFail == 1u) && (g_chgDsgControl == CHARGE_EN)) || 
            ((g_flagDsgBulkFail == 1u) && (g_chgDsgControl == DISCHARGE_EN)))
        {
            if ((LastErrorCode != 0xff01u) && (s_checkFlag.bitWord.bit9 == 0u))
            {
                s_checkFlag.bitWord.bit9 = 1u;
                ErrorCode = 0xff01u;
            }
        }
        else
        {
            if (s_checkFlag.bitWord.bit9 == 1u)
            {
                s_checkFlag.bitWord.bit9 = 0u;
                ErrorCode = 0x0000u;
            }
        }
        /*************************************************************************/
        if ((((g_flagChgComFail == 1u) || (g_flagChgAFEComFail == 1u)) && (g_chgDsgControl == CHARGE_EN)) || 
            (((g_flagDsgComFail == 1u) || (g_flagDsgAFEComFail == 1u)) && (g_chgDsgControl == DISCHARGE_EN)))
        {
            if ((LastErrorCode != 0xff02u) && (s_checkFlag.bitWord.bit10 == 0u))
            {
                s_checkFlag.bitWord.bit10 = 1u;
                ErrorCode = 0xff02u;
            }
        }
        else
        {
            if (s_checkFlag.bitWord.bit10 == 1u)
            {
                s_checkFlag.bitWord.bit10 = 0u;
                ErrorCode = 0x0000u;
            }
        }
        /*************************************************************************/
        if ((g_flagChgOtherProtect == 1u) && (g_chgDsgControl == CHARGE_EN))
        {
            if ((LastErrorCode != 0xff03u) && (s_checkFlag.bitWord.bit11 == 0u))
            {
                s_checkFlag.bitWord.bit11 = 1u;
                ErrorCode = 0xff03u;
            }
        }
        else
        {
            if (s_checkFlag.bitWord.bit11 == 1u)
            {
                s_checkFlag.bitWord.bit11 = 0u;
                ErrorCode = 0x0000u;
            }
        }
        /*************************************************************************/
        if ((g_flagDsgOtherProtect == 1u) && (g_chgDsgControl == DISCHARGE_EN))
        {
            if ((LastErrorCode != 0xff04u) && (s_checkFlag.bitWord.bit12 == 0u))
            {
                s_checkFlag.bitWord.bit12 = 1u;
                ErrorCode = 0xff04u;
            }
        }
        else
        {
            if (s_checkFlag.bitWord.bit12 == 1u)
            {
                s_checkFlag.bitWord.bit12 = 0u;
                ErrorCode = 0x0000u;
            }
        }
        /*************************************************************************/
        if (((g_flagChgParallelErr == 1u) && (g_chgDsgControl == CHARGE_EN)) || 
            ((g_flagDsgParallelErr == 1u) && (g_chgDsgControl == DISCHARGE_EN)))
        {
            if ((LastErrorCode != 0xff05u) && (s_checkFlag.bitWord.bit13 == 0u))
            {
                s_checkFlag.bitWord.bit13 = 1u;
                ErrorCode = 0xff05u;
            }
        }
        else
        {
            if (s_checkFlag.bitWord.bit13 == 1u)
            {
                s_checkFlag.bitWord.bit13 = 0u;
                ErrorCode = 0x0000u;
            }
        }
        
    }
    else
    {
        ErrorCode = 0u;
        g_errRegister.byte = 0u;
        s_checkFlag.word = 0u;
    }
    
    if (((LastErrorCode != ErrorCode) || (LastErrorRegister != g_errRegister.byte)) && (g_flagCompleteAddrAllocate == 1u))
    {
        flagSendEMCY = 1u;
        LastErrorCode = ErrorCode;
        LastErrorRegister = g_errRegister.byte;
    }
    
    if (flagSendEMCY == 1u)
    {
        flagSendEMCY = 0u;
        Can0_tx_message[0u] = (uint8_t)ErrorCode;
        Can0_tx_message[1u] = (uint8_t)(ErrorCode >> 8u);
        Can0_tx_message[2u] = g_errRegister.byte;
        Can0_tx_message[3u] = 0u;
        Can0_tx_message[4u] = 0u;
        Can0_tx_message[5u] = 0u;
        Can0_tx_message[6u] = 0u;
        Can0_tx_message[7u] = 0u;
        
        SendId = 0x80u + g_note_ID;
        if (SendId >= 0xffu)
        {
            SendId = 0xffu;
        }
        CAN1_MessageTransmit(SendId,0x8u,Can0_tx_message,CAN_MODE_NORMAL, CAN_MSG_ATTR_TX_FIFO_DATA_FRAME);
    }
}

/* Inventus Power Proprietary */

