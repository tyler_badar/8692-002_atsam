/**
 * @file  rs232V01.c
 *
 * REVISION HISTORY
 *==============================================
 * |Ticket   |Date      |Author       |Notes
 * |----:    |:----:    |:----:       |:----
 * |Creation |02/04/22  |Tom O'Connor |Document
 * |modified |02/23/22  |broberto     |Document
 *
 * @par   COPYRIGHT NOTICE: (c) 2022 Inventus Power.  All rights reserved.
 */
/* Inventus Power Proprietary*/
#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes
#include "common_defineV01.h"
#include "mainV01.h"
#include "rs232V01.h"
#include "i2cMasterV02.h"
#include "canV01.h"
#include "adcV01.h"
#include "temperatureV01.h"
#include "detectV01.h"
#include "chgdsgV01.h"
#include "ledV01.h"
#include "J1939V01.h"
#include "CANopenV01.h"


extern uint8_t g_flagTest;



uint8_t s_sendData[TX_DATA_LENGTH]; /**< Send buf */

/**
****************************************************************************
* @brief TxOperation
* @brief Generate CHECKSUM and add it to the send buffer s_sendData[T].
*
* @param NONE
* @return NONE
* @todo #if 0 need to be removed
*****************************************************************************
*/
void TxOperation(void)
{	
    uint8_t s_checkSum;
    uint8_t i;

/**
* This preprocessor directive needs to a reason to be here.
* Either it's a build option or the code is removed.
*/
#if 1	
    i = 0;
	s_sendData[i++] = g_canAddr;  
	s_sendData[i++] = (uint8_t)(g_IDFETstate[0].byte);
	s_sendData[i++] = (uint8_t)(g_IDFETEnDisCH[0].byte);
	s_sendData[i++] = (uint8_t)(g_IDChargeMode[0]);
	s_sendData[i++] = (uint8_t)(g_IDFETEnCH[0].byte);

	s_sendData[i++] = g_IDCellS[0]; 
	s_sendData[i++] = (uint8_t)(g_cellPosition); 
	s_sendData[i++] = (uint8_t)g_commMode;          
	s_sendData[i++] = g_chgDsgControl;
	s_sendData[i++] = g_FETstate.byte;

    s_sendData[i++] = (uint8_t)(g_FETEnState.byte);
	s_sendData[i++] = (uint8_t)(g_chargeMode); 
	s_sendData[i++] = (uint8_t)(g_FETEnDisCH.byte);
	s_sendData[i++] = (uint8_t)(g_FETEnCH.byte);
	s_sendData[i++] = (uint8_t)(g_flagPicChgProtect.word>>8);
	
	s_sendData[i++] = (uint8_t)(g_flagPicChgProtect.word); 
	s_sendData[i++] = (uint8_t)(g_flagPicDsgProtect.word >> 8u); 
    s_sendData[i++] = (uint8_t)(g_flagPicDsgProtect.word); 
	s_sendData[i++] = (uint8_t)(g_minCellVolt>>8); 
	s_sendData[i++] = (uint8_t)(g_minCellVolt);
	
	s_sendData[i++] = (uint8_t)(g_maxCellVolt>>8); 
	s_sendData[i++] = (uint8_t)(g_maxCellVolt); 
	s_sendData[i++] = (uint8_t)(g_battVolt>>8); 
	s_sendData[i++] = (uint8_t)(g_battVolt);
	s_sendData[i++] = (uint8_t)(g_IDFETstate[1].byte);
	
	s_sendData[i++] = (uint8_t)(g_IDFETEnDisCH[1].byte);
	s_sendData[i++] = (uint8_t)(g_IDChargeMode[1]); 
	s_sendData[i++] = (uint8_t)(g_IDFETEnCH[1].byte);
	s_sendData[i++] = g_IDCellS[1];
	s_sendData[i++] = g_flagTest;	   
	
	s_sendData[i++] = (uint8_t)(g_PerpheralStatus.word>>8); 
	s_sendData[i++] = (uint8_t)(g_PerpheralStatus.word);
	s_sendData[i++] = g_IDCHGProtect[0];
	s_sendData[i++] = g_IDCHGProtect[1];
	s_sendData[i++] = g_controlBqFet.bytes.CHGbyte;
	s_sendData[i++] = g_controlBqFet.bytes.DSGbyte; 
	s_sendData[i++] = (uint8_t)(g_flagJ1939Status.word>>8); 
	s_sendData[i++] = (uint8_t)(g_flagJ1939Status.word);
	s_sendData[i++] = g_nodeState;
	s_sendData[i++] = (uint8_t)((g_productCode & 0xff000000) >> 24);
	s_sendData[i++] = (uint8_t)((g_productCode & 0x00ff0000) >> 16); 
	
	s_sendData[i++] = (uint8_t)((g_productCode & 0x0000ff00) >> 8);
	s_sendData[i++] = (uint8_t)((g_productCode & 0x000000ff));
	s_sendData[i++] = (uint8_t)((g_lsspPoductCode & 0xff000000) >> 24);
	s_sendData[i++] = (uint8_t)((g_lsspPoductCode & 0x00ff0000) >> 16); 
	s_sendData[i++] = (uint8_t)((g_lsspPoductCode & 0x0000ff00) >> 8);

	s_sendData[i++] = (uint8_t)((g_lsspPoductCode & 0x000000ff));
	s_sendData[i++] = (uint8_t)(g_IDbqFetState[1]);
	s_sendData[i++] = (uint8_t)(g_systemError.byte);

	
#else
	i = 0;
	s_sendData[i++] = g_canAddr;  
	s_sendData[i++] = (uint8_t)(s_uidArray[0]>>24);
	s_sendData[i++] = (uint8_t)(g_IDFETstate[0].byte);
	s_sendData[i++] = (uint8_t)(g_IDregenCurrLimit[0]>>8);
	s_sendData[i++] = (uint8_t)(g_IDregenCurrLimit[0]);
	s_sendData[i++] = (uint8_t)(s_uidArray[1]>>24); 
	s_sendData[i++] = (uint8_t)(g_IDFETEnDisCH[0].byte);
	s_sendData[i++] = (uint8_t)(g_IDChargeMode[0]);
	s_sendData[i++] = (uint8_t)(g_IDChargeMode[1]);	
	s_sendData[i++] = (uint8_t)(g_cellPosition);
	s_sendData[i++] = (uint8_t)g_commMode;          

	s_sendData[i++] = g_chgDsgControl;
	s_sendData[i++] = (uint8_t)(g_flagPicChgProtect.word>>8);
	s_sendData[i++] = (uint8_t)(g_flagPicChgProtect.word);
	s_sendData[i++] = g_FETstate.byte;
	s_sendData[i++] = (uint8_t)(g_packVolt>>8); 
	s_sendData[i++] = (uint8_t)(g_packVolt);
	s_sendData[i++] = (uint8_t)(g_minCellVolt>>8); 
	s_sendData[i++] = (uint8_t)(g_minCellVolt);
	s_sendData[i++] = (uint8_t)(g_maxCellVolt>>8); 
	s_sendData[i++] = (uint8_t)(g_maxCellVolt); 
	s_sendData[i++] = (uint8_t)(g_battVolt>>8); 
	s_sendData[i++] = (uint8_t)(g_battVolt);
	s_sendData[i++] = (uint8_t)(g_testCommandStatus.word>>8); 
	s_sendData[i++] = (uint8_t)(g_testCommandStatus.word);
	s_sendData[i++] = g_flagTest;      
	s_sendData[i++] = (uint8_t)(g_IDFETstate[1].byte);
	s_sendData[i++] = (uint8_t)(g_IDChargeMode[1]);  
    s_sendData[i++] = (uint8_t)(g_8050ChgProtect2.word >> 8u);
    s_sendData[i++] = (uint8_t)(g_8050ChgProtect2.word); 
    s_sendData[i++] = (uint8_t)(g_8050DsgProtect1.word >> 8u); 
    s_sendData[i++] = (uint8_t)(g_8050DsgProtect1.word);  
    s_sendData[i++] = (uint8_t)(g_8050DsgProtect2.word >> 8u);
    s_sendData[i++] = (uint8_t)(g_8050DsgProtect2.word);
    s_sendData[i++] = (uint8_t)(g_FETEnState.byte);	
	s_sendData[i++] = (uint8_t)(g_chargeMode); 
	s_sendData[i++] = (uint8_t)(g_FETEnDisCH.byte);
	s_sendData[i++] = (uint8_t)(g_broadcastRegenCurrLimit >> 8u);
    s_sendData[i++] = (uint8_t)(g_broadcastRegenCurrLimit); 
	s_sendData[i++] = (uint8_t)(g_flagPicDsgProtect.word >> 8u);
    s_sendData[i++] = (uint8_t)(g_flagPicDsgProtect.word); 
    s_sendData[i++] = g_IDPackPosition[0];
	s_sendData[i++] = g_IDPackPosition[1];
	s_sendData[i++] = (uint8_t)(g_adcPackSP >> 8u);
    s_sendData[i++] = (uint8_t)(g_adcPackSP); 
	s_sendData[i++] = (uint8_t)(g_adcPackVolt >> 8u); 
    s_sendData[i++] = (uint8_t)(g_adcPackVolt);
    
	
	
#endif
/**
* uint8_t s_checkSum
* TX_DATA_LENGTH = 50 bytes
* 15 bytes is a good use for an 8bit checksum (intel hex file format).
* For 50 bytes, is an 8bit checksum a good idea?
*/
    s_checkSum = 0u;
    for(i = 0u; i < (TX_DATA_LENGTH - 1); i++)
    {
        s_checkSum += s_sendData[i];
    }
    s_sendData[TX_DATA_LENGTH - 1] = s_checkSum;
    
	SERCOM0_USART_Write(s_sendData,TX_DATA_LENGTH);
	
}
/* Inventus Power Proprietary */
