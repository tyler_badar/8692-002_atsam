/**
 * @file  i2cMasterV02.c
 *
 * REVISION HISTORY
 *==============================================
 * |Ticket   |Date      |Author       |Notes
 * |----:    |:----:    |:----:       |:----
 * |Creation |02/04/22  |Tom O'Connor |Document
 * |Modified |03/02/22  |Tyler Badar  |Added function descriptions
 *
 * @par   COPYRIGHT NOTICE: (c) 2022 Inventus Power.  All rights reserved.
 */
/* Inventus Power Proprietary */
/*********************************************************/
#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes
#include "common_defineV01.h"
#include "mainV01.h"
#include "temperatureV01.h"
#include "i2cMasterV02.h"
#include "arithmeticV01.h"

uint16_t g_access = 0x1234u;
uint16_t g_battTemp;
uint16_t g_battVolt;
uint16_t g_packVolt;
int16_t g_battCurr = 0u;
uint16_t g_battSOC;
uint16_t g_sysSOC;
uint16_t g_battSOH = 0u;
uint16_t g_operationMode = 0u;
uint16_t g_bqWarningAlarm = 0u;
define_16bits g_battStatus;
define_16bits g_battSignal;
uint16_t g_battChgCurr;
uint16_t g_battChgVolt;
define_fet g_controlBqFet;
uint16_t g_minCellVolt = 3600u;
uint16_t g_maxCellVolt = 3600u;
define_16bits g_8050ChgProtect1;
define_16bits g_8050ChgProtect2;
define_16bits g_8050DsgProtect1;
define_16bits g_8050DsgProtect2;

uint16_t g_CumulativeTotalAhChargeH = 0u;
uint16_t g_CumulativeTotalAhChargeL = 0u;
uint16_t g_AhExpendedSinceLastCharge = 0u;
uint16_t g_AhReturnedDuringLastCharge = 0u;

uint16_t g_totalCommTime = 100u;
uint16_t g_successCommTime = 100u;
uint8_t g_successRate = 100u; 


static uint8_t* i2cDataP;
static uint8_t s_CmdLength;
static uint8_t s_SmbCmd;
static uint8_t s_I2Cbuff[SBS_DATA_LENGTH];
static bool s_flagRD = false;
static bool s_flagComm = false;
static uint8_t s_CmdP = 0u;
uint16_t g_bqRunTimeToEmpty = 0xffffu;
uint16_t g_battSN = 0u;


extern uint8_t g_flagTest;

bool readBattInfo(void);

/**
****************************************************************************
* @brief writeBattData
*
* - write data at *P to pack with addr 0x0B 
* - if write failed, re-initialize I2C
*
* @param [in] smbCmd 
* @param [in] cmdLength
* @param [in] *P
*
* @return [true | false ]
*****************************************************************************
*/
bool writeBattData(uint8_t smbCmd,uint32_t cmdLength,uint8_t *P)
{
    uint16_t i;
    uint8_t s_I2CCrc;
    bool s_flagWrite = true;
    
    s_I2CCrc = basic_crc(0u,(uint8_t)(BATT_ADDR << 1u));
    s_I2CCrc = basic_crc(s_I2CCrc,smbCmd);
    s_I2Cbuff[0] = smbCmd;
    for (i = 0u; i< cmdLength; i++)
    {
        s_I2CCrc = basic_crc(s_I2CCrc,*P);
        s_I2Cbuff[i+1] = *P;
        P++;
    }
    s_I2Cbuff[cmdLength + 1u] = s_I2CCrc;
    if (SERCOM5_I2C_Write(BATT_ADDR,&s_I2Cbuff[0],(cmdLength + 2u)) == false)
    {
        s_flagWrite = false;
    }
    for (i = 0u; i < 20000; i++)
    {
        if ((SERCOM5_I2C_IsBusy() == false) || (s_flagWrite == false))
        {
            break;
        }
    }
    if (i >= 20000u)
    {
        s_flagWrite = false;
        SERCOM5_I2C_Initialize();
    }
    return s_flagWrite;
}

/**
****************************************************************************
* @brief readBattData
*
* - read data at *P to pack with addr 0x0B 
* - if write failed, re-initialize I2C
*
* @param [in] smbCmd 
* @param [in] cmdLength
* @param [out] *P
*
* @return [true | false ]
*****************************************************************************
*/
bool readBattData(uint8_t smbCmd,uint32_t cmdLength,uint8_t *P)
{
    uint8_t i;
    uint8_t s_I2CCrc;
    bool s_flagRead = true;
    if (SERCOM5_I2C_WriteRead(BATT_ADDR,&smbCmd,1u,&s_I2Cbuff[0],(cmdLength+1)) == false)
    {
        s_flagRead = false;
    }
    for (i = 0u; i < 20000; i++)
    {
        if ((SERCOM5_I2C_IsBusy() == false) || (s_flagRead == false))
        {
            break;
        }
    }
    if (i >= 20000u)
    {
        s_flagRead = false;
        SERCOM5_I2C_Initialize();
    }
    if (s_flagRead == true)
    {
        s_I2CCrc = basic_crc(0u,(uint8_t)(BATT_ADDR << 1u));
        s_I2CCrc = basic_crc(s_I2CCrc,smbCmd);
        s_I2CCrc = basic_crc(s_I2CCrc,(uint8_t)((BATT_ADDR << 1u) + 1u));
        for (i = 0u; i< cmdLength; i++)
        {
            s_I2CCrc = basic_crc(s_I2CCrc,s_I2Cbuff[i]);
        }
        if (s_I2CCrc == s_I2Cbuff[cmdLength])
        {
            for (i = 0u; i < cmdLength; i++)
            {
                *P = s_I2Cbuff[i];
                P++;
            }
        }
        else
        {
            s_flagRead = false;
        }
    }
    return s_flagRead;
}

/**
****************************************************************************
* @brief readBattInfo
*
* - read all batt data
*
* @param NONE
* @return [true | false ]
*****************************************************************************
*/
bool readBattInfo(void)
{			
    uint32_t const s_SbsCmdT[]= 
    {
    	(CMD_SIGNAL +       (0x02u * 256u)), BATT_SIGNAL_ADDR,
        (CMD_TEMP +         (0x02u * 256u)), TEMP_ADDR,
        (CMD_VOLT +         (0x02u * 256u)), VOLT_ADDR,
        (CMD_CURR +         (0x02u * 256u)), CURR_ADDR,
        (CMD_SIGNAL +       (0x02u * 256u)), BATT_SIGNAL_ADDR,
        (CMD_SOC +          (0x02u * 256u)), SOC_ADDR,
		(CMD_BATT_STATUS +  (0x02u * 256u)), BATT_STATUS_ADDR, 
        (CMD_MAX_CELLVOLT + (0x02U * 256U)), MAX_CELLVOLT_ADDR,
        (CMD_SIGNAL +       (0x02u * 256u)), BATT_SIGNAL_ADDR,
        (CMD_MIN_CELLVOLT + (0x02U * 256U)), MIN_CELLVOLT_ADDR,
        (CMD_CHG_PROTECT1 + (0x02U * 256U)), CHG_PROTECT_ADDR1,
        (CMD_CHG_PROTECT2 + (0x02U * 256U)), CHG_PROTECT_ADDR2,
        (CMD_SIGNAL +       (0x02u * 256u)), BATT_SIGNAL_ADDR,
        (CMD_DSG_PROTECT1 + (0x02U * 256U)), DSG_PROTECT_ADDR1,
        (CMD_DSG_PROTECT2 + (0x02U * 256U)), DSG_PROTECT_ADDR2,
        (CMD_MAX_TEMP +     (0x82u * 256u)), MAX_TEMP_ADDR,
        (CMD_SIGNAL +       (0x02u * 256u)), BATT_SIGNAL_ADDR,
        (CMD_MIN_TEMP +     (0x82u * 256u)), MIN_TEMP_ADDR,
        (CMD_MAXFET_TEMP +  (0x82u * 256u)), MAXFET_TEMP_ADDR,
        (CMD_MINFET_TEMP +  (0x82u * 256u)), MINFET_TEMP_ADDR,
        (CMD_SIGNAL +       (0x02u * 256u)), BATT_SIGNAL_ADDR,
        (CMD_MAXPACK_TEMP + (0x82u * 256u)), MAXPACK_TEMP_ADDR,
        (CMD_MINPACK_TEMP + (0x82u * 256u)), MINPACK_TEMP_ADDR,
        (CMD_CONTROL_FET +  (0x82U * 256U)), CONTROL_FET_ADDR,
        (CMD_SIGNAL +       (0x02u * 256u)), BATT_SIGNAL_ADDR,
        (CMD_SOH+           (0x02U * 256U)), SOH_ADDR,
        (CMD_PACK_VOLT+     (0x02U * 256U)), PACK_VOLT_ADDR,
        (CMD_BQ_TIME_TO_EMPTY+(0x02U * 256U)), BQ_TIME_TO_EMPTY_ADDR,
        (CMD_BQ_SN +  		(0x02U * 256U)), BQ_SN_ADDR,
        
        
        
    };
    uint8_t s_I2CCrc;
    uint8_t i;

    
    if (s_CmdP >= (sizeof(s_SbsCmdT)>>3))
	{			
        s_CmdP = 0u;
	}

    s_CmdLength = (uint8_t)(s_SbsCmdT[s_CmdP * 2u] / 256u);     
    s_SmbCmd = (uint8_t)s_SbsCmdT[s_CmdP * 2u];                 
    i2cDataP = (uint8_t*)s_SbsCmdT[(s_CmdP * 2u) + 1u];         
    if (s_CmdLength >= 0x80u)
    {
        s_flagRD = false;
        s_CmdLength &= 0x7fu;
    }
    else
    {
        s_flagRD = true;
    }
    if (s_flagRD)   
	{
        s_flagComm = SERCOM5_I2C_WriteRead(BATT_ADDR,&s_SmbCmd,1u,&s_I2Cbuff[0],(s_CmdLength+1));
    }
    else
    {
        s_I2CCrc = basic_crc(0u,(uint8_t)(BATT_ADDR << 1u));
        s_I2CCrc = basic_crc(s_I2CCrc,s_SmbCmd);
        s_I2Cbuff[0] = s_SmbCmd;
        for (i = 0u; i< s_CmdLength; i++)
        {
            s_I2CCrc = basic_crc(s_I2CCrc,*i2cDataP);
            s_I2Cbuff[i+1] = *i2cDataP;
            i2cDataP++;
        }
        s_I2Cbuff[s_CmdLength + 1u] = s_I2CCrc;
		s_flagComm = SERCOM5_I2C_Write(BATT_ADDR,&s_I2Cbuff[0],(s_CmdLength + 2u));
    }
    for (i = 0u; i < 20000; i++)
    {
        if ((SERCOM5_I2C_IsBusy() == false) || (s_flagComm == false))
        {
            break;
        }
    }
    if (i >= 20000u)
    {
        s_flagComm = false;
        SERCOM5_I2C_Initialize();
    }
    if ((s_flagRD) && (s_flagComm))
    {
        s_I2CCrc = basic_crc(0u,(uint8_t)(BATT_ADDR << 1u));
        s_I2CCrc = basic_crc(s_I2CCrc,s_SmbCmd);
        s_I2CCrc = basic_crc(s_I2CCrc,(uint8_t)((BATT_ADDR << 1u) + 1u));
        for (i = 0u; i< s_CmdLength; i++)
        {
            s_I2CCrc = basic_crc(s_I2CCrc,s_I2Cbuff[i]);
        }
        if (s_I2CCrc == s_I2Cbuff[s_CmdLength])
        {
            for (i = 0u; i< s_CmdLength; i++)
            {
                *i2cDataP = s_I2Cbuff[i];
                i2cDataP++;
            }
        }
        else
        {
            s_flagComm = false;
        }
    }
    if (s_flagComm)
	{
		s_CmdP++;
	}
    return (s_flagComm);		
}

/**
****************************************************************************
* @brief Communication
*
* - check readBattInfo() table and calculate successRate based on 
*   successCommTime and totalCommTime
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void Communication(void)
{
    if (readBattInfo() == true)
    {
        g_successCommTime++;
        g_flagCommFail = 0u;
    }
    else
    {
        g_flagCommFail = 1u;
    }
    g_totalCommTime++;						
    
	if (g_totalCommTime >= 65535u)
    {
    	g_totalCommTime = 100u;
    	g_successCommTime = g_successRate;
    }
    g_successRate = (uint8_t)(((uint32_t)g_successCommTime * 100u + g_totalCommTime / 2u) / g_totalCommTime);
}

/* Inventus Power Proprietary */
