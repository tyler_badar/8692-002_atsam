/**
 * @file  temperatureV01.c
 *
 * REVISION HISTORY
 *==============================================
 * |Ticket   |Date      |Author       |Notes
 * |----:    |:----:    |:----:       |:----
 * |Creation |02/04/22  |Tom O'Connor |Document
 * |modified |02/23/22  |broberto     |Document
 *
 * @par   COPYRIGHT NOTICE: (c) 2022 Inventus Power.  All rights reserved.
 */
/* Inventus Power Proprietary */

#define _UNIT_TEST_
#if defined(_UNIT_TEST_)
#include <stdint.h>
#include "../tests/TempFunction/TempFunction/test_temp_function.h"
#include "../inc/temperatureV01.h"
#else
#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes
#include "adcV01.h"
#include "mainV01.h"
#include "common_defineV01.h"
#include "temperatureV01.h"
#endif

#if defined(_UNIT_TEST_)
uint16_t TempFunction(uint16_t tmpAdc);
#else
static uint16_t TempFunction(uint16_t tmpAdc);
#endif

uint16_t g_maxTemp;
uint16_t g_minTemp;
uint16_t g_maxFetTemp;
uint16_t g_minFetTemp;
uint16_t g_maxPackTemp;
uint16_t g_minPackTemp;

uint16_t g_ntc3Temp;
uint16_t g_ntc4Temp;
uint16_t g_ntc5Temp;
uint16_t g_ntc6Temp;
uint16_t g_ntc8Temp;
uint16_t g_ntc10Temp;
uint16_t g_ntc11Temp;
uint16_t g_ntc12Temp;
uint16_t g_ntc14Temp;
uint16_t g_ntc15Temp;
uint16_t g_ntc16Temp;

/**
****************************************************************************
* @brief TemeratrueDetect
* - Read the ADC temperature values and store into global vars.
* - Save the new global vars into g_NTCTemperatureTable.
* - Determine max/min System and Pack temperature.
* - Determine FET temperature.
* - Reminder: NTC are negative temperature coefficient devices.
* The larger the resistance value, the lower the temperature.
*
* @param NONE
* @return NONE
*
*****************************************************************************
*/
void TemeratrueDetect(void)
{
    uint16_t s_ntcTmp; /**< serves as swap storage */

    g_ntc3Temp = TempFunction(g_adcNTC[0]);
    g_ntc4Temp = TempFunction(g_adcNTC[1]);
    g_ntc5Temp = TempFunction(g_adcNTC[2]);
    g_ntc6Temp = TempFunction(g_adcNTC[3]);
    g_ntc8Temp = TempFunction(g_adcNTC[8]);
    g_ntc10Temp = TempFunction(g_adcNTC[6]);
    g_ntc11Temp = TempFunction(g_adcNTC[7]);
    g_ntc12Temp = TempFunction(g_adcNTC[10]);
    g_ntc14Temp = TempFunction(g_adcNTC[9]);
    g_ntc15Temp = TempFunction(g_adcNTC[5]);
    g_ntc16Temp = TempFunction(g_adcNTC[4]);

    g_maxFetTemp = g_ntc3Temp;
    g_minFetTemp = g_ntc3Temp;


    g_NTCTemperatureTable[2] = g_ntc3Temp;
    g_NTCTemperatureTable[3] = g_ntc4Temp;
    g_NTCTemperatureTable[4] = g_ntc5Temp;
    g_NTCTemperatureTable[5] = g_ntc6Temp;
    g_NTCTemperatureTable[7] = g_ntc8Temp;
    g_NTCTemperatureTable[9] = g_ntc10Temp;
    g_NTCTemperatureTable[10] = g_ntc11Temp;
    g_NTCTemperatureTable[11] = g_ntc12Temp;
    g_NTCTemperatureTable[13] = g_ntc14Temp;
    g_NTCTemperatureTable[14] = g_ntc15Temp;
    g_NTCTemperatureTable[15] = g_ntc16Temp;

    /**
    * @brief Establish min SYSTEM(?) temp
    * - Look for the highest temperature among 4 specific NTCs
    * relative to ONE specific NTC - g_ntc4Temp.
    * - set g_minTemp to that value.
    */
    s_ntcTmp = g_ntc4Temp;
    if (s_ntcTmp >= g_ntc5Temp)
    {
        s_ntcTmp = g_ntc5Temp;
    }
    if (s_ntcTmp >= g_ntc6Temp)
    {
        s_ntcTmp = g_ntc6Temp;
    }
    if(s_ntcTmp >= g_ntc15Temp)
    {
        s_ntcTmp = g_ntc15Temp;
    }
    if(s_ntcTmp >= g_ntc16Temp)
    {
        s_ntcTmp = g_ntc16Temp;
    }
    g_minTemp = s_ntcTmp;

    /**
    * @brief Establish max SYSTEM(?) temp
    * - Look for the lowest temperature among 4 specific NTCs
    * relative to ONE specific NTC - g_ntc4Temp.
    * - set g_maxTemp to that value.
    */
    s_ntcTmp = g_ntc4Temp;
    if (s_ntcTmp < g_ntc5Temp)
    {
        s_ntcTmp = g_ntc5Temp;
    }
    if (s_ntcTmp < g_ntc6Temp)
    {
        s_ntcTmp = g_ntc6Temp;
    }
    if (s_ntcTmp < g_ntc15Temp)
    {
        s_ntcTmp = g_ntc15Temp;
    }
    if (s_ntcTmp < g_ntc16Temp)
    {
        s_ntcTmp = g_ntc16Temp;
    }
    g_maxTemp = s_ntcTmp;

    /**
    * @brief Establish min PACK temp
    * - Look for the higest temperature among 3 specific NTCs
    * relative to ONE specific NTC - g_ntc8Temp.
    * - set g_minPackTemp to that value.
    */
    s_ntcTmp = g_ntc8Temp;
    if (s_ntcTmp >= g_ntc10Temp)
    {
        s_ntcTmp = g_ntc10Temp;
    }
    if (s_ntcTmp >= g_ntc11Temp)
    {
        s_ntcTmp = g_ntc11Temp;
    }
    if (s_ntcTmp >= g_ntc12Temp)
    {
        s_ntcTmp = g_ntc12Temp;
    }
    g_minPackTemp = s_ntcTmp;

    /**
    * @brief Establish max PACK temp
    * - Look for the lowest temperature among 3 specific NTCs
    * relative to ONE specific NTC - g_ntc8Temp.
    * - set g_maxPackTemp to that value.
    */
    s_ntcTmp = g_ntc8Temp;
    if (s_ntcTmp < g_ntc10Temp)
    {
        s_ntcTmp = g_ntc10Temp;
    }
    if (s_ntcTmp < g_ntc11Temp)
    {
        s_ntcTmp = g_ntc11Temp;
    }
    if (s_ntcTmp < g_ntc12Temp)
    {
        s_ntcTmp = g_ntc12Temp;
    }
    g_maxPackTemp = s_ntcTmp;
}

/**
****************************************************************************
* @brief TempFunction
* @brief Derive temperature in Kelvin from NTC
* @param [in] tmpAdc - ADC value in ohms/10 -> resistance_meas
* @return uint16_6 in Kelvin
*
* @brief temp_in_Kelvin = range_factor + resistance_pcnt_in_range
* - range_factor = 2230 + (offset*100). The offset is based on an index.
* - index = 1 through 16. (values between THERMAL_N30C_VALUE (15127U) and THERMAL_120C_VALUE (953U).
* - The offset results in +10_Kelvin steps starting at 2230 (223 Kelvin)
* - resistance_pcnt_in_range = ((resistance_lim_up - resistance_meas)*100)/(resistance_lim_up - resistance_lim_lo)
*
* @ brief IF the input resistance (ADC value) is larger than or equal to the FIRST
* temperature/resistance table value -- THERMAL_N40C_VALUE -- 2330U will be returned.
* 233 Kelvin = -40C
* @brief ELSE the resistance value will be converted to a percentage of the given resistance range
* and scaled by 100.
*
* @brief Assume values in FILE temperatureV01.h are temperature(C) vs. resistance
* @brief Values from NTCs scaled DOWN by 10 to fit into 16bits.
* - THERMAL_N40C_VALUE = 15652U = 156.52k ohms
*****************************************************************************
*/
#if defined(_UNIT_TEST_)
/** put something here that will force the traget compiler
* to throw an error
* ex. printf("UNIT TEST MODE active\n")
*/
uint16_t TempFunction(uint16_t tmpAdc)
#else
static uint16_t TempFunction(uint16_t tmpAdc)
#endif
{
    /**< Table for thermal values  */
    static uint16_t const s_flashTempData[17]=
    {
        THERMAL_N40C_VALUE, /**< largest resistance value */
        THERMAL_N30C_VALUE,
        THERMAL_N20C_VALUE,
        THERMAL_N10C_VALUE,
        THERMAL_0C_VALUE,
        THERMAL_10C_VALUE,
        THERMAL_20C_VALUE,
        THERMAL_30C_VALUE,
        THERMAL_40C_VALUE,
        THERMAL_50C_VALUE,
        THERMAL_60C_VALUE,
        THERMAL_70C_VALUE,
        THERMAL_80C_VALUE,
        THERMAL_90C_VALUE,
        THERMAL_100C_VALUE,
        THERMAL_110C_VALUE,
        THERMAL_120C_VALUE
    };

    uint8_t i;
    uint16_t temp;

    for (i = 0U; i < 16U; i++)
    {
        if (tmpAdc >= s_flashTempData[i])
        {
            break;
        }
    }
    if (i == 0U)
    {
        temp = 2330U; /** -40deg C in Kelvin */
    }
    else
    {
#if 0
        /**
        * @brief Temperature scaling from resistance is in Kelvin
        * @brief range_factor = 2230 Kelvin + (offset * 100)
        * - ex 2230 + (index * 100)
        * - ex 2230 + (1 * 100) = 2330
        * - ex 2230 + (16 * 100) = 3830
        * @brief temp_in_Kelvin = range_factor + resistance_pcnt_in_range
        * @brief temp_in_Kelvin = range_factor + (resistance_lim_up - resistance_mea)*100)/resistance_range)
        */
        temp = 2230U + ((uint16_t)i * 100U) + (uint16_t)((((uint32_t)s_flashTempData[i - 1U] -
          (uint32_t)tmpAdc) * 100U) / ((uint32_t)s_flashTempData[i - 1U] - (uint32_t)s_flashTempData[i]));
#else
        /* refactor*/
        uint16_t range_factor        = 2230U + (uint16_t)(i * 100U);
        uint32_t resistance_range    = s_flashTempData[i - 1U] - s_flashTempData[i];
        uint32_t resistance_in_range = (s_flashTempData[i - 1U] - tmpAdc) * 100U;
        temp                         = range_factor + (resistance_in_range / resistance_range);
#endif
    }
    return(temp);
}
/* Inventus Power Proprietary */
