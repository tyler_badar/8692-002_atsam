/**
 * @file  ledV01.c
 * @brief LED control file
 *
 * REVISION HISTORY 
 *==============================================
 * |Ticket   |Date      |Author       |Notes 
 * |----:    |:----:    |:----:       |:---- 
 * |Creation |02/04/22  |Tom O'Connor |Document
 * |Modified |03/09/22  |Tyler Badar  |Added function descriptions
 *
 * @par   COPYRIGHT NOTICE: (c) 2022 Inventus Power.  All rights reserved.
 */
/* Inventus Power Proprietary */

#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes
#include "common_defineV01.h"
#include "mainV01.h"
#include "i2cMasterV02.h"
#include "chgdsgV01.h"
#include "canV01.h"
#include "detectV01.h"
#include "ledV01.h" 

uint8_t g_faultID;

/**
****************************************************************************
* @brief allLed_displayOff
*
* - clear all LEDs
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void allLed_displayOff(void)
{
    LED1_RED_Clear();
    LED1_Clear();
    LED2_Clear();
    LED3_Clear();
    LED4_Clear();
    LED5_Clear();    

  
}

/**
****************************************************************************
* @brief SOCredLed_displayOn
*
* - turn on red LED and all others off
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void SOCredLed_displayOn(void)
{
    LED1_RED_Set();
    LED1_Clear();
    LED2_Clear();
    LED3_Clear();
    LED4_Clear();
    LED5_Clear();    
}

/**
****************************************************************************
* @brief SOCgreenLed_displayOn
*
* - turn off red LED and all others (green) on
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void SOCgreenLed_displayOn(void)
{
    LED1_RED_Clear();
    LED1_Set();
    LED2_Set();
    LED3_Set();
    LED4_Set();
    LED5_Set();    
}

/**
****************************************************************************
* @brief SOCLed_displayOff
*
* - turn off all LEDs
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void SOCLed_displayOff(void)
{
    LED1_RED_Clear();
    LED1_Clear();
    LED2_Clear();
    LED3_Clear();
    LED4_Clear();
    LED5_Clear();    
}

/**
****************************************************************************
* @brief SOCLed_displayFault
*
* - toggle LEDs to display fault ID
*
* @param [in] ledFaultID
* @return NONE
*****************************************************************************
*/
void SOCLed_displayFault(uint8_t ledFaultID)
{
    LED1_RED_Set();
    LED1_Clear();
    switch(ledFaultID)
    {
        case LED_FAULTID_F0:
        {
            DISPLAY_FAULTID_F0;
            break;
        }
        case LED_FAULTID_F1:
        {
            DISPLAY_FAULTID_F1;
            break;
        }
        case LED_FAULTID_F2:
        {
            DISPLAY_FAULTID_F2;
            break;
        }
        case LED_FAULTID_F3:
        {
            DISPLAY_FAULTID_F3;
            break;
        }
        case LED_FAULTID_F4:
        {
            DISPLAY_FAULTID_F4;
            break;
        }
        case LED_FAULTID_F5:
        {
            DISPLAY_FAULTID_F5;
            break;
        }
        case LED_FAULTID_F6:
        {
            DISPLAY_FAULTID_F6;
            break;
        }
        case LED_FAULTID_F7:
        {
            DISPLAY_FAULTID_F7;
            break;
        }
        case LED_FAULTID_F8:
        {
            DISPLAY_FAULTID_F8;
            break;
        }
        case LED_FAULTID_F9:
        {
            DISPLAY_FAULTID_F9;
            break;
        }
        case LED_FAULTID_F10:
        {
            DISPLAY_FAULTID_F10;
            break;
        }
        case LED_FAULTID_F11:
        {
            DISPLAY_FAULTID_F11;
            break;
        }
        case LED_FAULTID_F12:
        {
            DISPLAY_FAULTID_F12;
            break;
        }
        case LED_FAULTID_F13:
        {
            DISPLAY_FAULTID_F13;
            break;
        }
        case LED_FAULTID_F14:
        {
            DISPLAY_FAULTID_F14;
            break;
        }
        default:
        {
            DISPLAY_FAULTID_F15;
            break;
        }
    }
}

/**
****************************************************************************
* @brief SOCLed_displaySOC
*
* - display SOC on LEDs (1 LED => SOC += 20%)
*
* @param [in] display_SOC 
* @param [in] flag_charge
* @param [in] flagBlinkFreq
*
* @return NONE
*****************************************************************************
*/
void SOCLed_displaySOC(uint16_t display_SOC,bool flag_charge,bool flagBlinkFreq)
{
    if (display_SOC >= 80)
    {
        if ((!flagBlinkFreq) && (flag_charge))
        {
            LED5_Clear();
        }
        else
        {
            LED5_Set();
        }
        LED1_RED_Clear();
        LED1_Set();
        LED2_Set();
        LED3_Set();
        LED4_Set();
    }
    else if (display_SOC >= 60)
    {
        if ((!flagBlinkFreq) && (flag_charge))
        {
            LED4_Clear();
        }
        else
        {
            LED4_Set();
        }
        LED1_RED_Clear();
        LED1_Set();
        LED2_Set();
        LED3_Set();
        LED5_Clear();
    }
    else if (display_SOC >= 40)
    {
        if ((!flagBlinkFreq) && (flag_charge))
        {
            LED3_Clear();
        }
        else
        {
            LED3_Set();
        }
        LED1_RED_Clear();
        LED1_Set();
        LED2_Set();
        LED4_Clear();
        LED5_Clear();
    }
    else if (display_SOC >= 20)
   {
        if ((!flagBlinkFreq) && (flag_charge))
        {
            LED2_Clear();
        }
        else
        {
            LED2_Set();
        }
        LED1_RED_Clear();
        LED1_Set();
        LED3_Clear();
        LED4_Clear();
        LED5_Clear();
    }
    else if (display_SOC >= 10)
    {
        if ((!flagBlinkFreq) && (flag_charge))
        {
            LED1_Clear();
        }
        else
        {
            LED1_Set();
        }
        LED1_RED_Clear();
        LED2_Clear();
        LED3_Clear();
        LED4_Clear();
        LED5_Clear();
    }
    else
    {
        if (!flagBlinkFreq)
        {
            LED1_RED_Clear();
        }
        else
        {
            LED1_RED_Set();
        }
        LED1_Clear();
        LED2_Clear();
        LED3_Clear();
        LED4_Clear();
        LED5_Clear();
    }
}

/**
****************************************************************************
* @brief ledFailIDfunc
*
* - check fault flags and set faultID 
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void ledFailIDfunc(void)
{   
    if (((g_flagDHT == 1u) && (g_chgDsgControl == DISCHARGE_EN)) || 
            ((g_flagCHT == 1u) && (g_chgDsgControl == CHARGE_EN)))
    {
        g_faultID = LED_FAULTID_F0;
    }
	else if (((g_flagDHT_MOS == 1u) && (g_chgDsgControl == DISCHARGE_EN)) || 
            ((g_flagCHT_MOS == 1u) && (g_chgDsgControl == CHARGE_EN)))
    {
        g_faultID = LED_FAULTID_F1;
    }
    else if (((g_flagCLT == 1u) && (g_chgDsgControl == CHARGE_EN)) || 
            ((g_flagDLT == 1u) && (g_chgDsgControl == DISCHARGE_EN)))
    {
        g_faultID = LED_FAULTID_F2;
    }
    else if (((g_flagOCP == 1u) && (g_chgDsgControl == CHARGE_EN)) || 
            ((g_flagOCD == 1u) && (g_chgDsgControl == DISCHARGE_EN)))
    {
        g_faultID = LED_FAULTID_F3;
    }
    else if (((g_flagChgShortCircuit == 1u) && (g_chgDsgControl == CHARGE_EN)) || 
            ((g_flagDsgShortCircuit == 1u) && (g_chgDsgControl == DISCHARGE_EN)))
    {
        g_faultID = LED_FAULTID_F5;
    }
    else if ((g_flagCUV == 1u) && (g_chgDsgControl == DISCHARGE_EN))
    {
        g_faultID = LED_FAULTID_F6;
    }
    else if ((g_flagOVP == 1u) && (g_chgDsgControl == CHARGE_EN))
    {
        g_faultID = LED_FAULTID_F7;
    }
	else if (((g_flagChgSUVP == 1u) && (g_chgDsgControl == CHARGE_EN)) || 
    	     ((g_flagDsgSUVP == 1u) && (g_chgDsgControl == DISCHARGE_EN)))
    {
        g_faultID = LED_FAULTID_F9;
    }
    else if ((g_flagPreChargeOvf == 1u) && (g_chgDsgControl == CHARGE_EN))
    {
        g_faultID = LED_FAULTID_F10;
    }
    else if ((g_flagDLT == 1u) && (g_chgDsgControl == DISCHARGE_EN))
    {
        g_faultID = LED_FAULTID_F12;
    }	
    else if (((g_flagChgOtherProtect == 1u) && (g_chgDsgControl == CHARGE_EN)) || 
            ((g_flagDsgOtherProtect == 1u) && (g_chgDsgControl == DISCHARGE_EN)) || (g_flagPositionError == 1u))

    {
        g_faultID = LED_FAULTID_F13;
    }
    else if (((g_flagChgBulkFail == 1u) && (g_chgDsgControl == CHARGE_EN)) || 
            ((g_flagDsgBulkFail == 1u) && (g_chgDsgControl == DISCHARGE_EN)))
    {
        g_faultID = LED_FAULTID_F14;
    }
    else if (((g_flagChgAFEComFail == 1u) && (g_chgDsgControl == CHARGE_EN)) || 
            ((g_flagDsgAFEComFail == 1u) && (g_chgDsgControl == DISCHARGE_EN)))
    {
        g_faultID = LED_FAULTID_F15;
    }
	else if (((g_flagPicChgProtect.word != 0u) && (g_chgDsgControl == CHARGE_EN)) || 
         ((g_flagPicDsgProtect.word != 0u) && (g_chgDsgControl == DISCHARGE_EN)))
    {
        g_faultID = LED_FAULTID_F13;
    }
    else 
    {
        g_faultID = LED_FAULTID_NOT;
    }   
}


/**
****************************************************************************
* @brief Led_displayCellPosition
*
* - display pack cellPosition value on LEDs
*
* @param [in] flagBlinkFreq
*
* @return NONE
*****************************************************************************
*/
void Led_displayCellPosition(bool flagBlinkFreq)
{
    LED1_RED_Clear();
    LED1_Clear();
    LED2_Clear();
    LED3_Clear();
    if (g_cellPosition == CELL_1S_POSITION)
    {
        LED4_Clear();
        if (flagBlinkFreq == 0u)
        {
            LED5_Clear();
        }
        else
        {
            LED5_Set();
        }
    }
    else
    {
        LED5_Clear();
        if (flagBlinkFreq == 0u)
        {
            LED4_Clear();
        }
        else
        {
            LED4_Set();
        }
    }
}



/**
****************************************************************************
* @brief ShipLed
*
* - set LEDs to ship mode display => | | | | | = > | | | | = > | | | => etc
*
* @param NONE
* @return NONE
*****************************************************************************
*/
static void ShipLed(void)
{
    static uint8_t counter = 0u;
    g_shipLedTimer++;
    counter = (uint8_t)(g_shipLedTimer / 200u);
    switch (counter)
    {
        case 0u:
        {
            LED1_RED_Clear();
            LED1_Set();
            LED2_Set();
            LED3_Set();
            LED4_Set();
            LED5_Set();
            break;
        }
        case 1u:
        {
            LED1_RED_Clear();
            LED1_Set();
            LED2_Set();
            LED3_Set();
            LED4_Set();
            LED5_Clear();
            break;
        }
        case 2u:
        {
            LED1_RED_Clear();
            LED1_Set();
            LED2_Set();
            LED3_Set();
            LED4_Clear();
            LED5_Clear();
            break;
        }
        case 3u:
        {
            LED1_RED_Clear();
            LED1_Set();
            LED2_Set();
            LED3_Clear();
            LED4_Clear();
            LED5_Clear();
            break;
        }
        case 4u:
        {
            LED1_RED_Clear();
            LED1_Set();
            LED2_Clear();
            LED3_Clear();
            LED4_Clear();
            LED5_Clear();
            break;
        }
        default: 
        {
            LED1_RED_Clear();
            LED1_Clear();
            LED2_Clear();
            LED3_Clear();
            LED4_Clear();
            LED5_Clear();
            g_shipLedTimer = 1000u;
            break;
        }
    }
}



/**
****************************************************************************
* @brief LedControl
*
* - read flags and control LEDs accordingly
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void LedControl(void)
{ 
    static uint16_t s_Socledtime = 0u;
    bool s_flagSocLEDBlink1Hz = false;
    bool s_flagChgLED;
    
    ledFailIDfunc();
    
    LED1_RED_OutputEnable();
    LED1_OutputEnable();
    LED2_OutputEnable();
    LED3_OutputEnable();
    LED4_OutputEnable();
    LED5_OutputEnable();

    
    if (g_testAllRLedOn == 1u)
    {
        SOCredLed_displayOn();
    }
    else if (g_testAllGLedOn == 1u)
    {
        SOCgreenLed_displayOn();
    }
	else if (g_flagEnterShipLed == 1u)
    {
        ShipLed();
    }
    else if ((g_flagKeyLed == 1u) || (g_chargeMode >= PRE_CHARGE_MODE))
    {
        s_Socledtime++;
        if (s_Socledtime >= TIMER5MS_1S)
        {
            s_Socledtime = 0u;
        }

		if (s_Socledtime >= TIMER5MS_500MS)
        {
            s_flagSocLEDBlink1Hz = true;
        }
		else
        {
            s_flagSocLEDBlink1Hz = false;
        }

		if (g_flagLedCellPosition == 1u)
        {
            Led_displayCellPosition(s_flagSocLEDBlink1Hz);
        }
        else if (g_faultID == LED_FAULTID_NOT)
        {
            if ((g_chargeMode >= PRE_CHARGE_MODE) && (g_chargeMode < TERMINATION_MODE) && (g_flagChgCurr == 1u))
            {
                s_flagChgLED = 1u;
            }
            else
            {
                s_flagChgLED = 0u;
            }
            SOCLed_displaySOC(g_battSOC,s_flagChgLED,s_flagSocLEDBlink1Hz);
        }
        else
        {
            SOCLed_displayFault(g_faultID);
        }
    }
    else
    {
        SOCLed_displayOff();
        s_Socledtime = 0u;
    }
}



/* Inventus Power Proprietary */
