/**
 * @file  CANopenV01_handler.c
 *
 * REVISION HISTORY
 *==============================================
 * |Ticket   |Date      |Author       |Notes
 * |----:    |:----:    |:----:       |:----
 * |Creation |02/15/22  |broberto     |Moved handlers from CANopenV01.c
 *
 * @par   COPYRIGHT NOTICE: (c) 2022 Inventus Power.  All rights reserved.
 */
/* Inventus Power Proprietary */

#include "CANopenV01_handler.h"

/*****************************************************************************************************
uint32_t handler_g_battTemp(int action, uint32_t value)
{
if (action == SET)
   g_battTemp = value;

   return g_battTemp;
}
*****************************************************************************************************/

uint8_t handler_g_flagCanErr (int action, uint8_t value )
{
if (action == SET)
   g_flagCanErr = value;

   return g_flagCanErr;
}

uint8_t handler_g_canMasterCmd (int action, uint8_t value )
{
if (action == SET)
   g_canMasterCmd = value;

   return g_canMasterCmd;
}

uint8_t handler_g_canMasterFuncC (int action, uint8_t value )
{
if (action == SET)
   g_canMasterFuncC = value;

   return g_canMasterFuncC;
}

uint8_t handler_g_nodeState (int action, uint8_t value )
{
if (action == SET)
   g_nodeState = value;

   return g_nodeState;
}

uint32_t handler_g_cobIDEMCY (int action, uint32_t value )
{
if (action == SET)
   g_cobIDEMCY  = value;

   return g_cobIDEMCY ;
}

uint16_t handler_g_inhibitTimeEMCY (int action, uint16_t value )
{
if (action == SET)
   g_inhibitTimeEMCY  = value;

   return g_inhibitTimeEMCY ;
}

uint32_t handler_g_consumerHeartbeatTime (int action, uint32_t value )
{
if (action == SET)
   g_consumerHeartbeatTime  = value;

   return g_consumerHeartbeatTime ;
}

uint16_t handler_g_producerHeartbeatTime (int action, uint16_t value )
{
if (action == SET)
   g_producerHeartbeatTime  = value;

   return g_producerHeartbeatTime ;
}

uint8_t handler_g_identityObjectSUB (int action, uint8_t value )
{
if (action == SET)
   g_identityObjectSUB  = value;

   return g_identityObjectSUB ;
}

uint32_t handler_g_vendorID (int action, uint32_t value )
{
if (action == SET)
   g_lssVendorID = value;

   return g_lssVendorID;
}

uint32_t handler_g_productCode (int action, uint32_t value )
{
if (action == SET)
   g_productCode  = value;

   return g_productCode ;
}

uint32_t handler_g_revisionNumber (int action, uint32_t value )
{
if (action == SET)
   g_revisionNumber  = value;

   return g_revisionNumber ;
}

uint32_t handler_g_serialNumber (int action, uint32_t value )
{
if (action == SET)
   g_serialNumber  = value;

   return g_serialNumber ;
}

uint8_t handler_g_sdoServer1SUB (int action, uint8_t value )
{
if (action == SET)
   g_sdoServer1SUB  = value;

   return g_sdoServer1SUB ;
}

uint32_t handler_g_cobIDrx (int action, uint32_t value )
{
if (action == SET)
   g_cobIDrx  = value;

   return g_cobIDrx ;
}

uint32_t handler_g_cobIDtx (int action, uint32_t value )
{
if (action == SET)
   g_cobIDtx  = value;

   return g_cobIDtx ;
}

uint32_t handler_g_cobIDtx (int action, uint32_t value )
{
if (action == SET)
   g_cobIDtx  = value;

   return g_cobIDtx ;
}

uint8_t handler_g_nodeIDofSDO (int action, uint8_t value )
{
if (action == SET)
   g_nodeIDofSDO  = value;

   return g_nodeIDofSDO ;
}

uint16_t handler_g_canErrTimes (int action, uint16_t value )
{
if (action == SET)
   g_canErrTimes  = value;

   return g_canErrTimes ;
}

uint16_t handler_g_canOvfTimes (int action, uint16_t value )
{
if (action == SET)
   g_canOvfTimes  = value;

   return g_canOvfTimes ;
}

uint8_t handler_g_pendNodeID (int action, uint8_t value )
{
if (action == SET)
   g_pendNodeID = value;

   return g_pendNodeID;
}

uint8_t handler_g_fixNodeID (int action, uint8_t value )
{
if (action == SET)
   g_fixNodeID = value;

   return g_fixNodeID;
}

uint8_t handler_g_pendBitRate (int action, uint8_t value )
{
if (action == SET)
   g_lssVendorID = value;

   return g_lssVendorID;
}

uint8_t handler_g_fixBitRate (int action, uint8_t value )
{
if (action == SET)
   g_fixBitRate = value;

   return g_fixBitRate;
}

uint8_t handler_g_lssState (int action, uint8_t value )
{
if (action == SET)
   g_lssVendorID = value;

   return g_lssVendorID;
}

uint8_t handler_g_lssSwitchIndx (int action, uint8_t value )
{
if (action == SET)
   g_lssVendorID = value;

   return g_lssVendorID;
}

uint16_t handler_g_lssUpdateRateTime (int action, uint16_t value )
{
if (action == SET)
   g_lssVendorID = value;

   return g_lssVendorID;
}

uint32_t handler_g_lssVendorID (int action, uint32_t value )
{
if (action == SET)
   g_lssVendorID = value;

   return g_lssVendorID;
}

uint32_t handler_g_lsspPoductCode (int action,  uint32_t value )
{
if (action == SET)
   g_lsspPoductCode = value;

   return g_lsspPoductCode;
}

uint32_t handler_g_lssRevisionNumber (int action, uint32_t value )
{
if (action == SET)
   g_lssRevisionNumber = value;

   return g_lssRevisionNumber;
}

uint32_t handler_g_lssSerialNumber (int action, uint32_t value)
{
if (action == SET)
   g_lssSerialNumber = value;

   return g_lssSerialNumber;
}

uint8_t handler_g_canBattStatus (int action , uint8_t value )
{
if (action == SET)
   g_canBattStatus = value;

   return g_canBattStatus;
}

uint8_t handler_g_chargerStatus (int action , uint8_t value ) 
{
if (action == SET)
   g_chargerStatus = value;

   return g_chargerStatus;
}

uint16_t handler_g_temperature (int action , uint16_t value ) 
{
if (action == SET)
   g_temperature = value;

   return g_temperature;
}

uint8_t handler_g_battParmetersSUB (int action , uint8_t value )
{
if (action == SET)
   g_battParmetersSUB = value;

   return g_battParmetersSUB;
}

uint8_t handler_g_batttype (int action , uint8_t value )
{
if (action == SET)
   g_freeData = value;

   return g_freeData;
}

uint16_t handler_g_batttCap (int action , uint16_t value )
{
if (action == SET)
   g_freeData = value;

   return g_freeData;
}

uint16_t handler_g_maxCHGcurr (int action , uint16_t value )
{
if (action == SET)
   g_freeData = value;

   return g_freeData;
}

uint16_t handler_g_cellNumber (int action , uint16_t value )
{
if (action == SET)
   g_freeData = value;

   return g_freeData;
}

uint8_t handler_g_battSNSUB (int action , uint8_t value )
{
if (action == SET)
   g_freeData = value;

   return g_freeData;
}

uint32_t handler_g_battSNLow (int action , uint32_t value )
{
if (action == SET)
   g_freeData = value;

   return g_freeData;
}

uint32_t handler_g_battSNHigh (int action , uint32_t value )
{
if (action == SET)
   g_freeData = value;

   return g_freeData;
}

uint32_t handler_g_cumulativeTotalCharge (int action , uint32_t value )
{
if (action == SET)
   g_freeData = value;

   return g_freeData;
}

uint32_t handler_g_expendSinceLastCharge (int action , uint32_t value )
{
if (action == SET)
   g_freeData = value;

   return g_freeData;
}

uint32_t handler_g_returnedDuringLastCharge (int action , uint32_t value )
{
if (action == SET)
   g_freeData = value;

   return g_freeData;
}

uint32_t handler_g_battVoltage (int action , uint32_t value )
{
if (action == SET)
   g_freeData = value;

   return g_freeData;
}

uint32_t handler_g_chargeCurrPequested (int action , uint32_t value )
{
if (action == SET)
   g_freeData = value;

   return g_freeData;
}

uint32_t handler_g_canBattSOC (int action , uint32_t value )
{
if (action == SET)
   g_freeData = value;

   return g_freeData;
}

uint32_t handler_g_freeData (int action , uint32_t value )
{
if (action == SET)
   g_freeData = value;

   return g_freeData;
}

uint8_t handler_g_testCommandSub  (int action, uint8_t value)
{
if (action == SET)
   g_testCommandSub = value;

   return g_testCommandSub;
}

uint8_t handler_g_canAbortCode (int action, uint8_t value)
{
if (action == SET)
   g_canAbortCode = value;

   return g_canAbortCode;
}

uint8_t handler_g_tpdoParameterSub (int action, uint8_t value)
{
if (action == SET)
   g_tpdoParameterSub = value;

   return g_tpdoParameterSub;
}

uint8_t handler_g_tpdoParmeterType (int action, uint8_t value)
{
if (action == SET)
   g_tpdoParmeterType = value;

   return g_tpdoParmeterType;
}

uint16_t handler_g_tpdoParameterTime (int action, uint16_t value)
{
if (action == SET)
   g_tpdoParameterTime = value;

   return g_tpdoParameterTime;
}

uint32_t handler_g_tpdoParameterNodeId1 (int action, uint32_t value)
{
if (action == SET)
   g_tpdoParameterNodeId1 = value;

   return g_tpdoParameterNodeId1;
}

uint32_t handler_g_tpdoParameterNodeId2 (int action, uint32_t value)
{
if (action == SET)
   g_tpdoParameterNodeId2 = value;

   return g_tpdoParameterNodeId2;
}

uint32_t handler_g_tpdoParameterNodeId3 (int action, uint32_t value)
{
if (action == SET)
   g_tpdoParameterNodeId3 = value;

   return g_tpdoParameterNodeId3;
}

uint32_t handler_g_tpdoParameterNodeId4 (int action, uint32_t value)
{
if (action == SET)
   g_flagLssIdentifyMatch = value;
   
   return g_flagLssIdentifyMatch;
}

uint16_t handler_g_heaterState(int action, uint16_t value)
{
if (action == SET)
   g_heaterState = value;
   
   return g_heaterState;
}

uint16_t handler_g_heaterControl(int action, uint16_t value)
{
if (action == SET)
   g_heaterControl = value;
   
   return g_heaterControl;
}

uint8_t handler_g_flagLssIdentifyMatch(int action, uint8_t value)
{
if (action == SET)
   g_flagLssIdentifyMatch = value;
   
   return g_flagLssIdentifyMatch;
}

uint8_t handler_g_consumerHeartbeatTimeSUB(int action, uint8_t value)
{
if (action == SET)
   g_consumerHeartbeatTimeSUB = value;
   
   return g_consumerHeartbeatTimeSUB;
}

uint8_t handler_g_lssIdentifyIndx(int action, uint8_t value)
{
if (action == SET)
  g_lssIdentifyIndx  = value;
   
   return g_flagLssIdentifyMatch;
}

uint8_t handler_g_flagLssIdentifyMatch(int action, uint8_t value)
{
if (action == SET)
   g_flagLssIdentifyMatch = value;
   
   return g_flagLssIdentifyMatch;
}










