/**
 * @file  detectV01.c
 *
 * REVISION HISTORY
 *==============================================
 * |Ticket   |Date      |Author       |Notes
 * |----:    |:----:    |:----:       |:----
 * |Creation |02/04/22  |Tom O'Connor |Document
 * |Modified |02/22/22  |Tyler Badar  |Added function descriptions
 *
 * @par   COPYRIGHT NOTICE: (c) 2022 Inventus Power.  All rights reserved.
 */
/* Inventus Power Proprietary */
#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes
#include "mainV01.h"
#include "common_defineV01.h"
#include "i2cMasterV02.h"
#include "chgdsgV01.h"
#include "detectV01.h"
#include "adcV01.h"
#include "canV01.h"
#include "J1939V01.h"
#include "temperatureV01.h"


static void CurrDetectFunction (void);
static void SignalDetectFunction (void);
static void MSmodeDetectFunction (void);
static void LimitCurrentJudgeFunction (void);
static void PreChgVoltDetect(void);
static void PackPositionDetectFunction(void);
static void keyFunction(void);


uint16_t g_ledOvf = 0u;
uint16_t g_picFetOvf = 0u;
uint16_t g_disabelBroadcastOvf = 0u;
uint16_t g_testHeaterOnOvf = 0u;
static uint16_t s_DSGCurrDebounce1 = 0u;
static uint16_t s_DSGCurrDebounce2 = 0u;
static uint16_t s_DSGCurrDebounce3 = 0u;

uint8_t g_chgDsgControl = NO_CHG_DSG;
uint8_t g_commMode = NONE_MODE;
uint8_t g_cellPosition = CELL_1S_POSITION;
uint8_t g_checkPositionResult = 0x55u;
uint16_t g_shipLedTimer = 0u;





/**
****************************************************************************
* @brief EIC_Interrupt
*
* - detect wakeup interrupt and pull pack out of sleep
*
* @param [in] s_Channel
* @return NONE
*****************************************************************************
*/
void EIC_Interrupt(uint8_t s_Channel)
{
    if (s_Channel == EIC_PIN_0)
    {
        g_flagDsgCurr = 1u;
        g_flagSleep = 0u;
    }
    if (s_Channel == EIC_PIN_1)
    {
        g_flagSleep = 0u;
    }
    if (s_Channel == EIC_PIN_4)
    {
        g_flagSleep = 0u;
    }
    if (s_Channel == EIC_PIN_5)
    {
        g_flagSleep = 0u;
    }
}

/**
****************************************************************************
* @brief DetectFunction 
*
* - detect button press
* - detect interlock, wake, and chg/dsg flags
* - detect current w/ direction and set corresponding FET flags
* - assign pack with default addr to master and others to slaves OR set 
*   mode to NONE 
* - calculate and set current limits
* - check pack position by cellPosition and adcPackSP
* - check 8050 ship mode flag and flag LEDs for ship mode if necessary
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void DetectFunction (void)
{
	keyFunction();
    SignalDetectFunction();   
    CurrDetectFunction();   
    MSmodeDetectFunction(); 
	
    LimitCurrentJudgeFunction();   
    

	PackPositionDetectFunction();
    if (g_flag8050Shipmode == 1u)
    {
        if (g_flagEnterShipLed == 0u)
        {
            g_flagEnterShipLed = 1u;
            g_shipLedTimer = 0u;
        }
    }
    else
    {
        g_flagEnterShipLed = 0u;
        g_shipLedTimer = 0u;
    }
}

#define PACK_VOLTAGR_2V_ADC_VALUE     432u
#define PACK_POSITION_1V8_ADC_VALUE   8937

/**
****************************************************************************
* @brief PackPositionDetectFunction
*
* - detect pack position by using cellPosition, adcPackSP, and adcPackVolt 
*   variables
*   - cellPosition == 0x01 -> adcPackSP >= 8937
*   - cellPosition == 0x02 -> adcPackSP < 8937
*   - && adcPackVolt >= 432 for 10s for flagDetectPackPosition => 1
* - set checkPositionResult variable based on success or failure
*
* @param NONE
* @return NONE
*****************************************************************************
*/
static void PackPositionDetectFunction(void)
{
    static uint8_t s_flagPackPositionCheckFail = 0u;
    static uint8_t s_flagDetectPackPosition = 0u;
    static uint16_t s_debounceCheckPositionNormal = 0u;
    static uint16_t s_debounceCheckPositionAbnormal = 0u;
    
    if ((((g_cellPosition == CELL_1S_POSITION) && (g_adcPackSP >= PACK_POSITION_1V8_ADC_VALUE)) || 
        ((g_cellPosition == CELL_2S_POSITION) && (g_adcPackSP < PACK_POSITION_1V8_ADC_VALUE))) && (g_adcPackVolt >= PACK_VOLTAGR_2V_ADC_VALUE))
    {
        if (++s_debounceCheckPositionNormal >= TIMER5MS_10S)
        {
            s_flagDetectPackPosition = 1u;
        }
    }
    else
    {
        s_debounceCheckPositionNormal = 0u;
    }
    
    if ((((g_cellPosition == CELL_1S_POSITION) && (g_adcPackSP < PACK_POSITION_1V8_ADC_VALUE)) || 
        ((g_cellPosition == CELL_2S_POSITION) && (g_adcPackSP >= PACK_POSITION_1V8_ADC_VALUE))) && 
        (g_adcPackVolt >= PACK_VOLTAGR_2V_ADC_VALUE) && (s_flagDetectPackPosition == 0u))
    {
        if (++s_debounceCheckPositionAbnormal >= TIMER5MS_10S)
        {
            s_flagPackPositionCheckFail = 1u;
        }
    }
    else
    {
        s_debounceCheckPositionAbnormal = 0u;
    }
    
    if ((g_chgDsgControl == NO_CHG_DSG) || (g_flagParallel == 0u))
    {
        s_flagPackPositionCheckFail = 0u;
        s_flagDetectPackPosition = 0u;
        s_debounceCheckPositionNormal = 0u;
        s_debounceCheckPositionAbnormal = 0u;
    }
    
    if (s_flagPackPositionCheckFail == 1u)
    {
        g_checkPositionResult = 0xAAu;
    }
    else
    {
        g_checkPositionResult = 0x55u;
    }
}


/**
****************************************************************************
* @brief keyFunction
*
* - detect how long SOC button was pressed and light LEDs accordingly
*
* @param NONE
* @return NONE
*****************************************************************************
*/
static void keyFunction(void)
{
    static uint16_t s_keyDebounce = 0u;
    static uint16_t s_upkeyDebounce = 0u;
    static uint16_t s_keyLedTime;
    WAKEUP_InputEnable();
    if (WAKEUP_Get() == 0x00)
    {
        s_upkeyDebounce = 0u;
        if (s_keyDebounce < TIMER5MS_20S)
        {
            s_keyDebounce++;
        }
        if (s_keyDebounce >= TIMER5MS_100MS)
        {
            g_flagUpKey = 0u; 
            g_flagLedSOC = 1u;
        }
        if (s_keyDebounce >= TIMER5MS_1S)
        {
        	g_flagKeyLed = 1u;
            g_flagLedCellPosition = 1u;
        }
        if ((s_keyDebounce >= TIMER5MS_5S) && (s_flagKey == 0u))
        {
            g_flagKeyLed = 1u;
            s_flagKey = 1u;
            g_cellPosition++;
            if (g_cellPosition > CELL_2S_POSITION)
            {
                g_cellPosition = CELL_1S_POSITION;
            }
        }
    }
    else
    {
        s_keyDebounce = 0u;
        s_upkeyDebounce++;
        if ((s_upkeyDebounce >= TIMER5MS_100MS) && (g_flagUpKey == 0u))
        {
            g_flagUpKey = 1u;
            g_flagKeyLed = 1u;
            s_flagKey = 0u;
            
        }
    }
    
    if ((g_flagKeyLed == 1u) && (WAKEUP_Get() != 0x00))
    {
        s_keyLedTime++;
        if (s_keyLedTime >= TIMER5MS_5S)
        {
            g_flagKeyLed = 0u;
            g_flagLedSOC = 0u;
            g_flagLedCellPosition = 0u;
        }
    }
    else
    {
        s_keyLedTime = 0u;
    }
}



/**
****************************************************************************
* @brief PreChgVoltDetect 
*
* @param NONE
* @return NONE
* @todo Empty Function
*****************************************************************************
*/
static void PreChgVoltDetect (void)
{
    
}

/**
****************************************************************************
* @brief LimitCurrentJudgeFunction
*
* - calculate and set current limits
*
* @param NONE
* @return NONE
*****************************************************************************
*/
static void LimitCurrentJudgeFunction (void)
{
    static uint8_t s_flagDsgEN[MAX_PARALLEL_COUNT] = {0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u};	
	static uint16_t DsgSOCPersent = 100u;
	static uint16_t DsgTempPersent = 100u;
    uint8_t i;
	uint16_t tmpDsgCurrLimit;
	uint16_t RegenCurrLimit_S1=0;
	uint16_t RegenCurrLimit_S2=0;
	uint16_t CHGCurrLimit_S1=0;
	uint16_t CHGCurrLimit_S2=0;
	uint16_t DSGCurrLimit_S1=0;
	uint16_t DSGCurrLimit_S2=0;
	
	if (g_commMode == MASTER_MODE)
    {
        g_IDdsgCurrLimit[0u] = g_dsgCurrLimit;
        g_IDchgCurrLimit[0u] = g_chgCurrLimit;
        g_IDregenCurrLimit[0u] = g_regenCurrLimit;  
    }  

    
	
	g_broadcastRegenCurrLimit = 0u;
   
	
    
	if (g_commMode == MASTER_MODE)
	{
		
        for(i = 0u; i < MAX_PARALLEL_COUNT; i++)
        {
        	if(g_IDCellS[i] == CELL_1S_POSITION)
        	{
            	RegenCurrLimit_S1 += g_IDregenCurrLimit[i];
				if (((g_IDVolt[i] + ABS_1V) >= g_maxDsgS1Volt) && (g_maxDsgS1Volt != 0u))
				{
					DSGCurrLimit_S1 += g_IDdsgCurrLimit[i];
				}
        	}
			else if(g_IDCellS[i] == CELL_2S_POSITION)
        	{
            	RegenCurrLimit_S2 += g_IDregenCurrLimit[i];
				if (((g_IDVolt[i] + ABS_1V) >= g_maxDsgS2Volt) && (g_maxDsgS2Volt != 0u))
				{
					DSGCurrLimit_S2 += g_IDdsgCurrLimit[i];
				}
        	}
        }

		if ((g_chargeCurr >= DELTA_Q_CHG_0A) && (g_chgDsgControl == CHARGE_EN))
        {
            g_broadcastChgCurrLimit = (g_chargeCurr - DELTA_Q_CHG_0A) * 5u;
        }
        else
        {
            g_broadcastChgCurrLimit = 0u;
        }

		if(RegenCurrLimit_S1 == 0)
		{
			g_broadcastRegenCurrLimit = RegenCurrLimit_S2;
		}
		else if(RegenCurrLimit_S2 == 0)
		{
			g_broadcastRegenCurrLimit = RegenCurrLimit_S1;
		}
		else if(RegenCurrLimit_S1 < RegenCurrLimit_S2)
		{
			g_broadcastRegenCurrLimit = RegenCurrLimit_S1;
		}
		else
		{
			g_broadcastRegenCurrLimit = RegenCurrLimit_S2;
		}


		

		if(DSGCurrLimit_S1 == 0)
		{
			tmpDsgCurrLimit = DSGCurrLimit_S2;
		}
		else if(DSGCurrLimit_S2 == 0)
		{
			tmpDsgCurrLimit = DSGCurrLimit_S1;
		}
		else if(DSGCurrLimit_S2 < DSGCurrLimit_S1)
		{
			tmpDsgCurrLimit = DSGCurrLimit_S2;
		}
		else
		{
			tmpDsgCurrLimit = DSGCurrLimit_S1;
		}

        if (tmpDsgCurrLimit == 0u)
        {
            g_flagNeedBroadcastBulkCurr = 0u;
            g_flagBroadcastBulkCurr = 0u;
        }
		
        if ((g_flagBroadcastBulkCurr == 0u) && (tmpDsgCurrLimit != 0u))
        {     
            g_flagNeedBroadcastBulkCurr = 1u;
        }
		
        if (g_flagNeedBroadcastBulkCurr == 1u)
        {
            g_broadcastDsgCurrLimit = BROADCAST_DSG_CURR_200MA;
        }
		else
		{
			g_broadcastDsgCurrLimit = tmpDsgCurrLimit;
		}
	}
	else
	{
		g_broadcastChgCurrLimit = 0u;
		g_broadcastDsgCurrLimit = 0u;
	}
    
    
    
	g_dsgCurrLimit = BROADCAST_DSG_CURR_60A; 
    if (((g_battSOC < CURR_LIMIT_SOC_25) || (g_minCellVolt < CURR_LIMIT_VOLT_3V45)) && (DsgSOCPersent == 100u))
    {
        DsgSOCPersent = 75u;
    }
    if ((g_battSOC >= CURR_LIMIT_SOC_30) && (g_minCellVolt >= CURR_LIMIT_VOLT_3V65) && (DsgSOCPersent == 75u))
    {
        DsgSOCPersent = 100u;
    }
    if (((g_battSOC < CURR_LIMIT_SOC_10) || (g_minCellVolt < CURR_LIMIT_VOLT_3V30)) && (DsgSOCPersent == 75u))
    {
        DsgSOCPersent = 50u;
    }
    if ((g_battSOC >= CURR_LIMIT_SOC_15) && (g_minCellVolt >= CURR_LIMIT_VOLT_3V50) && (DsgSOCPersent == 50u))
    {
        DsgSOCPersent = 75u;
    }
    
	if ((g_battTemp >= TEMPERATURE_65C) && (DsgTempPersent == 100u))
	{
		DsgTempPersent = 50u;
	}
	if ((g_battTemp < TEMPERATURE_60C) && (DsgTempPersent == 50u))
	{
		DsgTempPersent = 100u;
	}

    if (g_chgDsgControl == DISCHARGE_EN)
    {
        if (((g_flagDSGFET == 0u) && (g_chargeMode == DISCHARGE_MODE)) || (g_flagDSGPROTECT == 1u))
        {
            g_dsgCurrLimit = BROADCAST_DSG_CURR_0A;
        }
        
        else if (g_chargeMode == PRE_DISCHARGE_MODE)
        {
            g_dsgCurrLimit = BROADCAST_DSG_CURR_200MA;
        }
        else
        {
            g_dsgCurrLimit = (uint16_t)((((uint32_t)DsgSOCPersent * (uint32_t)DsgTempPersent) * (uint32_t)BROADCAST_DSG_CURR_60A) / (uint32_t)10000u);
        }
    }
    else
    {
        g_dsgCurrLimit = 0u;
    }    
    
    
    
    if ((g_chgDsgControl == DISCHARGE_EN) && (g_battTemp >= BASE_TEMP) && (g_flagPicChgProtect.word == 0u) && ((g_FETstate.byte & 0x07u) == 0x07u))
    {
        g_regenCurrLimit = BROADCAST_REGEN_CURR_50A;
    }
    else
    {
        g_regenCurrLimit = 0u;
    } 
}

/**
****************************************************************************
* @brief RemovePackDetectFunction
*
* - detect if parallel packs in use
* - check master IDParallelUpdateArray and flag parallel error if necessary
* - if flagged for parallel error, reset parallel pack FETstate, chargeMode,
*   voltage, and current
*
* @param NONE
* @return NONE
*****************************************************************************
*/
void RemovePackDetectFunction (void)
{
    uint8_t i;
    
    for (i = g_parallelQuantity; i < MAX_PARALLEL_COUNT; i++)
    {
    	g_IDParallelUpdateArray[i] = 0u;
    }
    if ((g_flagParallel == 1u) && (g_flagCompleteAddrAllocate == 1u) && (g_flagDisableBAM == 0u))
    {     
        
        if (g_commMode == MASTER_MODE)
        {
            for (i = 0u; i < MAX_PARALLEL_COUNT; i++)
            {
                if (++g_IDParallelUpdateArray[i] >= TIMER20MS_5S)
                {
                    g_flagDsgParallelErr = 1u;
                    g_flagChgParallelErr = 1u;
                    g_flagDisableBAM = 1u;  
                }
            }
        }
        else
        {
            if (++g_picParallelUpdateVaule >= TIMER20MS_5S)
            {
                g_flagDsgParallelErr = 1u;
                g_flagChgParallelErr = 1u;
                g_flagDisableBAM = 1u;  
            }
        }
        
        if (g_flagDisableBAM == 1u)
        {
            for (i = 0u; i < MAX_PARALLEL_COUNT; i++)
            {
                g_IDFETstate[i].byte = 0u;
                
                g_IDChargeMode[i] = 0u;
                g_IDVolt[i] = 0u;
                g_IDCurr[i] = 0;
                g_IDFETEnCH[i].byte = 0u;
                g_IDFETEnDisCH[i].byte = 0u;
            }
        }
    }
    else
    {
        for (i = 0u; i < MAX_PARALLEL_COUNT; i++)
        {
            g_IDParallelUpdateArray[i] = 0u;
        }
    }    
}


/**
****************************************************************************
* @brief MSmodeDetectFunction
*
* - if addr allocation completed, assign pack with default addr to master
* - this will run in parallel packs and assign others to slaves or to 
*   none_mode if addr allocation hasn't been completed
*
* @param NONE
* @return NONE
*****************************************************************************
*/
static void MSmodeDetectFunction (void)
{
    if (g_flagCompleteAddrAllocate == 1u)
    {
        if (g_canAddr == BATT_DEFAULT_ADDR)
        {
            g_commMode = MASTER_MODE;
        }
        else
        {
            g_commMode = SLAVE_MODE;
        }
    }
    else
    {
        g_commMode = NONE_MODE;
    }    
}

/**
****************************************************************************
* @brief TestCmdOvfDetectFunction
*
* @param NONE
* @return NONE
 @todo Empty Function
*****************************************************************************
*/
void TestCmdOvfDetectFunction (void)
{
    
    
    
}

/**
****************************************************************************
* @brief SignalDetectFunction
*
* - check interlock signal
* - check wake pin
* - check chg/dsg flags and set flags for chg/dsg ctrl/error
*
* @param NONE
* @return NONE
*****************************************************************************
*/
static void SignalDetectFunction (void)
{
    static uint16_t s_CHGSignalTime;
    static uint16_t s_DSGSignalTime;
    static uint16_t s_removeCHGSignalTime;
    static uint16_t s_removeDSGSignalTime;
    static uint16_t s_SignalErrTime;
    SYS_VINTLK_InputEnable();
    SYS_VWAKE_InputEnable();

    if (SYS_VINTLK_Get() == 0x00)
    {
        s_CHGSignalTime++;
        s_removeCHGSignalTime = 0u;
        if (s_CHGSignalTime >= TIMER5MS_500MS)
        {
            s_flagCHGsignal = 1u;    
        }
    }
    else
    {
        s_CHGSignalTime = 0u;
        s_removeCHGSignalTime++;
        if (s_removeCHGSignalTime >= TIMER5MS_100MS)
        {
            s_flagCHGsignal = 0u;    
        }
    }
    
    if (SYS_VWAKE_Get() == 0x00)
    {
        s_DSGSignalTime++;
        s_removeDSGSignalTime = 0u;
        if (s_DSGSignalTime >= TIMER5MS_500MS)
        {
            s_flagDSGsignal = 1u;    
        }
    }
    else
    {
        s_DSGSignalTime = 0u;
        s_removeDSGSignalTime++;
        if (s_removeDSGSignalTime >= TIMER5MS_100MS)
        {
            s_flagDSGsignal = 0u;    
        }
    }
    
    if ((s_flagDSGsignal == 0u) && (s_flagCHGsignal == 0u))
    {
        g_chgDsgControl = NO_CHG_DSG;
    }
    else if (s_flagCHGsignal == 1u)
    {
        if (g_chgDsgControl == NO_CHG_DSG) 
        {
            g_chgDsgControl = CHARGE_EN;
            g_requestAllocateTimes = 0u;
            g_flagRequestAddrAllocate = 1u;
            g_flagCompleteAddrAllocate = 0u;
			s_AddrAllocateTime = 0u;
            resetUidArray();
        }
        if (g_chgDsgControl == DISCHARGE_EN) 
        {
            g_chgDsgControl = SIGNAL_ERR;
        }
    }
    else
    {
        if (g_chgDsgControl == NO_CHG_DSG)
        {
            g_chgDsgControl = DISCHARGE_EN;
            g_requestAllocateTimes = 0u;
            g_flagRequestAddrAllocate = 1u;
            g_flagCompleteAddrAllocate = 0u;
			s_AddrAllocateTime = 0u;
            resetUidArray();
        } 
        if (g_chgDsgControl == CHARGE_EN) 
        {
            g_chgDsgControl = SIGNAL_ERR;
        }
    }
    
    if (g_chgDsgControl == SIGNAL_ERR)
    {
        s_SignalErrTime++;
        if (s_SignalErrTime >= TIMER5MS_5S)
        {
            g_chgDsgControl = NO_CHG_DSG;
        }
    }
    else
    {
        s_SignalErrTime = 0u;
    }
    
}

/**
****************************************************************************
* @brief CurrDetectFunction
*
* - detect current
* - set flags indicating whether chg and/or dsg current flowing
*
* @param NONE
* @return NONE
*****************************************************************************
*/
static void CurrDetectFunction (void)
{
    static uint16_t s_CHGCurrDebounce1 = 0u;
    static uint16_t s_CHGCurrDebounce2 = 0u;
    
    static uint16_t s_CHGCurrDebounce3 = 0u;
    static uint16_t s_CHGCurrDebounce4 = 0u;
    
    static uint16_t s_DSGCurrDebounce1 = 0u;
    static uint16_t s_DSGCurrDebounce2 = 0u;
    static uint16_t s_DSGCurrDebounce3 = 0u;
    CURR_INT_InputEnable(); 
	
    
    if ((g_battCurr >= CHG_CURR_500MA) && (g_chargeMode >= FAST_CHARGE_MODE) && (g_flagCommFail == 0u))
    {
        s_CHGCurrDebounce4 = 0u;
        s_CHGCurrDebounce3++;
        if (s_CHGCurrDebounce3 >= TIMER5MS_10S)
        {
            g_flagCurrEnDsgFET = 1u;
        }
    }
    else
    {
        s_CHGCurrDebounce3 = 0u;
        s_CHGCurrDebounce4++;
        if (s_CHGCurrDebounce4 >= TIMER5MS_1S)
        {
            g_flagCurrEnDsgFET = 0u;
        }
    }
        
    
    if ((g_battCurr >= CHG_CURR_500MA) && (g_flagCommFail == 0u))
    {
        s_CHGCurrDebounce1++;
        if (s_CHGCurrDebounce1 >= TIMER5MS_1S)
        {
            g_flagChgCurr = 1u;
        }
    }
    else
    {
        s_CHGCurrDebounce1 = 0u;
    }
    if ((g_battCurr < CHG_CURR_500MA) || (g_flagCommFail == 1u)) 
    {
        s_CHGCurrDebounce2++;
        if (s_CHGCurrDebounce2 >= TIMER5MS_1S)
        {
            g_flagChgCurr = 0u;
        }
    }
    else
    {
        s_CHGCurrDebounce2 = 0u;
    }
	
	if (((g_battCurr < DSG_CURR_5A) && (g_flagCommFail == 0u)) || (CURR_INT_Get() == 0x00)) 
    {
        s_DSGCurrDebounce1++;
        if (s_DSGCurrDebounce1 >= TIMER5MS_2S)
        {
            g_flagDsgCurr = 1u;
        }
    }
    else
    {
        s_DSGCurrDebounce1 = 0u;
    }
    
    if (((g_battCurr >= DSG_CURR_3A) || (g_flagCommFail == 1u)) && (CURR_INT_Get() != 0x00))
    {
        s_DSGCurrDebounce2++;
        if (s_DSGCurrDebounce2 >= TIMER5MS_5S)
        {
            g_flagDsgCurr = 0u;
        }
    }
    else
    {
        s_DSGCurrDebounce2 = 0u;
    }
}
/* Inventus Power Proprietary */

