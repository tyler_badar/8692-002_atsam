#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/8692-002-Inventus-Power-Battery-ATSAMC21J18A.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/8692-002-Inventus-Power-Battery-ATSAMC21J18A.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=-mafrlcsj
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../src/ledV01.c ../src/rs232V01.c ../src/i2cMasterV02.c ../src/arithmeticV01.c ../src/canV01.c ../src/adcV01.c ../src/temperatureV01.c ../src/detectV01.c ../src/J1939V01.c ../src/chgdsgV01.c ../src/CANopenV01.c ../src/ModBusV01.c ../src/pwmV01.c ../src/flashV01.c ../src/config/default/peripheral/nvmctrl/plib_nvmctrl.c ../src/config/default/peripheral/sercom/usart/plib_sercom1_usart.c ../src/config/default/peripheral/evsys/plib_evsys.c ../src/config/default/peripheral/sercom/usart/plib_sercom0_usart.c ../src/config/default/peripheral/can/plib_can1.c ../src/main.c ../src/config/default/initialization.c ../src/config/default/interrupts.c ../src/config/default/exceptions.c ../src/config/default/stdio/xc32_monitor.c ../src/config/default/peripheral/port/plib_port.c ../src/config/default/peripheral/clock/plib_clock.c ../src/config/default/peripheral/nvic/plib_nvic.c ../src/config/default/peripheral/systick/plib_systick.c ../src/config/default/peripheral/wdt/plib_wdt.c ../src/config/default/startup_xc32.c ../src/config/default/libc_syscalls.c ../src/config/default/peripheral/pm/plib_pm.c ../src/config/default/peripheral/sercom/i2c_master/plib_sercom5_i2c_master.c ../src/config/default/peripheral/can/plib_can0.c ../src/config/default/peripheral/sercom/i2c_slave/plib_sercom4_i2c_slave.c ../src/config/default/peripheral/eic/plib_eic.c ../src/config/default/peripheral/rstc/plib_rstc.c ../src/config/default/peripheral/tcc/plib_tcc1.c ../src/config/default/peripheral/adc/plib_adc0.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/1360937237/ledV01.o ${OBJECTDIR}/_ext/1360937237/rs232V01.o ${OBJECTDIR}/_ext/1360937237/i2cMasterV02.o ${OBJECTDIR}/_ext/1360937237/arithmeticV01.o ${OBJECTDIR}/_ext/1360937237/canV01.o ${OBJECTDIR}/_ext/1360937237/adcV01.o ${OBJECTDIR}/_ext/1360937237/temperatureV01.o ${OBJECTDIR}/_ext/1360937237/detectV01.o ${OBJECTDIR}/_ext/1360937237/J1939V01.o ${OBJECTDIR}/_ext/1360937237/chgdsgV01.o ${OBJECTDIR}/_ext/1360937237/CANopenV01.o ${OBJECTDIR}/_ext/1360937237/ModBusV01.o ${OBJECTDIR}/_ext/1360937237/pwmV01.o ${OBJECTDIR}/_ext/1360937237/flashV01.o ${OBJECTDIR}/_ext/1593096446/plib_nvmctrl.o ${OBJECTDIR}/_ext/504274921/plib_sercom1_usart.o ${OBJECTDIR}/_ext/1986646378/plib_evsys.o ${OBJECTDIR}/_ext/504274921/plib_sercom0_usart.o ${OBJECTDIR}/_ext/60165182/plib_can1.o ${OBJECTDIR}/_ext/1360937237/main.o ${OBJECTDIR}/_ext/1171490990/initialization.o ${OBJECTDIR}/_ext/1171490990/interrupts.o ${OBJECTDIR}/_ext/1171490990/exceptions.o ${OBJECTDIR}/_ext/163028504/xc32_monitor.o ${OBJECTDIR}/_ext/1865521619/plib_port.o ${OBJECTDIR}/_ext/1984496892/plib_clock.o ${OBJECTDIR}/_ext/1865468468/plib_nvic.o ${OBJECTDIR}/_ext/1827571544/plib_systick.o ${OBJECTDIR}/_ext/60184501/plib_wdt.o ${OBJECTDIR}/_ext/1171490990/startup_xc32.o ${OBJECTDIR}/_ext/1171490990/libc_syscalls.o ${OBJECTDIR}/_ext/829342769/plib_pm.o ${OBJECTDIR}/_ext/508257091/plib_sercom5_i2c_master.o ${OBJECTDIR}/_ext/60165182/plib_can0.o ${OBJECTDIR}/_ext/714983638/plib_sercom4_i2c_slave.o ${OBJECTDIR}/_ext/60167341/plib_eic.o ${OBJECTDIR}/_ext/1865585090/plib_rstc.o ${OBJECTDIR}/_ext/60181570/plib_tcc1.o ${OBJECTDIR}/_ext/60163342/plib_adc0.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/1360937237/ledV01.o.d ${OBJECTDIR}/_ext/1360937237/rs232V01.o.d ${OBJECTDIR}/_ext/1360937237/i2cMasterV02.o.d ${OBJECTDIR}/_ext/1360937237/arithmeticV01.o.d ${OBJECTDIR}/_ext/1360937237/canV01.o.d ${OBJECTDIR}/_ext/1360937237/adcV01.o.d ${OBJECTDIR}/_ext/1360937237/temperatureV01.o.d ${OBJECTDIR}/_ext/1360937237/detectV01.o.d ${OBJECTDIR}/_ext/1360937237/J1939V01.o.d ${OBJECTDIR}/_ext/1360937237/chgdsgV01.o.d ${OBJECTDIR}/_ext/1360937237/CANopenV01.o.d ${OBJECTDIR}/_ext/1360937237/ModBusV01.o.d ${OBJECTDIR}/_ext/1360937237/pwmV01.o.d ${OBJECTDIR}/_ext/1360937237/flashV01.o.d ${OBJECTDIR}/_ext/1593096446/plib_nvmctrl.o.d ${OBJECTDIR}/_ext/504274921/plib_sercom1_usart.o.d ${OBJECTDIR}/_ext/1986646378/plib_evsys.o.d ${OBJECTDIR}/_ext/504274921/plib_sercom0_usart.o.d ${OBJECTDIR}/_ext/60165182/plib_can1.o.d ${OBJECTDIR}/_ext/1360937237/main.o.d ${OBJECTDIR}/_ext/1171490990/initialization.o.d ${OBJECTDIR}/_ext/1171490990/interrupts.o.d ${OBJECTDIR}/_ext/1171490990/exceptions.o.d ${OBJECTDIR}/_ext/163028504/xc32_monitor.o.d ${OBJECTDIR}/_ext/1865521619/plib_port.o.d ${OBJECTDIR}/_ext/1984496892/plib_clock.o.d ${OBJECTDIR}/_ext/1865468468/plib_nvic.o.d ${OBJECTDIR}/_ext/1827571544/plib_systick.o.d ${OBJECTDIR}/_ext/60184501/plib_wdt.o.d ${OBJECTDIR}/_ext/1171490990/startup_xc32.o.d ${OBJECTDIR}/_ext/1171490990/libc_syscalls.o.d ${OBJECTDIR}/_ext/829342769/plib_pm.o.d ${OBJECTDIR}/_ext/508257091/plib_sercom5_i2c_master.o.d ${OBJECTDIR}/_ext/60165182/plib_can0.o.d ${OBJECTDIR}/_ext/714983638/plib_sercom4_i2c_slave.o.d ${OBJECTDIR}/_ext/60167341/plib_eic.o.d ${OBJECTDIR}/_ext/1865585090/plib_rstc.o.d ${OBJECTDIR}/_ext/60181570/plib_tcc1.o.d ${OBJECTDIR}/_ext/60163342/plib_adc0.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/1360937237/ledV01.o ${OBJECTDIR}/_ext/1360937237/rs232V01.o ${OBJECTDIR}/_ext/1360937237/i2cMasterV02.o ${OBJECTDIR}/_ext/1360937237/arithmeticV01.o ${OBJECTDIR}/_ext/1360937237/canV01.o ${OBJECTDIR}/_ext/1360937237/adcV01.o ${OBJECTDIR}/_ext/1360937237/temperatureV01.o ${OBJECTDIR}/_ext/1360937237/detectV01.o ${OBJECTDIR}/_ext/1360937237/J1939V01.o ${OBJECTDIR}/_ext/1360937237/chgdsgV01.o ${OBJECTDIR}/_ext/1360937237/CANopenV01.o ${OBJECTDIR}/_ext/1360937237/ModBusV01.o ${OBJECTDIR}/_ext/1360937237/pwmV01.o ${OBJECTDIR}/_ext/1360937237/flashV01.o ${OBJECTDIR}/_ext/1593096446/plib_nvmctrl.o ${OBJECTDIR}/_ext/504274921/plib_sercom1_usart.o ${OBJECTDIR}/_ext/1986646378/plib_evsys.o ${OBJECTDIR}/_ext/504274921/plib_sercom0_usart.o ${OBJECTDIR}/_ext/60165182/plib_can1.o ${OBJECTDIR}/_ext/1360937237/main.o ${OBJECTDIR}/_ext/1171490990/initialization.o ${OBJECTDIR}/_ext/1171490990/interrupts.o ${OBJECTDIR}/_ext/1171490990/exceptions.o ${OBJECTDIR}/_ext/163028504/xc32_monitor.o ${OBJECTDIR}/_ext/1865521619/plib_port.o ${OBJECTDIR}/_ext/1984496892/plib_clock.o ${OBJECTDIR}/_ext/1865468468/plib_nvic.o ${OBJECTDIR}/_ext/1827571544/plib_systick.o ${OBJECTDIR}/_ext/60184501/plib_wdt.o ${OBJECTDIR}/_ext/1171490990/startup_xc32.o ${OBJECTDIR}/_ext/1171490990/libc_syscalls.o ${OBJECTDIR}/_ext/829342769/plib_pm.o ${OBJECTDIR}/_ext/508257091/plib_sercom5_i2c_master.o ${OBJECTDIR}/_ext/60165182/plib_can0.o ${OBJECTDIR}/_ext/714983638/plib_sercom4_i2c_slave.o ${OBJECTDIR}/_ext/60167341/plib_eic.o ${OBJECTDIR}/_ext/1865585090/plib_rstc.o ${OBJECTDIR}/_ext/60181570/plib_tcc1.o ${OBJECTDIR}/_ext/60163342/plib_adc0.o

# Source Files
SOURCEFILES=../src/ledV01.c ../src/rs232V01.c ../src/i2cMasterV02.c ../src/arithmeticV01.c ../src/canV01.c ../src/adcV01.c ../src/temperatureV01.c ../src/detectV01.c ../src/J1939V01.c ../src/chgdsgV01.c ../src/CANopenV01.c ../src/ModBusV01.c ../src/pwmV01.c ../src/flashV01.c ../src/config/default/peripheral/nvmctrl/plib_nvmctrl.c ../src/config/default/peripheral/sercom/usart/plib_sercom1_usart.c ../src/config/default/peripheral/evsys/plib_evsys.c ../src/config/default/peripheral/sercom/usart/plib_sercom0_usart.c ../src/config/default/peripheral/can/plib_can1.c ../src/main.c ../src/config/default/initialization.c ../src/config/default/interrupts.c ../src/config/default/exceptions.c ../src/config/default/stdio/xc32_monitor.c ../src/config/default/peripheral/port/plib_port.c ../src/config/default/peripheral/clock/plib_clock.c ../src/config/default/peripheral/nvic/plib_nvic.c ../src/config/default/peripheral/systick/plib_systick.c ../src/config/default/peripheral/wdt/plib_wdt.c ../src/config/default/startup_xc32.c ../src/config/default/libc_syscalls.c ../src/config/default/peripheral/pm/plib_pm.c ../src/config/default/peripheral/sercom/i2c_master/plib_sercom5_i2c_master.c ../src/config/default/peripheral/can/plib_can0.c ../src/config/default/peripheral/sercom/i2c_slave/plib_sercom4_i2c_slave.c ../src/config/default/peripheral/eic/plib_eic.c ../src/config/default/peripheral/rstc/plib_rstc.c ../src/config/default/peripheral/tcc/plib_tcc1.c ../src/config/default/peripheral/adc/plib_adc0.c



CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/8692-002-Inventus-Power-Battery-ATSAMC21J18A.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=ATSAMC21J18A
MP_LINKER_FILE_OPTION=
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/1360937237/ledV01.o: ../src/ledV01.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ledV01.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ledV01.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/ledV01.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1360937237/ledV01.o.d" -o ${OBJECTDIR}/_ext/1360937237/ledV01.o ../src/ledV01.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1360937237/rs232V01.o: ../src/rs232V01.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/rs232V01.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/rs232V01.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/rs232V01.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1360937237/rs232V01.o.d" -o ${OBJECTDIR}/_ext/1360937237/rs232V01.o ../src/rs232V01.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1360937237/i2cMasterV02.o: ../src/i2cMasterV02.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/i2cMasterV02.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/i2cMasterV02.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/i2cMasterV02.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1360937237/i2cMasterV02.o.d" -o ${OBJECTDIR}/_ext/1360937237/i2cMasterV02.o ../src/i2cMasterV02.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1360937237/arithmeticV01.o: ../src/arithmeticV01.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/arithmeticV01.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/arithmeticV01.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/arithmeticV01.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1360937237/arithmeticV01.o.d" -o ${OBJECTDIR}/_ext/1360937237/arithmeticV01.o ../src/arithmeticV01.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1360937237/canV01.o: ../src/canV01.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/canV01.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/canV01.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/canV01.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1360937237/canV01.o.d" -o ${OBJECTDIR}/_ext/1360937237/canV01.o ../src/canV01.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1360937237/adcV01.o: ../src/adcV01.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/adcV01.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/adcV01.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/adcV01.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1360937237/adcV01.o.d" -o ${OBJECTDIR}/_ext/1360937237/adcV01.o ../src/adcV01.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1360937237/temperatureV01.o: ../src/temperatureV01.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/temperatureV01.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/temperatureV01.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/temperatureV01.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1360937237/temperatureV01.o.d" -o ${OBJECTDIR}/_ext/1360937237/temperatureV01.o ../src/temperatureV01.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1360937237/detectV01.o: ../src/detectV01.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/detectV01.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/detectV01.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/detectV01.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1360937237/detectV01.o.d" -o ${OBJECTDIR}/_ext/1360937237/detectV01.o ../src/detectV01.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1360937237/J1939V01.o: ../src/J1939V01.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/J1939V01.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/J1939V01.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/J1939V01.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1360937237/J1939V01.o.d" -o ${OBJECTDIR}/_ext/1360937237/J1939V01.o ../src/J1939V01.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1360937237/chgdsgV01.o: ../src/chgdsgV01.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/chgdsgV01.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/chgdsgV01.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/chgdsgV01.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1360937237/chgdsgV01.o.d" -o ${OBJECTDIR}/_ext/1360937237/chgdsgV01.o ../src/chgdsgV01.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1360937237/CANopenV01.o: ../src/CANopenV01.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/CANopenV01.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/CANopenV01.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/CANopenV01.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1360937237/CANopenV01.o.d" -o ${OBJECTDIR}/_ext/1360937237/CANopenV01.o ../src/CANopenV01.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1360937237/ModBusV01.o: ../src/ModBusV01.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ModBusV01.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ModBusV01.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/ModBusV01.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1360937237/ModBusV01.o.d" -o ${OBJECTDIR}/_ext/1360937237/ModBusV01.o ../src/ModBusV01.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1360937237/pwmV01.o: ../src/pwmV01.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/pwmV01.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/pwmV01.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/pwmV01.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1360937237/pwmV01.o.d" -o ${OBJECTDIR}/_ext/1360937237/pwmV01.o ../src/pwmV01.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1360937237/flashV01.o: ../src/flashV01.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/flashV01.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/flashV01.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/flashV01.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1360937237/flashV01.o.d" -o ${OBJECTDIR}/_ext/1360937237/flashV01.o ../src/flashV01.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1593096446/plib_nvmctrl.o: ../src/config/default/peripheral/nvmctrl/plib_nvmctrl.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1593096446" 
	@${RM} ${OBJECTDIR}/_ext/1593096446/plib_nvmctrl.o.d 
	@${RM} ${OBJECTDIR}/_ext/1593096446/plib_nvmctrl.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1593096446/plib_nvmctrl.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1593096446/plib_nvmctrl.o.d" -o ${OBJECTDIR}/_ext/1593096446/plib_nvmctrl.o ../src/config/default/peripheral/nvmctrl/plib_nvmctrl.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/504274921/plib_sercom1_usart.o: ../src/config/default/peripheral/sercom/usart/plib_sercom1_usart.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/504274921" 
	@${RM} ${OBJECTDIR}/_ext/504274921/plib_sercom1_usart.o.d 
	@${RM} ${OBJECTDIR}/_ext/504274921/plib_sercom1_usart.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/504274921/plib_sercom1_usart.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/504274921/plib_sercom1_usart.o.d" -o ${OBJECTDIR}/_ext/504274921/plib_sercom1_usart.o ../src/config/default/peripheral/sercom/usart/plib_sercom1_usart.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1986646378/plib_evsys.o: ../src/config/default/peripheral/evsys/plib_evsys.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1986646378" 
	@${RM} ${OBJECTDIR}/_ext/1986646378/plib_evsys.o.d 
	@${RM} ${OBJECTDIR}/_ext/1986646378/plib_evsys.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1986646378/plib_evsys.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1986646378/plib_evsys.o.d" -o ${OBJECTDIR}/_ext/1986646378/plib_evsys.o ../src/config/default/peripheral/evsys/plib_evsys.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/504274921/plib_sercom0_usart.o: ../src/config/default/peripheral/sercom/usart/plib_sercom0_usart.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/504274921" 
	@${RM} ${OBJECTDIR}/_ext/504274921/plib_sercom0_usart.o.d 
	@${RM} ${OBJECTDIR}/_ext/504274921/plib_sercom0_usart.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/504274921/plib_sercom0_usart.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/504274921/plib_sercom0_usart.o.d" -o ${OBJECTDIR}/_ext/504274921/plib_sercom0_usart.o ../src/config/default/peripheral/sercom/usart/plib_sercom0_usart.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/60165182/plib_can1.o: ../src/config/default/peripheral/can/plib_can1.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/60165182" 
	@${RM} ${OBJECTDIR}/_ext/60165182/plib_can1.o.d 
	@${RM} ${OBJECTDIR}/_ext/60165182/plib_can1.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/60165182/plib_can1.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/60165182/plib_can1.o.d" -o ${OBJECTDIR}/_ext/60165182/plib_can1.o ../src/config/default/peripheral/can/plib_can1.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1360937237/main.o: ../src/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1360937237/main.o.d" -o ${OBJECTDIR}/_ext/1360937237/main.o ../src/main.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1171490990/initialization.o: ../src/config/default/initialization.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1171490990" 
	@${RM} ${OBJECTDIR}/_ext/1171490990/initialization.o.d 
	@${RM} ${OBJECTDIR}/_ext/1171490990/initialization.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1171490990/initialization.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1171490990/initialization.o.d" -o ${OBJECTDIR}/_ext/1171490990/initialization.o ../src/config/default/initialization.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1171490990/interrupts.o: ../src/config/default/interrupts.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1171490990" 
	@${RM} ${OBJECTDIR}/_ext/1171490990/interrupts.o.d 
	@${RM} ${OBJECTDIR}/_ext/1171490990/interrupts.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1171490990/interrupts.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1171490990/interrupts.o.d" -o ${OBJECTDIR}/_ext/1171490990/interrupts.o ../src/config/default/interrupts.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1171490990/exceptions.o: ../src/config/default/exceptions.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1171490990" 
	@${RM} ${OBJECTDIR}/_ext/1171490990/exceptions.o.d 
	@${RM} ${OBJECTDIR}/_ext/1171490990/exceptions.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1171490990/exceptions.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1171490990/exceptions.o.d" -o ${OBJECTDIR}/_ext/1171490990/exceptions.o ../src/config/default/exceptions.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/163028504/xc32_monitor.o: ../src/config/default/stdio/xc32_monitor.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/163028504" 
	@${RM} ${OBJECTDIR}/_ext/163028504/xc32_monitor.o.d 
	@${RM} ${OBJECTDIR}/_ext/163028504/xc32_monitor.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/163028504/xc32_monitor.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/163028504/xc32_monitor.o.d" -o ${OBJECTDIR}/_ext/163028504/xc32_monitor.o ../src/config/default/stdio/xc32_monitor.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1865521619/plib_port.o: ../src/config/default/peripheral/port/plib_port.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1865521619" 
	@${RM} ${OBJECTDIR}/_ext/1865521619/plib_port.o.d 
	@${RM} ${OBJECTDIR}/_ext/1865521619/plib_port.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1865521619/plib_port.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1865521619/plib_port.o.d" -o ${OBJECTDIR}/_ext/1865521619/plib_port.o ../src/config/default/peripheral/port/plib_port.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1984496892/plib_clock.o: ../src/config/default/peripheral/clock/plib_clock.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1984496892" 
	@${RM} ${OBJECTDIR}/_ext/1984496892/plib_clock.o.d 
	@${RM} ${OBJECTDIR}/_ext/1984496892/plib_clock.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1984496892/plib_clock.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1984496892/plib_clock.o.d" -o ${OBJECTDIR}/_ext/1984496892/plib_clock.o ../src/config/default/peripheral/clock/plib_clock.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1865468468/plib_nvic.o: ../src/config/default/peripheral/nvic/plib_nvic.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1865468468" 
	@${RM} ${OBJECTDIR}/_ext/1865468468/plib_nvic.o.d 
	@${RM} ${OBJECTDIR}/_ext/1865468468/plib_nvic.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1865468468/plib_nvic.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1865468468/plib_nvic.o.d" -o ${OBJECTDIR}/_ext/1865468468/plib_nvic.o ../src/config/default/peripheral/nvic/plib_nvic.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1827571544/plib_systick.o: ../src/config/default/peripheral/systick/plib_systick.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1827571544" 
	@${RM} ${OBJECTDIR}/_ext/1827571544/plib_systick.o.d 
	@${RM} ${OBJECTDIR}/_ext/1827571544/plib_systick.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1827571544/plib_systick.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1827571544/plib_systick.o.d" -o ${OBJECTDIR}/_ext/1827571544/plib_systick.o ../src/config/default/peripheral/systick/plib_systick.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/60184501/plib_wdt.o: ../src/config/default/peripheral/wdt/plib_wdt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/60184501" 
	@${RM} ${OBJECTDIR}/_ext/60184501/plib_wdt.o.d 
	@${RM} ${OBJECTDIR}/_ext/60184501/plib_wdt.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/60184501/plib_wdt.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/60184501/plib_wdt.o.d" -o ${OBJECTDIR}/_ext/60184501/plib_wdt.o ../src/config/default/peripheral/wdt/plib_wdt.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1171490990/startup_xc32.o: ../src/config/default/startup_xc32.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1171490990" 
	@${RM} ${OBJECTDIR}/_ext/1171490990/startup_xc32.o.d 
	@${RM} ${OBJECTDIR}/_ext/1171490990/startup_xc32.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1171490990/startup_xc32.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1171490990/startup_xc32.o.d" -o ${OBJECTDIR}/_ext/1171490990/startup_xc32.o ../src/config/default/startup_xc32.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1171490990/libc_syscalls.o: ../src/config/default/libc_syscalls.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1171490990" 
	@${RM} ${OBJECTDIR}/_ext/1171490990/libc_syscalls.o.d 
	@${RM} ${OBJECTDIR}/_ext/1171490990/libc_syscalls.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1171490990/libc_syscalls.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1171490990/libc_syscalls.o.d" -o ${OBJECTDIR}/_ext/1171490990/libc_syscalls.o ../src/config/default/libc_syscalls.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/829342769/plib_pm.o: ../src/config/default/peripheral/pm/plib_pm.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/829342769" 
	@${RM} ${OBJECTDIR}/_ext/829342769/plib_pm.o.d 
	@${RM} ${OBJECTDIR}/_ext/829342769/plib_pm.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/829342769/plib_pm.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/829342769/plib_pm.o.d" -o ${OBJECTDIR}/_ext/829342769/plib_pm.o ../src/config/default/peripheral/pm/plib_pm.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/508257091/plib_sercom5_i2c_master.o: ../src/config/default/peripheral/sercom/i2c_master/plib_sercom5_i2c_master.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/508257091" 
	@${RM} ${OBJECTDIR}/_ext/508257091/plib_sercom5_i2c_master.o.d 
	@${RM} ${OBJECTDIR}/_ext/508257091/plib_sercom5_i2c_master.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/508257091/plib_sercom5_i2c_master.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/508257091/plib_sercom5_i2c_master.o.d" -o ${OBJECTDIR}/_ext/508257091/plib_sercom5_i2c_master.o ../src/config/default/peripheral/sercom/i2c_master/plib_sercom5_i2c_master.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/60165182/plib_can0.o: ../src/config/default/peripheral/can/plib_can0.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/60165182" 
	@${RM} ${OBJECTDIR}/_ext/60165182/plib_can0.o.d 
	@${RM} ${OBJECTDIR}/_ext/60165182/plib_can0.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/60165182/plib_can0.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/60165182/plib_can0.o.d" -o ${OBJECTDIR}/_ext/60165182/plib_can0.o ../src/config/default/peripheral/can/plib_can0.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/714983638/plib_sercom4_i2c_slave.o: ../src/config/default/peripheral/sercom/i2c_slave/plib_sercom4_i2c_slave.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/714983638" 
	@${RM} ${OBJECTDIR}/_ext/714983638/plib_sercom4_i2c_slave.o.d 
	@${RM} ${OBJECTDIR}/_ext/714983638/plib_sercom4_i2c_slave.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/714983638/plib_sercom4_i2c_slave.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/714983638/plib_sercom4_i2c_slave.o.d" -o ${OBJECTDIR}/_ext/714983638/plib_sercom4_i2c_slave.o ../src/config/default/peripheral/sercom/i2c_slave/plib_sercom4_i2c_slave.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/60167341/plib_eic.o: ../src/config/default/peripheral/eic/plib_eic.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/60167341" 
	@${RM} ${OBJECTDIR}/_ext/60167341/plib_eic.o.d 
	@${RM} ${OBJECTDIR}/_ext/60167341/plib_eic.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/60167341/plib_eic.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/60167341/plib_eic.o.d" -o ${OBJECTDIR}/_ext/60167341/plib_eic.o ../src/config/default/peripheral/eic/plib_eic.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1865585090/plib_rstc.o: ../src/config/default/peripheral/rstc/plib_rstc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1865585090" 
	@${RM} ${OBJECTDIR}/_ext/1865585090/plib_rstc.o.d 
	@${RM} ${OBJECTDIR}/_ext/1865585090/plib_rstc.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1865585090/plib_rstc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1865585090/plib_rstc.o.d" -o ${OBJECTDIR}/_ext/1865585090/plib_rstc.o ../src/config/default/peripheral/rstc/plib_rstc.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/60181570/plib_tcc1.o: ../src/config/default/peripheral/tcc/plib_tcc1.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/60181570" 
	@${RM} ${OBJECTDIR}/_ext/60181570/plib_tcc1.o.d 
	@${RM} ${OBJECTDIR}/_ext/60181570/plib_tcc1.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/60181570/plib_tcc1.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/60181570/plib_tcc1.o.d" -o ${OBJECTDIR}/_ext/60181570/plib_tcc1.o ../src/config/default/peripheral/tcc/plib_tcc1.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/60163342/plib_adc0.o: ../src/config/default/peripheral/adc/plib_adc0.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/60163342" 
	@${RM} ${OBJECTDIR}/_ext/60163342/plib_adc0.o.d 
	@${RM} ${OBJECTDIR}/_ext/60163342/plib_adc0.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/60163342/plib_adc0.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/60163342/plib_adc0.o.d" -o ${OBJECTDIR}/_ext/60163342/plib_adc0.o ../src/config/default/peripheral/adc/plib_adc0.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
else
${OBJECTDIR}/_ext/1360937237/ledV01.o: ../src/ledV01.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ledV01.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ledV01.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/ledV01.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1360937237/ledV01.o.d" -o ${OBJECTDIR}/_ext/1360937237/ledV01.o ../src/ledV01.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1360937237/rs232V01.o: ../src/rs232V01.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/rs232V01.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/rs232V01.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/rs232V01.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1360937237/rs232V01.o.d" -o ${OBJECTDIR}/_ext/1360937237/rs232V01.o ../src/rs232V01.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1360937237/i2cMasterV02.o: ../src/i2cMasterV02.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/i2cMasterV02.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/i2cMasterV02.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/i2cMasterV02.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1360937237/i2cMasterV02.o.d" -o ${OBJECTDIR}/_ext/1360937237/i2cMasterV02.o ../src/i2cMasterV02.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1360937237/arithmeticV01.o: ../src/arithmeticV01.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/arithmeticV01.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/arithmeticV01.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/arithmeticV01.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1360937237/arithmeticV01.o.d" -o ${OBJECTDIR}/_ext/1360937237/arithmeticV01.o ../src/arithmeticV01.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1360937237/canV01.o: ../src/canV01.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/canV01.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/canV01.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/canV01.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1360937237/canV01.o.d" -o ${OBJECTDIR}/_ext/1360937237/canV01.o ../src/canV01.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1360937237/adcV01.o: ../src/adcV01.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/adcV01.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/adcV01.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/adcV01.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1360937237/adcV01.o.d" -o ${OBJECTDIR}/_ext/1360937237/adcV01.o ../src/adcV01.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1360937237/temperatureV01.o: ../src/temperatureV01.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/temperatureV01.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/temperatureV01.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/temperatureV01.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1360937237/temperatureV01.o.d" -o ${OBJECTDIR}/_ext/1360937237/temperatureV01.o ../src/temperatureV01.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1360937237/detectV01.o: ../src/detectV01.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/detectV01.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/detectV01.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/detectV01.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1360937237/detectV01.o.d" -o ${OBJECTDIR}/_ext/1360937237/detectV01.o ../src/detectV01.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1360937237/J1939V01.o: ../src/J1939V01.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/J1939V01.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/J1939V01.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/J1939V01.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1360937237/J1939V01.o.d" -o ${OBJECTDIR}/_ext/1360937237/J1939V01.o ../src/J1939V01.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1360937237/chgdsgV01.o: ../src/chgdsgV01.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/chgdsgV01.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/chgdsgV01.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/chgdsgV01.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1360937237/chgdsgV01.o.d" -o ${OBJECTDIR}/_ext/1360937237/chgdsgV01.o ../src/chgdsgV01.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1360937237/CANopenV01.o: ../src/CANopenV01.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/CANopenV01.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/CANopenV01.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/CANopenV01.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1360937237/CANopenV01.o.d" -o ${OBJECTDIR}/_ext/1360937237/CANopenV01.o ../src/CANopenV01.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1360937237/ModBusV01.o: ../src/ModBusV01.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ModBusV01.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ModBusV01.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/ModBusV01.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1360937237/ModBusV01.o.d" -o ${OBJECTDIR}/_ext/1360937237/ModBusV01.o ../src/ModBusV01.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1360937237/pwmV01.o: ../src/pwmV01.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/pwmV01.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/pwmV01.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/pwmV01.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1360937237/pwmV01.o.d" -o ${OBJECTDIR}/_ext/1360937237/pwmV01.o ../src/pwmV01.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1360937237/flashV01.o: ../src/flashV01.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/flashV01.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/flashV01.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/flashV01.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1360937237/flashV01.o.d" -o ${OBJECTDIR}/_ext/1360937237/flashV01.o ../src/flashV01.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1593096446/plib_nvmctrl.o: ../src/config/default/peripheral/nvmctrl/plib_nvmctrl.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1593096446" 
	@${RM} ${OBJECTDIR}/_ext/1593096446/plib_nvmctrl.o.d 
	@${RM} ${OBJECTDIR}/_ext/1593096446/plib_nvmctrl.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1593096446/plib_nvmctrl.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1593096446/plib_nvmctrl.o.d" -o ${OBJECTDIR}/_ext/1593096446/plib_nvmctrl.o ../src/config/default/peripheral/nvmctrl/plib_nvmctrl.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/504274921/plib_sercom1_usart.o: ../src/config/default/peripheral/sercom/usart/plib_sercom1_usart.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/504274921" 
	@${RM} ${OBJECTDIR}/_ext/504274921/plib_sercom1_usart.o.d 
	@${RM} ${OBJECTDIR}/_ext/504274921/plib_sercom1_usart.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/504274921/plib_sercom1_usart.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/504274921/plib_sercom1_usart.o.d" -o ${OBJECTDIR}/_ext/504274921/plib_sercom1_usart.o ../src/config/default/peripheral/sercom/usart/plib_sercom1_usart.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1986646378/plib_evsys.o: ../src/config/default/peripheral/evsys/plib_evsys.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1986646378" 
	@${RM} ${OBJECTDIR}/_ext/1986646378/plib_evsys.o.d 
	@${RM} ${OBJECTDIR}/_ext/1986646378/plib_evsys.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1986646378/plib_evsys.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1986646378/plib_evsys.o.d" -o ${OBJECTDIR}/_ext/1986646378/plib_evsys.o ../src/config/default/peripheral/evsys/plib_evsys.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/504274921/plib_sercom0_usart.o: ../src/config/default/peripheral/sercom/usart/plib_sercom0_usart.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/504274921" 
	@${RM} ${OBJECTDIR}/_ext/504274921/plib_sercom0_usart.o.d 
	@${RM} ${OBJECTDIR}/_ext/504274921/plib_sercom0_usart.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/504274921/plib_sercom0_usart.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/504274921/plib_sercom0_usart.o.d" -o ${OBJECTDIR}/_ext/504274921/plib_sercom0_usart.o ../src/config/default/peripheral/sercom/usart/plib_sercom0_usart.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/60165182/plib_can1.o: ../src/config/default/peripheral/can/plib_can1.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/60165182" 
	@${RM} ${OBJECTDIR}/_ext/60165182/plib_can1.o.d 
	@${RM} ${OBJECTDIR}/_ext/60165182/plib_can1.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/60165182/plib_can1.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/60165182/plib_can1.o.d" -o ${OBJECTDIR}/_ext/60165182/plib_can1.o ../src/config/default/peripheral/can/plib_can1.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1360937237/main.o: ../src/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1360937237/main.o.d" -o ${OBJECTDIR}/_ext/1360937237/main.o ../src/main.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1171490990/initialization.o: ../src/config/default/initialization.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1171490990" 
	@${RM} ${OBJECTDIR}/_ext/1171490990/initialization.o.d 
	@${RM} ${OBJECTDIR}/_ext/1171490990/initialization.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1171490990/initialization.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1171490990/initialization.o.d" -o ${OBJECTDIR}/_ext/1171490990/initialization.o ../src/config/default/initialization.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1171490990/interrupts.o: ../src/config/default/interrupts.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1171490990" 
	@${RM} ${OBJECTDIR}/_ext/1171490990/interrupts.o.d 
	@${RM} ${OBJECTDIR}/_ext/1171490990/interrupts.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1171490990/interrupts.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1171490990/interrupts.o.d" -o ${OBJECTDIR}/_ext/1171490990/interrupts.o ../src/config/default/interrupts.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1171490990/exceptions.o: ../src/config/default/exceptions.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1171490990" 
	@${RM} ${OBJECTDIR}/_ext/1171490990/exceptions.o.d 
	@${RM} ${OBJECTDIR}/_ext/1171490990/exceptions.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1171490990/exceptions.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1171490990/exceptions.o.d" -o ${OBJECTDIR}/_ext/1171490990/exceptions.o ../src/config/default/exceptions.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/163028504/xc32_monitor.o: ../src/config/default/stdio/xc32_monitor.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/163028504" 
	@${RM} ${OBJECTDIR}/_ext/163028504/xc32_monitor.o.d 
	@${RM} ${OBJECTDIR}/_ext/163028504/xc32_monitor.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/163028504/xc32_monitor.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/163028504/xc32_monitor.o.d" -o ${OBJECTDIR}/_ext/163028504/xc32_monitor.o ../src/config/default/stdio/xc32_monitor.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1865521619/plib_port.o: ../src/config/default/peripheral/port/plib_port.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1865521619" 
	@${RM} ${OBJECTDIR}/_ext/1865521619/plib_port.o.d 
	@${RM} ${OBJECTDIR}/_ext/1865521619/plib_port.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1865521619/plib_port.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1865521619/plib_port.o.d" -o ${OBJECTDIR}/_ext/1865521619/plib_port.o ../src/config/default/peripheral/port/plib_port.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1984496892/plib_clock.o: ../src/config/default/peripheral/clock/plib_clock.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1984496892" 
	@${RM} ${OBJECTDIR}/_ext/1984496892/plib_clock.o.d 
	@${RM} ${OBJECTDIR}/_ext/1984496892/plib_clock.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1984496892/plib_clock.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1984496892/plib_clock.o.d" -o ${OBJECTDIR}/_ext/1984496892/plib_clock.o ../src/config/default/peripheral/clock/plib_clock.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1865468468/plib_nvic.o: ../src/config/default/peripheral/nvic/plib_nvic.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1865468468" 
	@${RM} ${OBJECTDIR}/_ext/1865468468/plib_nvic.o.d 
	@${RM} ${OBJECTDIR}/_ext/1865468468/plib_nvic.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1865468468/plib_nvic.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1865468468/plib_nvic.o.d" -o ${OBJECTDIR}/_ext/1865468468/plib_nvic.o ../src/config/default/peripheral/nvic/plib_nvic.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1827571544/plib_systick.o: ../src/config/default/peripheral/systick/plib_systick.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1827571544" 
	@${RM} ${OBJECTDIR}/_ext/1827571544/plib_systick.o.d 
	@${RM} ${OBJECTDIR}/_ext/1827571544/plib_systick.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1827571544/plib_systick.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1827571544/plib_systick.o.d" -o ${OBJECTDIR}/_ext/1827571544/plib_systick.o ../src/config/default/peripheral/systick/plib_systick.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/60184501/plib_wdt.o: ../src/config/default/peripheral/wdt/plib_wdt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/60184501" 
	@${RM} ${OBJECTDIR}/_ext/60184501/plib_wdt.o.d 
	@${RM} ${OBJECTDIR}/_ext/60184501/plib_wdt.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/60184501/plib_wdt.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/60184501/plib_wdt.o.d" -o ${OBJECTDIR}/_ext/60184501/plib_wdt.o ../src/config/default/peripheral/wdt/plib_wdt.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1171490990/startup_xc32.o: ../src/config/default/startup_xc32.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1171490990" 
	@${RM} ${OBJECTDIR}/_ext/1171490990/startup_xc32.o.d 
	@${RM} ${OBJECTDIR}/_ext/1171490990/startup_xc32.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1171490990/startup_xc32.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1171490990/startup_xc32.o.d" -o ${OBJECTDIR}/_ext/1171490990/startup_xc32.o ../src/config/default/startup_xc32.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1171490990/libc_syscalls.o: ../src/config/default/libc_syscalls.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1171490990" 
	@${RM} ${OBJECTDIR}/_ext/1171490990/libc_syscalls.o.d 
	@${RM} ${OBJECTDIR}/_ext/1171490990/libc_syscalls.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1171490990/libc_syscalls.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1171490990/libc_syscalls.o.d" -o ${OBJECTDIR}/_ext/1171490990/libc_syscalls.o ../src/config/default/libc_syscalls.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/829342769/plib_pm.o: ../src/config/default/peripheral/pm/plib_pm.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/829342769" 
	@${RM} ${OBJECTDIR}/_ext/829342769/plib_pm.o.d 
	@${RM} ${OBJECTDIR}/_ext/829342769/plib_pm.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/829342769/plib_pm.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/829342769/plib_pm.o.d" -o ${OBJECTDIR}/_ext/829342769/plib_pm.o ../src/config/default/peripheral/pm/plib_pm.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/508257091/plib_sercom5_i2c_master.o: ../src/config/default/peripheral/sercom/i2c_master/plib_sercom5_i2c_master.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/508257091" 
	@${RM} ${OBJECTDIR}/_ext/508257091/plib_sercom5_i2c_master.o.d 
	@${RM} ${OBJECTDIR}/_ext/508257091/plib_sercom5_i2c_master.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/508257091/plib_sercom5_i2c_master.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/508257091/plib_sercom5_i2c_master.o.d" -o ${OBJECTDIR}/_ext/508257091/plib_sercom5_i2c_master.o ../src/config/default/peripheral/sercom/i2c_master/plib_sercom5_i2c_master.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/60165182/plib_can0.o: ../src/config/default/peripheral/can/plib_can0.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/60165182" 
	@${RM} ${OBJECTDIR}/_ext/60165182/plib_can0.o.d 
	@${RM} ${OBJECTDIR}/_ext/60165182/plib_can0.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/60165182/plib_can0.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/60165182/plib_can0.o.d" -o ${OBJECTDIR}/_ext/60165182/plib_can0.o ../src/config/default/peripheral/can/plib_can0.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/714983638/plib_sercom4_i2c_slave.o: ../src/config/default/peripheral/sercom/i2c_slave/plib_sercom4_i2c_slave.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/714983638" 
	@${RM} ${OBJECTDIR}/_ext/714983638/plib_sercom4_i2c_slave.o.d 
	@${RM} ${OBJECTDIR}/_ext/714983638/plib_sercom4_i2c_slave.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/714983638/plib_sercom4_i2c_slave.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/714983638/plib_sercom4_i2c_slave.o.d" -o ${OBJECTDIR}/_ext/714983638/plib_sercom4_i2c_slave.o ../src/config/default/peripheral/sercom/i2c_slave/plib_sercom4_i2c_slave.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/60167341/plib_eic.o: ../src/config/default/peripheral/eic/plib_eic.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/60167341" 
	@${RM} ${OBJECTDIR}/_ext/60167341/plib_eic.o.d 
	@${RM} ${OBJECTDIR}/_ext/60167341/plib_eic.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/60167341/plib_eic.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/60167341/plib_eic.o.d" -o ${OBJECTDIR}/_ext/60167341/plib_eic.o ../src/config/default/peripheral/eic/plib_eic.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/1865585090/plib_rstc.o: ../src/config/default/peripheral/rstc/plib_rstc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1865585090" 
	@${RM} ${OBJECTDIR}/_ext/1865585090/plib_rstc.o.d 
	@${RM} ${OBJECTDIR}/_ext/1865585090/plib_rstc.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1865585090/plib_rstc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/1865585090/plib_rstc.o.d" -o ${OBJECTDIR}/_ext/1865585090/plib_rstc.o ../src/config/default/peripheral/rstc/plib_rstc.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/60181570/plib_tcc1.o: ../src/config/default/peripheral/tcc/plib_tcc1.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/60181570" 
	@${RM} ${OBJECTDIR}/_ext/60181570/plib_tcc1.o.d 
	@${RM} ${OBJECTDIR}/_ext/60181570/plib_tcc1.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/60181570/plib_tcc1.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/60181570/plib_tcc1.o.d" -o ${OBJECTDIR}/_ext/60181570/plib_tcc1.o ../src/config/default/peripheral/tcc/plib_tcc1.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
${OBJECTDIR}/_ext/60163342/plib_adc0.o: ../src/config/default/peripheral/adc/plib_adc0.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/60163342" 
	@${RM} ${OBJECTDIR}/_ext/60163342/plib_adc0.o.d 
	@${RM} ${OBJECTDIR}/_ext/60163342/plib_adc0.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/60163342/plib_adc0.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../inc" -I"../src" -I"../src/config/default" -I"../src/packs/ATSAMC21J18A_DFP" -I"../src/packs/CMSIS/CMSIS/Core/Include" -I"../src/packs/CMSIS/" -I"../inc" -I"../src/config/default/peripheral/port" -I"../src/config/default/peripheral/can" -I"../src/config/default/peripheral/adc" -I"../src/config/default/peripheral/sercom/usart" -MMD -MF "${OBJECTDIR}/_ext/60163342/plib_adc0.o.d" -o ${OBJECTDIR}/_ext/60163342/plib_adc0.o ../src/config/default/peripheral/adc/plib_adc0.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}/samc21
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compileCPP
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/8692-002-Inventus-Power-Battery-ATSAMC21J18A.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) -g   -mprocessor=$(MP_PROCESSOR_OPTION) -mno-device-startup-code -o dist/${CND_CONF}/${IMAGE_TYPE}/8692-002-Inventus-Power-Battery-ATSAMC21J18A.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}          -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,-D=__DEBUG_D,--defsym=_min_heap_size=512,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",-DROM_ORIGIN=0x8000,-DROM_LENGTH=0x38000,--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml,-DRAM_ORIGION=0x20000010,-DRAM_LENGTH=0x7FF0 -mdfp=${DFP_DIR}/samc21
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/8692-002-Inventus-Power-Battery-ATSAMC21J18A.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mprocessor=$(MP_PROCESSOR_OPTION) -mno-device-startup-code -o dist/${CND_CONF}/${IMAGE_TYPE}/8692-002-Inventus-Power-Battery-ATSAMC21J18A.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}          -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=_min_heap_size=512,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",-DROM_ORIGIN=0x8000,-DROM_LENGTH=0x38000,--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml,-DRAM_ORIGION=0x20000010,-DRAM_LENGTH=0x7FF0 -mdfp=${DFP_DIR}/samc21
	${MP_CC_DIR}\\xc32-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/8692-002-Inventus-Power-Battery-ATSAMC21J18A.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} 
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
