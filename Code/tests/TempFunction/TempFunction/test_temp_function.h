#if !defined(_UNIT_TEST_TEMP_FUNCTION_H_)
#define _UNIT_TEST_TEMP_FUNCTION_H_

#include <stdint.h>

extern uint16_t g_adcNTC[];
extern uint16_t g_NTCTemperatureTable[];

extern uint16_t TempFunction(uint16_t tmpAdc);

#endif
