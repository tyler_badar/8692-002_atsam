#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "test_temp_function.h"
#include "../../../inc/temperatureV01.h"

/* globals here or in a c file -- ex.  test_temp_function_globals.c */
#define NTC_SAMPLE_AMOUNT                      11u

uint16_t g_adcNTC[NTC_SAMPLE_AMOUNT] = { 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u };

uint16_t g_NTCTemperatureTable[16] = { 2980, 2980, 2980, 2980, 2980, 2980, 2980, 2980,
									  2980, 2980, 2980, 2980, 2980, 2980, 2980, 2980 };

int main(int argc, char* argv[])
{
	printf("fn %s()\n", __func__);

	FILE* in_file;  /* input test file  */
	FILE* out_file; /* output test file */

	//uint16_t tmpAdc = 0;
	unsigned int tmpAdc = 0;
	uint16_t temp;

	/* check test file */
	if (argc != 2) {
		fprintf(stdout, "Note enough args.\n");
		exit(1);
	}
	else if (fopen_s(&in_file, argv[1], "r") != 0) {
		fprintf(stdout, "Error opening file.\n");
		exit(1);
	}

	if (fopen_s(&out_file, "out.txt", "w") != 0) {
		fprintf(stdout, "Error opening file.\n");
		exit(1);
	}

	while (!feof(in_file)) {
		fscanf_s(in_file, "%u", &tmpAdc);
		temp = TempFunction(tmpAdc);
		printf("temp = %dK (%dC)\n", temp / 10, (temp / 10) - 273);
		fprintf(out_file, "temp = %dK (%dC)\n", temp / 10, (temp / 10) - 273);
	}

	fclose(in_file); /* done with the test files */
	fclose(out_file);
	return(0);
}